import flask 
from flask import Flask 

app = Flask(__name__, static_url_path="")

recepti = []
sastojci = [{"naziv": "jaja", "aktivan": True}, 
            {"naziv": "brasno", "aktivan": True}, 
            {"naziv": "jagode", "aktivan": True}]

@app.template_filter("duzina")
def duzina(lista):
    return len(lista)

@app.template_filter("dozvola_za_listu")
def dozvola_za_listu(lista):
    if len(lista) == 0:
        return False
    else:
        dozvola = False
        for recept in lista:
            if recept["aktivan"] == True:
                dozvola = True
        return dozvola

@app.route("/")
def main():
    return flask.render_template("sveForme.tpl.html", sastojci = sastojci)

@app.route("/dodaj", methods=["POST"])
def dodaj():
    if flask.request.form["dodaj"] == "Dodaj recept":
        dozvola = True
        for recept in recepti:
            if flask.request.form["naziv"] == recept["naziv"]:
                dozvola = False
 
        if dozvola:
            recepti.append({
                "naziv": flask.request.form["naziv"],
                "opis": flask.request.form["opis"],
                "lista_sastojaka": [],
                "aktivan": True
            })   

    elif flask.request.form["dodaj"] == "Ubaci sastojak u recept":
        for recept in recepti:
            if flask.request.form["naziv"] == recept["naziv"]:
                dozvola = True
                for sastojak in recept["lista_sastojaka"]:
                    if flask.request.form["sastojak"] == sastojak["naziv"]:
                        dozvola = False
                
                if dozvola:
                    recept["lista_sastojaka"].append({
                        "naziv": flask.request.form["sastojak"],
                        "gramaza": int(flask.request.form["gramaza"]),
                        "aktivan": True
                    })

    return flask.redirect("/")

@app.route("/tabelaPrikazRecepata")
def tabela_recepata():
    return flask.render_template("tabelaRecepata.tpl.html", recepti = recepti)

@app.route("/ukloniRecepat/<int:index>")
def ukloni_recepat(index):
    for i in range(0, len(recepti)):
        if i == index:
            recepti[i]["aktivan"] = False
    return flask.redirect("/tabelaPrikazRecepata")

@app.route("/formaZaDetaljnije/<int:index>")
def forma_za_detaljnije(index):
    for i in range(0, len(recepti)):
        if i == index:
            recept = recepti[i]
    return flask.render_template("formaZaDetaljnije.tpl.html", recept = recept)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, threaded=True)