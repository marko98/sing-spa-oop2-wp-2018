import flask
from flask import Flask

app = Flask(__name__, static_url_path="")

racunarske_komponente = [
    {
        "naziv": "hard disk", 
        "opis": "hard disk velicine 800GB",
        "dostupna_kolicina": 5,
        "cena": 200,
        "aktivna": True
    }
]

@app.template_filter("recnik_u_str")
def recnik_u_str(recnik):
    return str(recnik)

@app.route("/")
def main():
    return flask.render_template("racunarskeKomponente.tpl.html", komponente = racunarske_komponente)

@app.route("/formaDodajNovuKomponentu")
def forma_nova_komponenta():
    return flask.send_file("formaNovaKomponenta.html")

@app.route("/dodajKomponentu", methods=["POST"])
def dodaj_komponentu():
    racunarske_komponente.append({
        "naziv": flask.request.form["naziv"], 
        "opis": flask.request.form["opis"],
        "dostupna_kolicina": int(flask.request.form["kolicina"]),
        "cena": int(flask.request.form["cena"]),
        "aktivna": True
    })
    return flask.redirect("/")

@app.route("/ukloniKomponentu/<int:index>")
def ukloni_komponentu(index):
    for i in range(0, len(racunarske_komponente)):
        if i == index:
            racunarske_komponente[i]["aktivna"] = False
    return flask.redirect("/")

@app.route("/izmeniForma/<komponenta>/<int:index>")
def izmeni_forma(komponenta, index):
    komponenta = eval(komponenta)
    print(type(komponenta["dostupna_kolicina"]))
    return flask.render_template("formaIzmenaKomponente.tpl.html", komponenta = komponenta, index = index)

@app.route("/izmeniKomponentu/<int:index>", methods=["POST"])
def izmeni_komponentu(index):
    for i in range(0, len(racunarske_komponente)):
        if i == index:
            racunarske_komponente[i]["naziv"] = flask.request.form["naziv"]
            racunarske_komponente[i]["opis"] = flask.request.form["opis"]
            racunarske_komponente[i]["dostupna_kolicina"] = int(flask.request.form["kolicina"])
            racunarske_komponente[i]["cena"] = int(flask.request.form["cena"])
            racunarske_komponente[i]["aktivna"] = True
    return flask.redirect("/")

@app.route("/kupiForma/<int:index>")
def kupi_forma(index):
    for i in range(0, len(racunarske_komponente)):
        if i == index:
            komponenta = racunarske_komponente[i]
    return flask.render_template("formaKupiKomponentu.tpl.html", komponenta = komponenta, index = index)

@app.route("/kupiKomponentu/<int:index>", methods=["POST"])
def kupi_komponentu(index):
    for i in range(0, len(racunarske_komponente)):
        if i == index:
            racunarske_komponente[i]["dostupna_kolicina"] = racunarske_komponente[i]["dostupna_kolicina"] - int(flask.request.form["kolicina"])
            if racunarske_komponente[i]["dostupna_kolicina"] == 0:
                racunarske_komponente[i]["aktivna"] = False
    return flask.redirect("/")

app.run(host="0.0.0.0", port=5000, threaded=True)