#ifndef KOMPONENTANILI_H_INCLUDED
#define KOMPONENTANILI_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Komponenta.h"
#include "Pin.h"

using namespace std;

class KomponentaNILI: public Komponenta{
private:

public:
    KomponentaNILI() : Komponenta(){
        Pin *ppin1 = new Pin(false);
        Pin *ppin2 = new Pin(false);
        ulazniPinovi.push_back(ppin1);
        ulazniPinovi.push_back(ppin2);
    }

    virtual bool getIzlazniPin(){
        bool dozvola = false;
        for(unsigned int i = 0; i < ulazniPinovi.size(); i++){
            if(ulazniPinovi[i]->getStanje() == true){
                dozvola = true;
            }
        }
        if(dozvola){
            return false;
        } else {
            return true;
        }
    }
};

#endif // KOMPONENTANILI_H_INCLUDED
