#ifndef KOMPONENTANE_H_INCLUDED
#define KOMPONENTANE_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Komponenta.h"
#include "Pin.h"

using namespace std;

class KomponentaNE: public Komponenta{
private:

public:
    KomponentaNE() : Komponenta(){
        Pin *ppin1 = new Pin(false);
        ulazniPinovi.push_back(ppin1);
    }

    virtual bool getIzlazniPin(){
        if(ulazniPinovi[0]->getStanje()){
            return false;
        } else {
            return true;
        }
    }
};

#endif // KOMPONENTANE_H_INCLUDED
