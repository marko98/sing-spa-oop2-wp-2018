#ifndef PIN_H_INCLUDED
#define PIN_H_INCLUDED

#include <iostream>
#include <string>

using namespace std;

class Pin{
private:
    bool stanje;
public:
    Pin(bool stanje): stanje(stanje){};
    bool getStanje(){
        return stanje;
    }
    void setStanje(bool stanje){
        this->stanje = stanje;
    }
};

#endif // PIN_H_INCLUDED
