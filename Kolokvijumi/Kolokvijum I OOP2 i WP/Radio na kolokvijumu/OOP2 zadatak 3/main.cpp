#include <iostream>
#include <string>

#include "Pin.h"
#include "Komponenta.h"
#include "KomponentaAND.h"
#include "KomponentaILI.h"
#include "KomponentaNE.h"
#include "KomponentaNILI.h"

using namespace std;

int main()
{
    KomponentaAND komponentaAnd;
    KomponentaNE komponentaNe;
    KomponentaOR komponentaIli;
    KomponentaNILI komponentaNili;
    KomponentaOR komponentaNili2;

    cout << "---------------------------------" << endl;

    komponentaIli.prikaziUlaznoStanje();

    cout << komponentaIli.getIzlazniPin() << endl;

    komponentaIli.podesiUlaz(0, true);

    cout << komponentaIli.getIzlazniPin() << endl;

    komponentaNe.prikaziUlaznoStanje();

    komponentaNe.podesiUlaz(0, false);

    komponentaNe.prikaziUlaznoStanje();

    komponentaNe.spojiSaKomponentom(komponentaIli, 1);

    komponentaIli.prikaziUlaznoStanje();

    // stvaranje NILI

    cout << komponentaNili.getIzlazniPin() << endl;

    komponentaNili2.podesiUlaz(0, true);
    komponentaNili2.podesiUlaz(1, true);
    komponentaNili2.prikaziUlaznoStanje();
    cout << komponentaNili2.getIzlazniPin() << endl;

    komponentaNe.prikaziUlaznoStanje();
    cout << komponentaNe.getIzlazniPin() << endl;

    komponentaNili2.spojiSaKomponentom(komponentaNe, 0);
    cout << komponentaNe.getIzlazniPin() << endl;

    return 0;
}
