#include "Komponenta.h"

Komponenta::Komponenta(){}

void Komponenta::podesiUlaz(int ulaz, bool stanje){
    for (unsigned int i = 0; i < ulazniPinovi.size(); i++){
        if(i == ulaz){
            ulazniPinovi[i]->setStanje(stanje);
        }
    }
}

void Komponenta::prikaziUlaznoStanje(){
    cout << "---------------" << endl;
    for (unsigned int i = 0; i < ulazniPinovi.size(); i++){
        cout << ulazniPinovi[i]->getStanje() <<  endl;
    }
    cout << "---------------" << endl;
}
