#ifndef KOMPONENTAAND_H_INCLUDED
#define KOMPONENTAAND_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Komponenta.h"
#include "Pin.h"

using namespace std;

class KomponentaAND: public Komponenta{
private:

public:
    KomponentaAND();

    virtual bool getIzlazniPin();
};

#endif // KOMPONENTAAND_H_INCLUDED
