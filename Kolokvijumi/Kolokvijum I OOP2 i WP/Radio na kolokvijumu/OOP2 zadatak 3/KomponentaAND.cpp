#include "KomponentaAND.h"

KomponentaAND::KomponentaAND(): Komponenta(){
    Pin *ppin1 = new Pin(false);
    Pin *ppin2 = new Pin(false);
    ulazniPinovi.push_back(ppin1);
    ulazniPinovi.push_back(ppin2);
}

bool KomponentaAND::getIzlazniPin(){
    bool dozvola = true;
    for(unsigned int i = 0; i < ulazniPinovi.size(); i++){
        if(ulazniPinovi[i]->getStanje() == false){
            dozvola = false;
        }
    }
    if(dozvola){
        return true;
    } else {
        return false;
    }
}
