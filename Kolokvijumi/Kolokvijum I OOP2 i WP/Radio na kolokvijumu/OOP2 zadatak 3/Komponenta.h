#ifndef KOMPONENTA_H_INCLUDED
#define KOMPONENTA_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Pin.h"

using namespace std;

class Komponenta{
protected:
    vector <Pin *> ulazniPinovi;
public:
    Komponenta();

    void podesiUlaz(int ulaz, bool stanje);

    void spojiSaKomponentom(Komponenta &komponenta, int redniBrojUlaza){
        komponenta.podesiUlaz(redniBrojUlaza, this->getIzlazniPin());
    }

    void prikaziUlaznoStanje();

    virtual bool getIzlazniPin()=0;
};

#endif // KOMPONENTA_H_INCLUDED
