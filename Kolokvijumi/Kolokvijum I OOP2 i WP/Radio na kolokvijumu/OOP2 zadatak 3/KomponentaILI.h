#ifndef KOMPONENTAILI_H_INCLUDED
#define KOMPONENTAILI_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Komponenta.h"
#include "Pin.h"

using namespace std;

class KomponentaOR: public Komponenta{
private:

public:
    KomponentaOR() : Komponenta(){
        Pin *ppin1 = new Pin(false);
        Pin *ppin2 = new Pin(false);
        ulazniPinovi.push_back(ppin1);
        ulazniPinovi.push_back(ppin2);
    }

    virtual bool getIzlazniPin(){
        bool dozvola = false;
        for(unsigned int i = 0; i < ulazniPinovi.size(); i++){
            if(ulazniPinovi[i]->getStanje() == true){
                dozvola = true;
            }
        }
        if(dozvola){
            return true;
        } else {
            return false;
        }
    }
};

#endif // KOMPONENTAILI_H_INCLUDED
