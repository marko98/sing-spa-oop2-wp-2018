import flask
from flask import Flask

app = Flask(__name__, static_url_path = "")

recepti = [{"naziv": "naziv1","opis": "opis1" ,"sastojci": [{"ime": "ime1", "grami": "0"}], "aktivan": True}]
sastojci = ["mleko", "jaja", "brasno"]

@app.route("/ukloniRecept/<int:index>")
def ukloni_korisnika(index):
    recepti[index]["aktivan"] = False
    return flask.redirect("/receptiForma")

@app.route("/")
@app.route("/formaRecepta")
def lista_korisnika():
    return flask.render_template("formaRecepta.tpl.html", sastojci = sastojci)

@app.route("/izmenaRecepta/<int:index>")
def izmena_recepta(index):
    return flask.render_template("izmenaRecepta.tpl.html", recept = recepti[index], index = index)

@app.route("/izmeni/<int:recept_index>", methods=["POST"])
def izmena(recept_index):
    recepti[recept_index]["naziv"] = flask.request.form["naslov"]
    recepti[recept_index]["opis"] = flask.request.form["tekst"]
    recepti[recept_index]["aktivan"] = True
        
    return flask.redirect("/receptiForma")

@app.route("/receptiForma")
def receptiForma():
    return flask.render_template("recepti.tpl.html", recepti = recepti)

@app.route("/dodajRecept", methods=["POST"])
def dodaj_recept():  
    dozvola = True
    for recept in recepti:
        if(recept["naziv"] == flask.request.form["naslov"]):
            dozvola = False
    
    if(dozvola):
        recept = {
            "naziv": flask.request.form["naslov"],
            "opis": flask.request.form["tekst"],
            "sastojci": [],
            "aktivan": True
            }     
        recepti.append(recept)

    return flask.redirect("/receptiForma")
    
@app.route("/dodajSastojak", methods=["POST"])
def dodaj_sastojak():

    for recept in recepti:
        if recept["naziv"] == flask.request.form["naslovRecepta"]:
            recept["sastojci"].append({
                "ime": flask.request.form["sastojak"],
                "grami": flask.request.form["kolicina"]
            })


    return flask.redirect("/receptiForma")

app.run(host = "0.0.0.0", port = 5000, threaded = True)