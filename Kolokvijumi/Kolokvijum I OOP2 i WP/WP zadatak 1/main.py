import flask
from flask import Flask

zaposleni = [{"ime": "marko", 
            "prezime": "zahorodni", 
            "strucna_sprema": "fakultet", 
            "radno_mesto": "google", 
            "plata": 2000,
            "uklonjen": False}]

app = Flask(__name__, static_url_path="")

@app.route("/")
def main():
    return flask.render_template("tabelaZaposlenih.tpl.html", zaposleni = zaposleni)

@app.route("/ukloniZaposlenog/<int:index>")
def ukloni_zaposlenog(index):
    for i in range(0, len(zaposleni)):
        if i == index:
            print("uklonjen")
            zaposleni[i]["uklonjen"] = True
    return flask.redirect("/")

@app.route("/formaNovogZaposlenog")
def forma_novog_zaposlenog():
    return flask.render_template("dodavanjeNovogZaposlenog.tpl.html")

@app.route("/dodajNovogZaposlenog", methods=["POST"])
def dodaj_novog_zaposlenog():
    zaposleni.append({
        "ime": flask.request.form["ime"], 
        "prezime": flask.request.form["prezime"], 
        "strucna_sprema": flask.request.form["strucnaSprema"], 
        "radno_mesto": flask.request.form["radnoMesto"], 
        "plata": flask.request.form["plata"],
        "uklonjen": False
    })
    return flask.redirect("/")

app.run(host="0.0.0.0", port=5000, threaded=True)