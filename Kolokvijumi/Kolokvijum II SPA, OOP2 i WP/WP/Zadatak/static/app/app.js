(function(angular){
    var app = angular.module("MyApp", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "home", //Naziv stanja.
            url: "/", //URL koji se mapira na zadato stanje. Ovaj URL ce zapravo biti vidljiv kao identifikator fragmenta.
            templateUrl: "app/components/racunarskeKomponente/racunarskeKomponente.tpl.html", //URL do sablona za generisanje prikaza.
            controller: "RacunarskeKomponenteCtrl", //Kontroler koji je potrebno injektovati u prikaz.
            controllerAs: "rkc" //Posto ce biti upotrebljen imenovani kontroler, navodi se i ime kontrolera.
        }).state("racunarskaKomponenta", { //Naziv je izostavljen iz objekta stanja, ali je naveden kao prvi argument funkcije state.
            url: "/racunarskeKomponente/{id: int}", //Parametrizovana ruta, kljuc predstavlja naziv parametra a vrednost tip parametra.
            templateUrl: "app/components/racunarskaKomponenta/racunarskaKomponenta.tpl.html",
            controller: "RacunarskaKomponentaCtrl",
            controllerAs: "rkc"
        }).state("racunar", { //Naziv je izostavljen iz objekta stanja, ali je naveden kao prvi argument funkcije state.
            url: "/racunar", //Parametrizovana ruta, kljuc predstavlja naziv parametra a vrednost tip parametra.
            templateUrl: "app/components/racunar/racunar.tpl.html",
            controller: "RacunarCtrl",
            controllerAs: "rc"
        }); 

        $urlRouterProvider.otherwise("/");
    }]);
})(angular)