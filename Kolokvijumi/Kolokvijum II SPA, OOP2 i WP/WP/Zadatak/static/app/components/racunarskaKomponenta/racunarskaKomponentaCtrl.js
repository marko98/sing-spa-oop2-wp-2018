(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("RacunarskaKomponentaCtrl", ["$stateParams", "$http", function ($stateParams, $http) {
        var vm = this;
        vm.trazenaKomponenta = {};
        vm.izmenjenaKomponenta = {
            "naziv": "",
            "opis": null,
            "cena": 0,
            "na_lageru": 0
        };

        vm.dobaviKomponentu = function(id){
            $http.get("/racunarskeKomponente/" + id).then(function(response){
                vm.trazenaKomponenta = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju komponente! Kod: " + response.status);
            });
        };

        vm.izmeniKomponentu = function(){
            $http.put("/racunarskeKomponente/" + vm.trazenaKomponenta.id, vm.izmenjenaKomponenta).then(function(response){
                vm.dobaviKomponentu(vm.trazenaKomponenta.id);
            }, function(response) {
                console.log("Greska pri izmeni komponente! Kod: " + response.status);
            });
        };

        vm.dobaviKomponentu($stateParams["id"]);

    }]);
})(angular);