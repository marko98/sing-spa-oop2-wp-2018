(function(angular){
    var app = angular.module("MyApp");

    app.controller("RacunarCtrl", ["$http", function ($http) {
        var vm = this;
        vm.racunarskeKomponente = [];
        vm.deo = {
            "id": "",
            "kolicina": 0
        };
        vm.racunar = {
            "naziv": "racunar1",
            "opis": "racunar1 opis",
            "delovi": [],
            "cena": 0
        };

        vm.dodajURacunar = function(){
            vm.racunar.delovi.push(angular.copy(vm.deo));
            console.log(vm.racunar);
        };

        vm.sklopi = function(){
            $http.post("/racunar", vm.racunar).then(function(response){
                // vm.dobaviRacunarskeKomponente();
            }, function(response) {
                console.log("Greska! Kod: " + response.status);
            });
        };

        vm.dobaviRacunarskeKomponente = function(){
            $http.get("/racunarskeKomponente", {params:vm.pretraga}).then(function(response){
                vm.racunarskeKomponente = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju racunarskih komponenata! Kod: " + response.status);
            });
        };


        vm.dobaviRacunarskeKomponente();

    }]);

})(angular);