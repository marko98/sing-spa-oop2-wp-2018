(function(angular){
    var app = angular.module("MyApp");

    app.controller("RacunarskeKomponenteCtrl", ["$http", function ($http) {
        var vm = this;
        vm.racunarskeKomponente = [];
        vm.novaKomponenta = {
            "naziv": "",
            "opis": null,
            "cena": 0,
            "na_lageru": 0
        };
        vm.pretraga = {
            "naziv": "",
            "opis": "",
            "kolicinaOd": undefined,
            "kolicinaDo": undefined
        };

        vm.dobaviRacunarskeKomponente = function(){
            $http.get("/racunarskeKomponente", {params:vm.pretraga}).then(function(response){
                vm.racunarskeKomponente = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju racunarskih komponenata! Kod: " + response.status);
            });
        };

        vm.dodajKomponentu = function(){
            $http.post("/racunarskeKomponente", vm.novaKomponenta).then(function(response){
                vm.dobaviRacunarskeKomponente();
            }, function(response) {
                console.log("Greska pri dobavljanju komponente! Kod: " + response.status);
            });
        };

        vm.ukloniKomponentu = function(id){
            $http.delete("/racunarskeKomponente/" + id).then(function(response){
                vm.dobaviRacunarskeKomponente();
            }, function(response) {
                console.log("Greska pri brisanju komponente! Kod: " + response.status);
            });
        }

        vm.dobaviRacunarskeKomponente();

    }]);

})(angular);