import flask
import datetime
from flask import Flask
from flask import request

from flaskext.mysql import MySQL

from pymysql.cursors import DictCursor

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")

app.config['MYSQL_DATABASE_USER'] = 'student' # Korisnicko ime korisnika baze podataka.
app.config['MYSQL_DATABASE_PASSWORD'] = 'student' # Lozinka izabranog korisnika. 
app.config['MYSQL_DATABASE_DB'] = 'racunari' # Ime seme baze podataka koja se koristi.

mysql_db.init_app(app)

# ---------------- FILTERI -----------------------
@app.template_filter("str")
def string(object):
    return str(object)

# <td>{{korisnik["datum_rodjenja"]|date}}</td>
@app.template_filter("date")
def dateFilter(date_string):
    date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    return date.strftime("%d.%m.%Y.")

@app.template_filter("datumIsoformat")
def datum_u_isoformat(datum):
    datum = datum.isoformat()
    datum = datum.split("T")
    datum = datum[0]
    return datum
# ------------------------------------------------

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html")

# DOBAVI RACUNARE
@app.route("/racunarskeKomponente", methods=["GET"])
def dobavljanje_racunarskih_komponenti():
    print(request.args)
    upit = "SELECT * FROM delovi"
    selekcija = " WHERE "
    parametri_pretrage = []
    cr = mysql_db.get_db().cursor()

    if request.args.get("naziv") != "" and request.args.get("naziv") != None:
        parametri_pretrage.append("%" + request.args.get("naziv") + "%")
        selekcija += "naziv LIKE %s "

    if request.args.get("opis") != "" and request.args.get("opis") != None:
        parametri_pretrage.append("%" + request.args.get("opis") + "%")
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "opis LIKE %s "

    if request.args.get("kolicinaOd") != "" and request.args.get("kolicinaOd") != None:
        parametri_pretrage.append(request.args.get("kolicinaOd"))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "cena >= %s "

    if request.args.get("kolicinaDo") != "" and request.args.get("kolicinaDo") != None:
        parametri_pretrage.append(request.args.get("kolicinaDo"))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "cena <= %s "

    if len(parametri_pretrage) > 0:
        upit += selekcija

    print(upit)
    print(parametri_pretrage)
    cr.execute(upit, parametri_pretrage)
    racunarske_komponente = cr.fetchall()
    return flask.json.jsonify(racunarske_komponente)

# DOBAVI KOMPONENTU PO ID-JU
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! cr.FETCHONE() !!!!!!!! --------------
@app.route("/racunarskeKomponente/<int:komponenta_id>", methods=["GET"])
def dobavljanje_komponente_po_idu(komponenta_id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM delovi WHERE id=%s", (komponenta_id, ))
    komponenta = cr.fetchone()
    return flask.json.jsonify(komponenta)

# DODAJ KOMPONENTU
@app.route("/racunarskeKomponente", methods=["POST"])
def dodavanje_komponente():
    # print(request.json)
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO delovi (naziv, opis, cena, na_lageru) VALUES(%(naziv)s, %(opis)s, %(cena)s, %(na_lageru)s)", request.json)
    db.commit()
    return "", 201

# IZMENI KOMPONENTU PO ID-JU
@app.route("/racunarskeKomponente/<int:komponenta_id>", methods=["PUT"])
def izmeni_komponentu_po_idu(komponenta_id):
    data = dict(request.json)
    # print(data)
    data["komponenta_id"] = komponenta_id
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("UPDATE delovi SET naziv=%(naziv)s, opis=%(opis)s, cena=%(cena)s, na_lageru=%(na_lageru)s WHERE id=%(komponenta_id)s", data)
    db.commit()
    return "", 201

# OBRISI KOMPONENTU
@app.route("/racunarskeKomponente/<int:komponenta_id>", methods=["DELETE"])
def uklanjanje_komponente_po_idu(komponenta_id):
    db = mysql_db.get_db()
    cr = db.cursor()

    cr.execute("SELECT * FROM delovi WHERE id=%s", (komponenta_id, ))
    komponenta = cr.fetchone()
    # print(komponenta)
    if komponenta["logicki_obrisano"] == 0:
        cr.execute("UPDATE delovi SET logicki_obrisano=1 WHERE id=%s", (komponenta_id, ))
    else:
        cr.execute("DELETE FROM delovi WHERE id=%s", (komponenta_id, ))

    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

# DODAJ KOMPONENTU
@app.route("/racunar", methods=["POST"])
def dodavanje_racunara():
    
    data = dict(request.json)
    print(data)
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO racunari (naziv, opis, cena) VALUES(%(naziv)s, %(opis)s, %(cena)s)", data)
    db.commit()
    return "", 201

# # ------------- RAD SA OBJEKTIMA ------------------

# # DOBAVI OBJEKTE
# @app.route("/objekti", methods=["GET"])
# def dobavljanje_svih_objekata():
#     # print(request.args)
#     upit = "SELECT * FROM stavke"
#     selekcija = " WHERE "
#     parametri_pretrage = []
#     cr = mysql_db.get_db().cursor()
    
#     if request.args.get("naziv") != "" and request.args.get("naziv") != None:
#         parametri_pretrage.append("%" + request.args.get("naziv") + "%")
#         selekcija += "naziv LIKE %s "

#     if request.args.get("opis") != "" and request.args.get("opis") != None:
#         parametri_pretrage.append("%" + request.args.get("opis") + "%")
#         if len(parametri_pretrage) > 1:
#             selekcija += "AND "
#         selekcija += "opis LIKE %s "

#     if request.args.get("kolicinaOd") != "" and request.args.get("kolicinaOd") != None:
#         parametri_pretrage.append(request.args.get("kolicinaOd"))
#         if len(parametri_pretrage) > 1:
#             selekcija += "AND "
#         selekcija += "kolicina >= %s "

#     if request.args.get("kolicinaDo") != "" and request.args.get("kolicinaDo") != None:
#         parametri_pretrage.append(request.args.get("kolicinaDo"))
#         if len(parametri_pretrage) > 1:
#             selekcija += "AND "
#         selekcija += "kolicina <= %s "

#     if len(parametri_pretrage) > 0:
#         upit += selekcija

#     # print(upit)
#     # print(parametri_pretrage)
#     cr.execute(upit, parametri_pretrage)
#     objekti = cr.fetchall()
#     return flask.json.jsonify(objekti)

# # DOBAVI OBJEKAT PO ID-JU
# # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! cr.FETCHONE() !!!!!!!! --------------
# @app.route("/objekti/<int:objekat_id>", methods=["GET"])
# def dobavljanje_objekta_po_idu(objekat_id):
#     cr = mysql_db.get_db().cursor()
#     cr.execute("SELECT * FROM stavke WHERE id=%s", (objekat_id, ))
#     objekat = cr.fetchone()
#     return flask.json.jsonify(objekat)

# # DODAJ OBJEKTE
# @app.route("/objekti", methods=["POST"])
# def dodavanje_korisnika():
#     # print(request.json)
#     db = mysql_db.get_db()
#     cr = db.cursor()
#     cr.execute("INSERT INTO stavke (naziv, kolicina, opis) VALUES(%(naziv)s, %(kolicina)s, %(opis)s)", request.json)
#     db.commit()
#     return "", 201

# # IZMENI OBJEKAT PO ID-JU
# @app.route("/objekti/<int:objekat_id>", methods=["PUT"])
# def izmeni_objekat_po_idu(objekat_id):
#     data = dict(request.json)
#     data["objekat_id"] = objekat_id
#     db = mysql_db.get_db()
#     cr = db.cursor()
#     cr.execute("UPDATE stavke SET kolicina=%(kolicina)s, opis=%(opis)s WHERE id=%(objekat_id)s", data)
#     db.commit()
#     return "", 201

# # OBRISI OBJEKTE PO ID-JU
# @app.route("/objekti/<int:objekat_id>", methods=["DELETE"])
# def uklanjanje_objekta_po_idu(objekat_id):
#     db = mysql_db.get_db()
#     cr = db.cursor()
#     cr.execute("DELETE FROM stavke WHERE id=%s", (objekat_id, ))
#     db.commit()
#     return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

# # ------------- KRAJ RADA SA OBJEKTIMA ------------


if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)