-- MySQL Script generated by MySQL Workbench
-- 12/18/18 15:30:51
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema racunari
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema racunari
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `racunari` DEFAULT CHARACTER SET utf8 ;
USE `racunari` ;

-- -----------------------------------------------------
-- Table `racunari`.`delovi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `racunari`.`delovi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `naziv` TEXT NOT NULL,
  `opis` LONGTEXT NULL,
  `cena` DOUBLE NOT NULL,
  `na_lageru` INT NOT NULL,
  `logicki_obrisano` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `racunari`.`racunari`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `racunari`.`racunari` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `naziv` TEXT NOT NULL,
  `opis` LONGTEXT NULL,
  `cena` DOUBLE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `racunari`.`delovi_racunara`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `racunari`.`delovi_racunara` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `kolicina` INT NOT NULL,
  `deo_id` INT NOT NULL,
  `racunar_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_delovi_racunara_deo1_idx` (`deo_id` ASC),
  INDEX `fk_delovi_racunara_racunar1_idx` (`racunar_id` ASC),
  CONSTRAINT `fk_delovi_racunara_deo1`
    FOREIGN KEY (`deo_id`)
    REFERENCES `racunari`.`delovi` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delovi_racunara_racunar1`
    FOREIGN KEY (`racunar_id`)
    REFERENCES `racunari`.`racunari` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `racunari`.`delovi`
-- -----------------------------------------------------
START TRANSACTION;
USE `racunari`;
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (1, 'HDD 2.5\" SATA2 5400 1TB Toshiba MQ01ABD100', NULL, 5000, 21, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (2, 'HDD 2.5\" SATA3 5400 1TB WD Blue WD10JPVX', NULL, 6000, 12, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (3, 'LGA1151 i9-9900K', NULL, 85000, 2, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (4, 'LGA1151 i7-8700K', NULL, 55000, 5, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (5, 'ASRock LGA1151 H370 Performance DDR4/SATA3/GLAN/7.1/USB 3.1', NULL, 15000, 3, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (6, 'Asus LGA1151 Z390 ROG STRIX Z390-F GAMING DDR4/SATA3/GLAN/7.1/USB 3.1', NULL, 31500, 6, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (7, 'Asus LGA1151 Z390 TUF Z390-PRO GAMING DDR4/SATA3/GLAN/7.1/USB 3.1', NULL, 26000, 5, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (8, 'Memorija DIMM DDR4 2x8GB 2666MHz GeIL EVO X CL16 RGB, GEXB416GB2666C16ADC', NULL, 18000, 2, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (9, 'GeForce RTX 2080Ti Zotac AMP Extr. 11GB DDR6,HDMI/3xDP/USB/352bit/ZT-T20810B-10P', NULL, 190000, 1, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (10, 'GeForce RTX 2080 Zotac AMP Core 8GB DDR6, HDMI/3xDP/USB-C/256bit/ZT-T20800C-10P', NULL, 115000, 5, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (11, 'Kuciste be quiet! Dark Base 900 , orange', NULL, 25000, 10, DEFAULT);
INSERT INTO `racunari`.`delovi` (`id`, `naziv`, `opis`, `cena`, `na_lageru`, `logicki_obrisano`) VALUES (12, 'Napajanje 700W Cooler MasterWatt Lite 700W 80 PLUS Bronze, MPX-7001-ACABW-EU', NULL, 9000, 12, DEFAULT);

COMMIT;


-- -----------------------------------------------------
-- Data for table `racunari`.`racunari`
-- -----------------------------------------------------
START TRANSACTION;
USE `racunari`;
INSERT INTO `racunari`.`racunari` (`id`, `naziv`, `opis`, `cena`) VALUES (1, 'Racunar kancelarijski', 'Racunar za kancelariju', NULL);
INSERT INTO `racunari`.`racunari` (`id`, `naziv`, `opis`, `cena`) VALUES (2, 'Racunar multimedia', NULL, 350000);

COMMIT;


-- -----------------------------------------------------
-- Data for table `racunari`.`delovi_racunara`
-- -----------------------------------------------------
START TRANSACTION;
USE `racunari`;
INSERT INTO `racunari`.`delovi_racunara` (`id`, `kolicina`, `deo_id`, `racunar_id`) VALUES (1, 1, 1, 1);
INSERT INTO `racunari`.`delovi_racunara` (`id`, `kolicina`, `deo_id`, `racunar_id`) VALUES (2, 1, 4, 1);
INSERT INTO `racunari`.`delovi_racunara` (`id`, `kolicina`, `deo_id`, `racunar_id`) VALUES (3, 1, 7, 1);
INSERT INTO `racunari`.`delovi_racunara` (`id`, `kolicina`, `deo_id`, `racunar_id`) VALUES (4, 1, 8, 1);
INSERT INTO `racunari`.`delovi_racunara` (`id`, `kolicina`, `deo_id`, `racunar_id`) VALUES (5, 1, 11, 1);
INSERT INTO `racunari`.`delovi_racunara` (`id`, `kolicina`, `deo_id`, `racunar_id`) VALUES (6, 1, 12, 1);

COMMIT;

