from jednostruko_povezana_lista import JednostrukoPovezanaLista

class Stek:
    def __init__(self):
        self.data = JednostrukoPovezanaLista()

    def push(self, element): # dodavanje elementa na vrh steka
        self.data.add_first(element)

    def pop(self): # uklanjanje i vracanje elementa sa vrha steka
        if len(self.data) > 0:
            prvi = self.data.head.element
            self.data.remove_first()
            return prvi
        else:
            return "Stek je prazan."

    def top(self): # vracanje elementa sa vrha steka
        if len(self.data) > 0:
            return self.data.head.element
        else:
            return None

    def __len__(self): # vracanje broj elemenata na steku
        return len(self.data)

    def is_empty(self): # proverava da li je stek prazan
        if len(self.data) == 0:
            return True
        else:
            return False

    def print_elements(self): # prikazuje elemente na steku
        elements = []
        if len(self.data) == 0:
            print("Stek je prazan.")
        else:
            current = self.data.head
            while True:
                elements.append(current.element)
                if current.pointer == None:
                    break
                current = current.pointer
            print("Elementi na steku: ", elements)