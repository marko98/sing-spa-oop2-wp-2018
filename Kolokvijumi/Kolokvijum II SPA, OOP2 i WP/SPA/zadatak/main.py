from jednostruko_povezana_lista import JednostrukoPovezanaLista
from stek import Stek

# print("-----------------JEDNOSTRUKO POVEZANA LISTA-----------------------")
# jednostruko_povezana_lista = JednostrukoPovezanaLista()

# jednostruko_povezana_lista.add_first(5)
# jednostruko_povezana_lista.add_first(8)

# jednostruko_povezana_lista.add_last(10)
# jednostruko_povezana_lista.add_last(12)

# jednostruko_povezana_lista.remove_first()

# print("Duzina liste: ", len(jednostruko_povezana_lista))
# print("Lista je prazna: ", jednostruko_povezana_lista.is_empty())
# jednostruko_povezana_lista.print_elements()
# print("----------------------------------------")

print("-----------------STEK-----------------------")
stek = Stek()

# print("Dodavanje elementa 5 na vrh steka.")
# stek.push(5) # dodavanje elementa na vrh steka
# stek.print_elements()

# print("Dodavanje elementa 8 na vrh steka.")
# stek.push(8) # dodavanje elementa na vrh steka
# stek.print_elements()

# print("Dodavanje elementa 10 na vrh steka.")
# stek.push(10) # dodavanje elementa na vrh steka
# stek.print_elements()

print("Metoda pop vraca: ", stek.pop()) # uklanjanje i vracanje elementa sa vrha steka ako nije prazan
stek.print_elements()

print("Poslednje dodat element na steku, koji nije obrisan: ", stek.top()) # vracanje elementa sa vrha steka
stek.print_elements()

print("Broj elemenata na steku: ", len(stek))
print("Da li je stek prazan: ", stek.is_empty())
stek.print_elements()
print("----------------------------------------")