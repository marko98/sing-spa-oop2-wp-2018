from cvor_za_jednostruku import Cvor

class JednostrukoPovezanaLista: # clasa jednostruko povezane liste
    def __init__(self, head = None, tail = None, size = 0):
        self.head = head # pocetak liste
        self.tail = tail # kraj liste
        self.size = size # velicina liste

    def add_first(self, element): # dodaje na pocetak liste element
        new_cvor = Cvor(element)
        if self.size == 0:
            self.tail = new_cvor
        else:
            new_cvor.pointer = self.head
        self.head = new_cvor
        self.size = self.size + 1

    def add_last(self, element): # dodaje na kraj liste element
        new_cvor = Cvor(element)
        if self.size == 0:
            self.head = new_cvor
        else:
            self.tail.pointer = new_cvor
        self.tail = new_cvor
        self.size = self.size + 1

    def remove_first(self): # brise element sa pocetka liste
        if self.size == 0:
            return "Lista je prazna."
        elif self.size == 1:
            self.head = None
            self.tail = None
            self.size = 0
        else:
            self.head = self.head.pointer
            self.size = self.size - 1

    def __len__(self): # vraca broj elemenata u listi
        return self.size

    def is_empty(self): # proverava da li je lista prazna
        if self.size == 0:
            return True
        else:
            return False

    def print_elements(self): # stampa elemente u listi
        elements = []
        if self.size == 0:
            print("Lista je prazna.")
        else:
            current = self.head
            while True:
                elements.append(current.element)
                if current.pointer == None:
                    break
                current = current.pointer
            print("Elementi liste: ", elements)