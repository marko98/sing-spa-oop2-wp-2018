#ifndef TILESET_H_INCLUDED
#define TILESET_H_INCLUDED

#include <iostream>
#include <istream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <map>

#include "Tile.h"
#include "Coordinate.h"

using namespace std;

class TileSet{
private:
    map<char, Tile*> tiles;
    SDL_Texture *texture;
    SDL_Rect *destRect;
public:
    TileSet(istream &inputStream, SDL_Renderer *eRenderer);
    void draw(char code, Coordinate coordinate, SDL_Renderer *eRenderer);
    map<char, Tile*> getTiles(){ return tiles; };
};

#endif // TILESET_H_INCLUDED
