#ifndef FUEL_H_INCLUDED
#define FUEL_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Drawable.h"
#include "Movable.h"
#include "Vector.h"
#include "SpriteSheet.h"
#include "Sprite.h"

using namespace std;

class Fuel: public Sprite{
private:

public:
    Fuel(SpriteSheet *sheet): Sprite(sheet){};
};

#endif // FUEL_H_INCLUDED
