#ifndef GRUPA_H_INCLUDED
#define GRUPA_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Movable.h"
#include "Drawable.h"
#include "Sprite.h"
#include "eventListeners.h"

using namespace std;

class Grupa: public Drawable, public Movable, public KeyboardEventListener{
public:
    typedef vector<Sprite *> GrupaSprajtova;
private:
    GrupaSprajtova grupa;
public:
    Grupa(): Movable(), Drawable(), KeyboardEventListener(){};

    virtual void listen(SDL_KeyboardEvent &event){
        if(event.type == SDL_KEYDOWN){
            for(int i = 0; i < grupa.size(); i++){
                if(event.keysym.sym == SDLK_LEFT){
                    grupa[i]->state |= Sprite::LEVO;
                } else if(event.keysym.sym == SDLK_RIGHT){
                    grupa[i]->state |= Sprite::DESNO;
                } else if(event.keysym.sym == SDLK_UP){
                    grupa[i]->state |= Sprite::GORE;
                } else if(event.keysym.sym == SDLK_DOWN){
                    grupa[i]->state |= Sprite::DOLE;
                }
            }
        } else if(event.type == SDL_KEYUP){
            for(int i = 0; i < grupa.size(); i++){
                if(event.keysym.sym == SDLK_LEFT){
                    grupa[i]->state &= ~Sprite::LEVO;
                } else if(event.keysym.sym == SDLK_RIGHT){
                    grupa[i]->state &= ~Sprite::DESNO;
                } else if(event.keysym.sym == SDLK_UP){
                    grupa[i]->state &= ~Sprite::GORE;
                } else if(event.keysym.sym == SDLK_DOWN){
                    grupa[i]->state &= ~Sprite::DOLE;
                }
            }
        }
    }

    virtual void draw(SDL_Renderer *renderer){
        for(int i = 0; i < grupa.size(); i++){
            grupa[i]->draw(renderer);
        }
    };

    void brojUGrupi(){
        cout << grupa.size() << endl;
    }

    virtual void setPos(int x, int y){};
    virtual void move(int dx, int dy){
        for(int i = 0; i < grupa.size(); i++){
            grupa[i]->move(dx, dy);
        }
    };

    GrupaSprajtova getGrupa(){ return grupa; };

    void dodajUGrupu(Sprite *sprite){
        SDL_Rect *rect = sprite->getSpriteRect();
        rect->x = 0;
        rect->y = 0;
        grupa.push_back(sprite);
        if(grupa.size()>0){
            rect->x = rect->w*grupa.size();
        }
    }
};

struct promeniKretanje{
    Grupa *grupa;
    promeniKretanje(Grupa *grupa): grupa(grupa){};
    // PROMENI KRETANJE SPRITE-A NA NEKI NPR DOGADJAJ SA TASTATURE
};

#endif // GRUPA_H_INCLUDED
