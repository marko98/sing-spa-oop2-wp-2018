#ifndef EVENTLISTENERS_H_INCLUDED
#define EVENTLISTENERS_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Drawable.h"
#include "Vector.h"
#include "SpriteSheet.h"
#include "fueledSprite.h"

using namespace std;

class EventListener{
public:
    virtual void listen(SDL_Event &event) = 0;
};

class KeyboardEventListener : public EventListener{
public:
    virtual void listen(SDL_Event &event){
        if(event.type == SDL_KEYDOWN | event.type == SDL_KEYUP){
            listen(event.key);
        };
    };
    virtual void listen(SDL_KeyboardEvent &event) = 0;
};

// PLAYER NIJE SPRITE
class Player : public KeyboardEventListener, public Drawable{
private:
    Sprite *playerSprite;
public:
    Player(Sprite *ps){
        playerSprite = ps;
    }
    Player(FueledSprite *fs){
        playerSprite = fs;
    }

    virtual void listen(SDL_KeyboardEvent &event){
        if(event.type == SDL_KEYDOWN){
            if(event.keysym.sym == SDLK_LEFT){
                playerSprite->state |= Sprite::LEVO;
            } else if(event.keysym.sym == SDLK_RIGHT){
                playerSprite->state |= Sprite::DESNO;
            } else if(event.keysym.sym == SDLK_UP){
                playerSprite->state |= Sprite::GORE;
            } else if(event.keysym.sym == SDLK_DOWN){
                playerSprite->state |= Sprite::DOLE;
            }
        } else if(event.type == SDL_KEYUP){
            if(event.keysym.sym == SDLK_LEFT){
                playerSprite->state &= ~Sprite::LEVO;
            } else if(event.keysym.sym == SDLK_RIGHT){
                playerSprite->state &= ~Sprite::DESNO;
            } else if(event.keysym.sym == SDLK_UP){
                playerSprite->state &= ~Sprite::GORE;
            } else if(event.keysym.sym == SDLK_DOWN){
                playerSprite->state &= ~Sprite::DOLE;
            }
        }
    }

    virtual void move(int dx, int dy){
        playerSprite->move(dx, dy);
    }

    virtual void draw(SDL_Renderer *eRenderer){
        playerSprite->draw(eRenderer);
    }

    virtual void setPos(int x, int y){};
};

#endif // EVENTLISTENERS_H_INCLUDED
