#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Drawable.h"
#include "Movable.h"
#include "Vector.h"
#include "SpriteSheet.h"

using namespace std;

class Sprite: public Drawable, public Movable{
protected:
    SDL_Rect *spriteRect;
    //SDL_Texture *spriteTexture;
    int width, height, currentFrame, totalFrames;
    //int frameCount;
    //SDL_Rect *frameRect;
    SpriteSheet *sheet;

    friend ostream& operator<<(ostream &out, const Sprite &sprite);
public:
    // tip je kretanje
    enum kretanje {STOP, DESNO, LEVO, GORE = 4, DOLE = 8};
    int state;

    Sprite(SpriteSheet *sheet);

    // kopija instance
    // nema duboke kopije
    // samo plitke do nekog nivoa
    // u ovoj kopiji problem ce nastati kod spriteRect, spriteTexture, frameRect jer su pokazivaci
    /*Sprite(const Sprite &spriteKopijaInstance){
        spriteRect = spriteKopijaInstance.spriteRect;
        spriteTexture = spriteKopijaInstance.spriteTexture;
        frameWidth = spriteKopijaInstance.frameWidth;
        frameHeight = spriteKopijaInstance.frameHeight;
        frameCount = spriteKopijaInstance.frameCount;
        currentFrame = spriteKopijaInstance.currentFrame;
        totalFrames = spriteKopijaInstance.totalFrames;
        frameRect = spriteKopijaInstance.frameRect;
    };*/

    SDL_Rect * getSpriteRect(){ return spriteRect; };
    int getState(){ return state; };

    virtual void draw(SDL_Renderer *eRenderer);
    virtual void move(int dx, int dy);

    void setWidthAndHeight(istream &inputStream){
        /*spriteRect->w = w;
        spriteRect->h = h;*/
        inputStream >> spriteRect->w >> spriteRect->h;
    };

    void setWidthAndHeight(int w, int h){
        spriteRect->w = w;
        spriteRect->h = h;
    };

    void setState(istream &inputStream){
        //this->state = state;
        inputStream >> state;
    };

    virtual void setPos(istream &inputStream){
        /*spriteRect->x = x;
        spriteRect->y = y;*/
        inputStream >> spriteRect->x >> spriteRect->y;
    };

    virtual void setPos(int x, int y){
        spriteRect->x = x;
        spriteRect->y = y;
    };
};

struct Mesto{
    Sprite *sprite;
    Mesto(Sprite *sprite): sprite(sprite){};

    // npr void operator() operator+(){} za redefinisanje operatora +
    void operator() (){
        cout << sprite->getSpriteRect()->x << ", " << sprite->getSpriteRect()->y << endl;
    }
};

/*struct Gravitacija{
    Sprite *sprite;
    Mesto(Sprite *sprite): sprite(sprite){};

    // npr void operator() operator+(){} za redefinisanje operatora +
    void operator() (){
        cout << sprite->getSpriteRect()->x << ", " << sprite->getSpriteRect()->y << endl;
    }
};*/

#endif // SPRITE_H_INCLUDED
