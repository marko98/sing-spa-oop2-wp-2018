#ifndef FUELEDSPRITE_H_INCLUDED
#define FUELEDSPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Drawable.h"
#include "Movable.h"
#include "Vector.h"
#include "SpriteSheet.h"
#include "Sprite.h"
#include "fuel.h"

using namespace std;

class FueledSprite: public Sprite{
private:
    int maxFuel, fuel;
    friend ostream& operator<<(ostream &out, const FueledSprite &fueledSprite);
public:
    FueledSprite(SpriteSheet *sheet, int maxFuel = 10000): Sprite(sheet){
        this->maxFuel = maxFuel;
        this->fuel = maxFuel;
    };

    bool sipajGorivo(Fuel *fuel){
        if (this->spriteRect->x > fuel->getSpriteRect()->x && this->spriteRect->x < fuel->getSpriteRect()->x + 50 &&
            this->spriteRect->y < fuel->getSpriteRect()->y && this->spriteRect->y < fuel->getSpriteRect()->y + 50){
            cout << "sipam" << endl;
            this->fuel += maxFuel*0.3;
            return true;
        } else {
            cout << "ne sipam" << endl;
            return false;
        }
    };

    void setMaxFuel(istream &inputStream){
        //this->maxFuel = maxFuel;
        inputStream >> maxFuel;
    };

    void setFuel(istream &inputStream){
        //this->fuel = fuel;
        inputStream >> fuel;
    };

    virtual void move(int dx, int dy){
        if(fuel > 0){
            if(state != 0){
                if(state & 1){
                    spriteRect->x += dx;
                }
                // logicko and
                if(state & 2){
                    spriteRect->x -= dx;
                }
                if(state & 4){
                    spriteRect->y -= dy;
                }
                // ako je ukljucen bit koji ima stanje broj 8
                if(state & 8){
                    spriteRect->y += dy;
                }
                fuel -= 1;
            }
        }
    };

    virtual void draw(SDL_Renderer *eRenderer){
        if(fuel > 0){
            if(state & Sprite::LEVO){
                sheet->draw("left", currentFrame, spriteRect, eRenderer);
            } else if(state & Sprite::DESNO){
                sheet->draw("right", currentFrame, spriteRect, eRenderer);
            } else if(state & Sprite::GORE){
                sheet->draw("up", currentFrame, spriteRect, eRenderer);
            } else if(state & Sprite::DOLE){
                sheet->draw("down", currentFrame, spriteRect, eRenderer);
            } else {
                sheet->draw("down", 0, spriteRect, eRenderer);
            }

            //SDL_RenderCopy(eRenderer, spriteTexture, frameRect, spriteRect);

            totalFrames++;
            if(totalFrames%10 == 0){
                currentFrame++;
                if(currentFrame >= 9){
                    currentFrame = 0;
                }
                totalFrames = 0;
            }
        } else {
            sheet->draw("left", currentFrame, spriteRect, eRenderer);
        };
    };

};

#endif // FUELEDSPRITE_H_INCLUDED
