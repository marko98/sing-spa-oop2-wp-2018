package main;

public class Node {
	private Node pNext, pPrevious;
	private int element;
	
	public Node () {
		super();
		this.pNext = null;
		this.pPrevious = null;
	}
	
	public Node (int element) {
		super();
		this.element = element;
		this.pNext = null;
		this.pPrevious = null;
	}
	
	public int getElement() {
		return element;
	}
	
	public Node getPNext() {
		return pNext;
	}
	
	public void setPNext(Node node) {
		pNext = node;
	}
	
	public Node getPPrevious() {
		return pPrevious;
	}
	
	public void setPPrevious(Node node) {
		pPrevious = node;
	}

}
