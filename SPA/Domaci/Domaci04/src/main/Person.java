package main;

import java.util.ArrayList;

public class Person {
	private String name;
	private boolean free;
	private ArrayList<Person> listaOsoba, proseno;
	private Person partner;
	
	public Person(String name) {
		super();
		this.name = name;
		this.free = true;
		this.partner = null;
		this.listaOsoba = new ArrayList<Person>();
		this.proseno = new ArrayList<Person>();
	}
	
	public String getName() {
		return name;
	}
	
	public boolean getFree() {
		return free;
	}
	
	public void setFree(boolean newFree) {
		free = newFree;
	}
	
	public Person getPartner() {
		return partner;
	}
	
	public void setPartner(Person noviPartner) {
		partner = noviPartner;
	}
	
	public void addPerson(Person person0, Person person1, Person person2, Person person3) {
		listaOsoba.add(person0);
		listaOsoba.add(person1);
		listaOsoba.add(person2);
		listaOsoba.add(person3);
	}
	
	public void prikaziListuOsoba() {
		for(int i = 0; i < listaOsoba.size(); i++) {
			System.out.println(listaOsoba.get(i).getName());
		}
	}
	
	public Person devojkaKojuJosNijeZaprosio() {
		for(Person devojka:listaOsoba) {
//			System.out.println(devojka.getName());
			if(!proseno.contains(devojka)) {
//				System.out.println(devojka.getName());
				proseno.add(devojka);
				return devojka;
			}
		}
		return null;
	}
	
	public boolean viseVoliMomkaOdVerenika(Person momak) {
		int pozicijaMomka = 0, pozicijaPartnera = 0;
//		this.getPartner().getName()
		for(int i = 0; i < listaOsoba.size(); i++) {
			if(listaOsoba.get(i).getName() == partner.getName()) {
				pozicijaPartnera = i;
				//System.out.println("Devojka " + name + ", partner: " + partner.getName() + " " + i);
			} else if(listaOsoba.get(i).getName() == momak.getName()) {
				pozicijaMomka = i;
				//System.out.println("Devojka " + name + ", momak: " + momak.getName() + " " + i);
			}
		}
		
		if(pozicijaMomka < pozicijaPartnera) {
			//System.out.println("Devojka " + name + ", vise voli: " + momak.getName() + ", od partnera: " + partner.getName());
			return true;
		}
		
		return false;
	}
}
