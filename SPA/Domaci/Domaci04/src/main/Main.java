package main;

import java.util.ArrayList;
import java.util.Vector;

public class Main {

	public static void main(String[] args) {
		
		// DVOSTRUKO POVEZANA LISTA
		DvostrukoPovezanaLista dvostrukoPovezanaLista = new DvostrukoPovezanaLista();

		dvostrukoPovezanaLista.addFirst(5);
		dvostrukoPovezanaLista.addFirst(4);
		dvostrukoPovezanaLista.addFirst(13);
		
		dvostrukoPovezanaLista.addLast(1);
		
		dvostrukoPovezanaLista.addElement(10, 3);
		dvostrukoPovezanaLista.addElement(9, 3);
		
		dvostrukoPovezanaLista.removeFirst();
		
		dvostrukoPovezanaLista.removeLast();
		
		dvostrukoPovezanaLista.removeElement(3);
		
		
		dvostrukoPovezanaLista.showElements();
		
		System.out.println("\n----------------------------------------------------\n");
		
		// PROBLEM STABILNIH BRAKOVA

		// DEVOJKE
		Person ana = new Person("Ana");
		Person maja = new Person("Maja");
		Person lana = new Person("Lana");
		Person sara = new Person("Sara");
		
		// MOMCI
		Person stefan = new Person("Stefan");
		Person nikola = new Person("Nikola");
		Person david = new Person("David");
		Person djordje = new Person("Djordje");
		
		//---------------------------------------
		
		stefan.addPerson(maja, ana, sara, lana);
		//stefan.prikaziListuOsoba();
		
		nikola.addPerson(lana, ana, maja, sara);
		//nikola.prikaziListuOsoba();
		
		david.addPerson(ana, sara, lana, maja);
		//david.prikaziListuOsoba();
		
		djordje.addPerson(ana, maja, sara, lana);
		//djordje.prikaziListuOsoba();
		
		//---------------------------------------
		
		ana.addPerson(stefan, nikola, djordje, david);
		//ana.prikaziListuOsoba();
		
		maja.addPerson(stefan, djordje, david, nikola);
		//maja.prikaziListuOsoba();
		
		lana.addPerson(david, nikola, stefan, djordje);
		//lana.prikaziListuOsoba();
		
		sara.addPerson(djordje, david, nikola, stefan);
		//sara.prikaziListuOsoba();
		
		//-----------------------------------------
		
		ArrayList<Person> listaMomaka = new ArrayList<Person>();
		listaMomaka.add(stefan);
		listaMomaka.add(nikola);
		listaMomaka.add(david);
		listaMomaka.add(djordje);
		
		ArrayList<Person> listaDevojaka = new ArrayList<Person>();
		listaDevojaka.add(ana);
		listaDevojaka.add(maja);
		listaDevojaka.add(lana);
		listaDevojaka.add(sara);
		
		ArrayList<ArrayList<Person>> parovi = smp(listaMomaka, listaDevojaka);
		for(ArrayList<Person> par:parovi) {
			Person momak = par.get(0);
			Person devojka = par.get(1);
			System.out.println(momak.getName() + " i " + devojka.getName() + " su par.");
		}
	}
	
	public static ArrayList<ArrayList<Person>> smp(ArrayList<Person> listaMomaka, ArrayList<Person> listaDevojaka) {
		
		ArrayList<ArrayList<Person>> parovi = new ArrayList<ArrayList<Person>>();
		
		// U PRVOJ ITERACIJI KROZ LISTU MOMAKA, DAVID OSTAJE SAM, JER ANA VISE VOLI DJORDJA
		// odkomentarisati for petlju i zakomentarisati while petlju
//		for(Person momak:listaMomaka) {
//			System.out.println(momak.getName());
//			
//			Person d = momak.devojkaKojuJosNijeZaprosio();
//			if(d != null) {
//				if(d.getFree()) {
//					// DEVOJKA JE SLOBODNA
//					d.setFree(false);
//					d.setPartner(momak);
//					
//					momak.setFree(false);
//					momak.setPartner(d);
//				} else {
//					// DEVOJKA JE ZAUZETA
//					if(d.viseVoliMomkaOdVerenika(momak)) {
//						d.getPartner().setPartner(null);
//						d.getPartner().setFree(true);
//						
//						momak.setFree(false);
//						momak.setPartner(d);
//						
//						d.setPartner(momak);
//					}
//				}
//			}
//		}
		
		while(postojiSlobodanMladic(listaMomaka) != null) {
			Person momak = postojiSlobodanMladic(listaMomaka);
			Person d = momak.devojkaKojuJosNijeZaprosio();
			if(d != null) {
				if(d.getFree()) {
					// DEVOJKA JE SLOBODNA
					d.setFree(false);
					d.setPartner(momak);
					
					momak.setFree(false);
					momak.setPartner(d);
				} else {
					// DEVOJKA JE ZAUZETA
					if(d.viseVoliMomkaOdVerenika(momak)) {
						d.getPartner().setPartner(null);
						d.getPartner().setFree(true);
						
						momak.setFree(false);
						momak.setPartner(d);
						
						d.setPartner(momak);
					}
				}
			}
		}
		
//		System.out.println("\n");
		for(Person momak:listaMomaka) {
			if(momak.getPartner() != null) {
				ArrayList<Person> par = new ArrayList<Person>();
//				System.out.println(momak.getName() + " i " + momak.getPartner().getName() + " su par.");
				par.add(momak);
				par.add(momak.getPartner());
				parovi.add(par);
			}
		}	
		
		return parovi;
	}
	
	public static Person postojiSlobodanMladic(ArrayList<Person> listaMomaka) {
		for(int i = 0; i < listaMomaka.size(); i++) {
			if(listaMomaka.get(i).getFree()) {
				return listaMomaka.get(i);
			}
		}
		return null;		
	}

}
