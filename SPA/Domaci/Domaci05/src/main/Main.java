package main;

public class Main {

	public static void main(String[] args) {
		Stek stek = new Stek();
		Red red = new Red();
		

//		STEK -----------------------------
		System.out.println("----------STEK------------");
		
		stek.push("5");
		System.out.println("-" + "|" + stek.stackContents());
		stek.push("3");
		System.out.println("-" + "|" + stek.stackContents());
		System.out.println(stek.size() + "|" + stek.stackContents());
		System.out.println(stek.pop() + "|" + stek.stackContents());
		System.out.println(stek.isEmpty() + "|" + stek.stackContents());
		System.out.println(stek.pop() + "|" + stek.stackContents());
		System.out.println(stek.isEmpty() + "|" + stek.stackContents());
		System.out.println(stek.pop() + "|" + stek.stackContents());
		stek.push("7");
		System.out.println("-" + "|" + stek.stackContents());
		stek.push("9");
		System.out.println("-" + "|" + stek.stackContents());
		System.out.println(stek.top() + "|" + stek.stackContents());
		stek.push("4");
		System.out.println("-" + "|" + stek.stackContents());
		System.out.println(stek.size() + "|" + stek.stackContents());
		System.out.println(stek.pop() + "|" + stek.stackContents());
		stek.push("6");
		System.out.println("-" + "|" + stek.stackContents());
		stek.push("8");
		System.out.println("-" + "|" + stek.stackContents());
		System.out.println(stek.pop() + "|" + stek.stackContents());
		
//		RED -----------------------------
		System.out.println("----------RED------------");
		
		red.enqueue("5");
		System.out.println("-" + "|" + red.redContents());
		red.enqueue("3");
		System.out.println("-" + "|" + red.redContents());
		System.out.println(red.size() + "|" + red.redContents());
		System.out.println(red.dequeue() + "|" + red.redContents());
		System.out.println(red.isEmpty() + "|" + red.redContents());
		System.out.println(red.dequeue() + "|" + red.redContents());
		System.out.println(red.isEmpty() + "|" + red.redContents());
		System.out.println(red.dequeue() + "|" + red.redContents());
		red.enqueue("7");
		System.out.println("-" + "|" + red.redContents());
		red.enqueue("9");
		System.out.println("-" + "|" + red.redContents());
		System.out.println(red.first() + "|" + red.redContents());
		red.enqueue("4");
		System.out.println("-" + "|" + red.redContents());
		System.out.println(red.size() + "|" + red.redContents());
		System.out.println(red.dequeue() + "|" + red.redContents());
		
		
		String palindrom = "sir ima miris";
		
		Stek stek2 = new Stek();
		Red red2 = new Red();
		
		int brojac = 0;
		for(int i = 0; i < palindrom.length(); i++) {
			char c = palindrom.charAt(i); 
			stek2.push(String.valueOf(c));
			red2.enqueue(String.valueOf(c));
			brojac ++;
		}
		
		String porukaStek = "";
		String porukaRed = "";
		for(int i = 0; i < brojac; i++) {
			porukaStek = porukaStek + stek2.pop();
			porukaRed = porukaRed + red2.dequeue();
		}
		System.out.println(porukaStek);
		System.out.println(porukaRed);
	}

}
