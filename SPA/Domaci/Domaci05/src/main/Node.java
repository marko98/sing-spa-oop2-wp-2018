package main;

public class Node {
	private Node pNext, pPrevious;
	private String element;
	
	public Node () {
		super();
		this.pNext = null;
		this.pPrevious = null;
	}
	
	public Node (String element) {
		super();
		this.element = element;
		this.pNext = null;
		this.pPrevious = null;
	}
	
	public String getElement() {
		return element;
	}
	
	public Node getPNext() {
		return pNext;
	}
	
	public void setPNext(Node node) {
		pNext = node;
	}
	
	public Node getPPrevious() {
		return pPrevious;
	}
	
	public void setPPrevious(Node node) {
		pPrevious = node;
	}

}
