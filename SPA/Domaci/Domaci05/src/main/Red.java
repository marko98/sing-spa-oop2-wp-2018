package main;

import java.util.ArrayList;

// FIFO
public class Red {
	private DvostrukoPovezanaLista data;
	
	public Red() {
		super();
		this.data = new DvostrukoPovezanaLista();
	}
	
	public void enqueue(String element) {
		data.addLast(element);
	}
	
	public String dequeue() {
		if(data.listaJePrazna()) {
			String elementNaPocetku = data.getHead().getPNext().getElement();
			data.removeFirst();
			return elementNaPocetku;
		} else {
			return null;
		}
	}
	
	public String first() {
		return data.getHead().getPNext().getElement();
	}
  
	public boolean isEmpty() {
		return !data.listaJePrazna();
	}
	
	public int size() {
		return data.getSize();
	}

    public String redContents() {
    	ArrayList<String> niz = new ArrayList<String>();
    	Node current = data.getHead().getPNext();
    	while(current.getPNext() != null) {
    		niz.add(current.getElement());
    		current = current.getPNext();
    	}
    	
    	String nizString = "";

    	for (String s : niz){
    		nizString += " " + s;
    	}
    	
    	return nizString;
    }
    
}
