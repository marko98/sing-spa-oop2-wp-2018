package main;

import java.util.ArrayList;

// LIFO
public class Stek {
	private DvostrukoPovezanaLista data;
	
	public Stek() {
		super();
		this.data = new DvostrukoPovezanaLista();
	}
	
	public void push(String element) {
		data.addFirst(element);
	}
	
	public String pop() {
		if(data.listaJePrazna()) {
			String poslednjiElement = data.getHead().getPNext().getElement();
			data.removeFirst();
			return poslednjiElement;
		} else {
			return null;
		}
	}
	
	public String top() {
		if(data.listaJePrazna()) {
			return data.getHead().getPNext().getElement();
		} else {
			return null;
		}
	}
	
	public boolean isEmpty() {
		return !data.listaJePrazna();
	}
	
	public int size() {
		return data.getSize();
	}

    public String stackContents() {
    	ArrayList<String> niz = new ArrayList<String>();
    	Node current = data.getHead().getPNext();
    	while(current.getPNext() != null) {
    		niz.add(current.getElement());
    		current = current.getPNext();
    	}
    	
    	String nizString = "";

    	for (String s : niz){
    		nizString += " " + s;
    	}
    	
    	return nizString;
    }
    
}
