package main;

public class DvostrukoPovezanaLista {
	private Node head, tail;
	private int size;
	
	public DvostrukoPovezanaLista() {
		super();
		this.size = 0;
		this.head = new Node();
		this.tail = new Node();
		
		this.head.setPNext(this.tail);
		this.tail.setPPrevious(this.head);
	}
	
	public Node getHead() {
		return head;
	}
	
	public Node getTail() {
		return tail;
	}

	public int getSize() {
		return size;
	}
	
	//	ADD FIRST ---------------------------
	public void addFirst(String element) {
		Node newNode = new Node(element);
		
		newNode.setPPrevious(head);
		newNode.setPNext(head.getPNext());
		
		head.getPNext().setPPrevious(newNode);
		head.setPNext(newNode);
		
		size++;
	}
	
	// ADD LAST -----------------------------
	public void addLast(String element) {
		Node newNode = new Node(element);
		
		newNode.setPNext(tail);
		newNode.setPPrevious(tail.getPPrevious());
		
		tail.getPPrevious().setPNext(newNode);
		tail.setPPrevious(newNode);
		
		size++;
	}
	
	// ADD ELEMENT ---------------------------
	public void addElement(String element, int index) {
		if(provera(index)) {
			Node newNode = new Node(element);
			
			Node current = head.getPNext();
			int brojac = 0;
			while(index != brojac) {
				current = current.getPNext();
				brojac++;
			}
			
	//		System.out.println(current.getElement());
			newNode.setPPrevious(current.getPPrevious());
			newNode.setPNext(current);
			
			current.getPPrevious().setPNext(newNode);
			current.setPPrevious(newNode);
			
			size++;
		}
	}

	// REMOVE FIRST --------------------------
	public void removeFirst() {
		head.getPNext().getPNext().setPPrevious(head);
		head.setPNext(head.getPNext().getPNext());
		
		size--;
	}
	
	// REMOVE LAST --------------------------
	public void removeLast() {
		tail.getPPrevious().getPPrevious().setPNext(tail);
		tail.setPPrevious(tail.getPPrevious().getPPrevious());
		
		size--;
	}

	// REMOVE ELEMENT -----------------------
	public void removeElement(int index) {
		if(provera(index)) {
			Node current = head.getPNext();
			int brojac = 0;
			while(index != brojac) {
				current = current.getPNext();
				brojac++;
			}
	//		System.out.println(current.getElement());
			
			current.getPPrevious().setPNext(current.getPNext());
			current.getPNext().setPPrevious(current.getPPrevious());
			
			size--;
		}
	}

	// PROVERA INDEXA --------------------
	public boolean provera(int index) {
		
		Node current = head.getPNext();
		int brojac = 0;
		while(current.getPNext() != null) {
			if(index == brojac) {
				return true;
			}
			current = current.getPNext();
			brojac++;
		}
		System.out.println("Izabrali ste nepostojeci index.");
		return false;
	}
	
	// STAMPANJE ---------------------------
	public void showElements() {
		Node current = head.getPNext();
		while(current.getPNext() != null) {
			System.out.println(current.getElement());
			current = current.getPNext();			
		}
	}
	
	public boolean listaJePrazna() {
		if (size != 0) {
			return true;
		} else {
			return false;
		}
	}
	
}
