package Main;

import java.util.Random;

public class SortiranjeNizova {

	public static void main(String[] args) {
		
		Random randomBrojevi = new Random();
		int[] a = new int[100000];
		int[] b = new int[100000];
		int[] c = new int[100000];

		int i = 0;
		while(true) {
			int n = randomBrojevi.nextInt(100) + 1;
			a[i] = n;
			b[i] = n;
			c[i] = n;

			if(i == 99999) {
				break;
			} else {
				i++;
			}
		}
		
		long bubbleSortStart = System.currentTimeMillis(); 
		bubbleSort(a, a.length);
		long bubbleSortEnd = System.currentTimeMillis();
		long bubbleSortVreme = bubbleSortEnd - bubbleSortStart;
		System.out.println("bubble sort");
		System.out.println(bubbleSortVreme + " ms");
		System.out.println("----------------------------------");
//		for(int j = 0; j < a.length; j++) {
//			System.out.print(a[j] + ", ");
//		}
		
		System.out.println("\n");
		
		long insertSortStart = System.currentTimeMillis(); 
		insertSort(b, b.length);
		long insertSortEnd = System.currentTimeMillis();
		long insertSortVreme = insertSortEnd - insertSortStart;
		System.out.println("insert sort");
		System.out.println(insertSortVreme + " ms");
		System.out.println("----------------------------------");
//		for(int g = 0; g < b.length; g++) {
//			System.out.print(b[g] + ", ");
//		}
		
		System.out.println("\n");
		
		long selectSortStart = System.currentTimeMillis(); 
		selectSort(c, c.length);
		long selectSortEnd = System.currentTimeMillis();
		long selectSortVreme = selectSortEnd - selectSortStart;
		System.out.println("select sort");
		System.out.println(selectSortVreme + " ms");
		System.out.println("----------------------------------");
//		for(int f = 0; f < c.length; f++) {
//			System.out.print(c[f] + ", ");
//		}
	

	}
	
	public static void selectSort(int[] a, int n) {
		for(int i = 0; i < n; i++) {
			int j = min(a, i, n);
			swap(a, i, j);
		}
	}
	
	public static int min(int[] a, int i, int n) {
		int najmanji_broj = a[i];
		int pozicija_najmanjeg_broja = i;
		for(int j = i; j < n; j++) {
			if(a[j] < najmanji_broj) {
				najmanji_broj = a[j];
				pozicija_najmanjeg_broja = j;
			}
		}
		return pozicija_najmanjeg_broja;
	}
	
	public static void insertSort(int[] a, int n) {
		for(int i = 0; i < n ; i++) {
			int j = i;
			while((j > 0) && (a[j] < a[j-1])) {
				swap(a, j, j-1);
				j = j - 1;
			}
		}
	}
	
	public static void bubbleSort(int[] a, int n) {
		for(int i = 0; i <= n - 1 ; i++) {
			for(int j = n - 1; j >= i + 1; j--) {
				if(a[j] < a[j-1]) {
					swap(a, j, j-1);				
				}
			}
		}
	}
	
	public static void swap(int[] a, int i, int j) {
		int privremena = a[i];
		a[i] = a[j];
		a[j] = privremena;	
	}
	
	

}
