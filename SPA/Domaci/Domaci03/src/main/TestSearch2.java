package main;

import java.util.Random;

public class TestSearch2 {

	public static void main(String[] args) {
		Random randomBrojevi = new Random();
		int[] a = new int[100000];

		int i = 0;
		while(true) {
			int n = randomBrojevi.nextInt(100) + 1;
			a[i] = n;

			if(i == 99999) {
				break;
			} else {
				i++;
			}
		}

		insertSort(a, a.length);
		
		int x = randomBrojevi.nextInt(100) + 1;
		
		long seqSearchStart = System.currentTimeMillis();
		int pozicijaSeqSearch = seqSearch(a, a.length, x);
		long seqSearchEnd = System.currentTimeMillis();
		long seqSearchVreme = seqSearchEnd - seqSearchStart;
		
		long binSearchStart = System.currentTimeMillis();
		int pozicijaBinSearch = binSearch(a, a.length, x);
		long binSearchEnd = System.currentTimeMillis();
		long binSearchVreme = binSearchEnd - binSearchStart;
		
		System.out.println("Pozicija u nizu: " + pozicijaSeqSearch + ", vreme izvrsavanja: "
				+ seqSearchVreme + " (seqSearch) " + "\n");
		System.out.println("Pozicija u nizu: " + pozicijaBinSearch + ", vreme izvrsavanja: "
				+ binSearchVreme + " (binSearch) " + "\n");
	}
	
	public static int seqSearch(int[] a, int n, int x) {
		for(int i = 0; i < n; i++) {
			if(a[i] == x) {
				return i;
			}
		}
		return 0;
	}
	
	public static int binSearch(int[] a, int n, int x) {
		int i = 0, j = n - 1;
		while(i <= j) {
			int k = (i + j)/2;
			if(x < a[k]) {
				j = k - 1;
			}
			else if(x > a[k]) {
				i = k + 1;
			}
			else {
				return k;
			}
		}
		
		return 0;
	}
	
	public static void insertSort(int[] a, int n) {
		for(int i = 0; i < n ; i++) {
			int j = i;
			while((j > 0) && (a[j] < a[j-1])) {
				swap(a, j, j-1);
				j = j - 1;
			}
		}
	}
	
	public static void swap(int[] a, int i, int j) {
		int privremena = a[i];
		a[i] = a[j];
		a[j] = privremena;	
	}
	
	
}
