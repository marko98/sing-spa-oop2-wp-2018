package main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Provera {

	public static int proveraBroja(String poruka) {
		Integer broj;
		while(true) {
			try {
				System.out.println(poruka);
				Scanner tastatura = new Scanner(System.in);
				broj = tastatura.nextInt();
				break;
			} catch (InputMismatchException e) {
				System.out.println("Molimo unesite broj\n");
			}
		}
		return broj;
	}
	
	public static boolean proveraBrojaIGranica(int gornjaGranica, int donjaGranica, int opcija) {
		boolean dozvola = false;
		
		if(opcija <= gornjaGranica && opcija >= donjaGranica) {
			dozvola = true;
		}
		
		return dozvola;
	}

}
