package main;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Integer> niz_brojeva = new ArrayList<Integer>();
		
		while(true) {
			if(niz_brojeva.isEmpty()) {
				System.out.println("Niz je trenutno prazan\n");
				Integer broj = Provera.proveraBroja("Unesite broj koji zelite da dodate u niz: ");
				niz_brojeva.add(broj);
			} else {
				System.out.println("Opcije: \n1. Unesite broj u niz\n2. Prikazi mi najveci broj unutar niza i njegovu poziciju\n3. Ugasi program\n");
				Integer odabir = Provera.proveraBroja("Odaberite opciju: ");
				if(Provera.proveraBrojaIGranica(3, 1, odabir)) {
					if(odabir == 1) {
						Integer broj = Provera.proveraBroja("Unesite broj koji zelite da dodate u niz: ");
						niz_brojeva.add(broj);
					} else if (odabir == 2) {
						
						Integer najveci_broj = niz_brojeva.get(0);
						Integer brojac = 0;
						Integer pozicija_najveceg_broja = 0;
						
						for (Integer trenutni_broj : niz_brojeva) {
							if(trenutni_broj > najveci_broj) {
								najveci_broj = trenutni_broj;
								pozicija_najveceg_broja = brojac;
							}
							brojac++;
						}
						
						//Duplikati
						boolean duplikati = false;
						int brojac_duplikata = 0;
						for (Integer trenutni_broj : niz_brojeva) {
							if(najveci_broj == trenutni_broj) {
								brojac_duplikata++;
							}
						}
						if(brojac_duplikata >= 2) {
							duplikati = true;
						}
						
						if(duplikati) {
							System.out.println("Najveci broj je: " + najveci_broj + " i on je na poziciji: " + 
									pozicija_najveceg_broja + ", pocevsi od 0\nU nizu se nalazi vise brojeva sa istom vrednoscu\n");
						} else {
							System.out.println("Najveci broj je: " + najveci_broj + " i on je na poziciji: " + 
									pozicija_najveceg_broja + ", pocevsi od 0\n");
						}
						
					} else {
						break;
					}
				}
			}
		}
			
	}

}
