package Main;
import java.util.Random;

public class MinMaxElement {

	public static void main(String[] args) {

		Random randomBrojevi = new Random();
		int[] a = new int[100];

		int i = 0;
		while(true) {
			int n = randomBrojevi.nextInt(100000) - 50000;
			a[i] = n;

			if(i == 99) {
				break;
			} else {
				i++;
			}
		}

		for (int j = 0; j < a.length; j++) {
			
			if(j%15 == 0 && j != 0) {
				System.out.print('\n');
			}

			if(j == a.length - 1) {
				System.out.print(a[j] + "\n");
			} else {
				System.out.print(a[j] + ", ");
			}
			
			i++;
		}	

		minMax(a);
	}

	public static void minMax(int[] a) {

		int max = a[0];
		int min = a[1];
		int i = 0;
		
		while (i < a.length) {
			if (a[i] > a[i + 1]) {
				if(a[i] > max) {
					max = a[i];
				}
				if(a[i + 1] < min) {
					min = a[i + 1];
				}
			} else {
				if(a[i + 1] > max) {
					max = a[i + 1];
				}
				if(a[i] < min) {
					min = a[i];
				}				
			}
			i = i + 2;
		}

		System.out.println("Maksimum: " + max + ", minimum: " + min);

	}

}
