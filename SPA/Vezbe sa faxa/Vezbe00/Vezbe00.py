#za nazive klasa koristi camilcase
#funkcije definisane u klasi su METODE!
#email domacispa@gmail.com

#----------------------------------------------------------------------

class Osoba():

    def __init__(self, ime, prezime, starost):
        self.ime = ime
        self.prezime = prezime
        self.starost = starost

    def __eq__(self, druga_osoba):
        #samo poredjenje brojeva vraca true ili false
        return self.starost == druga_osoba.starost

    def __ne__(self, druga_osoba):
        #samo poredjenje brojeva vraca true ili false
        return self.starost != druga_osoba.starost

    def __lt__(self, druga_osoba):
        #samo poredjenje brojeva vraca true ili false
        return self.starost < druga_osoba.starost

    def __gt__(self, druga_osoba):
        #samo poredjenje brojeva vraca true ili false
        return self.starost > druga_osoba.starost

    def poredjenje_godina(self, druga_osoba):
        if self == druga_osoba:
            return self.ime + " je istog godista kao i " + druga_osoba.ime
        elif self < druga_osoba:
            return self.ime + " je mladji od " + druga_osoba.ime
        elif self > druga_osoba:
            return self.ime + " je stariji od " + druga_osoba.ime
        else:
            return "Nisu isto godiste"


osoba1 = Osoba("Mina", "Gusman", 28)
osoba2 = Osoba("Marko", "Zahorodni", 28)
osoba3 = Osoba("Maja", "Petrovic", 37)

print(osoba1.poredjenje_godina(osoba2) + "\n")

niz_osoba = []

niz_osoba.append(osoba1)
niz_osoba.append(osoba2)
niz_osoba.append(osoba3)

duzina_niz_osoba = len(niz_osoba)

if duzina_niz_osoba != 0:
    najstariji = niz_osoba[0]
    for i in range(0, duzina_niz_osoba):
        if najstariji.starost < niz_osoba[i].starost:
            najstariji = niz_osoba[i]
    print("Najstarija osoba je " + najstariji.ime + " i ima " + str(najstariji.starost) + " godina" + "\n")
else:
    print("Prazna lista osoba.\n")

#----------------------------------------------------------------------

class Automobil():

    def __init__(self, marka, model):
        self.marka = marka
        self.model = model

    def ispisi_podatke(self):
        return "Marka: {} i model: {}".format(self.marka, self.model)

    #ne treba self u zagradi zbog static
    @staticmethod
    def primer():
        return None


automobil1 = Automobil("fiat", "500")
automobil1.marka = "ford"

print("Marka kola je: " + automobil1.marka + "\n")

#konkatanacija nije dozvoljena sa None
print(Automobil.primer(), "\n")
print(automobil1.ispisi_podatke() + "\n")

#----------------------------------------------------------------------

niz = [1, 2, 54]
duzina = len(niz)

if duzina != 0:
    maximum = niz[0]
    for i in range(0, duzina):
        if niz[i] > maximum:
            maximum = niz[i]
    print("(Prvi nacin) Najveci broj je:", maximum, "\n")
else:
    print("Nema clanove\n")

# print("(Drugi nacin) Najveci broj je:", max(niz))

#----------------------------------------------------------------------