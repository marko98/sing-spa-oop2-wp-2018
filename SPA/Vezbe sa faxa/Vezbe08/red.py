from cvor import Cvor
from dvostrukoPovezanaLista import DvostrukoPovezanaLista

# FIFO
class Red:
    def __init__(self):
        self.data = DvostrukoPovezanaLista()

    def enqueue(self, element):
        self.data.add_last(element)
    
    def dequeue(self):
        if self.data.lista_je_prazna():
            element_na_pocetku = self.data.head.p_next.element
            self.data.remove_first()
            return element_na_pocetku
        else:
            poruka = "Red je prazan."
            return poruka

    def first(self):
        return self.data.head.p_next.element

    def is_empty(self):
        return not self.data.lista_je_prazna()

    def __len__(self):
        return self.data.size

    def red_contents(self):
        niz = []
        current = self.data.head.p_next
        while(current.p_next is not None):
            niz.append(current.element)
            current = current.p_next
        return niz