from cvor import Cvor
from jednostrukoPovezanaLista import JednostrukoPovezanaLista
from dvostrukoPovezanaLista import DvostrukoPovezanaLista
from stek import Stek
from red import Red

# lista1 = JednostrukoPovezanaLista()

# lista1.add_first(5)
# lista1.add_last(6)
# lista1.add_first(4)
# lista1.add_last(7)
# lista1.add_first("prvi")

# lista1.printing_element()

# print("---------------")

# lista1.remove_first()
# lista1.printing_element()

# print("---------------")

# lista1.remove_last()
# lista1.printing_element()

# print("---------------")

# lista1.remove_last()
# lista1.printing_element()

# DVOSTRUKO POVEZANA LISTA -----------------------------
print("----------DVOSTRUKO POVEZANA LISTA------------")
dvostruko_povezana_lista = DvostrukoPovezanaLista()

dvostruko_povezana_lista.add_first(5)
dvostruko_povezana_lista.add_first(6)
dvostruko_povezana_lista.add_first(7)
dvostruko_povezana_lista.add_last(8)

# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_element(0)

#dvostruko_povezana_lista.add_element(7, 2)

#dvostruko_povezana_lista.remove_element(3)

dvostruko_povezana_lista.printing_elements()
 

# STEK -----------------------------
print("----------STEK------------")
stek = Stek()

stek.push(5)
print("-", "|", stek.stack_contents())
stek.push(3)
print("-", "|", stek.stack_contents())
print(len(stek), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
print(stek.is_empty(), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
print(stek.is_empty(), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
stek.push(7)
print("-", "|", stek.stack_contents())
stek.push(9)
print("-", "|", stek.stack_contents())
print(stek.top(), "|", stek.stack_contents())
stek.push(4)
print("-", "|", stek.stack_contents())
print(len(stek), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
stek.push(6)
print("-", "|", stek.stack_contents())
stek.push(8)
print("-", "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())

# RED -----------------------------
print("----------RED------------")
red = Red()

red.enqueue(5)
print("-", "|", red.red_contents())
red.enqueue(3)
print("-", "|", red.red_contents())
print(len(red), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())
print(red.is_empty(), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())
print(red.is_empty(), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())
red.enqueue(7)
print("-", "|", red.red_contents())
red.enqueue(9)
print("-", "|", red.red_contents())
print(red.first(), "|", red.red_contents())
red.enqueue(4)
print("-", "|", red.red_contents())
print(len(red), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())