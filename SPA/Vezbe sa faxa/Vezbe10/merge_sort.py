from random import randint
from time import time
s = list()
n = 1000000
pocetak = time()
for i in range(n):
    s.append(randint(0, 10))
kraj = time()
print("Vreme za petlju:", kraj-pocetak)

# s = [6, 5, 3, 1, 8, 7, 2, 4]
# print(s)

def podeli(lista):
    if len(lista) > 1:
        polovina = len(lista)//2
        s1 = lista[:polovina]
        s2 = lista[polovina:]
        # print(s1, s2)
        # if(len(s1) > 1):
        podeli(s1)
        # if(len(s2) > 1):
        podeli(s2)
        sortiranje(s1, s2, lista)
        
    else:
        return

# s = [48, 10, 12, 11, 89, 15]
# s1 = [10, 12, 89]
# s2 = [11, 15, 48]

def sortiranje(s1, s2, lista):
    k = 0
    i = 0
    j = 0
    # print(lista)
    while(i < len(s1) and j < len(s2)):
        if s1[i] < s2[j]:
            lista[k] = s1[i]
            k = k + 1
            i = i + 1
        else:
            lista[k] = s2[j]
            k = k + 1
            j = j + 1

    while(True):
        if i == len(s1):
            lista[k] = s2[j]
            k = k + 1
            j = j + 1
        else:
            lista[k] = s1[i]
            k = k + 1
            i = i + 1
        if k == len(lista):
            break
    # print(lista)

start = time()
podeli(s)
end = time()
# print(s)
print("Vreme: ", end - start)

lista = [15, 22, 32, 59, 98]
def binarna_pretraga(lista, pocetak, kraj, element):
    if not 0 <= pocetak <= kraj <= len(lista)-1:
        print("Element nije u listi.")
        return
    else:
        polovina = (pocetak + kraj)//2
        if lista[polovina] == element:
            print("Index elementa u listi: ", lista.index(element))
        else:
            if element < lista[polovina]:
                binarna_pretraga(lista, pocetak, polovina-1, element)
            else:
                binarna_pretraga(lista, polovina+1, kraj, element)

binarna_pretraga(lista, 0, len(lista)-1, 22)