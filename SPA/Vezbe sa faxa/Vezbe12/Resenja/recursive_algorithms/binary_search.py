# uslov za ispravan rad binarne pretrage jeste da je lista sortirana
def binary_search(data, target, low, high):
    """
    Binarna pretraga
    :param data: sortirana lista elemenata
    :param target: element koji trazimo u listi data
    :param low: indeks pocetka strukture od kojeg trazimo target element
    :param high: indeks kraja strukture do kojeg trazimo target element
    :return: True ako je element pronadjen, u suprotnom False.
    """
    if not 0 <= low <= high <= len(data)-1:  # proverimo da li ima smisla vrsiti pretragu, low mora biti manji ili jednak high, i takodje mora biti u opsegu indeksa liste data
        return False
    else:
        mid = (low + high) // 2  # trazimo srednji element liste
        if target == data[mid]:  # ukoliko je bas taj srednji element i trazeni
            return True  # vratimo da je pronadjen
        elif target < data[mid]:  # ako nije pronadjen uporedimo ga da li je manji od srednjeg elementa
            return binary_search(data, target, low, mid-1)  # ponovo pozovemo binarnu pretragu od pocetka liste do srednjeg elementa
        else:  # u suprotnom trazeni element se verovatno nalazi u desnom delu
            return binary_search(data, target, mid+1, high) # ponovo pozivamo binarnu pretragu od srednjeg elementa do kraja liste
