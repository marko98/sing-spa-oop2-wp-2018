from merge_sort import merge_sort
from binary_search import binary_search
from quick_sort import quick_sort
from factorial import factorial

# Lista za sortiranje i pretragu
lista = [13, 6, 2, 10, 15, 4, 32, 11, 18, 20, 3, 8]
print("Nesortirana lista:")
for i in lista:
    print(i, end=" ")
print("\n--------------Merge sort--------------")
ms_list = lista.copy()
merge_sort(ms_list)
print("Sortirana lista:")
for i in ms_list:
    print(i, end=" ")
print("\n--------------Quick sort----------------")
qs_list = lista.copy()
quick_sort(qs_list)
print("Sortirana lista:")
for i in qs_list:
    print(i, end=" ")
print("\n--------------Binary search--------------")
# Da bismo mogli da uradimo binarnu pretragu prvi uslov
# je da li je lista sortirna, tako da cemo izabrati neku
# od prethodno sortiranih listi
res = binary_search(qs_list, 32, 0, len(qs_list)-1)
if res:
    print("U listi je pronadjen broj 32")
else:
    print("U listi nije pronadjen broj 32")
