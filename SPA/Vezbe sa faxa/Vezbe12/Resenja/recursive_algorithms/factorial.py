def factorial(n):
    """
    Racuna vrednost faktorijela broj n
    :param n: broj od kojeg trazimo faktorijel
    :return: faktorijel broja
    """
    if n == 0: # trivijalan problem i resenje, ujedno i uslov za izlazak iz rekurzije
        return 1
    return n * factorial(n - 1)  # rekurzivni poziv funkcije faktorial za broj manji od prethodnog
