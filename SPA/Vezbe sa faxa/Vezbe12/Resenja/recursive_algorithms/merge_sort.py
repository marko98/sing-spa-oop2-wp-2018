def merge(s1, s2, s):
    """
    Spaja sortirane sekvence s1 i s2 u rezultujucu sekvencu s.
    :param s1: sortirana leva polovna liste s
    :param s2: sortirana desna polovina liste s
    :param s: lista koju cemo reorganizovati spram prethodno sortiranih polovina
    :return:
    """
    i = j = k = 0  # indeksi za kretanje po sekvencama s1, s2 i s
    # za s1 koristimo i, za s2 koristimo j, i za s koristimo k
    while i < len(s1) and j < len(s2):  # ukoliko nismo prosli bar jednu od listi
        if s1[i] < s2[j]:  # uporedi trenutnim pozicijama listi s1 i s2 koji je manji element
            s[k] = s1[i]  # dodeli manji element u listu s
            i += 1 # inkrementiraj brojac i, kako se dodati broj vise ne bi poredio sa drugim elementima
        else:  # ukoliko je veci
            s[k] = s2[j]  # dodeli veci element u listu s
            j += 1  # inkrementiraj brojac j
        k += 1  # inkrementiraj brojac k kojim označavamo da smo popunili odgovarajući element liste

    while i < len(s1):  # ukoliko se desi da nismo preuzeli sve elemente iz liste
        # ovo se moze dogoditi kada neka od listi ima vise elemenata (npr. prilikom podele na dve liste
        # koja ima 11 elemenata, u jednoj ce biti 5, a u drugoj 6
        s[k] = s1[i]  # dodati ih na kraj liste s
        i += 1  # inkrementirati brojace i i k
        k += 1

    while j < len(s2):  # isto kao i za prethodnu while petlju
        s[k] = s2[j]
        j += 1
        k += 1

def merge_2(s1, s2, s):  # druga merge funkcija, radi istu stvar malo drugacijim pristupom i proverama
    i = j = 0  # inicijalizujemo indekse za kretanje kroz strukturu
    while i + j < len(s):  # proverimo da li smo prosli kroz sve elemente listi, ako nismo
        if (j == len(s2)) or ((i < len(s1)) and (s1[i] < s2[j])):  # proverimo da li indeks j je dosegao duzinu liste s2
            # ako jeste znaci da treba da uzimamo elemente samo iz liste cije vrednosti eventualno nismo iscitali
            # pored toga (nakon or operatora) proveravamo da li je ostalo jos elemenata u s1 koje nismo posetili, i ako jeste,
            # uzimamo element na i-toj poziciji i poredimo ga sa elementom na j-toj poziciji s2 liste.
            s[i+j] = s1[i]  # ukoliko je ispunjen uslov da je manji, ili da su svi iz s2 ocitani, uzimamo taj element na i-toj poziciji
            # i smestamo ga u rezultujucu listu
            i += 1  # inkrementiramo brojac i
        else:  # u suprotnom, kada nema elemenata u listi s1 i ako element na i-toj poziciji s1 je veci ili jednak elementu
            # na j-toj poziciji s2
            s[i+j] = s2[j]  # dodaj ga u rezultujucu listu
            j += 1  # inkrementiraj brojac j

def merge_sort(s):
    """
    Funkcija koja omogucuje sortiranje listi pomocu podeli i zavladaj sablona
    :param s: sekvenca (lista) s koju treba da sortiramo
    :return:
    """
    n = len(s)  # cuvamo podatak o duzini sekvence
    if n < 2:  # ako lista ima manje od dva elementa to znaci da je onda sortirana, i izlazimo iz funkcije
        return
    # podeli
    mid = n // 2  # trazimo indeks srednjeg elementa liste
    s1 = s[0:mid]  # kreiramo sekvencu s1 koja sadrzi levu polovinu sekvence s
    s2 = s[mid:n]  # kreiramo sekvencu s2 koja sadrzi desnu polovinu sekvence s
    # zavladaj
    merge_sort(s1)  # primenimo rekurzivni poziv sortiranja na sekvenci s1
    merge_sort(s2)  # primenimo rekurzivni poziv sortiranja na sekvenci s2
    # kombinuj
    merge_2(s1, s2, s)  # spojimo sortirane liste s1 i s2 u rezultujucu listu s