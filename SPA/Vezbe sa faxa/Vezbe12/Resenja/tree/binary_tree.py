from tree import Tree


class BinaryTree(Tree):
    """
    Apstraktna klasa koja predstavlja binarno stablo.
    """
    def left(self, p):
        """
        Ova metoda treba da vrati poziciju koja predstavlja levo dete od pozicije p ili None, ukoliko ne postoji.
        :param p: pozicija elementa
        :return: levo dete od p ili None
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def right(self, p):
        """
        Ova metoda treba da vrati poziciju koja predstavlja desno dete od pozicije p ili None, ukoliko ne postoji.
        :param p: pozicija elementa
        :return: desno dete od p ili None
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def sibling(self, p):
        """
        Ova metoda vraca poziciju koja predstavlja rodjaka od pozicije p, ili None ukoliko ne postoji.
        :param p: pozicija elementa
        :return: rodjak od p ili None
        """
        parent = self.parent(p)
        if parent is None:  # p must be the root
            return None  # root has no sibling
        else:
            if p == self.left(parent):
                return self.right(parent)  # possibly None
            else:
                return self.left(parent)  # possibly None

    def children(self, p):
        """
        Ova metoda generise iteracije pozicija koje predstavljaju decu od pozicije p.
        :param p: pozicija elementa
        :return: generator objekat
        """
        left = self.left(p)
        if left is not None:
            yield left
        right = self.right(p)
        if right is not None:
            yield right
