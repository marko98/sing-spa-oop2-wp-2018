from linked_binary_tree import LinkedBinaryTree
from binary_searched_tree import BinarySearchedTree


# tree = LinkedBinaryTree()
# print("Tree", tree)
# rp = tree.add_root("*")
# print("RP con", rp.container)
# rlp = tree.add_left(rp, "+")
# rrp = tree.add_right(rp, "-")
# tree.add_left(rlp, "a")
# tree.add_right(rlp, "b")
# tree.add_left(rrp, "a")
# tree.add_right(rrp, "c")


bst = BinarySearchedTree()
bst.insert_element(101)
bst.insert_element(2)
bst.insert_element(3)
bst.insert_element(52)
bst.insert_element(5)
bst.insert_element(456)
bst.insert_element(7)
bst.insert_element(42)
bst.insert_element(9)

for el in bst:
    print(el, end=" ")