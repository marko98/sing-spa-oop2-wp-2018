class Tree:
    def root(self):
        """
        Ova metoda treba da vrati poziciju korenskog elementa
        :return:
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def parent(self, p):
        """
        Ova metoda treba da vrati poziciju roditelja od p, ili None ako je p koren.
        :param p: pozicija elementa cijeg roditelja trazimo
        :return: pozicija roditelja, ukoliko postoji, ili None
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def num_children(self, p):
        """
        Ova metoda treba da vrati broj direktnih naslednika elementa na poziciji p.
        :param p: pozicija elementa
        :return: broj dece
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def children(self, p):
        """
        Generise iteraciju pozicija koje predstavljaju decu od pozicije p.
        :param p: pozicija elementa
        :return: iteracije pozicija
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def __len__(self):
        """
        Ova metoda treba da vrati ukupan broj cvorova stabla.
        :return: broj cvorova
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def is_root(self, p):
        """
        Ova metoda treba da proveri da li element na poziciji p je korenski cvor ili ne.
        :param p: pozicija elementa
        :return: True ako je korenski, u suprotnom False
        """
        return self.root() == p

    def is_leaf(self, p):
        """
        Ova metoda treba da proveri da li element na poziciji p je lisni cvor ili ne.
        :param p: pozicija elementa
        :return: True ako jeste, u suprotnom False
        """
        return self.num_children(p) == 0

    def is_empty(self):
        """
        Ova metoda treba da proveri da li je stablo prazno ili ne.
        :return: True ako je stablo prazno, u suprotnom False
        """
        return len(self) == 0

    def positions(self):
        """
        Generise iteracije svih pozicija u stablu. Za ovu metodu ce nam trebati neki od traversala
        :return:
        """
        raise NotImplementedError("Mora biti implementirano u podklasi.")

    def __iter__(self):
        """
        Generise iteracije svih elemenata koji su smesteni u stablu.
        :return:
        """
        for p in self.positions():  # primenjujemo isti poredak kao iz metode positions
            yield p.element()  # ali vracamo svaki element

    def depth(self, p):
        """
        Ova metoda treba da vrati broj koji predstavlja dubinu pozicije p u odnosu na korenski cvor.
        :param p: pozicija elementa
        :return: dubina pozicije
        """
        if self.is_root(p):
            return 0
        else:
            return 1 + self.depth(self.parent(p))

    def height_r(self, p):
        """
        Ova metoda treba da rekurzivno izracuna visinu podstabla ciji je koren na poziciji p.
        :param p: pozicija elementa
        :return: visina datog podstabla
        """
        if self.is_leaf(p):
            return 0
        else:
            return 1 + max(self.height_r(c) for c in self.children(p))

    def height(self, p=None):
        """
        Ova metoda treba da vrati visinu postabla sa korenom na poziciji p. Ukoliko je p None, onda vraca visinu celog stabla.
        :param p: pozicija elementa
        :return: visina podstabla, ili visina celog stabla
        """
        if p is None:
            p = self.root()
        return self.height_r(p)
