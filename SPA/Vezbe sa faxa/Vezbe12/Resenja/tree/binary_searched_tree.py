from linked_binary_tree import LinkedBinaryTree

class BinarySearchedTree(LinkedBinaryTree):
    def __init__(self):
        # sve se nasledjuje
        super().__init__()

    def __iter__(self):
        for p in self.positions():
            yield p.element()

    def inorder(self):
        if not self.is_empty():
            for p in self.subtree_inorder(self.root()):
                yield p
        
    def subtree_inorder(self, p):
        if self.left(p) is not None:
            for child in self.subtree_inorder(self.left(p)):
                yield child
        yield p
        if self.right(p) is not None:
            for child in self.subtree_inorder(self.right(p)):
                yield child

    def insert_element(self, e):
        if self.root() is None:
            self.add_root(e)
        else:
            self.insert_node(self.root(), e)

    def insert_node(self, root_p, value):
        if value <= root_p.element():
            if root_p.node.left:
                self.insert_node(self.left(root_p), value)
            else:
                self.add_left(root_p, value)
            
        elif value > root_p.element():
            if root_p.node.right:
                self.insert_node(self.right(root_p), value)
            else:
                self.add_right(root_p, value) 

    def positions(self):
        return self.inorder()       

    def preorder(self):
        if not self.is_empty():
            for p in self.subtree_preorder(self.root()):
                yield p
        
    def subtree_preorder(self, p):
        yield p
        for child in self.children(p):
            for other in self.subtree_preorder(child):
                yield other
        

    def postorder(self):
        if not self.is_empty():
            for p in self.subtree_postorder(self.root()):
                yield p
        
    def subtree_postorder(self, p):
        for child in self.children(p):
            for other in self.subtree_postorder(child):
                yield other
        yield p

                
    
