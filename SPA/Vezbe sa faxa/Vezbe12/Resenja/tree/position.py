class APosition:
    """
    Apstraktna klasa koja predstavlja poziciju cvora u stablu
    """
    def element(self):
        """
        Dobavlja element koji se nalazi na datoj poziciji
        :return: element sa pozicije
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def __eq__(self, other):
        """
        Proverava da li su trenutna i other pozicija jednake.
        :param other: pozicija sa kojom se poredi
        :return: True ako su jednake, u suprotnom False
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def __ne__(self, other):
        """
        Proverava da li su trenutna i other pozicija razlicite.
        NAPOMENA: Vodite racuna kada pisete metode za poredjenje, odaberite neku u kojoj cete vrsiti poredjenje
        a druga ce uvek biti samo negacija prve, kako ne biste prilikom jednog poredjenja dobili jedan rezultat, a
        prilikom drugog drugi rezultat.
        :param other: pozicija sa kojom se poredi
        :return: True ako su razlicite, u suprotnom False
        """
        return not self == other