from binary_tree import BinaryTree
from linked_binary_tree_node import Node
from linked_binary_tree_position import Position


class LinkedBinaryTree(BinaryTree):
    def _validate(self, p):
        """
        Privatna metoda koja vraca cvor spram pozicije ukoliko je validna.
        :param p: pozicija cvora
        :return: cvor na poziciji p
        :raise ValueError: ukoliko pozicija ne pripada stablu ili je nevalidna nakon brisanja.
        :raise TypeError: ukoliko p nije tipa Position
        """
        if not isinstance(p, Position):
            raise TypeError("p must be proper Position type.")
        if p.container is not self:
            print(p.container)
            raise ValueError("p does not belong to this container.")
        if p.node.parent is p.node:
            raise ValueError("p is no longer valid.")
        return p.node

    def _make_position(self, node):
        """
        Vraca instancu pozicije za prosledjeni cvor ili None ako nema node-a.
        :param node: cvor za koji kreiramo poziciju
        :return: pozicija ukoliko ima node, u suprotnom None
        """
        return Position(self, node) if node is not None else None

    def __init__(self):
        """
        Inicijalizator praznog binarnog stabla.
        """
        self.root_node = None  # nemamo korenski cvor
        self.size = 0  # broj cvorova

    def __len__(self):
        """
        Vraca ukupan broj cvorova u stablu.
        :return:
        """
        return self.size

    def root(self):
        """
        Vraca poziciju korenskog cvora.
        :return:
        """
        return self._make_position(self.root_node)

    def parent(self, p):
        """
        Vraca poziciju roditeljskog cvora, od cvora smestenog na poziciji p
        :param p: pozicija cvora cijeg roditelja trazimo
        :return: pozicija roditeljskog cvora
        """
        node = self._validate(p)  # prvo moramo validirati poziciju da bismo dobili cvor sa iste
        return self._make_position(node.parent)

    def left(self, p):
        """
        Vraca poziciju levog deteta od cvora smestenog na poziciji p.
        :param p: pozicija cvora cije levo dete trazimo
        :return: pozicija levog deteta
        """
        node = self._validate(p)
        return self._make_position(node.left)

    def right(self, p):
        """
        Vraca poziciju desnog deteta od cvora smestenog na poziciji p.
        :param p: pozicija cvora cije desno dete trazimo
        :return: pozicija desnog deteta
        """
        node = self._validate(p)
        return self._make_position(node.right)

    def num_children(self, p):
        """
        Vraca broj direktnih naslednika cvora smestenog na poziciji p.
        :param p: pozicija
        :return: broj dece
        """
        node = self._validate(p)
        count = 0
        if node.left is not None:
            count += 1
        if node.right is not None:
            count += 1
        return count

    def add_root(self, e):
        """
        Dodaje korenski cvor sa elementom e u stablo
        :param e: element koji ce biti smesten u cvor
        :return: pozicija novododatog cvora
        """
        if self.root_node is not None:
            raise ValueError("Root already exists.")
        self.size = 1
        self.root_node = Node(e)  # kada dodajemo korenski cvor on nema ni parent-a, ni levo ni desno dete
        return self._make_position(self.root_node)

    def add_left(self, p, e):
        """
        Dodaje novi cvor sa elementom e kao levo dete cvora smestenog na poziciji p.
        :param p: pozicija cvora kojem dodajemo dete
        :param e: vrednost novog cvora
        :return: pozicija novododatog cvora
        :raise ValueError: ako cvor na poziciji p vec ima levo dete
        """
        node = self._validate(p)
        if node.left is not None:
            raise ValueError("Left child exists.")
        self.size += 1
        node.left = Node(e, node)
        return self._make_position(node.left)

    def add_right(self, p, e):
        """
        Dodaje novi cvor sa elementom e kao desno dete cvora smestenog na poziciji p.
        :param p: pozicija cvora kojem dodajemo dete
        :param e: vrednost novog cvora
        :return: pozicija novododatog cvora
        :raise ValueError: ako cvor na poziciji p vec ima desno dete
        """
        node = self._validate(p)
        if node.right is not None:
            raise ValueError("Right child exists.")
        self.size += 1
        node.right = Node(e, node)
        return self._make_position(node.right)

    def replace(self, p, e):
        """
        Menja vrednost elementa za cvor na poziciji p i vraca staru vrednost elementa.
        :param p: pozicija cvora ciju vrednost menjamo
        :param e: nova vrednost
        :return: stara vrednost
        """
        node = self._validate(p)
        old = node.element
        node.element = e
        return old

    def delete(self, p):
        """
        Brise cvor na poziciji p i menja ga njegovim detetom, ako ga ima.
        :param p: pozicija cvora koji brisemo
        :return: vrednost elementa koji je bio na poziciji p
        :raise ValueError: ako cvor na poziciji p ima dva deteta
        """
        node = self._validate(p)
        if self.num_children(p) == 2:
            raise ValueError("P has two children.")
        child = node.left if node.left else node.right
        if child is not None:
            child.parent = node.parent
        if node is self.root_node:
            self.root_node = child
        else:
            parent = node.parent
            if node is parent.left:
                parent.left = child
            else:
                parent.right = child
        self.size -= 1
        node.parent = node
        return node.element

    def attach(self, p, t1, t2):
        """
        Dodaje stabla t1 i t2 kao levo i desno podstablo pozicije p.
        :param p: pozicija gde dodajemo stabla
        :param t1: podstablo
        :param t2: podstablo
        :return:
        :raise ValueError: ako cvor na poziciji p nije lisni cvor
        :raise TypeError: ako t1 i t2 nisu stabla

        """
        node = self._validate(p)
        if not self.is_leaf(p):
            raise ValueError("Position p must be a leaf.")
        if not type(self) is type(t1) is type(t2):
            raise TypeError("Tree types must match.")
        self.size += len(t1) + len(t2)  # sabiramo velicine stabala
        if not t1.is_empty():
            t1.root_node.parent = node
            node.left = t1.root_node
            t1.root_node = None
            t1.size = 0
        if not t2.is_empty():
            t2.root_node.parent = node
            node.right = t2.root_node
            t2.root_node = None
            t2.size = 0
