class Node:
    """
    Klasa koja predstavlja cvor u stablu
    """
    def __init__(self, element, parent=None, left=None, right=None):
        """
        Inicijalizator Node-a.
        :param element: element koji ce biti smesten u cvor
        :param parent: roditelj cvora
        :param left: levi potomak
        :param right: desni potomak
        """
        self.element = element
        self.parent = parent
        self.left = left
        self.right = right
