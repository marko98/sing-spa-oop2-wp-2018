from position import APosition

class Position(APosition):
    """
    Klasa koja predstavlja poziciju elementa u binarnom stablu
    """
    def __init__(self, container, node):
        """
        Inicijalizator
        :param container: linked binary tree
        :param node: cvor koji ce biti smesten na tu poziciju
        """
        self.container = container
        self.node = node

    def element(self):
        return self.node.element

    def __eq__(self, other):
        """
        Vrsi poredjenje pozicija. Pozicije su iste ako reprezentuju cvorove na istim lokacijama.
        :param other: pozicija sa kojom poredimo
        :return: True, ako su iste, u suprotnom False
        """
        return type(other) is type(self) and other.node is self.node
