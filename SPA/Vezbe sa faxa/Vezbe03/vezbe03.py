import random 

#bubble sort
def bubble_sort(niz):
    for i in range(0, len(niz)):
        for j in range(len(niz)-1, i, -1):
            if niz[j] < niz[j-1]:
                niz[j], niz[j-1] = niz[j-1], niz[j]

    return niz

# bubble sort 2
def bubble_sort2(niz):
    s = True

    # ono sto je u javi !true pa je false, ovde je not true pa je false
    while(s):
        s = False
        for j in range(len(niz)-1, 0, -1):
            if niz[j] < niz[j-1]:
                niz[j], niz[j-1] = niz[j-1], niz[j]
                s = True

    return niz

# bubble sort 2 optimizovani
def bubble_sort3(niz):
    s = True
    i = 1
    while(s):
        s = False
        for j in range(len(niz)-1, 0, -1):
            if niz[j] < niz[j-1]:
                niz[j], niz[j-1] = niz[j-1], niz[j]
                s = True
            i = i + 1

    return niz

#insertion sort
def insertion_sort(niz):
    for  i in range(0, len(niz)):
        j = i
        while(j > 0 and niz[j] < niz[j-1] ):
            niz[j], niz[j-1] = niz[j-1], niz[j]
            j = j - 1

    return niz

#selection sort
def selection_sort(niz):
    for i in range(0, len(niz)):
        # 29 - 30 str u knjizi pseudo kod za minr
        j = minr(niz, i)
        niz[i], niz[j] = niz[j], niz[i]

    return niz

def minr(niz, i):
    najmanji_broj = niz[i]
    pozicija = i
    for j in range(i, len(niz)):
        if(niz[j] < najmanji_broj):
            najmanji_broj = niz[j]
            pozicija = j

    return pozicija

niz = []
broj_elemenata = int(input("Unesite broj elemenata: "))
for i in range (0, broj_elemenata):
    element = random.randint(1, 100)
    niz.append(element)

print(niz)

selection_sort(niz)

print(niz)