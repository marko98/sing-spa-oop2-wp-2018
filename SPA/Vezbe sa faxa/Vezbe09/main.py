from cvor import Cvor
from jednostrukoPovezanaLista import JednostrukoPovezanaLista
from dvostrukoPovezanaLista import DvostrukoPovezanaLista
from stek import Stek
from red import Red
from dek import Dek
from dek2 import Dek2
from dek3 import Dek3

# lista1 = JednostrukoPovezanaLista()

# lista1.add_first(5)
# lista1.add_last(6)
# lista1.add_first(4)
# lista1.add_last(7)
# lista1.add_first("prvi")

# lista1.printing_element()

# print("---------------")

# lista1.remove_first()
# lista1.printing_element()

# print("---------------")

# lista1.remove_last()
# lista1.printing_element()

# print("---------------")

# lista1.remove_last()
# lista1.printing_element()

# DVOSTRUKO POVEZANA LISTA -----------------------------
# print("----------DVOSTRUKO POVEZANA LISTA------------")
# dvostruko_povezana_lista = DvostrukoPovezanaLista()

# dvostruko_povezana_lista.add_first(5)
# dvostruko_povezana_lista.add_first(6)
# dvostruko_povezana_lista.add_first(7)
# dvostruko_povezana_lista.add_last(8)

# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_element(0)

#dvostruko_povezana_lista.add_element(7, 2)

#dvostruko_povezana_lista.remove_element(3)

# dvostruko_povezana_lista.printing_elements()
 

# STEK -----------------------------
print("----------STEK------------")
stek = Stek()

stek.push(5)
print("-", "|", stek.stack_contents())
stek.push(3)
print("-", "|", stek.stack_contents())
print(len(stek), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
print(stek.is_empty(), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
print(stek.is_empty(), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
stek.push(7)
print("-", "|", stek.stack_contents())
stek.push(9)
print("-", "|", stek.stack_contents())
print(stek.top(), "|", stek.stack_contents())
stek.push(4)
print("-", "|", stek.stack_contents())
print(len(stek), "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())
stek.push(6)
print("-", "|", stek.stack_contents())
stek.push(8)
print("-", "|", stek.stack_contents())
print(stek.pop(), "|", stek.stack_contents())

# RED -----------------------------
print("----------RED------------")
red = Red()

red.enqueue(5)
print("-", "|", red.red_contents())
red.enqueue(3)
print("-", "|", red.red_contents())
print(len(red), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())
print(red.is_empty(), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())
print(red.is_empty(), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())
red.enqueue(7)
print("-", "|", red.red_contents())
red.enqueue(9)
print("-", "|", red.red_contents())
print(red.first(), "|", red.red_contents())
red.enqueue(4)
print("-", "|", red.red_contents())
print(len(red), "|", red.red_contents())
print(red.dequeue(), "|", red.red_contents())

# DEK -----------------------------
print("----------DEK------------")
dek = Dek()

dek.add_last(5)
print("-", "|", dek.dek_contents())
dek.add_first(3)
print("-", "|", dek.dek_contents())
dek.add_first(7)
print("-", "|", dek.dek_contents())
print(dek.first(), "|", dek.dek_contents())
print(dek.delete_last(), "|", dek.dek_contents())
print(len(dek), "|", dek.dek_contents())
print(dek.delete_last(), "|", dek.dek_contents())
print(dek.delete_last(), "|", dek.dek_contents())
dek.add_first(6)
print("-", "|", dek.dek_contents())
print(dek.last(), "|", dek.dek_contents())
dek.add_first(8)
print("-", "|", dek.dek_contents())
print(dek.is_empty(), "|", dek.dek_contents())
print(dek.last(), "|", dek.dek_contents())

# DEK2 -----------------------------
print("----------DEK2------------")
dek2 = Dek2()

dek2.add_last(5)
print("-", "|", dek2.dek_contents())
dek2.add_first(3)
print("-", "|", dek2.dek_contents())
dek2.add_first(7)
print("-", "|", dek2.dek_contents())
print(dek2.first(), "|", dek2.dek_contents())
print(dek2.delete_last(), "|", dek2.dek_contents())
print(len(dek2), "|", dek2.dek_contents())
print(dek2.delete_last(), "|", dek2.dek_contents())
print(dek2.delete_last(), "|", dek2.dek_contents())
dek2.add_first(6)
print("-", "|", dek2.dek_contents())
print(dek2.last(), "|", dek2.dek_contents())
dek2.add_first(8)
print("-", "|", dek2.dek_contents())
print(dek2.is_empty(), "|", dek2.dek_contents())
print(dek2.last(), "|", dek2.dek_contents())

# DEK3 -----------------------------
print("----------DEK3------------")
dek3 = Dek3()

dek3.add_first(9)
dek3.add_last(5)
dek3.add_first(2)
dek3.add_first(3)
dek3.add_last(7)
dek3.elements()

dek3.delete_first()
dek3.delete_last()

dek3.elements()

print(dek3.first())
print(dek3.last())
print(dek3.is_empty())