from cvor import Cvor
from jednostrukoPovezanaLista import JednostrukoPovezanaLista

class Dek2:
    def __init__(self):
        self.data = JednostrukoPovezanaLista()

    def add_first(self, element):
        self.data.add_first(element)

    def add_last(self, element):
        self.data.add_last(element)

    def delete_first(self):
        if self.data.head != None:
            vrednost = self.data.head.element
            self.data.remove_first()
            return vrednost
        else:
            return "dek je prazan"

    def delete_last(self):
        if self.data.tail != None:
            vrednost = self.data.tail.element
            self.data.remove_last()
            return vrednost
        else:
            return "dek je prazan"

    def first(self):
        if self.data.head != None:
            return self.data.head.element
        else:
            return "dek je prazan"
        
    def last(self):
        if self.data.tail != None:
            return self.data.tail.element
        else:
            return "dek je prazan"


    def is_empty(self):
        if self.data.size == 0:
            return True
        else: 
            return False

    def __len__(self):
        return self.data.size

    def dek_contents(self):
        niz = []
        if self.data.head != None:
            current = self.data.head
            while(current is not None):
                niz.append(current.element)
                current = current.pointer
        return niz