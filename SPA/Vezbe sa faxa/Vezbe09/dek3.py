class Dek3:
    def __init__(self):
        self.data = list()
    
    def add_first(self, element):
        self.data.insert(0, element)

    def add_last(self, element):
        self.data.append(element)

    def delete_first(self):
        self.data.pop(0)

    def delete_last(self):
        self.data.pop()

    def first(self):
        if len(self.data) == 0:
            return "Dek je prazan"
        else:
            return self.data[0]

    def last(self):
        if len(self.data) == 0:
            return "Dek je prazan"
        else:
            return self.data[len(self.data)-1]
    
    def is_empty(self):
        if len(self.data) == 0:
            return True
        else: 
            return False

    def elements(self):
        print(self.data)