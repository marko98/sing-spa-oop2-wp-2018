from cvor import Cvor
from dvostrukoPovezanaLista import DvostrukoPovezanaLista

class Dek:
    def __init__(self):
        self.data = DvostrukoPovezanaLista()

    def add_first(self, element):
        self.data.add_first(element)

    def add_last(self, element):
        self.data.add_last(element)

    def delete_first(self):
        vrednost = self.data.head.p_next.element
        self.data.remove_first()
        return vrednost

    def delete_last(self):
        vrednost = self.data.tail.p_previous.element
        self.data.remove_last()
        return vrednost

    def first(self):
        if self.data.head.p_next.element != None:
            return self.data.head.p_next.element
        else:
            return "dek je prazan"
        
    def last(self):
        if self.data.tail.p_previous.element != None:
            return self.data.tail.p_previous.element
        else:
            return "dek je prazan"

    def is_empty(self):
        return not self.data.lista_je_prazna()

    def __len__(self):
        return self.data.size

    def dek_contents(self):
        niz = []
        current = self.data.head.p_next
        while(current.p_next is not None):
            niz.append(current.element)
            current = current.p_next
        return niz