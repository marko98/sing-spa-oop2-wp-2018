from cvorZaJednostruku import Cvor

class JednostrukoPovezanaLista:
    def __init__(self, head = None, tail = None, size = 0):
        self.head = head
        self.tail = tail
        self.size = size

    def add_first(self, element):
        new_cvor = Cvor(element)
        if self.size == 0:
            self.tail = new_cvor
            # self.head = new_cvor -> dodato u nastavku
        else: 
            new_cvor.pointer = self.head
        self.head = new_cvor
        self.size = self.size + 1

    def add_last(self, element):
        new_cvor = Cvor(element)
        if self.size == 0:
            self.head = new_cvor
            # self.tail = new_cvor -> dodato u nastavku
        else:
            self.tail.pointer = new_cvor
        self.tail = new_cvor
        self.size = self.size + 1

    def printing_element(self):
        current = self.head
        while(current is not None):
            print(current.element)
            current = current.pointer

    def remove_first(self):
        if self.head is None or self.tail is None:
            print("Greska! Lista je prazna.")
        self.head = self.head.pointer
        self.size = self.size - 1

    def remove_last(self):
        if self.head is None or self.tail is None:
            print("Greska! Lista je prazna.")
        elif self.size == 1:
            self.head = None
            self.tail = None
            self.size = self.size - 1
        else:
            pretposlednji = self.head
            brojac = 0
            while(True):
                if (brojac == self.size-2):
                    pretposlednji.pointer = None
                    self.tail = pretposlednji
                    self.size = self.size - 1
                    break
                else:
                    pretposlednji = pretposlednji.pointer
                brojac = brojac + 1
        
                


        # poslednji = self.head
        # prethodni = poslednji.pointer
        # while(poslednji.pointer != None):
        #     prethodni = poslednji.pointer
        #     poslednji = prethodni.pointer
        
