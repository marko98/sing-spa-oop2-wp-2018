from cvor import Cvor
from dvostrukoPovezanaLista import DvostrukoPovezanaLista

# LIFO
class Stek:
    def __init__(self):
        self.data = DvostrukoPovezanaLista()

    def push(self, element):
        self.data.add_first(element)

    def pop(self):
        if self.data.lista_je_prazna():
            poslednji_element = self.data.head.p_next.element
            self.data.remove_first()
            return poslednji_element
        else:
            poruka = "Stek je prazan."
            return poruka

    def top(self):
        if self.data.lista_je_prazna():
            return self.data.head.p_next.element
        else:
            poruka = "Stek je prazan."
            return poruka

    def is_empty(self):
        return not self.data.lista_je_prazna()

    def __len__(self):
        return self.data.size
        
    def stack_contents(self):
        niz = []
        current = self.data.head.p_next
        while(current.p_next is not None):
            niz.append(current.element)
            current = current.p_next
        return niz