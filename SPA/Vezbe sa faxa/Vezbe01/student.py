from osoba import Osoba

class Student(Osoba):
    def __init__(self, ime, prezime, starost, broj_indeksa, smer, prosecna_ocena):
        super().__init__(ime, prezime, starost)
        self.broj_indeksa = broj_indeksa
        self.smer = smer
        self.prosecna_ocena = prosecna_ocena

    def __eq__(self, other):
        return self.prosecna_ocena == other.prosecna_ocena

    def __gt__(self, other):
        return self.prosecna_ocena > other.prosecna_ocena
    
    def __le__(self, other):
        return self.prosecna_ocena < other.prosecna_ocena

    
