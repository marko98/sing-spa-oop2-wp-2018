from tacka2D import Tacka2D

class Tacka3D(Tacka2D):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z
    def ispisi_kordinate(self):
        print("Vrednost tacke na x osi je: {0},na y osi je: {1}, a na z osi je: {2}".format(self.x, self.y, self.z))

      
if __name__=="__main__":
    t2 = Tacka3D(2, 3, 5)
    t2.ispisi_kordinate()