class Tabla:
    def __init__(self):
        self.matrica = [[" " for x in range(3)] for y in range(3)]

    def predstavi_se(self):
        for red in self.matrica:
            prikaz = ""
            for element in red:
                prikaz = prikaz + element + "|"
            print(prikaz)

    def upisi(self, red, kolona, vrednost):
        red = red - 1
        kolona = kolona - 1
        if red < 3 and kolona < 3:
            if self.matrica[red][kolona] == " ":
                self.matrica[red][kolona] = vrednost
                return True
        else:
            return False

    def provera(self, igrac):
        for i in range(0, 3):
            if self.matrica[i][0] == igrac and self.matrica[i][1] == igrac and self.matrica[i][2] == igrac:
                return True
            elif self.matrica[0][i] == igrac and self.matrica[1][i] == igrac and self.matrica[2][i] == igrac:
                return True
        if self.matrica[0][0] == igrac and self.matrica[1][1] == igrac and self.matrica[2][2] == igrac:
            return True
        elif self.matrica[2][2] == igrac and self.matrica[1][1] == igrac and self.matrica[0][0] == igrac:
            return True
        else:
            return False


tabla = Tabla()

x = True
while(True):

    if x:
        red = int(input("Red: "))
        kolona = int(input("Kolona: "))
        if tabla.upisi(red, kolona, "x"):
            x = False
            if tabla.provera("x"):
                print("Igrac x je pobednik!")
                break
        else:
            print("Igrac x je na potezu. Ponovo unesite red i kolonu.")
    else:
        red = int(input("Red: "))
        kolona = int(input("Kolona: "))
        if tabla.upisi(red, kolona, "o"):
            x = True
            if tabla.provera("o"):
                print("Igrac o je pobednik!")
                break
        else:
            print("Igrac o je na potezu. Ponovo unesite red i kolonu.")
    tabla.predstavi_se()


    