import pprofile
from time import time

# za kratak kod
from timeit import timeit

def profile_function():
    prof = pprofile.Profile()
    with prof():
        lista = list()
        for i in range(10000):
            lista.append(i)
    
    prof.print_stats()

profile_function()

def time_function():
    start = time()
    lista = list()
    for i in range(10000):
        lista.append(i)
    end = time()
    print("Proteklo vreme: {}".format(end-start))
    
time_function()

s = """\
lista = list()
for i in range(10000):
    lista.append(i)
"""
print(timeit(s, number=1))