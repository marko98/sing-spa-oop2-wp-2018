from vezbe03 import *
from time import time


niz = []
broj_elemenata = int(input("Unesite broj elemenata: "))
for i in range (0, broj_elemenata):
    element = random.randint(1, 100)
    niz.append(element)

x = random.randint(1, 100)

print("Nesortirani niz: " + str(niz))

selection_sort(niz)

print("Sortirani niz: " + str(niz))

print("Broj: " + str(x) + "\n")

def seqPretraga(niz, x):
    start_seq = time()
    for i in range(0, len(niz)):
        if(niz[i] == x):
            return i
    end_seq = time()
    print("Proteklo vreme: {}".format(end_seq-start_seq))
    return "Broj: " + str(x) + ", se ne nalazi u nizu."


def binPretraga(niz, x):
    start_bin = time()
    i = 0
    j = len(niz) - 1
    while(i<=j):
        k = (i + j)//2
        if(x == niz[k]):
            return k
        elif(x < niz[k]):
            j = k - 1
        else:
            i = k + 1
    end_bin = time()
    print("Proteklo vreme: {}".format(end_bin-start_bin))
    return "Broj: " + str(x) + ", se ne nalazi u nizu."
    

print ("Sekvencijalna pretraga: " + str(seqPretraga(niz, x)) + "\n")

print ("Binarna pretraga: " + str(binPretraga(niz, x)))