from cvor import Cvor
from jednostrukoPovezanaLista import JednostrukoPovezanaLista

lista1 = JednostrukoPovezanaLista()

lista1.add_first(5)
lista1.add_last(6)
lista1.add_first(4)
lista1.add_last(7)
lista1.add_first("prvi")

lista1.printing_element()

print("---------------")

lista1.remove_first()
lista1.printing_element()

print("---------------")

lista1.remove_last()
lista1.printing_element()

print("---------------")

lista1.remove_last()
lista1.printing_element()
