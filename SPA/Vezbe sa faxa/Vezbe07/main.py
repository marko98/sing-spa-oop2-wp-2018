from cvor import Cvor
from jednostrukoPovezanaLista import JednostrukoPovezanaLista
from dvostrukoPovezanaLista import DvostrukoPovezanaLista

# lista1 = JednostrukoPovezanaLista()

# lista1.add_first(5)
# lista1.add_last(6)
# lista1.add_first(4)
# lista1.add_last(7)
# lista1.add_first("prvi")

# lista1.printing_element()

# print("---------------")

# lista1.remove_first()
# lista1.printing_element()

# print("---------------")

# lista1.remove_last()
# lista1.printing_element()

# print("---------------")

# lista1.remove_last()
# lista1.printing_element()

dvostruko_povezana_lista = DvostrukoPovezanaLista()

dvostruko_povezana_lista.add_first(5)
dvostruko_povezana_lista.add_first(6)
dvostruko_povezana_lista.add_first(7)
dvostruko_povezana_lista.add_last(8)

# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_last()
# dvostruko_povezana_lista.remove_element(0)

#dvostruko_povezana_lista.add_element(7, 2)

#dvostruko_povezana_lista.remove_element(3)

dvostruko_povezana_lista.printing_elements()
 