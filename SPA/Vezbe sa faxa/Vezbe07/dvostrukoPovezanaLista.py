from cvor import Cvor

class DvostrukoPovezanaLista:
    def __init__(self, head = None, tail = None, size = 0):
        self.head = Cvor(None)
        self.tail = Cvor(None)

        self.head.p_next = self.tail
        self.tail.p_previous = self.head

        self.size = size

    def add_first(self, element):
        new_cvor = Cvor(element)

        new_cvor.p_previous = self.head
        new_cvor.p_next = self.head.p_next

        self.head.p_next.p_previous = new_cvor
        #  ova linija mora posle prethodne!
        self.head.p_next = new_cvor
        
        self.size = self.size + 1

    def add_last(self, element):
        new_cvor = Cvor(element)

        new_cvor.p_next = self.tail
        new_cvor.p_previous = self.tail.p_previous

        self.tail.p_previous.p_next = new_cvor
        self.tail.p_previous = new_cvor

        self.size = self.size + 1

    def add_element(self, element, mesto):
        new_cvor = Cvor(element)

        if mesto >= 0 and mesto < self.size:
            current = self.head.p_next
            brojac = 0
            while(True):
                if mesto == brojac:
                    break
                current = current.p_next
                brojac = brojac + 1
            # print(current.element)

            new_cvor.p_next = current
            new_cvor.p_previous = current.p_previous

            current.p_previous.p_next = new_cvor
            current.p_previous = new_cvor
            self.size = self.size + 1
        else:
            print("Promenite mesto.")

    def remove_first(self):
        if(self.lista_je_prazna()):
            self.head.p_next = self.head.p_next.p_next
            self.head.p_next.p_previous = self.head

            self.size = self.size - 1

    def remove_last(self):
        if(self.lista_je_prazna()):
            self.tail.p_previous.p_previous.p_next = self.tail
            self.tail.p_previous = self.tail.p_previous.p_previous

            self.size = self.size - 1

    def remove_element(self, mesto):
        if(self.lista_je_prazna()):
            if mesto >= 0 and mesto < self.size:
                current = self.head.p_next
                brojac = 0
                while(True):
                    if mesto == brojac:
                        break
                    current = current.p_next
                    brojac = brojac + 1
                # print(current.element)

                current.p_previous.p_next = current.p_next
                current.p_next.p_previous = current.p_previous

                self.size = self.size - 1
            else:
                print("Izabrani index ne postoji.")

    def printing_elements(self):
        if self.size != 0:
            current = self.head.p_next
            while(current.p_next is not None):
                print(current.element)
                current = current.p_next
        else:
            print("Lista je ispraznjena.")

    def lista_je_prazna(self):
        if self.size != 0:
            return True
        else:
            print("Lista je prazna.")
            return False