class LinkedBinaryTreeNode:
    def __init__(self, element, p_parent = None, p_next_left = None, p_next_right = None):
        self.element = element
        self.p_parent = p_parent
        self.p_next_left = p_next_left
        self.p_next_right = p_next_right