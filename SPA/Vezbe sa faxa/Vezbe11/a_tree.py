from a_position import APosition

class Tree(APosition):
    def root(self):
        raise NotImplementedError("Nije implementirana f-ja")

    def is_root(self, pozicija):
        return self.root == pozicija

    def parent(self, pozicija):
        raise NotImplementedError("Nije implementirana f-ja")

    def num_children(self, pozicija):
        raise NotImplementedError("Nije implementirana f-ja")

    def children(self, pozicija):
        raise NotImplementedError("Nije implementirana f-ja")

    def is_leaf(self, pozicija):
        if self.num_children == 0:
            return True
        return False

    def __len__(self):
        raise NotImplementedError("Nije implementirana f-ja")

    def is_empty(self):
        if len(self) == 0:
            return True
        return False       
    