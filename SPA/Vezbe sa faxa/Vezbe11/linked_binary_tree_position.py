from a_position import APosition

class LinkedBinaryTreePosition(APosition):
        def __init__(self, container, node):
            self.container = container
            self.node = node

        def element(self):
            return self.node.element

        def __eq__(self, other):
            # razlika izmedju == i is je u tome sto is pokazuje da li te dve vrednosti pokazuju na isti objekat
            # == pokazuje da li su jednaki po vrednosti
            if type(self) is type(other) and self.node is other.node:
                return True
            else:
                return False
            