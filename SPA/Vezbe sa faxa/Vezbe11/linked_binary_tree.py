from linked_binary_tree_position import LinkedBinaryTreePosition
from linked_binary_tree_node import LinkedBinaryTreeNode
from binary_tree import BinaryTree

class LinkedBinaryTree(BinaryTree):
    # root je cvor
    def __init__(self, root_node, size):
        self.root_node = root_node
        self.size = size

    def __len__(self):
        return self.size

    # type hint node:LinkedBinaryTreeNode ne ogranicava tip
    def make_position(self, node:LinkedBinaryTreeNode):
        # ako je if ispunjen (LinkedBinaryTreePosition(self, node)) if uslov(node is not None) else ako nije ispunjen(None)
        return LinkedBinaryTreePosition(self, node) if node is not None else None

    def root(self):
        return self.make_position(self.root_node)

    # type hint pozicija:LinkedBinaryTreePosition ne ogranicava tip
    def validacija_pozicije(self, pozicija:LinkedBinaryTreePosition):
        # ili preko funkcije type
        if not isinstance(pozicija, LinkedBinaryTreePosition):
            raise TypeError("Pozicija nije odgovarajuceg tipa")
        if pozicija.container is not self:
            raise ValueError("Greska vrednosti")
        if pozicija.node.p_parent is pozicija.node:
            # baca izuzetak i izlazi iz funkcije samo moramo uhvatiti exception inace ce sve puci (u funkciji parent kod vracanja funkcije validacija_pozicije)
            raise ValueError("Pozicija nije validna")

        return pozicija.node

    def parent(self, pozicija):
        node = self.validacija_pozicije(pozicija)
        if node is not None:
            return self.make_position(node.p_parent)

    def num_children(self, pozicija):
        brojac = 0
        cvor:LinkedBinaryTreeNode = self.validacija_pozicije(pozicija)
        if cvor.p_next_right is not None:
            brojac = brojac + 1
        if cvor.p_next_left is not None:
            brojac = brojac + 1
        return brojac

    