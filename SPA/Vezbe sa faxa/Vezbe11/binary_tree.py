from a_tree import Tree

class BinaryTree(Tree):
    def left(self, position):
        raise NotImplementedError("Nije implementirana f-ja")

    def right(self, position):
        raise NotImplementedError("Nije implementirana f-ja")

    def sibling(self, position):
        position_of_parent = self.parent(position)

        if position_of_parent is None:
            return None
        else:
            if position == self.left(position_of_parent):
                return self.right(position_of_parent)
            else:
                return self.left(position_of_parent)
