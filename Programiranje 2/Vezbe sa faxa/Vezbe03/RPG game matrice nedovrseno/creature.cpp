#include "creature.h"

Creature::Creature(double hp, double baseDamage, string name) : hp(hp), baseDamage(baseDamage), name(name) {
}

void Creature::setHp(double hp) {
    this->hp = hp;
}

void Creature::setBaseDamage(double bd) {
    this->baseDamage = bd;
}

void Creature::setName(string name) {
    this->name = name;
}

bool Creature::isAlive() {
    return hp > 0;
}

string Creature::introduce() {
    return "I am " + name + "!";
}

void Creature::attack(Creature &other) {
    other.setHp(other.getHp() - baseDamage);
}
