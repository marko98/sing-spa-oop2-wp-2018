#ifndef INVENTORY_H_INCLUDED
#define INVENTORY_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "item.h"

class Inventory{
private:
    // pokazivac na pokazivac
    Item ***items;
    int rows, columns;
    // adresa niza je adresa prvog elementa!

public:
    // ako se nista ne navede za capacity imace 10
    // u konstruktoru se prvo pisu varijablje za koje se mora uneti vrednost, pa ostale
    Inventory(int rows = 3, int columns = 3): rows(rows), columns(columns){
        size = 0;
        items = new Item **[rows];
        for(int i = 0; i < rows; i++){
            items[i] = new Item*[columns];
        }
    };
    ~Inventory(){
        for(int i = 0; i < size; i++){
            for(int j = 0; j < columns; j++){
                delete items[i][j];
            }
            delete items[i];
        }
        delete []items;
    }

    bool remove(int i, int j){
        // provera ?
        /*if(i < rows && j < columns && i<=0 && j<=0){
            return false;
        }*/
        delete items[i][j];
        return true;
    }

    Item* getItem(int i, int j){
        return items[i][j];
    };

    bool addItem(Item *item){
        if(size >= capacity){
            return false;
        }
        items[size] = item;
        size = size + 1;
        return true;
    }

    int getSize(){
        return size;
    }
    int getCapacity(){
        return capacity;
    }

};

#endif // INVENTORY_H_INCLUDED
