#include <iostream>

#include "creature.h"
#include "magic_creature.h"
#include "Inventory.h"

using namespace std;

int main()
{
    Inventory *inv = new Inventory();

    Item *i1 = new Item("Moj item");
    inv->addItem(i1);
    inv->addItem(new Item("Neki item"));

    /*for(int i = 0; i < 100; i++){
        inv->addItem(new Item("Test item "));
    }*/

    for(int i = 0; i < inv->getSize(); i++){
        inv->getItem(i)->use();
    }

    cout <<  inv->getCapacity() << endl;
    cout <<  inv->getSize() << endl;

    delete inv;
    //i1->use();

    /*Creature player(10, 2, "Player");
    player.addItem(new Item("useless trinket"));
    MagicCreature hostileMage(20, 1, 3, "Angry Mage");

    cout << player.introduce() << endl;
    cout << hostileMage.introduce() << endl;
    cout << endl;
    cout << "Battle phase!" << endl;
    cout << endl;

    player.attack(hostileMage);
    player.getItem(0)->use();
    if(hostileMage.isAlive()) {
        hostileMage.magicAttack(player);
    }

    cout << "Round 1 results:" << endl;
    cout << "\t" << player.getName() << ":" << endl;
    cout << "\t\tHP: " << player.getHp() << endl;
    cout << endl;
    cout << "\t" << hostileMage.getName() << ":" << endl;
    cout << "\t\tHP: " << hostileMage.getHp() << endl;*/
    return 0;
}
