#ifndef CREATURE_H_INCLUDED
#define CREATURE_H_INCLUDED

#include <iostream>
#include <vector>

#include "item.h"

using namespace std;

class Creature {
    protected:
        double hp;
        double baseDamage;
        string name;
        vector<Item*> inventory;

    public:
        Creature(double, double, string);
        double getHp() {return hp;}
        void setHp(double);
        double getBaseDamage() {return baseDamage;}
        void setBaseDamage(double);
        string getName() {return name;}
        void setName(string);
        void addItem(Item* item) {inventory.push_back(item);}
        Item* getItem(int index) {return inventory[index];}
        bool isAlive();
        string introduce();
        void attack(Creature&);
};


#endif // CREATURE_H_INCLUDED
