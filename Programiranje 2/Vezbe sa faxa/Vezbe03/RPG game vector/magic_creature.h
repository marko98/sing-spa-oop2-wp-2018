#ifndef MAGIC_CREATURE_H_INCLUDED
#define MAGIC_CREATURE_H_INCLUDED

#include <iostream>

#include "creature.h"

using namespace std;

class MagicCreature : public Creature {
    protected:
        double mp;

    public:
        MagicCreature(double, double, double, string);
        double getMp() {return mp;}
        void setMp(double mp) {this->mp = mp;}
        string introduce();
        void magicAttack(Creature &other);
};

#endif // MAGIC_CREATURE_H_INCLUDED
