#include "magic_creature.h"

MagicCreature::MagicCreature(double hp, double baseDamage, double mp, string name) : Creature(hp, baseDamage, name), mp(mp) {
}

string MagicCreature::introduce() {
    return Creature::introduce() + " Wooosh! You have a small wand!";
}

void MagicCreature::magicAttack(Creature &other) {
    attack(other);
}
