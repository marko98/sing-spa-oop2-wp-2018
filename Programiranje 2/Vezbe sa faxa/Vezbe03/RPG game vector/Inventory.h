#ifndef INVENTORY_H_INCLUDED
#define INVENTORY_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "item.h"

class Inventory{
private:
    // pokazivac na pokazivac
    Item **items;
    int size, capacity;
    // adresa niza je adresa prvog elementa!

public:
    // ako se nista ne navede za capacity imace 10
    // u konstruktoru se prvo pisu varijablje za koje se mora uneti vrednost, pa ostale
    Inventory(int capacity = 10): capacity(capacity){
        size = 0;
        items = new Item *[capacity];
    };
    ~Inventory(){
        for(int i = 0; i < size; i++){
            delete items[i];
        }
        // ako ne stavimo [] lokacija samo na prvom elementu ce biti obrisana
        //delete items;
        delete []items;
    }

    bool remove(int index){
        if(index < 0 || index >= size){
            return false;
        }
        //item nema unutar dinamicki locirane stvari stoga ce osloboditi memoriju (oslobadja samo staticki definisano)
        delete items[index];
        for(int i = index; i < size - 1; i++){
            items[i] = items[i+1];
        }
        size--;

    }

    Item* getItem(int index){
        return items[index];
        // dereferenciranjem se vracamo na Item* (skidamo jedno *)
        //return *(items + index);
    };

    bool addItem(Item *item){
        if(size >= capacity){
            return false;
        }
        items[size] = item;
        size = size + 1;
        return true;
    }

    int getSize(){
        return size;
    }
    int getCapacity(){
        return capacity;
    }

};

#endif // INVENTORY_H_INCLUDED
