#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED

#include <iostream>
#include <istream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

using namespace std;

class Tile{
private:
    SDL_Rect *tileRect;
public:
    Tile(int x, int y, int w, int h);
    Tile(istream &inputStream);

    SDL_Rect *getRect();
    int getRectW(){ return tileRect->w; };
    int getRectH(){ return tileRect->h; };
};

#endif // TILE_H_INCLUDED
