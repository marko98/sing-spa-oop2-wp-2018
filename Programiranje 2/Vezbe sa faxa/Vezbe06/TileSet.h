#ifndef TILESET_H_INCLUDED
#define TILESET_H_INCLUDED

#include <iostream>
#include <istream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <map>

#include "Tile.h"

using namespace std;

class Tileset{
private:
    map<char, Tile*> tiles;
    SDL_Texture *texture;
    SDL_Rect *destRect;
public:
    Tileset(istream &inputStream, SDL_Renderer *renderer);
    void draw(char code, int x, int y, SDL_Renderer *renderer);
};

#endif // TILESET_H_INCLUDED
