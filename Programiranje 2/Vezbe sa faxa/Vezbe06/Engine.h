#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <fstream>
#include <vector>

#include "TileSet.h"
#include "Level.h"

using namespace std;

class Engine{
private:
    vector<Drawable *> drawables;
    SDL_Window *window;
    SDL_Renderer *renderer;
    string title;

public:
    Engine(const string &gameTitle);
    ~Engine();

    void run();

private:
    void init();
    void free();
};

#endif // ENGINE_H_INCLUDED
