#include "Level.h"

Level::Level(istream &inputStream ,Tileset *tileset): tileset(tileset){
    int rows, columns;
    char code;
    inputStream >> rows >> columns;
    for(int i = 0; i < rows; i++){
        level.push_back(vector<char> ());
        for(int j = 0; j < columns; j++){
            inputStream >> code;
            level[i].push_back(code);
        }
    }
}

void Level::draw(SDL_Renderer *renderer){
    for(size_t i = 0; i < level.size(); i++){
        for(size_t j = 0; j < level[i].size(); j++){
            tileset->draw(level[i][j], j*32, i*32, renderer);
        }
    }
}
