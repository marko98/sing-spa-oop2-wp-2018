#include "Engine.h"

Engine::Engine(const string &gameTitle): title(gameTitle){
    init();
}

Engine::~Engine(){
    free();
}

void Engine::init(){
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void Engine::free(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = nullptr;
    renderer = nullptr;

    IMG_Quit();
    SDL_Quit();
}

void Engine::run(){
    bool quit = false;
    SDL_Event event;

    ifstream tilesetFile("resources/tilesets/grass_tileset.txt");
    Tileset *t = new Tileset(tilesetFile, renderer);

    ifstream levelFile("resources/levels/level1.txt");
    Level *l = new Level(levelFile, t);

    drawables.push_back(l);

    while(!quit){
        SDL_Delay(10);
        while(SDL_PollEvent(&event)){
            switch(event.type){
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                case SDLK_ESCAPE:
                    quit = true;
                default:
                    break;
                break;
                }
            default:
                break;
            }
        }
        SDL_SetRenderDrawColor(renderer, 230, 230, 230, 255);
        SDL_RenderClear(renderer);

        for(size_t i = 0; i < drawables.size(); i++){
            drawables[i]->draw(renderer);
        }

        SDL_RenderPresent(renderer);
    }

}
