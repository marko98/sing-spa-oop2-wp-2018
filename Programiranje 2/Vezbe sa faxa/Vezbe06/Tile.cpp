#include "Tile.h"

Tile::Tile(int x, int y, int w, int h){
    tileRect = new SDL_Rect {x, y, w, h};
}

Tile::Tile(istream &inputStream){
    tileRect = new SDL_Rect;
    inputStream >> tileRect->x >> tileRect->y >> tileRect->w >> tileRect->h;
}

SDL_Rect * Tile::getRect(){
    return tileRect;
}
