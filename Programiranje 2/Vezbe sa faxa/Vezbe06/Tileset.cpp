#include "TileSet.h"

Tileset::Tileset(istream &inputStream, SDL_Renderer *renderer){
    string path;
    inputStream >> path;
    SDL_Surface *surface = IMG_Load(path.c_str());
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    surface = nullptr;

    char code;
    while(!inputStream.eof()){
        inputStream >> code;
        tiles[code] = new Tile(inputStream);
    }

    destRect = new SDL_Rect;
}

void Tileset::draw(char code, int x, int y, SDL_Renderer *renderer){
    destRect->x = x;
    destRect->y = y;
    destRect->w = tiles[code]->getRectW();
    destRect->h = tiles[code]->getRectH();
    SDL_RenderCopy(renderer, texture, tiles[code]->getRect(), destRect);
}
