#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <string>

#include "Engine.h"
#include "Tile.h"

using namespace std;

int main(int argv, char ** args)
{
    Engine *pEngine = new Engine("Game");
    pEngine->run();

    delete pEngine;

    return 0;
}
