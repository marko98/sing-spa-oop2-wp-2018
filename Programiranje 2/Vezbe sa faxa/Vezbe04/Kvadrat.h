#ifndef KVADRAT_H_INCLUDED
#define KVADRAT_H_INCLUDED

#include <iostream>
#include <string>
#include <math.h>

#include "Oblik.h"

using namespace std;

class Kvadrat: public Oblik{
private:
    double a;

public:
    Kvadrat(double a): Oblik(), a(a){};
    virtual double povrsina() {
        return a*a;
    }
    virtual double obim(){
        return 4*a;
    }
    virtual void iscrtavanje(){
        cout<< "Kvadrat povrsine: " << this->povrsina() << ", obima: " << this->obim() << endl;
    }

    double getA(){
        return a;
    }
    void setA(double a){
        this->a = a;
    }
};

#endif // KVADRAT_H_INCLUDED
