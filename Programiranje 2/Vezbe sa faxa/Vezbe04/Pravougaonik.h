#ifndef PRAVOUGAONIK_H_INCLUDED
#define PRAVOUGAONIK_H_INCLUDED

#include <iostream>
#include <string>
#include <math.h>

#include "Oblik.h"

using namespace std;

class Pravougaonik: public Oblik{
private:
    double a, b;
public:
    Pravougaonik(double a, double b): Oblik(), a(a), b(b){};
    virtual double povrsina() {
        return a*b;
    }
    virtual double obim(){
        return 2*a+2*b;
    }
    virtual void iscrtavanje(){
        cout<< "Pravugaonik povrsine: " << this->povrsina() << ", obima: " << this->obim() << endl;
    }

    double getA(){
        return a;
    }
    void setA(double a){
        this->a = a;
    }
    double getB(){
        return b;
    }
    void setB(double b){
        this->b = b;
    }
};

#endif // PRAVOUGAONIK_H_INCLUDED
