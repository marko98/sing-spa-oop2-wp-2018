#ifndef RECEPT_H_INCLUDED
#define RECEPT_H_INCLUDED

#include <iostream>
#include <string>
#include <math.h>
#include <vector>
#include "Sastojak.h"
#include "PodatakOSastojku.h"

using namespace std;

class Recept{
private:
    vector<PodatakOSastojku *> podaci;
    string naziv;
public:
    Recept(string naziv){};

    void izracunajEnergetskaVrednostJela(){
        double energetskaVrednostJela = 0;
        for(unsigned int i = 0; i < podaci.size(); i++){
            energetskaVrednostJela = energetskaVrednostJela + podaci[i]->getSastojak().getEnegretskaVrednost() * podaci[i]->getGrami()/100;
        }
        cout << "energetska vrednost jela: " << energetskaVrednostJela << endl;
    }

    void izracunajCenuJela(){
        double cenaJela = 0;
        for(unsigned int i = 0; i < podaci.size(); i++){
            cenaJela = cenaJela + podaci[i]->getSastojak().getCena() * podaci[i]->getGrami()/100;
        }
        cout << "cena jela: " << cenaJela << endl;
    }

    void dodajPodatke(PodatakOSastojku *podatak){
        podaci.push_back(podatak);
        cout << "dodato" << endl;
    }
    void izbaciPodatke(PodatakOSastojku *podatak){
        bool status = false;
        for(unsigned int i = 0; i < podaci.size(); i++){
            if(podaci[i]->getSastojak().getName() == podatak->getSastojak().getName()){
                podaci.erase(podaci.begin()+i);
                cout << "izbaceno" << endl;
                status = true;
                break;
            }
        }
        if(!status){
            cout << "neuspesno" << endl;
        }
    }
    void predstaviSe(){
        if(podaci.size() != 0){
            for(int i = 0; i < podaci.size(); i++){
                cout << "Naziv sastojka: " << podaci[i]->getSastojak().getName() << endl;
            }
        } else {
            cout << "lista je prazna" << endl;
        }
    }

};

#endif // RECEPT_H_INCLUDED
