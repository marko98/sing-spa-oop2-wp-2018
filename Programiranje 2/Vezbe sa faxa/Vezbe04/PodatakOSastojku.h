#ifndef PODATAKOSASTOJKU_H_INCLUDED
#define PODATAKOSASTOJKU_H_INCLUDED

#include <iostream>
#include <string>
#include <math.h>

#include "Sastojak.h"

using namespace std;

class PodatakOSastojku{
private:
    Sastojak sastojak;
    double grami;
public:
    PodatakOSastojku(Sastojak sastojak, double grami): sastojak(sastojak), grami(grami){};
    Sastojak getSastojak(){
        return sastojak;
    }
    double getGrami(){
        return grami;
    }
};

#endif // PODATAKOSASTOJKU_H_INCLUDED
