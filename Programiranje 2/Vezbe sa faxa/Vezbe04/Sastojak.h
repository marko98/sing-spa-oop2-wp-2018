#ifndef SASTOJAK_H_INCLUDED
#define SASTOJAK_H_INCLUDED

#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class Sastojak{
private:
    string name;
    double cena, enegretskaVrednost;
public:
    Sastojak(){};
    Sastojak(string name, double cena, double enegretskaVrednost): name(name), cena(cena), enegretskaVrednost(enegretskaVrednost){};
    string getName(){
        return name;
    }
    void setName(string name){
        this->name = name;
    }
    double getCena(){
        return cena;
    }
    void setCena(double cena){
        this->cena = cena;
    }
    double getEnegretskaVrednost(){
        return enegretskaVrednost;
    }
    void setEnegretskaVrednost (double enegretskaVrednost){
        this->enegretskaVrednost = enegretskaVrednost;
    }
};

#endif // SASTOJAK_H_INCLUDED
