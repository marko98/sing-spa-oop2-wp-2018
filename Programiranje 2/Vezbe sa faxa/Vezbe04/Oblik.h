#ifndef OBLIK_H_INCLUDED
#define OBLIK_H_INCLUDED

#include <iostream>
#include <string>
using namespace std;

class Oblik{
private:

public:
    Oblik(){};
    virtual double povrsina() = 0;
    virtual double obim() = 0;
    virtual void iscrtavanje() = 0;
};

#endif // OBLIK_H_INCLUDED
