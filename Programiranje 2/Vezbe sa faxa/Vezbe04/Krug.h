#ifndef KRUG_H_INCLUDED
#define KRUG_H_INCLUDED

#include <iostream>
#include <string>
#include <math.h>

#include "Oblik.h"
using namespace std;

class Krug: public Oblik{
private:
    double poluprecnik;
public:
    Krug(double poluprecnik): Oblik(), poluprecnik(poluprecnik){};
    virtual double povrsina() {
        return poluprecnik*poluprecnik*atan(1)*4;
    }
    virtual double obim(){
        return 2*poluprecnik*atan(1)*4;
    }
    virtual void iscrtavanje(){
        cout<< "Krug povrsine: " << this->povrsina() << ", obima: " << this->obim() << endl;
    }

    double getPoluprecnik(){
        return poluprecnik;
    }
    void setPoluprecnik(double poluprecnik){
        this->poluprecnik = poluprecnik;
    }


};

#endif // KRUG_H_INCLUDED
