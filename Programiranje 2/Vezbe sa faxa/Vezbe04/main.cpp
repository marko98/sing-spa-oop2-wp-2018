#include <iostream>
#include <string>
#include <vector>

#include "Oblik.h"
#include "Krug.h"
#include "Pravougaonik.h"
#include "Kvadrat.h"
#include "Recept.h"
#include "Sastojak.h"
#include "PodatakOSastojku.h"

using namespace std;

int main()
{
    Oblik *pkrug = new Krug(5);
    Oblik *pkvadrat = new Kvadrat(6);
    Oblik *ppravougaonik = new Pravougaonik(6, 4);


    Sastojak s("jaja", 50, 5);
    Sastojak s2("mleko", 50, 5);

    PodatakOSastojku *pointer = new PodatakOSastojku(s2, 20.5);

    Recept recept1 ("recept1");

    recept1.dodajPodatke(pointer);

    pointer = new PodatakOSastojku(s, 20.5);

    recept1.izbaciPodatke(pointer);

    recept1.predstaviSe();
    recept1.izracunajEnergetskaVrednostJela();
    recept1.izracunajCenuJela();

    /*pkrug->iscrtavanje();
    pkvadrat->iscrtavanje();
    ppravougaonik->iscrtavanje();*/


    return 0;
}
