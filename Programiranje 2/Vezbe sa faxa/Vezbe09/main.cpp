#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Sprite.h"
#include "Coordinate.h"
#include "Vector.h"
#include "Engine.h"
#include "SpriteSheet.h"

using namespace std;

int main(int argc, char ** argv)
{
    /*for(int i = 0; i < 4; i++){
        for(int j = 0; j < 9; j++){
            cout << j*64 << " " << i*64 << " " << 64 << " " << 64 << " ";
        }
        cout << endl;
    }*/

    Engine *pEngine = new Engine("Mario");
    pEngine->init();

    pEngine->run();
    delete pEngine;

    return 0;
}
