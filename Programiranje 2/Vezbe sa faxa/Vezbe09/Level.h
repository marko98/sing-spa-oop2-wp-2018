#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <fstream>

#include "TileSet.h"
#include "Drawable.h"

using namespace std;

class Level: public Drawable{
private:
    int x, y;
    TileSet *tileSet;

    // novo definisani tip
    // obrati paznju na > >, jer ne moze >>
    typedef vector<vector<char> > nivo;
    nivo level;

    friend ostream& operator<<(ostream& out, const Level &level);
public:
    Level(istream &inputStream ,TileSet *tileSet);
    virtual void draw(SDL_Renderer *renderer);
    virtual void setPos(int x, int y);
    virtual void move(int dx, int dy);
};

#endif // LEVEL_H_INCLUDED
