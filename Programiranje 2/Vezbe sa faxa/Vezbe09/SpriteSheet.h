#ifndef SPRITESHEET_H_INCLUDED
#define SPRITESHEET_H_INCLUDED


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <map>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Drawable.h"
#include "Vector.h"
#include "Tile.h"

using namespace std;

typedef Tile Frame;
typedef vector<Frame*> Frames;

class SpriteSheet{
private:
    SDL_Texture *texture;
    map<string, Frames> animations;

public:
    SpriteSheet(istream &input, SDL_Renderer *eRenderer){
        string path;
        input >> path;
        SDL_Surface *surface = IMG_Load(path.c_str());
        texture = SDL_CreateTextureFromSurface(eRenderer, surface);
        SDL_FreeSurface(surface);
        surface = nullptr;

        string animation;
        int frameCount;
        while(!input.eof()){
            input >> animation;
            input >> frameCount;
            animations[animation] = Frames();
            for(int i = 0; i < frameCount; i++){
                animations[animation].push_back(new Frame(input));
            }
        }
    };

    void draw(string imeAnimacije, int frame, SDL_Rect *destRect,SDL_Renderer *eRenderer){
        SDL_RenderCopy(eRenderer, texture, animations[imeAnimacije][frame]->getSrcTileRect(), destRect);
    };
};

#endif // SPRITESHEET_H_INCLUDED
