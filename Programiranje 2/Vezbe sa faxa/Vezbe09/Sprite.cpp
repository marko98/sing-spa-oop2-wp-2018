#include "Sprite.h"

Sprite::Sprite(SpriteSheet *sheet){
    width = 64;
    height = 64;
    currentFrame = 0;
    totalFrames = 0;
    state = 0;

    spriteRect = new SDL_Rect;
    spriteRect->x = 0;
    spriteRect->y = 0;
    spriteRect->h = height;
    spriteRect->w = width;

    this->sheet = sheet;
}

void Sprite::draw(SDL_Renderer *eRenderer){
    //frameRect->x = currentFrame*frameWidth;
    if(state & Sprite::LEVO){
        sheet->draw("left", currentFrame, spriteRect, eRenderer);
    } else if(state & Sprite::DESNO){
        sheet->draw("right", currentFrame, spriteRect, eRenderer);
    } else if(state & Sprite::GORE){
        sheet->draw("up", currentFrame, spriteRect, eRenderer);
    } else if(state & Sprite::DOLE){
        sheet->draw("down", currentFrame, spriteRect, eRenderer);
    } else {
        sheet->draw("down", 0, spriteRect, eRenderer);
    }

    //SDL_RenderCopy(eRenderer, spriteTexture, frameRect, spriteRect);

    totalFrames++;
    if(totalFrames%10 == 0){
        currentFrame++;
        if(currentFrame >= 9){
            currentFrame = 0;
        }
        totalFrames = 0;
    }

}

void Sprite::move(int dx, int dy){
    /*0000
    0001
    0010
    0011
    0100
    0101
    0110
    0111
    1000
    1001
    1010*/

    /*
    0001
    0010
    0100
    1000*/

    if(state != 0){
        if(state & 1){
            spriteRect->x += dx;
        }
        // logicko and
        if(state & 2){
            spriteRect->x -= dx;
        }
        if(state & 4){
            spriteRect->y -= dy;
        }
        // ako je ukljucen bit koji ima stanje broj 8
        if(state & 8){
            spriteRect->y += dy;
        }
    }
}


