#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Drawable.h"
#include "Vector.h"

using namespace std;

class Sprite: public Drawable{
private:
    SDL_Rect *spriteRect;
    SDL_Texture *spriteTexture;
    int frameWidth, frameHeight, frameCount, currentFrame, totalFrames;
    SDL_Rect *frameRect;
public:
    int state;

    Sprite(SDL_Renderer *eRenderer);
    virtual void draw(SDL_Renderer *eRenderer);
    virtual void move(int dx, int dy);
    virtual void setPos(int x, int y){};
};

#endif // SPRITE_H_INCLUDED
