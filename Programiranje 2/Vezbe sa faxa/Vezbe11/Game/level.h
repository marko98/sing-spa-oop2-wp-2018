#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "tileset.h"

using namespace std;

class Level : public Drawable, public Movable{
public:
    typedef vector<string> LevelRow;
    typedef vector<LevelRow > LevelMatrix;

private:
    Tileset *tileset;
    LevelMatrix level;
    int x, y;
public:
    Level(istream &inputStream, Tileset *tileset);
    ~Level();

    const LevelMatrix & getLevelMatrix() const;
    void setX(int x){
        this->x = x;
    };

    virtual void draw(SDL_Renderer *renderer);
    virtual void move(){};
    virtual void move(int dx, int dy){
        x += dx;
        y += dy;
    };

    friend ostream& operator<<(ostream&, const Level&);
};

#endif // LEVEL_H_INCLUDED
