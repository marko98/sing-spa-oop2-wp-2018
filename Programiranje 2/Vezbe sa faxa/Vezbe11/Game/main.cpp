#include <iostream>

#include "engine.h"
#include "level.h"

using namespace std;

// http://www.cplusplus.com/doc/oldtutorial/templates/
// FUNCTION TEMPLATES----
// PARAMETRI ISTOG TIPA
template <class T>
T getMax(T a, T b){
    if(a < b){
        return b;
    };
    return a;
};
// PARAMETRI RAZLICITOG TIPA
template <class T, class U>
T getMin(T a, U b){
    if(a < b){
        return a;
    };
    return b;
};
//-------------------------

// CLASS TEMPLATES----------
template <class T>
class mypair1 {
    T values [2];
  public:
    mypair1 (T first, T second)
    {
      values[0]=first; values[1]=second;
    }

    T getMinimum();
};


template <class T>
class mypair2 {
    T a, b;
  public:
    mypair2 (T first, T second): a(first), b(second){};
    T getMinimum ();
};

template <class T>
T mypair2<T>::getMinimum(){
    if(a < b){
        return a;
    };
    return b;
};
//--------------------------

int main(int argc, char** argv)
{

    int i=5, j=6, k;
    long l=10, m=5, n;
    k=getMax<int>(i,j);
    n=getMax<long>(l,m);
    cout << k << endl;
    cout << n << endl;

    /*int i,j;
    long l;*/
    //i = GetMin<int,long> (j,l); ILI
    i = getMin (j,l);
    cout << i << endl;


    //mypair<double> myobjectDouble (115, 36);
    mypair1<double> *myobjectDouble = new mypair1<double> (3.0, 2.18);
    //mypair<int> myobjectInt (115, 36);
    mypair1<int> *myobjectInt = new mypair1<int> (115, 36);

    mypair2 <int> myobject (100, 75);
    cout << myobject.getMinimum() << endl;


    /*Engine *engine = new Engine("Super Mario");

    engine->addTileset("resources/tilesets/tilesets_level0_template.txt", "level0_template");
    engine->addTileset("resources/tilesets/tilesets_level0.txt", "level0_tilesets");

    ifstream levelTemplateStream("resources/levels/level0_template_tile.txt");
    Level *level = new Level(levelTemplateStream, engine->getTileset("level0_template"));
    level->move(0, 0);
    engine->addDrawable(level);

    ifstream levelStream("resources/levels/level0_tiles.txt");
    level = new Level(levelStream, engine->getTileset("level0_tilesets"));
    level->move(0, 304);
    engine->addDrawable(level);

    engine->run();
    delete engine;*/

    return 0;
}
