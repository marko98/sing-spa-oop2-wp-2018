#include <iostream>
#include <string>
#include <vector>
#include "creature.h"
#include "item.h"
#include "magicCreature.h"
#include "passiveItem.h"
#include "damageAmplifierAmulet.h"
#include "goblin.h"

#include <random>
#include <ctime>


using namespace std;

int main()
{
    //generator
    mt19937 gen(time(0));
    //uniform_real_distribution<float> urd(0, 1);
    uniform_int_distribution<int> urd(0, 5);

    cout << urd(gen) << endl;


    /*Creature c1 (5, 8, "Marko");
    Creature c2 (8, 8, "Mina");
    MagicCreature mc1 (5, 8, "David", 7);

    Item i1;
    Item i2;

    // &pa neki objekat -> dobavlja adresu
    c1.inventory.push_back(&i1);
    c1.inventory.push_back(&i2);

    // size vraca broj el u kolekciji
    cout << c1.inventory.size() << endl;
    cout << c2.inventory.size() << endl;
    cout << "----------------" << endl;

    // capacity vraca maksimalni moguci broj u listi
    c1.inventory.pop_back();
    cout << c1.inventory.capacity() << "|" << c1.inventory.size() << endl;
    cout << c2.inventory.capacity() << endl;
    cout << "----------------" << endl;

    // . pristupamo atributima i metodama obejkta
    // *() dereferenciramo vrednost

    (*c1.inventory[0]).use();
    // MOZEMO deferencirati samo reference a ne objekte
    // skraceni zapis ovoga (*c1.inventory[0]).use();
    c1.inventory[0]->use();


    mc1.introduce();
    c1.attack(mc1);
    mc1.introduce();*/

    cout << "-----------------------" << endl;
    //Item *i = new Item("asd");//ABSTRACT CLASS

    Item *item = new DamageAmplifierAmulet();
    Item *item2 = new PassiveItem();
    Item *item3 = new PassiveItem();

    DamageAmplifierAmulet da = DamageAmplifierAmulet();
    PassiveItem &pitem = da;

    item->use();
    pitem.use();

    Creature *pcreature1 = new Goblin(5, 8, "Dalibor", 7, 8);
    Creature *pcreature2 = new MagicCreature(5, 8, "Mina", 7);

    cout << "-----------------------" << endl;


    pcreature1->inventory.push_back(item);
    pcreature1->inventory.push_back(item2);
    pcreature1->inventory.push_back(item3);
    cout << pcreature1->inventory.capacity() << "|" << pcreature1->inventory.size() << endl;

    for(unsigned int i = 0; i < pcreature1->inventory.size(); i++){
        pcreature1->inventory[i]->use();
    }

    cout << "-----------------------" << endl;

    pcreature1->attack(*pcreature2);

    //item->use(*pcreature);

    return 0;
}
