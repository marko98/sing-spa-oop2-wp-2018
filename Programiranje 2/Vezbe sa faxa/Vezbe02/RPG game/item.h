#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED

#include <iostream>
#include <string>

using namespace std;

class Creature;

class Item {
protected:
    string name;


public:
    string type;
    Item(string name): name(name){};
    virtual void use() = 0; // cista virtuelna metoda (to znaci da ne moramo definisati dalje metodu use da ne bi doslo do greske (valjda))
    virtual void use(Creature &target) = 0;

};

#endif // ITEM_H_INCLUDED
