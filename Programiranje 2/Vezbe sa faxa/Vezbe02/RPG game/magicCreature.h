#ifndef MAGICCREATURE_H_INCLUDED
#define MAGICCREATURE_H_INCLUDED

#include <iostream>
#include <string>
#include "creature.h"

using namespace std;

class MagicCreature: public Creature {
private:
    double mp;

public:
    MagicCreature(double hp, double baseDamage, string name, double mp): Creature(hp, baseDamage, name), mp(mp){}

    void magicAttack(Creature&){}

    virtual void introduce(){
        cout << "My name is: " << name << endl;
        cout << "Hp: " << hp << endl;
        cout << "Base damage: " << baseDamage << endl;
    }

    double getMp (){
        return mp;
    }
    void setMp(double mp) {
        this->mp = mp;
        if(mp < 0){
            mp = 0;
        }
    }

};

#endif // MAGICCREATURE_H_INCLUDED
