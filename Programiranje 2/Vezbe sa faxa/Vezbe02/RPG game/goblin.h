#ifndef GOBLIN_H_INCLUDED
#define GOBLIN_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include "magicCreature.h"

using namespace std;

class Goblin: public MagicCreature{
private:
    int specialPower;

public:
    Goblin(double hp, double baseDamage, string name, double mp, int specialPower):MagicCreature(hp, baseDamage, name, mp), specialPower(specialPower){}

    void specialAttack(MagicCreature&){
        cout << "Special attack used!" << endl;
    };

    int getSpecialPower (){
        return specialPower;
    }
    void setSpecialPower(int specialPower) {
        this->specialPower = specialPower;
        if(specialPower < 0){
            specialPower = 0;
        }
    }

};

#endif // GOBLIN_H_INCLUDED
