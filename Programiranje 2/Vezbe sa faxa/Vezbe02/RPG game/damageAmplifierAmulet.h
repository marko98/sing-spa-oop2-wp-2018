#ifndef DAMAGEAMPLIFIERAMULET_H_INCLUDED
#define DAMAGEAMPLIFIERAMULET_H_INCLUDED


#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "passiveItem.h"

class Creature;

class DamageAmplifierAmulet: public PassiveItem{
public:
    DamageAmplifierAmulet(): PassiveItem(){
        type = "Passive";
    };
    virtual void use(){
        /*Item::use();
        PassiveItem::use();*/
        cout << "Using damage amplifier!" << endl;
    }

    virtual void use(Creature &target);
};

#endif // DAMAGEAMPLIFIERAMULET_H_INCLUDED
