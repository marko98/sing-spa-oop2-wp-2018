#include "damageAmplifierAmulet.h"
#include "creature.h"

void DamageAmplifierAmulet::use(Creature &target){
    cout << "Item " << name << " used on " << target.getName() << endl;
}
