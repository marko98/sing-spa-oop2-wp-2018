#include "magicCreature.h"

Creature::Creature(double hp, double baseDamage, string name){
    this->hp = hp;
    this->baseDamage = baseDamage;
    this->name = name;
}

// & pristupamo referenci
void Creature::attack(Creature& other){
    for(unsigned int i = 0; i < this->inventory.size(); i++){
        this->inventory[i]->use();
    }

    cout << "Napadnut je: " << other.getName() << "\n" << endl;
    other.setHp(other.getHp() - baseDamage);
}
