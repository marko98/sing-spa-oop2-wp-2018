#ifndef PASSIVEITEM_H_INCLUDED
#define PASSIVEITEM_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "item.h"
#include "creature.h"

//class Creature;

class PassiveItem : public Item{
public:
    PassiveItem():Item("PassiveItem"){};

    virtual void use(){
        cout << "Using passive item" << endl;
    };
    virtual void use(Creature &target){
        cout << "Item " << name << " used on " << target.getName() << endl;
    }

};

#endif // PASSIVEITEM_H_INCLUDED
