#include "Sprite.h"

Sprite::Sprite(SDL_Renderer *eRenderer){
    frameWidth = 39;
    frameHeight = 30;
    currentFrame = 0;
    totalFrames = 0;
    state = 0;

    SDL_Surface *surface = IMG_Load("resources/sheets/mario.png");
    frameCount = surface->w/frameWidth;
    spriteTexture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_FreeSurface(surface);
    surface = nullptr;

    frameRect = new SDL_Rect;
    frameRect->y = 156;
    frameRect->h = frameHeight;
    frameRect->w = frameWidth;

    spriteRect = new SDL_Rect;
    spriteRect->x = 0;
    spriteRect->y = 0;
    spriteRect->h = frameHeight;
    spriteRect->w = frameWidth;
}

void Sprite::draw(SDL_Renderer *eRenderer){
    frameRect->x = currentFrame*frameWidth;

    SDL_RenderCopy(eRenderer, spriteTexture, frameRect, spriteRect);
    totalFrames++;
    if(totalFrames%10 == 0){
        currentFrame++;
        if(currentFrame >= frameCount){
            currentFrame = 0;
        }
        totalFrames = 0;
    }

}

void Sprite::move(int dx, int dy){
    /*0000
    0001
    0010
    0011
    0100
    0101
    0110
    0111
    1000
    1001
    1010*/

    /*
    0001
    0010
    0100
    1000*/

    if(state != 0){
        if(state & 1){
            spriteRect->x += dx;
        }
        // logicko and
        if(state & 2){
            spriteRect->x -= dx;
        }
        if(state & 4){
            spriteRect->y -= dy;
        }
        // ako je ukljucen bit koji ima stanje broj 8
        if(state & 8){
            spriteRect->y += dy;
        }
    }
}


