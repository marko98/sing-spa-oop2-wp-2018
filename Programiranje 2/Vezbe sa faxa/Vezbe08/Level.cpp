#include "Level.h"

Level::Level(istream &inputStream ,TileSet *tileSet): tileSet(tileSet){
    x = 0;
    y = 0;
    int rows, columns;
    char code;
    inputStream >> rows >> columns;
    for(int i = 0; i < rows; i++){
        level.push_back(vector<char> ());
        for(int j = 0; j < columns; j++){
            inputStream >> code;
            level[i].push_back(code);
        }
    }
}

void Level::setPos(int x, int y){
    this->x = x;
    this->y = y;
}

void Level::move(int dx, int dy){
    x += dx;
    y += dy;
}

void Level::draw(SDL_Renderer *eRenderer){
    for(size_t i = 0; i < level.size(); i++){
        for(size_t j = 0; j < level[i].size(); j++){
            int w = tileSet->getTiles()[level[i][j]]->getSrcTileRectWidth();
            int h = tileSet->getTiles()[level[i][j]]->getSrcTileRectHeight();
            //cout << tileSet->getTiles()[level[i][j]]->getSrcTileRectWidth() << tileSet->getTiles()[level[i][j]]->getSrcTileRectHeight() << endl;
            if(i != 0){
                if(j != 0){
                    int w = tileSet->getTiles()[level[i][j-1]]->getSrcTileRectWidth();
                    int h = tileSet->getTiles()[level[i-1][j]]->getSrcTileRectHeight();
                    tileSet->draw(level[i][j], Coordinate(x+w, y+h), eRenderer);
                } else {
                    int h = tileSet->getTiles()[level[i-1][0]]->getSrcTileRectHeight();
                    tileSet->draw(level[i][j], Coordinate(x+0, y+h), eRenderer);
                }

            } else {
                if(j != 0){
                    int w = tileSet->getTiles()[level[0][j-1]]->getSrcTileRectWidth();
                    tileSet->draw(level[i][j], Coordinate(x+w, y+0), eRenderer);
                } else {
                    tileSet->draw(level[i][j], Coordinate(x+0, y+0), eRenderer);
                }

            }

        }
    }
}
