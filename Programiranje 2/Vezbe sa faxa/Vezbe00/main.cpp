#include <iostream>

using namespace std;
// :: globalni namespace

// deklaracija funkcije
double povrsina(double, double);
void ispisBinarnogBroja(int);

class Pravougaonik{
private:
    float sirina;
    float visina;

public:
    Pravougaonik(float, float);

    void setSirina(float sirina){
        this->sirina = sirina;
    };

    void setVisina(float v){
        visina = v;
    };

    float getSirina(){
        return sirina;
    }

    float getVisina();

    float povrsina();
};

Pravougaonik::Pravougaonik(float a, float b)
{
    sirina = a;
    this->visina = b;
}

float Pravougaonik::getVisina(){
    return visina;
}

// iz prostora imena Pravougaonik uzima metodu povrsina
float Pravougaonik::povrsina(){
    return ::povrsina(sirina, visina);
}

int main()
{
    // F5 za stavljanje break pointa
    double x = 0;
    cout << x << endl;
    cout << "Hello world!" << endl;
    cout << povrsina(5, 8) << endl;

    Pravougaonik p = Pravougaonik(5, 5);
    cout << p.povrsina() << endl;
    float niz[10] = {1,2,3,4,5,6,7,8,9,10};
    double suma = 0;
    for(unsigned int i = 0; i < 10; i++){
        suma += niz[i];
    }

    ispisBinarnogBroja(5);

    // cout je iz prostora imena std
    cout << suma / 10 << endl;

    return 0;
}

//DEFINICIJA
double povrsina(double a, double b)
{
    return a*b;
}
// https://gitlab.com/ivanradosavljevic/sing-oop2-2018.git

// funkcija ispisuje broj u obrnutom smeru npr 6 je 011 tj 110
void ispisBinarnogBroja(int broj) {
    // ako pise samo bool kraj po difoltu je false
    bool kraj = false;
    while (!kraj) {
        cout << broj%2;
        broj /= 2 ;
        if (broj == 0) {
            kraj = !kraj;
        }
    }
    cout << endl ;
}


