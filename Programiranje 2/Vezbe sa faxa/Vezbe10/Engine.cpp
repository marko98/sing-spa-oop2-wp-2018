#include "Engine.h"

Engine::Engine(){
    eTitle = "";
}

Engine::Engine(const string &gameTitle): eTitle(gameTitle){

}

Engine::~Engine(){
    free();
}

void Engine::init(){
    SDL_Init(SDL_INIT_VIDEO);
    // eWindow = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 432, SDL_WINDOW_RESIZABLE);
    // eWindow = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 432, SDL_WINDOW_RESIZABLE | SDL_WINDOW_BORDERLESS | SDL_WINDOW_MAXIMIZED);
    eWindow = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 432, SDL_WINDOW_RESIZABLE | SDL_WINDOW_BORDERLESS);
    eRenderer = SDL_CreateRenderer(eWindow, -1, SDL_RENDERER_ACCELERATED);
    TTF_Init();
    eFont = TTF_OpenFont("Super-Mario-World.ttf", 20);
}

void Engine::free(){
    SDL_DestroyRenderer(eRenderer);
    SDL_DestroyWindow(eWindow);
    eWindow = nullptr;
    eRenderer = nullptr;
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void Engine::drawText(string text, Coordinate coordinate, TTF_Font* eFont, SDL_Renderer* eRenderer){
    SDL_Color white = {255, 255, 255};

    stringstream ss;
    ss << text ;
    SDL_Surface *surface = nullptr;
    SDL_Texture *texture = nullptr;

    surface = TTF_RenderText_Solid(eFont, ss.str().c_str(), white);
    texture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_Rect message;
    message.x = coordinate.getCoordinateX();
    message.y = coordinate.getCoordinateY();
    message.h = surface->h;
    message.w = surface->w;

    SDL_RenderCopy(eRenderer, texture, NULL, &message);
}

ostream& operator<<(ostream &out, const Level &level){
    return out << "Koordinata levela X: " << level.x << ", koordinata levela Y: " << level.y;
};

ostream& operator<<(ostream &out, const Sprite &sprite){
    return out << "Koordinata sprite-a X: " << sprite.spriteRect->x << ", koordinata sprite-a Y: " << sprite.spriteRect->y << ", frameWidth: " << sprite.width << ", frameHeight: " << sprite.height;
};

bool Engine::run(){
    bool quit = false;
    SDL_Event event;

    // SLIKA LEVELA 0 ----------------------
    ifstream tilesetImageFileLevel0("resources/tilesets/tilesets_level0_image.txt");
    TileSet *tSIFL0 = new TileSet(tilesetImageFileLevel0, eRenderer);

    ifstream level0ImageFile("resources/levels/level0_image.txt");
    Level *l0IF = new Level(level0ImageFile, tSIFL0);

    drawables.push_back(l0IF);
    // --------------------------------

    Level * level = dynamic_cast<Level*>(drawables[0]);
    cout << *(level) << endl;

    /*ifstream tilesetFileLevel0("resources/tilesets/tilesets_level0.txt");
    TileSet *tSFL0 = new TileSet(tilesetFileLevel0, eRenderer);

    ifstream level0File("resources/levels/level0_tiles.txt");
    Level *l0F = new Level(level0File, tSFL0);

    drawables.push_back(l0F);*/

    ifstream spriteSheetStream("resources/sheets/spriteSheet1.txt");
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, eRenderer);

    Sprite *sp = new Sprite(sheet);
    Sprite *sp1 = new Sprite(sheet);
    Sprite *sp2 = new Sprite(sheet);
    //sp1->move(50, 0);

    /*Player *player = new Player(sp);
    drawables.push_back(player);
    eventListeners.push_back(player);*/

    /*Sprite *spCopy = new Sprite(*sp);
    cout << *spCopy << endl;*/

    Grupa *grupa = new Grupa();
    grupa->dodajUGrupu(sp);
    grupa->dodajUGrupu(sp1);
    grupa->dodajUGrupu(sp2);

    drawables.push_back(grupa);
    eventListeners.push_back(grupa);

    grupa->brojUGrupi();

    Mesto *mesto =  new Mesto (sp);

    int startTime, endTime;
    int maxDelay = 16;

    while(!quit){
        startTime = SDL_GetTicks();
        while(SDL_PollEvent(&event)){
            if(event.type == SDL_QUIT){
                quit = true;
            } else if(event.key.keysym.sym == SDLK_ESCAPE){
                quit = true;
            } else {
                for(size_t i = 0; i < eventListeners.size(); i++){
                    eventListeners[i]->listen(event);
                }
            }
        }

        SDL_SetRenderDrawColor(eRenderer, 230, 230, 230, 255);
        SDL_RenderClear(eRenderer);


        for(size_t i = 0; i < drawables.size(); i++){
            drawables[i]->draw(eRenderer);
            //drawables[i]->move(-1, 0);
        }


        grupa->move(1, 1);

        SDL_SetRenderDrawColor(eRenderer, 0, 200, 0, 255);


        SDL_RenderPresent(eRenderer);
        endTime = SDL_GetTicks();
        if(endTime - startTime < maxDelay){
            SDL_Delay(maxDelay - (endTime - startTime));
        }
    }

}
