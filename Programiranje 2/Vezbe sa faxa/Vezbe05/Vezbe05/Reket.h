#ifndef REKET_H_INCLUDED
#define REKET_H_INCLUDED

#include <SDL.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

#include "Line.h"

using namespace std;

class Reket: public Line{
private:
    Line gornjiDeoReketa, donjiDeoReketa;
public:
    Reket(Line gornjiDeoReketa): Line(), gornjiDeoReketa(gornjiDeoReketa){
        this->donjiDeoReketa = Line (Tacka(gornjiDeoReketa.getPocetna().getX(), gornjiDeoReketa.getPocetna().getY()+20), Tacka(gornjiDeoReketa.getKrajnja().getX(), gornjiDeoReketa.getKrajnja().getY()+20));
    };

    virtual void draw(SDL_Renderer *renderer){
        gornjiDeoReketa.draw(renderer);
        donjiDeoReketa.draw(renderer);
    };

    virtual void move(int dX, int dY){
        gornjiDeoReketa.move(dX, dY);
        donjiDeoReketa.move(dX, dY);
    };

    Line getOsnovnaStranica(){ return gornjiDeoReketa; };

};

#endif // REKET_H_INCLUDED
