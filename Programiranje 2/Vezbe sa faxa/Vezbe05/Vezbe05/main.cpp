#include <SDL.h>
#include <SDL_ttf.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

#include "Shape.h"
#include "Tacka.h"
#include "Line.h"
#include "Circle.h"
#include "Reket.h"
#include "Score.h"
#include "Trougao.h"
#include "Pravougaonik.h"

using namespace std;

int main(int argc, char ** argv)
{
    bool quit = false;
    SDL_Event event;

    vector<Shape *> lista;
    lista.push_back(new Line(Tacka(20, 200), Tacka(620, 200)));
    //lista.push_back(new Pravougaonik(Tacka(320-25, 240-20), 50, 40));
    lista.push_back(new Line(Tacka(20, 280), Tacka(620, 280)));

    //Reket *reket = new Reket(Line(Tacka(20, 230), Tacka(60, 230)));
    Trougao *trougao = new Trougao(Line(Tacka(20, 240), Tacka(60, 240)), 5);
    Pravougaonik *pravougaonik = new Pravougaonik(Tacka(320-25, 240-20), 50, 40);


    // init SDL ======================================

    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow("Programiranje 2 Singidunum",
                                            SDL_WINDOWPOS_CENTERED,
                                            SDL_WINDOWPOS_CENTERED,
                                            640, 480, 0);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    int ret = TTF_Init();
    TTF_Font *font = TTF_OpenFont("lazy.ttf", 24);

    // ================================================
    int px = 1;
    int py = 0;

    int dx = 1;
    double dy = 0;
    int nastaviUPravcu = 0;

    int score = 0;
    int brojPokusaja = 0;

    // main loop ======================================

    while(!quit){
        SDL_Delay(2);
        SDL_PollEvent(&event);
        // event handling ------------------

        switch(event.type){
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_LEFT:
                        dx = -1;
                        break;
                    case SDLK_RIGHT:
                        dx = 1;
                        break;
                    case SDLK_UP:
                        dy = -1;
                        break;
                    case SDLK_DOWN:
                        dy = 1;
                        break;
                    case SDLK_r:
                        brojPokusaja = 0;
                        score = 0;
                        break;
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    case SDLK_SPACE:
                        if (dx != 0){
                            nastaviUPravcu = dx;
                            dx = 0;
                            brojPokusaja ++;
                            if(trougao->getOsnovnaStranica().getPocetna().getX() > pravougaonik->getTacka().getX() &&  trougao->getOsnovnaStranica().getKrajnja().getX() < pravougaonik->getTacka().getX() + pravougaonik->getPravougaonik().w){
                                score ++;
                            }
                        } else {
                            dx = nastaviUPravcu;
                        }
                        break;
                    default:
                        break;
                }
                break;
        }
        trougao->move(dx, dy);
        pravougaonik->move(px, py);

        if(pravougaonik->getTacka().getX() < 0){
            px = 1;
        }

        if(pravougaonik->getTacka().getX() + pravougaonik->getPravougaonik().w > 640){
            px = -1;
        }

        if(trougao->getOsnovnaStranica().getPocetna().getX() < 0){
            dx = 1;
        }

        if(trougao->getOsnovnaStranica().getKrajnja().getX() > 640){
            dx = -1;
        }

        if(trougao->getOsnovnaStranica().getPocetna().getY() < 206){
            dy = 1;
        }

        if(trougao->getOsnovnaStranica().getPocetna().getY() > 279){
            dy = -1;
        }

        // clear window --------------------
        SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
        SDL_RenderClear(renderer);

        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
        for(int i = 0; i < lista.size(); i++){
            lista[i]->draw(renderer);
        }

        SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
        trougao->draw(renderer);
        pravougaonik->draw(renderer);

        Score().drawScore(score, brojPokusaja, font, renderer);
        // drawScore(score, total, font, renderer);

        // render window -------------------
        SDL_RenderPresent(renderer);
    }

    // cleanup SDL ======================================

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();

    return 0;
}
