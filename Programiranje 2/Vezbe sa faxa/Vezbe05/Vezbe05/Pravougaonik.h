#ifndef PRAVOUGAONIK_H_INCLUDED
#define PRAVOUGAONIK_H_INCLUDED

#include <SDL.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

#include "Line.h"
#include "Tacka.h"
#include "Shape.h"

using namespace std;

class Pravougaonik: public Shape{
private:
    Tacka pocetnaTacka;
    int w, h;
    SDL_Rect pravougaonik;
public:
    Pravougaonik(Tacka pocetnaTacka, int w, int h): pocetnaTacka(pocetnaTacka), w(w), h(h){};

    virtual void draw(SDL_Renderer *renderer){
        this->pravougaonik.x = pocetnaTacka.getX();
        this->pravougaonik.y = pocetnaTacka.getY();
        this->pravougaonik.h = h;
        this->pravougaonik.w = w;
        SDL_RenderDrawRect(renderer, &pravougaonik);
    };

    virtual void move(int dX, int dY){
        pocetnaTacka.setX(pocetnaTacka.getX() + dX);
        pocetnaTacka.setY(pocetnaTacka.getY() + dY);
    };

    Tacka getTacka(){ return pocetnaTacka; };

    SDL_Rect getPravougaonik() { return pravougaonik; };
};

#endif // PRAVOUGAONIK_H_INCLUDED
