#ifndef CREATURE_H_INCLUDED
#define CREATURE_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include "item.h"

using namespace std;

class Creature {
private:
    double hp;
    double baseDamage;
    string name;
    // pointer
public:
    // da ne bi radili nad kopijama instanacama objekta, vec referencama
    // ocekuje se pokazivac, koristi se *
    vector<Item*> inventory;

public:
    Creature(double, double, string);

    // & pristupamo referenci
    void attack(Creature&);

    bool isAlive(){
        if(hp <= 0){
            return false;
        }
        return true;
    }

    void introduce(){
        cout << "My name is: " << name << endl;
        cout << "Hp: " << hp << endl;
        cout << "Base damage: " << baseDamage << endl;
    };

    double getHp (){
        return hp;
    }
    void setHp(double h) {
        this->hp = h;
        if(hp < 0){
            hp = 0;
        }
    }

    double getBaseDamage (){
        return baseDamage;
    }
    void setBaseDamage(double baseDamage) {
        this->baseDamage = baseDamage;
        if(baseDamage < 0){
            baseDamage = 0;
        }
    }

    string getName (){
        return name;
    }
    void setName(string name) {
        this->name = name;
    }
};

#endif // CREATURE_H_INCLUDED
