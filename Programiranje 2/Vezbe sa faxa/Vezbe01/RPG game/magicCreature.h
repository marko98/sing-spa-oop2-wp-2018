#ifndef MAGICCREATURE_H_INCLUDED
#define MAGICCREATURE_H_INCLUDED

#include <iostream>
#include <string>
#include "creature.h"

using namespace std;

class MagicCreature: public Creature {
private:
    double mp;

public:
    MagicCreature(double hp, double baseDamage, string name, double mp): Creature(hp, baseDamage, name), mp(mp){}

    void magicAttack(Creature){

    }

    double getMp (){
        return mp;
    }
    void setMp(double mp) {
        this->mp = mp;
        if(mp < 0){
            mp = 0;
        }
    }

};

#endif // MAGICCREATURE_H_INCLUDED
