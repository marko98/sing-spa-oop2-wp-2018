#include <iostream>
#include <string>
#include <vector>
#include "creature.h"
#include "item.h"
#include "magicCreature.h"

using namespace std;

int main()
{
    Creature c1 (5, 8, "Marko");
    Creature c2 (8, 8, "Mina");
    MagicCreature mc1 (5, 8, "David", 7);

    Item i1;
    Item i2;

    // &pa neki objekat -> dobavlja adresu
    c1.inventory.push_back(&i1);
    c1.inventory.push_back(&i2);

    // size vraca broj el u kolekciji
    cout << c1.inventory.size() << endl;
    cout << c2.inventory.size() << endl;
    cout << "----------------" << endl;

    // capacity vraca maksimalni moguci broj u listi
    c1.inventory.pop_back();
    cout << c1.inventory.capacity() << "|" << c1.inventory.size() << endl;
    cout << c2.inventory.capacity() << endl;
    cout << "----------------" << endl;

    // . pristupamo atributima i metodama obejkta
    // *() dereferenciramo vrednost

    (*c1.inventory[0]).use();
    // MOZEMO deferencirati samo reference a ne objekte
    // skraceni zapis ovoga (*c1.inventory[0]).use();
    c1.inventory[0]->use();


    mc1.introduce();
    c1.attack(mc1);
    mc1.introduce();

    return 0;
}
