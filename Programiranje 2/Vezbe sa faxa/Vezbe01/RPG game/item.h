#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED

#include <iostream>
#include <string>

using namespace std;

class Item {
public:
    void use(){
        cout << "Item used." << endl;
    };

};

#endif // ITEM_H_INCLUDED
