#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <fstream>

#include "TileSet.h"
#include "Drawable.h"

using namespace std;

class Level: public Drawable{
private:
    TileSet *tileSet;
    // obrati paznju na > >, jer ne moze >>
    vector<vector<char> > level;
public:
    Level(istream &inputStream ,TileSet *tileSet);
    virtual void draw(SDL_Renderer *renderer);
};

#endif // LEVEL_H_INCLUDED
