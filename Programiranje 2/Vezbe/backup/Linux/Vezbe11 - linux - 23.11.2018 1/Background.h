#ifndef BACKGROUND_H_INCLUDED
#define BACKGROUND_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Vector.h"
#include "Coordinate.h"

using namespace std;

class Background{
private:
    Coordinate startCoordinate, endCoordinate;
protected:
    const Vector gravity = Vector(0, -2);
public:
    Background(){
        startCoordinate = Coordinate(0, 400);
        endCoordinate = Coordinate(640, 400);
    };

    Vector getGravity(){ return gravity; };
    Coordinate getStartCoordinate(){ return startCoordinate; };
    Coordinate getEndCoordinate(){ return endCoordinate; };

    void render(SDL_Renderer *eRenderer){
        SDL_RenderDrawLine(eRenderer, startCoordinate.getCoordinateX(), startCoordinate.getCoordinateY(), endCoordinate.getCoordinateX(), endCoordinate.getCoordinateY());
    }
};

#endif // BACKGROUND_H_INCLUDED
