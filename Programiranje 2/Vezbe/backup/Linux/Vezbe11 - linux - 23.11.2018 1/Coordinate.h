#ifndef COORDINATE_H_INCLUDED
#define COORDINATE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Vector.h"

using namespace std;

class Coordinate{
private:
    int coordinateX, coordinateY;
public:
    Coordinate();
    Coordinate(int x, int y);

    void addVector(Vector v);

    int getCoordinateX();
    void setCoordinateX(int x);

    int getCoordinateY();
    void setCoordinateY(int y);
};

#endif // COORDINATE_H_INCLUDED
