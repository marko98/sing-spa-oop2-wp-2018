#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Background.h"
#include "Vector.h"

using namespace std;

class Sprite: public Background{
private:
    SDL_Texture *spriteTexture;
    int spriteHeight, spriteWidth;
protected:
    Coordinate position;
    Vector velocity;
    int height, weight;
    int jump, frameJump;
public:
    Sprite();
    Sprite(Coordinate position, Vector velocity);
    ~Sprite();
    void free();

    bool loadFromFile(string path, SDL_Renderer *renderer);
    void render(Coordinate coordinate, SDL_Renderer *renderer, SDL_Rect *clip = nullptr);

    Coordinate getPosition();
    void setPosition(Coordinate coordinate);
    Vector getVelocity();
    void setVelocity(Vector newVelocity);
    int getHeight();
    void setHeight(int newHeight);
    int getWeight();
    void setWeight(int newWeight);

    virtual void update(Background *background) = 0;
    virtual void handleEvent(SDL_Event *pEvent) = 0;

    virtual SDL_Rect *getSpriteClip(int iClip) = 0;
    virtual void loadSpriteWalking() = 0;
    virtual int getWalkingAnimationFrames() = 0;
    virtual string getPath() = 0;
};

#endif // SPRITE_H_INCLUDED
