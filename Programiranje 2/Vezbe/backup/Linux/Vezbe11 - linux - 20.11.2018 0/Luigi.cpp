#include "Luigi.h"

const int walkingAnimationFramesLuigi = 6;
SDL_Rect spriteLuigiClips[2*walkingAnimationFramesLuigi];

Luigi::Luigi(): Sprite(){
    path = "resources/sheets/luigi.png";
}

Luigi::Luigi(Coordinate position): Sprite(position){
    path = "resources/sheets/luigi.png";
}

string Luigi::getPath(){
    return path;
}

void Luigi::loadSpriteWalking(){
    for(int i = 0; i < 6; i++){
        if(i == 3){
            spriteLuigiClips[i].x = 320;
        } else if(i == 4){
            spriteLuigiClips[i].x = 347;

        } else{
            spriteLuigiClips[i].x = 234 + i*30;
        }
        spriteLuigiClips[i].y = 116;
        spriteLuigiClips[i].w = 18;
        spriteLuigiClips[i].h = 30;
    }
    for(int i = 0; i < 6; i++){
        if(i == 2){
            spriteLuigiClips[6+i].x = 110;
        } else if(i >= 3){
            spriteLuigiClips[6+i].x = 138 + (i - 3)*30;
        } else {
            spriteLuigiClips[6+i].x = 53 + i*30;
        }
        spriteLuigiClips[6+i].y = 116;
        spriteLuigiClips[6+i].w = 18;
        spriteLuigiClips[6+i].h = 30;
    }
}

SDL_Rect * Luigi::getSpriteClip(int iClip){
 return &spriteLuigiClips[iClip];
}

int Luigi::getWalkingAnimationFrames(){
    return walkingAnimationFramesLuigi;
}
