#include "Vector.h"


Vector::Vector(){
    vectorX = 0;
    vectorY = 0;
}

Vector::Vector(int x, int y): vectorX(x), vectorY(y){

}

int Vector::getVectorX(){
    return vectorX;
}

void Vector::setVectorX(int x){
    if(x < 0){
        x = 0;
    }
    vectorX = x;
}

int Vector::getVectorY(){
    return vectorY;
}

void Vector::setVectorY(int y){
    if(y < 0){
        y = 0;
    }
    vectorY = y;
}
