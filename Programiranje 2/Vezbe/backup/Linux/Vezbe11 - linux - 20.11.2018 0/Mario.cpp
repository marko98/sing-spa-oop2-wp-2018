#include "Mario.h"

const int walkingAnimationFramesMario = 5;
SDL_Rect spriteMarioClips[2*walkingAnimationFramesMario];

Mario::Mario(): Sprite(){
    path = "resources/sheets/mario.png";
}

Mario::Mario(Coordinate position): Sprite(position){
    path = "resources/sheets/mario.png";
}

string Mario::getPath(){
    return path;
}

void Mario::loadSpriteWalking(){
    for(int i = 0; i < 5; i++){
        spriteMarioClips[i].x = 7 + (5 + i)*40;
        spriteMarioClips[i].y = 156;
        spriteMarioClips[i].w = 18;
        spriteMarioClips[i].h = 29;
    }
    for(int i = 0; i < 5; i++){
        spriteMarioClips[5+i].x = 7 + i*40;
        spriteMarioClips[5+i].y = 156;
        spriteMarioClips[5+i].w = 18;
        spriteMarioClips[5+i].h = 29;
    }
}

SDL_Rect * Mario::getSpriteClip(int iClip){
 return &spriteMarioClips[iClip];
}

int Mario::getWalkingAnimationFrames(){
    return walkingAnimationFramesMario;
}
