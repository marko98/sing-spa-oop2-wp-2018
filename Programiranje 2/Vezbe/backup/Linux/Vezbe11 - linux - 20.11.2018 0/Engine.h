#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Vector.h"
#include "Sprite.h"
#include "Mario.h"
#include "Luigi.h"

using namespace std;

class Engine{
private:
    string eTitle;
    SDL_Window* eWindow;
    SDL_Renderer* eRenderer;
    TTF_Font* eFont;
public:
    Engine();
    Engine(const string &gameTitle);
    ~Engine();
    void free();

    void init();
    //void loadMedia();
    void drawText(string text, Coordinate coordinate, TTF_Font* eFont, SDL_Renderer* eRenderer);
    bool run();
};

#endif // ENGINE_H_INCLUDED
