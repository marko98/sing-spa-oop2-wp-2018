#ifndef MARIO_H_INCLUDED
#define MARIO_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Sprite.h"
#include "Coordinate.h"
#include "Vector.h"

using namespace std;

class Mario: public Sprite{
private:
    string path;
    Vector velocity;
    Vector gravity;
public:
    Mario();
    Mario(Coordinate position);
    string getPath();

    virtual void loadSpriteWalking();
    virtual SDL_Rect *getSpriteClip(int iClip);
    virtual int getWalkingAnimationFrames();
};

#endif // MARIO_H_INCLUDED
