#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "keyboardeventlistener.h"
#include "level.h"

class Sprite;

using namespace std;

class Player: public Drawable, public Movable, public KeyboardEventListener{
private:
    Sprite *sprite;
    bool dvostrukiSkok = false;
    int brojacSkoka = 0;
    int brojProzora = 1;
    Level *backgroundImage;
    Level *backgroundTiles;
public:
    Player(Sprite *sprite);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move(int dx, int dy);
    virtual void move();
    virtual void listen(SDL_KeyboardEvent &event);

    Sprite * getSprite(){ return sprite; };
    int getBrojProzora(){ return brojProzora; };
    void setBrojProzora(int brojProzora){ this->brojProzora = brojProzora; };

    void setBackgroundImage(Level *backgroundImage);
    void setBackgroundTiles(Level *backgroundTiles);
};

#endif // PLAYER_H_INCLUDED
