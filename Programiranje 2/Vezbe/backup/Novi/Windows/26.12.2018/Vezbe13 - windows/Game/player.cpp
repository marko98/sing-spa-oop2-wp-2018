#include "player.h"
#include "sprite.h"

Player::Player(Sprite *sprite): Drawable(), Movable(), KeyboardEventListener(){
    this->sprite = sprite;
}

void Player::setBackgroundImage(Level *backgroundImage){
    this->backgroundImage = backgroundImage;
}

void Player::setBackgroundTiles(Level *backgroundTiles){
    this->backgroundTiles = backgroundTiles;
}

void Player::draw(SDL_Renderer *renderer){
    sprite->draw(renderer);
}

void Player::move(){
    if(sprite->getState() != Sprite::STOP){

        if(sprite->getState()&Sprite::LEFT && sprite->getSpriteRect()->x > 0){
            move(-2, 0);
        }
        if(sprite->getState()&Sprite::RIGHT && sprite->getSpriteRect()->x + sprite->getSpriteRect()->w < 1280){
            move(2, 0);
        }

        if(sprite->getState()&Sprite::UP){
            move(0, -8);
        }

    };

    if(brojacSkoka > 0){
        brojacSkoka--;
    } else if(brojacSkoka == 0){
        sprite->setState(sprite->getState()&~Sprite::UP);
    }
}

void Player::move(int dx, int dy){
    sprite->move(dx, dy);
}

void Player::listen(SDL_KeyboardEvent &event){
    if(event.type == SDL_KEYDOWN){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()|Sprite::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()|Sprite::RIGHT);
        } /*else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()|Sprite::DOWN);
        } */else if(event.keysym.sym == SDLK_SPACE){
            if(sprite->getOnTheGround()){
                brojacSkoka += 8;
                dvostrukiSkok = false;
                sprite->setState(sprite->getState()|Sprite::UP);
            } else if(!sprite->getOnTheGround() && !dvostrukiSkok){
                brojacSkoka += 8;
                dvostrukiSkok = true;
                sprite->setState(sprite->getState()|Sprite::UP);
            }
        }

    } else if(event.type == SDL_KEYUP){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()&~Sprite::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()&~Sprite::RIGHT);
        } /*else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()&~Sprite::DOWN);
        } else if(event.keysym.sym == SDLK_SPACE){
            sprite->setState(sprite->getState()&~Sprite::UP);
        }*/
    }
}
