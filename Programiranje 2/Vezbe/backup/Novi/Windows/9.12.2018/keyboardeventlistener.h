#ifndef KEYBOARDEVENTLISTENER_H_INCLUDED
#define KEYBOARDEVENTLISTENER_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "eventlistener.h"

using namespace std;

class KeyboardEventListener: public EventListener{
    virtual void listen(SDL_Event &event);
    virtual void listen(SDL_KeyboardEvent &event) = 0;
};

#endif // KEYBOARDEVENTLISTENER_H_INCLUDED
