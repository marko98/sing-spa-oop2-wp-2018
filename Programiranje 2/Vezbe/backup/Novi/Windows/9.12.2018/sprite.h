#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "spritesheet.h"
#include "vector.h"
#include "level.h"

using namespace std;

class Sprite: public Drawable, public Movable{
public:
    enum State: short int{STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8,
                        LEFT_UP=LEFT|UP, LEFT_DOWN=LEFT|DOWN,
                        RIGHT_UP=RIGHT|UP, RIGHT_DOWN=RIGHT|DOWN};
private:
    short int state;
    SpriteSheet *sheet;
    SDL_Rect *spriteRect;
    int currentFrame, frameCounter, frameSkip;
public:
    Sprite(SpriteSheet *sheet, int width=20, int height=30);
    int getFrameSkip();
    void setFrameSkip(int frameSkip);
    short int getState();
    void setState(short int state);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    SDL_Rect * getSpriteRect(){ return spriteRect; };
    friend ostream& operator<<(ostream&, const Sprite&);
};

struct Mesto{
    Sprite *sprite;
    Mesto(Sprite *sprite): sprite(sprite){};

    // npr void operator() operator+(){} za redefinisanje operatora +
    void operator() (){
        cout << sprite->getSpriteRect()->x << ", " << sprite->getSpriteRect()->y << endl;
    }
};

struct Gravity{
    Sprite *sprite;
    Level *level;
    //Vector *gravity = new Vector(0, -2);
    Gravity(Level *level, Sprite *sprite): level(level), sprite(sprite){};

    void gravity(){
        //cout << sprite->getSpriteRect()->x << endl;
        //cout << sprite->getSpriteRect()->w/2 << endl;
        int xCoordinate = sprite->getSpriteRect()->x + sprite->getSpriteRect()->w/2;
        int yCoordinate = sprite->getSpriteRect()->y + sprite->getSpriteRect()->h;
        //int yCoordinate = sprite->getSpriteRect()->y;

        int column = xCoordinate/16;
        //cout << xCoordinate << endl;
        if(xCoordinate%16 == 0){
            column -= 1;
        }
        if(sprite->getSpriteRect()->x == 0){
            column = 0;
        }
        //column += 1;
        //cout << column << endl;

        //cout << yCoordinate << endl;
        int row = yCoordinate/16;
        if(yCoordinate%16 == 0){
            row -= 1;
        }
        if(sprite->getSpriteRect()->y == 0){
            row = 0;
        }


        //cout << sprite->getSpriteRect()->h << endl;
        //cout << yCoordinate << endl;
        //cout << row << endl;

        int plocica = 0;
        int i = 0;
        if(row > 27-level->getLevelMatrix().size()-1){
            //cout << column << endl;
            //cout << row << endl;
            row = row - (27-level->getLevelMatrix().size());
            /*cout << "usao" << endl;
            cout << row << endl;
            cout << "izasao" << endl;*/
            i = row;
            bool broji = false;

            for(i; i < level->getLevelMatrix().size(); i++){

                //cout << level->getLevelMatrix()[i][column] << endl;
                if(broji){
                    if(level->getLevelMatrix()[i][column] != "0"){
                        //cout << level->getLevelMatrix()[i][column] << endl;
                        plocica++;
                    }
                }

                if(i < level->getLevelMatrix().size()-1){
                    if(level->getLevelMatrix()[i+1][column] == "t" || level->getLevelMatrix()[i+1][column] == "t7" || level->getLevelMatrix()[i+1][column] == "t9"){
                        broji = true;
                        //cout << "trava" << endl;
                    }
                }

            }

        } else {
            for(i; i < level->getLevelMatrix().size(); i++){
                if(level->getLevelMatrix()[i][column] != "0"){
                    //cout << level->getLevelMatrix()[i][column] << endl;
                    plocica++;
                }
            }
        }


        if(yCoordinate < 432-plocica*16){
            sprite->getSpriteRect()->y += 2;
        }

        // OGRANICENJE SA LEVE STRANE
        if(xCoordinate <= 10){
            sprite->getSpriteRect()->x = 0;
        }
    };
};

#endif // SPRITE_H_INCLUDED
