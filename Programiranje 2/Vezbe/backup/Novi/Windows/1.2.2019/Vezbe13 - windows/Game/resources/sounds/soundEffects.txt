coin	resources/sounds/smw_coin.wav
game_over	resources/sounds/smw_game_over.wav
bonus_game_end	resources/sounds/smw_bonus_game_end.wav
castle_clear	resources/sounds/smw_castle_clear.wav
jump	resources/sounds/smw_jump.wav
kick	resources/sounds/smw_kick.wav
lost_a_life	resources/sounds/smw_lost_a_life.wav
1-up	resources/sounds/smw_1-up.wav