#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "spritesheet.h"
#include "vector.h"
#include "level.h"
#include "player.h"

using namespace std;

class Sprite: public Drawable, public Movable{
public:
    enum State: short int{STOP=0, LEFT=1, RIGHT=2, UP=4, CROUCH=8, VIEW=16,
                        LEFT_UP=LEFT|VIEW, LEFT_DOWN=LEFT|CROUCH,
                        RIGHT_UP=RIGHT|VIEW, RIGHT_DOWN=RIGHT|CROUCH};
private:
    short int state;
    short int lastState;
    SpriteSheet *sheet;
    SDL_Rect *spriteRect;
    int currentFrame, frameCounter, frameSkip;
    bool onTheGround = false;
public:
    Sprite(SpriteSheet *sheet, int x=0, int width=20, int height=30);
    int getFrameSkip();
    void setFrameSkip(int frameSkip);
    short int getState();
    void setState(short int state);
    short int getLastState();
    void setLastState(short int lastState);
    void setOnTheGround(bool onTheGround){ this->onTheGround = onTheGround; };
    bool getOnTheGround(){ return onTheGround; };

    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    SDL_Rect * getSpriteRect(){ return spriteRect; };
    friend ostream& operator<<(ostream&, const Sprite&);
};

struct Mesto{
    Sprite *sprite;
    Mesto(Sprite *sprite): sprite(sprite){};

    // npr void operator() operator+(){} za redefinisanje operatora +
    void operator() (){
        cout << sprite->getSpriteRect()->x << ", " << sprite->getSpriteRect()->y << endl;
    }
};

struct Gravity{
    Sprite *sprite = nullptr;
    Level *backgroundImage = nullptr;
    Level *backgroundTiles = nullptr;
    Level *generalTiles = nullptr;
    Player *player = nullptr;
    // pomeraj za 1 prozor je 80
    int pomeraj = 0;

    Gravity(Level *backgroundImage, Level *backgroundTiles, Level *generalTiles, Sprite *sprite): backgroundImage(backgroundImage), backgroundTiles(backgroundTiles), generalTiles(generalTiles), sprite(sprite){};
    Gravity(Level *backgroundImage, Level *backgroundTiles, Level *generalTiles, Player *player): backgroundImage(backgroundImage), backgroundTiles(backgroundTiles), generalTiles(generalTiles), sprite(player->getSprite()), player(player){};

    void gravity(){

        int xCoordinate = sprite->getSpriteRect()->x + sprite->getSpriteRect()->w/2;
        int yCoordinate = sprite->getSpriteRect()->y + sprite->getSpriteRect()->h;

        // COLUMNS-----------------------------
        int column = (xCoordinate - generalTiles->getX())/16;
        if(xCoordinate%16 == 0){
            column -= 1;
        }
        if(sprite->getSpriteRect()->x == 0){
            column = 0;
        }
        //-----------------------------------

        // ROWS--------------------------------
        int row = yCoordinate/16;
        if(yCoordinate%16 == 0){
            row -= 1;
        }
        if(sprite->getSpriteRect()->y == 0){
            row = 0;
        }
        //-----------------------------------

        // RACUNANJE PLOCICA za backgroundTiles--------------------------------------------------------------------
        int plocica = 0;
        int j = 0;
        int i = 0;
        int brojTrava = 0;
        int rowGrass = row;

        if(row > 27-backgroundTiles->getLevelMatrix().size()-1){
            i = row - (27-backgroundTiles->getLevelMatrix().size());
            rowGrass = row - (27-backgroundTiles->getLevelMatrix().size());
            //i = row;
            bool brojiBackgroundTiles = false;
            bool brojiGeneralTiles = false;

            // MOZE JEDNA FOR PETLJA JER JE I(row - (27-backgroundTiles->getLevelMatrix().size())) isto sto i j(row - (27-generalTiles->getLevelMatrix().size()))
            // tj backgroundTiles->getLevelMatrix().size() == generalTiles->getLevelMatrix().size()
            for(i; i < backgroundTiles->getLevelMatrix().size(); i++){
                if(brojiBackgroundTiles){
                    if(backgroundTiles->getLevelMatrix()[i][column+pomeraj] != "0"){
                        plocica++;
                        if(backgroundTiles->getLevelMatrix()[i][column+pomeraj] == "t"){
                            brojTrava++;
                        }
                    }
                } else if(brojiGeneralTiles){
                    if(generalTiles->getLevelMatrix()[i][column+pomeraj] != "0"){
                        plocica++;
                    }
                };

                if(i < backgroundTiles->getLevelMatrix().size()-1){
                    if(backgroundTiles->getLevelMatrix()[i+1][column+pomeraj] == "t" ||
                       backgroundTiles->getLevelMatrix()[i+1][column+pomeraj] == "t7" ||
                       backgroundTiles->getLevelMatrix()[i+1][column+pomeraj] == "t9" ||
                       backgroundTiles->getLevelMatrix()[i+1][column+pomeraj] == "t478" ||
                       backgroundTiles->getLevelMatrix()[i+1][column+pomeraj] == "t896"){
                        brojiBackgroundTiles = true;
                    } else if(generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "yellow_tube_478" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "yellow_tube_896" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "purple_tube_478" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "purple_tube_896" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "green_tube_478" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "green_tube_896" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "gray_tube_478" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "gray_tube_896" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "question5" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "question_hit" ||
                               generalTiles->getLevelMatrix()[i+1][column+pomeraj] == "smiley"){
                        brojiGeneralTiles = true;
                    };
                }

            }

        } else {
            for(i; i < backgroundTiles->getLevelMatrix().size(); i++){
                if(backgroundTiles->getLevelMatrix()[i][column+pomeraj] != "0"){
                    plocica++;
                } else if(generalTiles->getLevelMatrix()[i][column+pomeraj] != "0"){
                    plocica++;
                };
            }
        }
        // ------------------------------------------------------------------------------------

        // GRASS ----------------------------------------------------------------
        if(brojTrava == 2){
            if(backgroundTiles->getLevelMatrix()[rowGrass][column+pomeraj+1] == "t478" ||
               backgroundTiles->getLevelMatrix()[rowGrass][column+pomeraj+1] == "t4"){
                sprite->move(-1, 0);
                backgroundImage->move(3, 0);
                backgroundTiles->move(3, 0);
                generalTiles->move(3, 0);
            }
        }
        //------------------------------------------------------------------------

        // RACUNANJE PLOCICA za generalTiles--------------------------------------------------------------------
        /*if(row > 27-generalTiles->getLevelMatrix().size()-1){
            j = row - (27-generalTiles->getLevelMatrix().size());
            bool broji = false;

            cout << "j: " << j << endl;
            cout << "generalTiles->getLevelMatrix().size(): " << generalTiles->getLevelMatrix().size() << endl;
            for(j; j < generalTiles->getLevelMatrix().size(); j++){
                if(broji){
                    if(generalTiles->getLevelMatrix()[j][column+pomeraj] != "0"){
                        plocica++;
                    }
                }

                if(j < generalTiles->getLevelMatrix().size()-1){
                    if(generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "yellow_tube_478" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "yellow_tube_896" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "purple_tube_478" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "purple_tube_896" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "green_tube_478" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "green_tube_896" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "gray_tube_478" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "gray_tube_896" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "question5" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "question_hit" ||
                       generalTiles->getLevelMatrix()[j+1][column+pomeraj] == "smiley"){
                        broji = true;
                    }
                }
            }
        } else {
            for(j; j < generalTiles->getLevelMatrix().size(); j++){
                if(generalTiles->getLevelMatrix()[j][column+pomeraj] != "0"){
                    plocica++;
                };
            }
        };*/
        // ------------------------------------------------------------------------------------


        // PROVERAVANJE SKOKA PLAYERA---------------------------------------------------------
        if(player != nullptr){
            if(!player->getSprite()->getOnTheGround()){
                //cout << "nisam na zemlji" << endl;
                //cout << row - (27-generalTiles->getLevelMatrix().size()) << endl;

                int k = row - (27-generalTiles->getLevelMatrix().size());
                if(k > 0 && k < 8){
                    //cout << "row: " << k << ", column: " << column << endl;

                    if(generalTiles->getLevelMatrix()[k][column+pomeraj+1] == "question5" || generalTiles->getLevelMatrix()[k][column+pomeraj+1] == "n0" && generalTiles->getLevelMatrix()[k-1][column+pomeraj+1] == "question5"){
                        sprite->move(-1, 0);
                        backgroundImage->move(3, 0);
                        backgroundTiles->move(3, 0);
                        generalTiles->move(3, 0);
                    } else if (generalTiles->getLevelMatrix()[k][column+pomeraj-1] == "question5" || generalTiles->getLevelMatrix()[k][column+pomeraj-1] == "n0" && generalTiles->getLevelMatrix()[k-1][column+pomeraj-1] == "question5"){
                        sprite->move(1, 0);
                        backgroundImage->move(-3, 0);
                        backgroundTiles->move(-3, 0);
                        generalTiles->move(-3, 0);
                    }

                    //cout << "proveri" << endl;
                    if(generalTiles->getLevelMatrix()[k-1][column+pomeraj] == "question5"){
                        //cout << "bice sudara" << endl;
                        player->getSprite()->setState(sprite->getState()&~Sprite::UP);
                        //cout << "glavom je kao udario" << endl;

                        generalTiles->changeLevel(k-1, column+pomeraj, "question_hit");

                        /*ofstream myfile;
                        myfile.open ("resources/levels/general_tiles.txt");
                        myfile << "9 320" << endl;

                        for(size_t i = 0; i < sprites.size(); i++){
                            myfile << *sprites[i] << endl;

                        }

                        for(size_t i = 0; i < fueledSprites.size(); i++){
                            myfile << *fueledSprites[i] << endl;

                        }
                        myfile.close();*/

                    };
                };
            };
        };
        // ------------------------------------------------------------------------------------

        if(yCoordinate < 432-plocica*16){
            sprite->getSpriteRect()->y += 2;
            sprite->setOnTheGround(false);
        }

        if(!sprite->getOnTheGround()){
            if (yCoordinate == 432-plocica*16) {
                sprite->setOnTheGround(true);
            }
        }

        if(player == nullptr){

            /*cout << backgroundImage->getX() << endl;
            if(sprite->getSpriteRect()->x == backgroundImage->getX()){
                sprite->setState(Sprite::RIGHT);
            }
            if(sprite->getSpriteRect()->x == 5120){
                sprite->setState(Sprite::LEFT);
            }*/

        }

        //PLAYER --------------------------------------------
        /*if(player != nullptr){
            if(player->getSprite()->getSpriteRect()->x == 0){
                if(player->getBrojProzora() > 1){
                    pomeraj -= 80;
                    player->setBrojProzora(player->getBrojProzora() - 1);
                    sprite->getSpriteRect()->x = 1280 - sprite->getSpriteRect()->w;
                    backgroundImage->move(1280, 0);
                    backgroundTiles->move(1280, 0);
                    generalTiles->move(1280, 0);
                };
            } else if(player->getSprite()->getSpriteRect()->x + player->getSprite()->getSpriteRect()->w == 1280){
                if(player->getBrojProzora() < 4){
                    pomeraj += 80;
                    player->setBrojProzora(player->getBrojProzora() + 1);
                    sprite->getSpriteRect()->x = sprite->getSpriteRect()->w/2;
                    backgroundImage->move(-1280, 0);
                    backgroundTiles->move(-1280, 0);
                    generalTiles->move(-1280, 0);
                };
            };
        };*/
        //----------------------------------------------------

    };
};

#endif // SPRITE_H_INCLUDED
