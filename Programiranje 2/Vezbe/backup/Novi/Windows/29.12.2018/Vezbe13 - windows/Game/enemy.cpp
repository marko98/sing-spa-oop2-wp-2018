#include "enemy.h"
#include "sprite.h"

//generator
mt19937 gen(time(0));
//uniform_real_distribution<float> urd(0, 1);
uniform_int_distribution<int> urd(1, 2);

Enemy::Enemy(Sprite *sprite): Drawable(), Movable(), KeyboardEventListener(){
    this->sprite = sprite;
    this->timeInCurrentState = 0;
}

void Enemy::setBackgroundImage(Level *backgroundImage){
    this->backgroundImage = backgroundImage;
}

void Enemy::setBackgroundTiles(Level *backgroundTiles){
    this->backgroundTiles = backgroundTiles;
}

void Enemy::setGeneralTiles(Level *generalTiles){
    this->generalTiles = generalTiles;
}

void Enemy::randomChangeStates(){
    if(sprite->getOnTheGround()){

        sprite->setState(Sprite::RIGHT);

        /*if(timeInCurrentState == 0){
            sprite->setState(urd(gen));
        };

        if(timeInCurrentState <= 150){
            timeInCurrentState++;
        } else {
            timeInCurrentState = 0;
        };

        cout << timeInCurrentState << endl;*/
    }
}

void Enemy::move(int dx, int dy){
    sprite->move(dx, dy);
}

void Enemy::move(){
    if(!backgroundImage->getMoving()){
        cout << "ne krece se" << endl;
        if(sprite->getState() != Sprite::STOP){
            if(sprite->getState()&Sprite::LEFT){
                move(-1, 0);
            };
            if(sprite->getState()&Sprite::RIGHT){
                move(1, 0);
            };
            if(sprite->getState()&Sprite::UP){
                move(0, -8);
            };
        };
    } else {

        if(backgroundImage->getState() == Level::LEFT){
            cout << "kerce se u levo" << endl;
            if(sprite->getState() == Sprite::RIGHT){
                cout << "enemy se kerce u desno" << endl;
                move(0, 0);
            } else if(sprite->getState() == Sprite::LEFT){
                cout << "enemy se kerce u levo" << endl;
                move(-4, 0);
            };

        } else {
            cout << "kerce se u desno" << endl;
            if(sprite->getState() == Sprite::LEFT){
                cout << "enemy se kerce u levo" << endl;
                move(0, 0);
            } else if(sprite->getState() == Sprite::RIGHT){
                cout << "enemy se kerce u desno" << endl;
                move(4, 0);
            }

        }

    }
}

void Enemy::draw(SDL_Renderer *renderer){
    //cout << "Player position: " << backgroundImage->getPlayer()->getSprite()->getSpriteRect()->x << " " << backgroundImage->getPlayer()->getSprite()->getSpriteRect()->y << endl;
    //cout << "Enemy position: " << sprite->getSpriteRect()->x << " " << sprite->getSpriteRect()->y << endl;
    sprite->draw(renderer);
}
