#include "player.h"
#include "sprite.h"

Player::Player(Sprite *sprite): Drawable(), Movable(), KeyboardEventListener(){
    this->sprite = sprite;
}

void Player::setBackgroundImage(Level *backgroundImage){
    this->backgroundImage = backgroundImage;
}

void Player::setBackgroundTiles(Level *backgroundTiles){
    this->backgroundTiles = backgroundTiles;
}

void Player::setGeneralTiles(Level *generalTiles){
    this->generalTiles = generalTiles;
}

void Player::draw(SDL_Renderer *renderer){
    sprite->draw(renderer);
}

void Player::turnOnBackground(){
    backgroundImage->setMoving(true);
    backgroundTiles->setMoving(true);
    generalTiles->setMoving(true);
}

void Player::turnOffBackground(){
    backgroundImage->setMoving(false);
    backgroundTiles->setMoving(false);
    generalTiles->setMoving(false);
}

void Player::moveBackgroundToLeft(){
    backgroundImage->setState(backgroundImage->getState()|Level::LEFT);
    backgroundTiles->setState(backgroundTiles->getState()|Level::LEFT);
    generalTiles->setState(generalTiles->getState()|Level::LEFT);
    turnOnBackground();
}

void Player::moveBackgroundToRight(){
    backgroundImage->setState(backgroundImage->getState()|Level::RIGHT);
    backgroundTiles->setState(backgroundTiles->getState()|Level::RIGHT);
    generalTiles->setState(generalTiles->getState()|Level::RIGHT);
    turnOnBackground();
}

void Player::move(){
    if(sprite->getState() != Sprite::STOP){

        if(sprite->getState() != Sprite::LEFT_DOWN && sprite->getState() != Sprite::LEFT_UP){
            if(sprite->getState()&Sprite::LEFT && sprite->getSpriteRect()->x > 0){
                move(-1, 0);
                moveBackgroundToRight();
            }/* else {
                turnOffBackground();
            };*/
        };

        if(sprite->getState() != Sprite::RIGHT_DOWN && sprite->getState() != Sprite::RIGHT_UP){
            if(sprite->getState()&Sprite::RIGHT && sprite->getSpriteRect()->x + sprite->getSpriteRect()->w < 1280){
                move(1, 0);
                moveBackgroundToLeft();
            }/* else {
                turnOffBackground();
            };*/
        };

        if(sprite->getState()&Sprite::UP){
            move(0, -8);
        };

    };

    if(brojacSkoka > 0){
        brojacSkoka--;
    } else if(brojacSkoka == 0){
        sprite->setState(sprite->getState()&~Sprite::UP);
    };
}

void Player::move(int dx, int dy){
    sprite->move(dx, dy);
}

void Player::listen(SDL_KeyboardEvent &event){
    if(event.type == SDL_KEYDOWN){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()|Sprite::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()|Sprite::RIGHT);
        } else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()|Sprite::CROUCH);
        } else if(event.keysym.sym == SDLK_UP){
            sprite->setState(sprite->getState()|Sprite::VIEW);
        } else if(event.keysym.sym == SDLK_SPACE){
            if(sprite->getOnTheGround()){
                brojacSkoka += 8;
                dvostrukiSkok = false;
                sprite->setState(sprite->getState()|Sprite::UP);
            } else if(!sprite->getOnTheGround() && !dvostrukiSkok){
                brojacSkoka += 8;
                dvostrukiSkok = true;
                sprite->setState(sprite->getState()|Sprite::UP);
            }
        }
        // PAMTIMO POSLEDNJE STANJE
        sprite->setLastState(sprite->getState());
    } else if(event.type == SDL_KEYUP){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()&~Sprite::LEFT);
            backgroundImage->setState(backgroundImage->getState()&~Level::RIGHT);
            backgroundTiles->setState(backgroundTiles->getState()&~Level::RIGHT);
            generalTiles->setState(generalTiles->getState()&~Level::RIGHT);
            turnOffBackground();
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()&~Sprite::RIGHT);
            backgroundImage->setState(backgroundImage->getState()&~Level::LEFT);
            backgroundTiles->setState(backgroundTiles->getState()&~Level::LEFT);
            generalTiles->setState(generalTiles->getState()&~Level::LEFT);
            turnOffBackground();
        } else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()&~Sprite::CROUCH);
        } else if(event.keysym.sym == SDLK_UP){
            sprite->setState(sprite->getState()&~Sprite::VIEW);
        } else if(event.keysym.sym == SDLK_SPACE){
            sprite->setState(sprite->getState()&~Sprite::UP);
        }
    }
}
