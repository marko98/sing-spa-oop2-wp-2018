#include "Engine.h"

/*& operator<<(ostream& out, const Level& l) {
    int rows = l.getLevelMatrix().size();
    int cols = 0;
    if(rows > 0) {
        cols = l.getLevelMatrix()[0].size();
    }
    out << rows << " " << cols << endl;

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++) {
            out << l.getLevelMatrix()[i][j] << " ";
        }
        out << endl;
    }

    return out;
}*/

ostream& operator<<(ostream& out, const Sprite& s) {
    out << s.spriteRect->x << " " << s.spriteRect->y << " " << s.spriteRect->w << " " << s.spriteRect->h << " " << s.onTheGround << endl;

    return out;
}

Engine::Engine(const string &gameTitle){
    SDL_Init(SDL_INIT_VIDEO);
    // window = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 432, SDL_WINDOW_RESIZABLE);
    window = SDL_CreateWindow(gameTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 432, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    TTF_Init();
    font = TTF_OpenFont("Super-Mario-World.ttf", 20);
}

Engine::~Engine(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = nullptr;
    renderer = nullptr;
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void Engine::addTileset(const string &path, const string &name){
    ifstream putanja(path);
    tilesets[name] = new Tileset(putanja, renderer);
}

Tileset* Engine::getTileset(const string &name) {
    return tilesets[name];
}

void Engine::addLevel(const string &path, const string &name, int x, int y, int tileWidth, int tileHeight){
    ifstream levelTemplateStream(path);
    Level *current_level = new Level(levelTemplateStream, tilesets[name], tileWidth, tileHeight);
    current_level->move(x, y);
    addDrawable(current_level);
    addMovable(current_level);
}

void Engine::addPlayer(const string &spriteSheatPath, int frameSkip){
    ifstream spriteSheetStream(spriteSheatPath);
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, renderer);

    Sprite *sprite = new Sprite(sheet);
    sprite->setFrameSkip(frameSkip);

    Player *player = new Player(sprite);
    Gravity *gravity =  new Gravity (dynamic_cast<Level*>(drawables[0]), dynamic_cast<Level*>(drawables[1]), dynamic_cast<Level*>(drawables[2]), player);

    addDrawable(player);
    addMovable(player);
    eventListeners.push_back(player);
    addGravitable(gravity);

    leadPlayer = player;

    // VEZIVANJE BACKGROUND IMAGE ZA GLAVNOG PLAYERA
    player->setBackgroundImage(dynamic_cast<Level*>(drawables[0]));
    // VEZIVANJE GLAVNOG PLAYERA ZA BACKGROUND IMAGE
    dynamic_cast<Level*>(drawables[0])->setPlayer(player);

    // VEZIVANJE GLAVNOG PLAYERA ZA BACKGROUND TILES
    player->setBackgroundTiles(dynamic_cast<Level*>(drawables[1]));
    // VEZIVANJE BACKGROUND TILES ZA GLAVNOG PLAYERA
    dynamic_cast<Level*>(drawables[1])->setPlayer(player);

    // VEZIVANJE GLAVNOG PLAYERA ZA GENERAL TILES
    player->setGeneralTiles(dynamic_cast<Level*>(drawables[2]));
    // VEZIVANJE GENERAL TILES ZA GLAVNOG PLAYERA
    dynamic_cast<Level*>(drawables[2])->setPlayer(player);
}

void Engine::addEnemy(const string &spriteSheatPath, int frameSkip, int x){
    ifstream spriteSheetStream(spriteSheatPath);
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, renderer);

    Sprite *sprite = new Sprite(sheet, x);
    sprite->setFrameSkip(frameSkip);

    Enemy *enemy = new Enemy(sprite);

    Gravity *gravity =  new Gravity (dynamic_cast<Level*>(drawables[0]), dynamic_cast<Level*>(drawables[1]), dynamic_cast<Level*>(drawables[2]), sprite);

    addDrawable(enemy);
    addMovable(enemy);
    enemies.push_back(enemy);
    //eventListeners.push_back(sprite);
    addGravitable(gravity);

    // VEZIVANJE BACKGROUND IMAGE ZA ENEMY
    enemy->setBackgroundImage(dynamic_cast<Level*>(drawables[0]));
    // VEZIVANJE GLAVNOG PLAYERA ZA ENEMY
    enemy->setBackgroundTiles(dynamic_cast<Level*>(drawables[1]));
    // VEZIVANJE GLAVNOG PLAYERA ZA ENEMY
    enemy->setGeneralTiles(dynamic_cast<Level*>(drawables[2]));

}

void Engine::addDrawable(Drawable* drawable){
    drawables.push_back(drawable);
}

void Engine::addMovable(Movable* movable){
    movables.push_back(movable);
}

void Engine::addGravitable(Gravity* gravitable){
    gravitables.push_back(gravitable);
}

/*void Engine::drawText(string text, Coordinate coordinate, TTF_Font* eFont, SDL_Renderer* eRenderer){
    SDL_Color white = {255, 255, 255};

    stringstream ss;
    ss << text ;
    SDL_Surface *surface = nullptr;
    SDL_Texture *texture = nullptr;

    surface = TTF_RenderText_Solid(eFont, ss.str().c_str(), white);
    texture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_Rect message;
    message.x = coordinate.getCoordinateX();
    message.y = coordinate.getCoordinateY();
    message.h = surface->h;
    message.w = surface->w;

    SDL_RenderCopy(eRenderer, texture, NULL, &message);
}*/

bool Engine::run(){
    int maxDelay = 1000/frameCap;
    int frameStart = 0;
    int frameEnd = 0;

    bool quit = false;
    SDL_Event event;

    //cout << (*dynamic_cast<Level*>(drawables[2])) << endl;

    while(!quit){
        //cout << *(leadPlayer->getSprite()) << endl;
        frameStart = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT){
                quit = true;
            } else if(event.key.keysym.sym == SDLK_ESCAPE){
                quit = true;
            } else {
                for(size_t i = 0; i < eventListeners.size(); i++){
                    eventListeners[i]->listen(event);
                }
            }
        }
        //cout << *sp << endl;

        for(size_t i = 0; i < gravitables.size(); i++) {
            gravitables[i]->gravity();
        }

        for(size_t i = 0; i < movables.size(); i++) {
            movables[i]->move();
        }

        for(size_t i = 0; i < enemies.size(); i++) {
            enemies[i]->randomChangeStates();
        }


        SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
        SDL_RenderClear(renderer);

        for(size_t i = 0; i < drawables.size(); i++) {
            drawables[i]->draw(renderer);
        }

        SDL_RenderPresent(renderer);
        frameEnd = SDL_GetTicks();
        if(frameEnd - frameStart < maxDelay){
            SDL_Delay(maxDelay - (frameEnd - frameStart));
        }
    }
}
