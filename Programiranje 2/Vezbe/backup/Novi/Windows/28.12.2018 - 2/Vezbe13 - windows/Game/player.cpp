#include "player.h"
#include "sprite.h"

Player::Player(Sprite *sprite): Drawable(), Movable(), KeyboardEventListener(){
    this->sprite = sprite;
}

void Player::setBackgroundImage(Level *backgroundImage){
    this->backgroundImage = backgroundImage;
}

void Player::setBackgroundTiles(Level *backgroundTiles){
    this->backgroundTiles = backgroundTiles;
}

void Player::setGeneralTiles(Level *generalTiles){
    this->generalTiles = generalTiles;
}

void Player::draw(SDL_Renderer *renderer){
    sprite->draw(renderer);
}

void Player::move(){
    if(sprite->getState() != Sprite::STOP){
        if(sprite->getState()&Sprite::LEFT && sprite->getSpriteRect()->x > 0){
            move(-1, 0);
        }
        if(sprite->getState()&Sprite::RIGHT && sprite->getSpriteRect()->x + sprite->getSpriteRect()->w < 1280){
            move(1, 0);
        }

        if(sprite->getState()&Sprite::UP){
            move(0, -8);
        }

    };


    /*int xCoordinate = sprite->getSpriteRect()->x + sprite->getSpriteRect()->w/2;
    int yCoordinate = sprite->getSpriteRect()->y + sprite->getSpriteRect()->h;

    // COLUMNS-----------------------------
    int currentColumn = xCoordinate/16;
    if(xCoordinate%16 == 0){
        currentColumn -= 1;
    }
    if(sprite->getSpriteRect()->x == 0){
        currentColumn = 0;
    }
    //-----------------------------------

    // ROWS--------------------------------
    int currentRow = yCoordinate/16;
    if(yCoordinate%16 == 0){
        currentRow -= 1;
    }
    if(sprite->getSpriteRect()->y == 0){
        currentRow = 0;
    }
    //-----------------------------------
    column = currentColumn;
    row = currentRow;
    cout << "player row: " << row << ", player column: " << column << endl;*/


    if(brojacSkoka > 0){
        brojacSkoka--;
    } else if(brojacSkoka == 0){
        sprite->setState(sprite->getState()&~Sprite::UP);
    }
}

void Player::move(int dx, int dy){
    sprite->move(dx, dy);
}

void Player::listen(SDL_KeyboardEvent &event){
    if(event.type == SDL_KEYDOWN){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()|Sprite::LEFT);
            backgroundImage->setState(backgroundImage->getState()|Level::RIGHT);
            backgroundTiles->setState(backgroundTiles->getState()|Level::RIGHT);
            generalTiles->setState(generalTiles->getState()|Level::RIGHT);
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()|Sprite::RIGHT);
            backgroundImage->setState(backgroundImage->getState()|Level::LEFT);
            backgroundTiles->setState(backgroundTiles->getState()|Level::LEFT);
            generalTiles->setState(generalTiles->getState()|Level::LEFT);
        } /*else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()|Sprite::DOWN);
        } */else if(event.keysym.sym == SDLK_SPACE){
            if(sprite->getOnTheGround()){
                brojacSkoka += 8;
                dvostrukiSkok = false;
                sprite->setState(sprite->getState()|Sprite::UP);
            } else if(!sprite->getOnTheGround() && !dvostrukiSkok){
                brojacSkoka += 8;
                dvostrukiSkok = true;
                sprite->setState(sprite->getState()|Sprite::UP);
            }
        }

    } else if(event.type == SDL_KEYUP){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()&~Sprite::LEFT);
            backgroundImage->setState(backgroundImage->getState()&~Level::RIGHT);
            backgroundTiles->setState(backgroundTiles->getState()&~Level::RIGHT);
            generalTiles->setState(generalTiles->getState()&~Level::RIGHT);
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()&~Sprite::RIGHT);
            backgroundImage->setState(backgroundImage->getState()&~Level::LEFT);
            backgroundTiles->setState(backgroundTiles->getState()&~Level::LEFT);
            generalTiles->setState(generalTiles->getState()&~Level::LEFT);
        } /*else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()&~Sprite::DOWN);
        } else if(event.keysym.sym == SDLK_SPACE){
            sprite->setState(sprite->getState()&~Sprite::UP);
        }*/
    }
}
