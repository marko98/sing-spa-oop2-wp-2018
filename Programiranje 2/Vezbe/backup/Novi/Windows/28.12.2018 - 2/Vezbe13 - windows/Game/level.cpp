#include "level.h"
#include "player.h"
#include "sprite.h"

Level::Level(istream &inputStream, Tileset *tileset, int levelTileWidth, int levelTileHeight): Drawable(){
    this->x = 0;
    this->y = 0;
    this->tileset = tileset;
    this->levelTileWidth = levelTileWidth;
    this->levelTileHeight = levelTileHeight;
    this->state = Level::STOP;
    int rows, columns;
    string tileCode;
    inputStream >> rows >> columns;
    for(int i = 0; i < rows; i++){
        level.push_back(LevelRow());
        for(int j = 0; j < columns; j++){
            inputStream >> tileCode;
            level[i].push_back(tileCode);
        }
    }
}

Level::~Level(){
    delete tileset;
}

short int Level::getState(){
    return state;
}

void Level::setState(short int state){
    this->state = state;
}

void Level::changeLevel(int row, int column, const string &name){
    level[row][column] = name;
}

void Level::move(){
    if(state != STOP){
        if(state&LEFT && x > -3840){
            move(-3, 0);
        };
        if(state&RIGHT && x < 0){
            move(3, 0);
        };
    };
}

void Level::setPlayer(Player *player){
    this->player = player;
}

const Level::LevelMatrix & Level::getLevelMatrix() const {
    return level;
}

void Level::draw(SDL_Renderer *renderer){
    //IZMENJENO U ODNOSU NA Vezbe11 - windows
    /*for(size_t i = 0; i < level.size(); i++){
        for(size_t j = 0; j < level[i].size(); j++){
            int w = tileset->getTileset()[level[i][j]]->getRect()->w;
            int h = tileset->getTileset()[level[i][j]]->getRect()->h;
            //cout << tileset->getTileset()[level[i][j]]->getSrcTileRectWidth() << tileset->getTileset()[level[i][j]]->getSrcTileRectHeight() << endl;
            if(i != 0){
                if(j != 0){
                    int w = tileset->getTileset()[level[i][j-1]]->getRect()->w;
                    int h = tileset->getTileset()[level[i-1][j]]->getRect()->h;
                    tileset->draw(level[i][j], w, h, renderer);
                } else {
                    int h = tileset->getTileset()[level[i-1][0]]->getRect()->h;
                    tileset->draw(level[i][j], 0, h, renderer);
                }

            } else {
                if(j != 0){
                    int w = tileset->getTileset()[level[0][j-1]]->getRect()->w;
                    tileset->draw(level[i][j], w, 0, renderer);
                } else {
                    tileset->draw(level[i][j], 0, 0, renderer);
                }

            }

        }
    }*/
    for(size_t i = 0; i < level.size(); i++) {
        for(size_t j = 0; j < level[i].size(); j++) {
            tileset->draw(level[i][j], x+j*levelTileWidth, y+i*levelTileHeight, renderer);
        }
    }
}
