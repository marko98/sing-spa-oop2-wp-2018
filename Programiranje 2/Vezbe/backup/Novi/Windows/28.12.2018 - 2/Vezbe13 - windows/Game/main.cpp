#include <iostream>

#include "engine.h"
#include "level.h"

using namespace std;

int main(int argc, char** argv)
{
    Engine *engine = new Engine("Super Mario");

    // 1. BACKGROUND IMAGE
    engine->addTileset("resources/tilesets/tilesets_level0_template.txt", "level0_background_image");
    engine->addLevel("resources/levels/level0_template_tile.txt", "level0_background_image", 0, 0, 16, 16);

    // 2. BACKGROUND TILES
    engine->addTileset("resources/tilesets/tilesets_level0.txt", "level0_tiles");
    engine->addLevel("resources/levels/level0_tiles.txt", "level0_tiles", 0, 288, 16, 16);

    // 3. GENERAL TILES
    engine->addTileset("resources/tilesets/general_tiles.txt", "general_tiles");
    engine->addLevel("resources/levels/general_tiles.txt", "general_tiles", 0, 288, 16, 16);

    // 4. ADD LEAD PLAYER
    engine->addPlayer("resources/creatures/mario_sprite_sheet.txt", 6);

    engine->run();
    delete engine;

    return 0;
}
