#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

using namespace std;


int main(int argc, char** argv){

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

    SDL_Window *window = SDL_CreateWindow("igra", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 432, SDL_WINDOW_RESIZABLE);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
        cerr << "Error: " << Mix_GetError() << endl;

    Mix_Music *backgroundMusic = Mix_LoadMUS("resources/sounds/Little Girl.mp3");// ako je duze od 10 sek
    Mix_Chunk *soundEffect1 = Mix_LoadWAV("resources/sounds/smw_bonus_game_end.wav");
    Mix_Chunk *soundEffect2 = Mix_LoadWAV("resources/sounds/smw_castle_clear.wav");



    bool quit = false;
    SDL_Event event;
    while(!quit){
        //cout << *(leadPlayer->getSprite()) << endl;
        //frameStart = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT){
                quit = true;
            } else if(event.key.keysym.sym == SDLK_ESCAPE){
                quit = true;
            } else if(event.key.keysym.sym == SDLK_p){
                Mix_PlayMusic(backgroundMusic, 0);
                /*if(!Mix_PlayingMusic()){
                    Mix_PlayMusic(backgroundMusic, 0);// -1 dok joj ne kazemo da stane
                } else if(Mix_PausedMusic()){
                    Mix_ResumeMusic();
                } else {
                    Mix_PauseMusic();
                }*/
            } else if(event.key.keysym.sym == SDLK_o){
                if(Mix_PausedMusic()){
                    Mix_ResumeMusic();
                }
            } else if(event.key.keysym.sym == SDLK_s){
                //Mix_HaltMusic();
                Mix_PauseMusic();
            } else if(event.key.keysym.sym == SDLK_1){
                Mix_PlayChannel(1, soundEffect1, 0);
                Mix_HaltChannel(2);
            } else if(event.key.keysym.sym == SDLK_2){
                Mix_PlayChannel(2, soundEffect2, 0);
                Mix_HaltChannel(1);
            }
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = nullptr;
    renderer = nullptr;

    Mix_FreeChunk(soundEffect1);
    Mix_FreeChunk(soundEffect2);
    Mix_FreeMusic(backgroundMusic);

    soundEffect1 = nullptr;
    soundEffect2 = nullptr;
    backgroundMusic = nullptr;

    Mix_Quit();
    SDL_Quit();

    return 0;
}
