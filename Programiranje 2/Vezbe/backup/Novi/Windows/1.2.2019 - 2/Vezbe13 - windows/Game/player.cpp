#include "player.h"
#include "sprite.h"
#include "engine.h"

Player::Player(Sprite *sprite, Engine *engine): Drawable(), Movable(), KeyboardEventListener(), engine(engine){
    this->sprite = sprite;
    this->health = 5;
    this->coins = 0;
    this->points = 0;
}

Player::~Player(){
    delete sprite;
}

int Player::getHealth(){
    return health;
}

void Player::setHealth(int health){
    if(health <= 0){
        Mix_PlayChannel(1, backgroundTiles->getSoundEffect("game_over"), 0);
        health = 0;
    };
    this->health = health;
}

int Player::getCoins(){
    return coins;
}

void Player::setCoins(int coins){
    if(coins < 0){
        coins = 0;
    }
    this->coins = coins;
}

int Player::getPoints(){
    return points;
}

void Player::setPoints(int points){
    if(points < 0){
        points = 0;
    }
    this->points = points;
}

void Player::setBackgroundImage(Level *backgroundImage){
    this->backgroundImage = backgroundImage;
}

void Player::setBackgroundTiles(Level *backgroundTiles){
    this->backgroundTiles = backgroundTiles;
}

void Player::setGeneralTiles(Level *generalTiles){
    this->generalTiles = generalTiles;
}

void Player::draw(SDL_Renderer *renderer){
    sprite->draw(renderer);
}

void Player::turnOnBackground(){
    backgroundImage->setMoving(true);
    backgroundTiles->setMoving(true);
    generalTiles->setMoving(true);
}

void Player::turnOffBackground(){
    backgroundImage->setMoving(false);
    backgroundTiles->setMoving(false);
    generalTiles->setMoving(false);
}

void Player::moveBackgroundToLeft(){
    backgroundImage->setState(backgroundImage->getState()|Level::LEFT);
    backgroundTiles->setState(backgroundTiles->getState()|Level::LEFT);
    generalTiles->setState(generalTiles->getState()|Level::LEFT);
    turnOnBackground();
}

void Player::moveBackgroundToRight(){
    backgroundImage->setState(backgroundImage->getState()|Level::RIGHT);
    backgroundTiles->setState(backgroundTiles->getState()|Level::RIGHT);
    generalTiles->setState(generalTiles->getState()|Level::RIGHT);
    turnOnBackground();
}

void Player::attack(Enemy *enemy){
    if(!sprite->getOnTheGround()){
        if(sprite->getSpriteRect()->x >= enemy->getSprite()->getSpriteRect()->x && sprite->getSpriteRect()->x <= enemy->getSprite()->getSpriteRect()->x + enemy->getSprite()->getSpriteRect()->w/4*3 &&
           sprite->getSpriteRect()->y + sprite->getSpriteRect()->h <= enemy->getSprite()->getSpriteRect()->y + enemy->getSprite()->getSpriteRect()->h/3 &&
           sprite->getSpriteRect()->y + sprite->getSpriteRect()->h >= enemy->getSprite()->getSpriteRect()->y - enemy->getSprite()->getSpriteRect()->h/4){
            //cout << "kill enemy" << endl;
            Mix_PlayChannel(1, backgroundTiles->getSoundEffect("kick"), 0);

            for(size_t i = 0; i < engine->enemies.size(); i++) {
                if(engine->enemies[i] == enemy){
                    delete engine->enemies[i];
                    engine->enemies.erase(engine->enemies.begin()+i);
                };
            };

            for(size_t i = 0; i < engine->gravitables.size(); i++) {
                if(engine->gravitables[i]->getSprite() == enemy->getSprite()){
                    delete engine->gravitables[i];
                    engine->gravitables.erase(engine->gravitables.begin()+i);
                };
            };

            for(size_t i = 0; i < engine->movables.size(); i++) {
                if(engine->movables[i] == enemy){
                    delete engine->movables[i];
                    engine->movables.erase(engine->movables.begin()+i);
                };
            };

            for(size_t i = 0; i < engine->drawables.size(); i++) {
                if(engine->drawables[i] == enemy){
                    delete engine->drawables[i];
                    engine->drawables.erase(engine->drawables.begin()+i);
                };
            };

            points += 200;
            coins += 1;

        };
    };

}

void Player::move(){
    if(sprite->getState() != Sprite::STOP){

        if(sprite->getState() != Sprite::LEFT_DOWN && sprite->getState() != Sprite::LEFT_UP){
            if(sprite->getState()&Sprite::LEFT && sprite->getSpriteRect()->x > 0){
                move(-1, 0);
                moveBackgroundToRight();
            };
        };

        if(sprite->getState() != Sprite::RIGHT_DOWN && sprite->getState() != Sprite::RIGHT_UP){
            if(sprite->getState()&Sprite::RIGHT && sprite->getSpriteRect()->x + sprite->getSpriteRect()->w < 1280){
                move(1, 0);
                moveBackgroundToLeft();
            };
        };

        if(sprite->getState()&Sprite::UP){
            move(0, -8);
            sprite->setOnTheGround(false);
            Mix_PlayChannel(1, backgroundTiles->getSoundEffect("jump"), 0);
        };

    };

    if(brojacSkoka > 0){
        brojacSkoka--;
    } else if(brojacSkoka == 0){
        sprite->setState(sprite->getState()&~Sprite::UP);
    };
}

void Player::move(int dx, int dy){
    sprite->move(dx, dy);
}

void Player::listen(SDL_KeyboardEvent &event){
    if(event.type == SDL_KEYDOWN){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()|Sprite::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()|Sprite::RIGHT);
        } else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()|Sprite::CROUCH);
        } else if(event.keysym.sym == SDLK_UP){
            sprite->setState(sprite->getState()|Sprite::VIEW);
        } else if(event.keysym.sym == SDLK_SPACE){
            if(sprite->getOnTheGround()){
                brojacSkoka += 8;
                dvostrukiSkok = false;
                sprite->setState(sprite->getState()|Sprite::UP);
            } else if(!sprite->getOnTheGround() && !dvostrukiSkok){
                brojacSkoka += 8;
                dvostrukiSkok = true;
                sprite->setState(sprite->getState()|Sprite::UP);
            }
        }
        // PAMTIMO POSLEDNJE STANJE
        sprite->setLastState(sprite->getState());
    } else if(event.type == SDL_KEYUP){
        if(event.keysym.sym == SDLK_LEFT){
            sprite->setState(sprite->getState()&~Sprite::LEFT);
            backgroundImage->setState(backgroundImage->getState()&~Level::RIGHT);
            backgroundTiles->setState(backgroundTiles->getState()&~Level::RIGHT);
            generalTiles->setState(generalTiles->getState()&~Level::RIGHT);
            turnOffBackground();
        } else if(event.keysym.sym == SDLK_RIGHT){
            sprite->setState(sprite->getState()&~Sprite::RIGHT);
            backgroundImage->setState(backgroundImage->getState()&~Level::LEFT);
            backgroundTiles->setState(backgroundTiles->getState()&~Level::LEFT);
            generalTiles->setState(generalTiles->getState()&~Level::LEFT);
            turnOffBackground();
        } else if(event.keysym.sym == SDLK_DOWN){
            sprite->setState(sprite->getState()&~Sprite::CROUCH);
        } else if(event.keysym.sym == SDLK_UP){
            sprite->setState(sprite->getState()&~Sprite::VIEW);
        } else if(event.keysym.sym == SDLK_SPACE){
            sprite->setState(sprite->getState()&~Sprite::UP);
        }
    }
}
