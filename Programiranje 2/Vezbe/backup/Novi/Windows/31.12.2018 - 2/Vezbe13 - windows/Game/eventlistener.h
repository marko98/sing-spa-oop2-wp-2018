#ifndef EVENTLISTENER_H_INCLUDED
#define EVENTLISTENER_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

using namespace std;

class EventListener{
public:
    virtual void listen(SDL_Event &event) = 0;
};

#endif // EVENTLISTENER_H_INCLUDED
