#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "tileset.h"
#include "drawable.h"
#include "movable.h"
#include "spritesheet.h"
#include "sprite.h"
#include "enemy.h"
#include "eventlistener.h"
#include "player.h"
#include "level.h"

using namespace std;

class Engine{
public:
    typedef map<string, Tileset*> Tilesets;
    typedef vector<Drawable*> Drawables;
    typedef vector<Movable*> Movables;
    typedef vector<EventListener*> EventListeners;
    typedef vector<Gravity*> Gravitables;
    typedef vector<Enemy*> Enemies;
private:
    Tilesets tilesets;
    Drawables drawables;
    Movables movables;
    EventListeners eventListeners;
    Gravitables gravitables;
    Enemies enemies;

    Player *leadPlayer;
    SDL_Window *window;
    SDL_Renderer *renderer;
    TTF_Font *font;

    int frameCap = 60;
    int enemiesCount = 0;
public:
    Engine(const string &gameTitle);
    ~Engine();

    //void drawText(string text, Coordinate coordinate, TTF_Font* eFont, SDL_Renderer* eRenderer);
    bool run();

    void addTileset(const string &path, const string &name);
    void addLevel(const string &path, const string &name, int x, int y, int tileWidth, int tileHeight);
    void addEnemy(const string &spriteSheatPath, int frameSkip, int x = 0);
    void addPlayer(const string &spriteSheatPath, int frameSkip);
    Tileset* getTileset(const string &name);
    void addDrawable(Drawable* drawable);
    void addMovable(Movable* movable);
    void addGravitable(Gravity* gravitable);
};

#endif // ENGINE_H_INCLUDED
