#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "spritesheet.h"
#include "vector.h"

using namespace std;

class Sprite: public Drawable, public Movable{
public:
    enum State: short int{STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8,
                        LEFT_UP=LEFT|UP, LEFT_DOWN=LEFT|DOWN,
                        RIGHT_UP=RIGHT|UP, RIGHT_DOWN=RIGHT|DOWN};
private:
    short int state;
    SpriteSheet *sheet;
    SDL_Rect *spriteRect;
    int currentFrame, frameCounter, frameSkip;
public:
    Sprite(SpriteSheet *sheet, int width=18, int height=29);
    int getFrameSkip();
    void setFrameSkip(int frameSkip);
    short int getState();
    void setState(short int state);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    SDL_Rect * getSpriteRect(){ return spriteRect; };
};

struct Mesto{
    Sprite *sprite;
    Mesto(Sprite *sprite): sprite(sprite){};

    // npr void operator() operator+(){} za redefinisanje operatora +
    void operator() (){
        cout << sprite->getSpriteRect()->x << ", " << sprite->getSpriteRect()->y << endl;
    }
};

struct Gravity{
    Sprite *sprite;
    //Vector *gravity = new Vector(0, -2);
    Gravity(Sprite *sprite): sprite(sprite){};

    void gravity(){
        if(sprite->getSpriteRect()->y <= 354){
            sprite->getSpriteRect()->y += 2;
        }
    };
};

#endif // SPRITE_H_INCLUDED
