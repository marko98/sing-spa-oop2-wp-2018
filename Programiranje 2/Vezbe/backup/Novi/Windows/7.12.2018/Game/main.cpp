#include <iostream>

#include "engine.h"
#include "level.h"

using namespace std;

int main(int argc, char** argv)
{
    Engine *engine = new Engine("Super Mario");

    engine->addTileset("resources/tilesets/tilesets_level0_template.txt", "level0_template");
    engine->addTileset("resources/tilesets/tilesets_level0.txt", "level0_tilesets");

    ifstream levelTemplateStream("resources/levels/level0_template_tile.txt");
    Level *level = new Level(levelTemplateStream, engine->getTileset("level0_template"));
    engine->addDrawable(level);

    ifstream levelStream("resources/levels/level0_tiles.txt");
    level = new Level(levelStream, engine->getTileset("level0_tilesets"));
    level->move(0, 320);
    engine->addDrawable(level);

    engine->run();
    delete engine;

    return 0;
}
