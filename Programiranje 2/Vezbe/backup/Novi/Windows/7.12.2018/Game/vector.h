#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

using namespace std;

class Vector{
private:
    int vectorX, vectorY;
public:
    Vector();
    Vector(int vectorX, int vectorY);
    void operator+(Vector v);
    void operator-(int downFor);

    int getVectorX();
    void setVectorX(int x);

    int getVectorY();
    void setVectorY(int y);
};

#endif // VECTOR_H_INCLUDED
