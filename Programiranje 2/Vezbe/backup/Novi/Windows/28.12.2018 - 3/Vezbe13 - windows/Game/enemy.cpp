#include "enemy.h"
#include "sprite.h"

Enemy::Enemy(Sprite *sprite): Drawable(), Movable(), KeyboardEventListener(){
    this->sprite = sprite;
}

void Enemy::move(int dx, int dy){
    sprite->move(dx, dy);
}

void Enemy::move(){
    if(sprite->getState() != Sprite::STOP){
        if(sprite->getState()&Sprite::LEFT){
            move(-1, 0);
        };
        if(sprite->getState()&Sprite::RIGHT){
            move(1, 0);
        };
        if(sprite->getState()&Sprite::UP){
            move(0, -8);
        };
    };
}

void Enemy::draw(SDL_Renderer *renderer){
    sprite->draw(renderer);
}
