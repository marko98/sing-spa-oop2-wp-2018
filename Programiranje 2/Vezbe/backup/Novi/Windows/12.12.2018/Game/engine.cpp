#include "Engine.h"

ostream& operator<<(ostream& out, const Level& l) {
    int rows = l.getLevelMatrix().size();
    int cols = 0;
    if(rows > 0) {
        cols = l.getLevelMatrix()[0].size();
    }
    out << rows << " " << cols << endl;

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++) {
            out << l.getLevelMatrix()[i][j] << " ";
        }
        out << endl;
    }

    return out;
}

ostream& operator<<(ostream& out, const Sprite& s) {
    out << s.spriteRect->x << " " << s.spriteRect->y << " " << s.spriteRect->w << " " << s.spriteRect->h << " " << s.onTheGround << endl;

    return out;
}

Engine::Engine(const string &gameTitle){
    SDL_Init(SDL_INIT_VIDEO);
    // window = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 432, SDL_WINDOW_RESIZABLE);
    window = SDL_CreateWindow(gameTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 432, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    TTF_Init();
    font = TTF_OpenFont("Super-Mario-World.ttf", 20);
}

Engine::~Engine(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = nullptr;
    renderer = nullptr;
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void Engine::addTileset(const string &path, const string &name){
    ifstream putanja(path);
    tilesets[name] = new Tileset(putanja, renderer);
}

Tileset* Engine::getTileset(const string &name) {
    return tilesets[name];
}

void Engine::addDrawable(Drawable* drawable){
    drawables.push_back(drawable);
}

/*void Engine::drawText(string text, Coordinate coordinate, TTF_Font* eFont, SDL_Renderer* eRenderer){
    SDL_Color white = {255, 255, 255};

    stringstream ss;
    ss << text ;
    SDL_Surface *surface = nullptr;
    SDL_Texture *texture = nullptr;

    surface = TTF_RenderText_Solid(eFont, ss.str().c_str(), white);
    texture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_Rect message;
    message.x = coordinate.getCoordinateX();
    message.y = coordinate.getCoordinateY();
    message.h = surface->h;
    message.w = surface->w;

    SDL_RenderCopy(eRenderer, texture, NULL, &message);
}*/

bool Engine::run(){
    int maxDelay = 1000/frameCap;
    int frameStart = 0;
    int frameEnd = 0;

    bool quit = false;
    SDL_Event event;

    ifstream spriteSheetStream("resources/creatures/mario_sprite_sheet.txt");
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, renderer);

    Sprite *sp = new Sprite(sheet);
    sp->setFrameSkip(6);

    Player *player = new Player(sp);
    Gravity *gravity =  new Gravity (dynamic_cast<Level*>(drawables[0]), dynamic_cast<Level*>(drawables[1]), player);

    drawables.push_back(player);
    movables.push_back(player);
    eventListeners.push_back(player);
    gravitables.push_back(gravity);

    /*Sprite *sp1 = new Sprite(sheet);
    sp1->setFrameSkip(8);

    Player *player1 = new Player(sp1);
    Gravity *gravity1 =  new Gravity (dynamic_cast<Level*>(drawables[0]), dynamic_cast<Level*>(drawables[1]), sp1);
    sp1->move(50, 0);

    drawables.push_back(player1);
    movables.push_back(player1);
    //eventListeners.push_back(player1);
    gravitables.push_back(gravity1);*/

    cout << (*dynamic_cast<Level*>(drawables[0])) << endl;


    while(!quit){
        frameStart = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT){
                quit = true;
            } else if(event.key.keysym.sym == SDLK_ESCAPE){
                quit = true;
            } else {
                for(size_t i = 0; i < eventListeners.size(); i++){
                    eventListeners[i]->listen(event);
                }
            }
        }
        //cout << *sp << endl;

        for(size_t i = 0; i < gravitables.size(); i++) {
            gravitables[i]->gravity();
        }

        for(size_t i = 0; i < movables.size(); i++) {
            movables[i]->move();
        }



        SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
        SDL_RenderClear(renderer);

        for(size_t i = 0; i < drawables.size(); i++) {
            drawables[i]->draw(renderer);
        }

        SDL_RenderPresent(renderer);
        frameEnd = SDL_GetTicks();
        if(frameEnd - frameStart < maxDelay){
            SDL_Delay(maxDelay - (frameEnd - frameStart));
        }
    }
}
