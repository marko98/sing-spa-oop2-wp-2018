#include <iostream>

#include "engine.h"
#include "level.h"

using namespace std;

int main(int argc, char** argv)
{
    Engine *engine = new Engine("Super Mario World");

    // 1. BACKGROUND IMAGE
    engine->addTileset("resources/tilesets/tilesets_level0_template.txt", "level0_background_image");
    engine->addLevel("resources/levels/level0_template_tile.txt", "level0_background_image", 0, 0, 16, 16);

    // 2. BACKGROUND TILES
    engine->addTileset("resources/tilesets/tilesets_level0.txt", "level0_tiles");
    engine->addLevel("resources/levels/level0_tiles.txt", "level0_tiles", 0, 288, 16, 16);

    // 3. GENERAL TILES
    engine->addTileset("resources/tilesets/general_tiles.txt", "general_tiles");
    engine->addLevel("resources/levels/general_tiles.txt", "general_tiles", 0, 288, 16, 16);

    // 4. ADD ENEMIES
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 700);
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 800);

    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 450);
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 300);

    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 570);
    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 200);
    //-------------------------------------------------------------------------------------
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 756);
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 546);

    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 123);
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 785);

    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 123);
    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 456);
    //-------------------------------------------------------------------------------------
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 785);
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 45);

    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 12);
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 65);

    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 570);
    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 456);
    //-------------------------------------------------------------------------------------
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 785);
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 45);

    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 12);
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 65);

    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 570);
    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 456);
    //-------------------------------------------------------------------------------------
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 785);
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 6, 45);

    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 12);
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 6, 65);

    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 570);
    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 6, 456);
    //-------------------------------------------------------------------------------------


    // 5. ADD MUSIC
    engine->addBackgroundMusic("resources/sounds/Super Mario World - Overworld Theme Music.mp3");

    // 6. ADD COINS
    engine->addCoin("resources/tilesets/coin_sprite_sheet.txt", 6, 1145, 2);

    // 7. ADD LEAD PLAYER
    engine->addPlayer("resources/creatures/mario_sprite_sheet.txt", 6, engine);


    engine->run();
    delete engine;

    return 0;
}
