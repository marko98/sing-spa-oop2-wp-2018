#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "keyboardeventlistener.h"
#include "sprite.h"

using namespace std;

class Player: public Drawable, public Movable, public KeyboardEventListener{
private:
    Sprite *sprite;
    bool dvostrukiSkok = false;
    int brojacSkoka = 0;
public:
    Player(Sprite *sprite);
    virtual void draw(SDL_Renderer *renderer);
    virtual void move(int dx, int dy);
    virtual void move();
    virtual void listen(SDL_KeyboardEvent &event);
};

#endif // PLAYER_H_INCLUDED
