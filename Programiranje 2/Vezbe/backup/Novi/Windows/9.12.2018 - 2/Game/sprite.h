#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "spritesheet.h"
#include "vector.h"
#include "level.h"

using namespace std;

class Sprite: public Drawable, public Movable{
public:
    enum State: short int{STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8,
                        LEFT_UP=LEFT|UP, LEFT_DOWN=LEFT|DOWN,
                        RIGHT_UP=RIGHT|UP, RIGHT_DOWN=RIGHT|DOWN};
private:
    short int state;
    SpriteSheet *sheet;
    SDL_Rect *spriteRect;
    int currentFrame, frameCounter, frameSkip;
    bool onTheGround = false;
public:
    Sprite(SpriteSheet *sheet, int width=20, int height=30);
    int getFrameSkip();
    void setFrameSkip(int frameSkip);
    short int getState();
    void setState(short int state);
    void setOnTheGround(bool onTheGround){ this->onTheGround = onTheGround; };
    bool getOnTheGround(){ return onTheGround; };

    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    SDL_Rect * getSpriteRect(){ return spriteRect; };
    friend ostream& operator<<(ostream&, const Sprite&);
};

struct Mesto{
    Sprite *sprite;
    Mesto(Sprite *sprite): sprite(sprite){};

    // npr void operator() operator+(){} za redefinisanje operatora +
    void operator() (){
        cout << sprite->getSpriteRect()->x << ", " << sprite->getSpriteRect()->y << endl;
    }
};

struct Gravity{
    Sprite *sprite;
    Level *background;
    Level *level;
    int pomeraj = 0;
    int prozor = 1;
    //Vector *gravity = new Vector(0, -2);
    Gravity(Level *background, Level *level, Sprite *sprite): background(background), level(level), sprite(sprite){};

    void gravity(){
        //cout << sprite->getSpriteRect()->x << endl;
        //cout << sprite->getSpriteRect()->w/2 << endl;
        int xCoordinate = sprite->getSpriteRect()->x + sprite->getSpriteRect()->w/2;
        int yCoordinate = sprite->getSpriteRect()->y + sprite->getSpriteRect()->h;
        //int yCoordinate = sprite->getSpriteRect()->y;

        int column = xCoordinate/16;
        //cout << xCoordinate << endl;
        if(xCoordinate%16 == 0){
            column -= 1;
        }
        if(sprite->getSpriteRect()->x == 0){
            column = 0;
        }
        //column += 1;
        //cout << column << endl;

        //cout << yCoordinate << endl;
        int row = yCoordinate/16;
        if(yCoordinate%16 == 0){
            row -= 1;
        }
        if(sprite->getSpriteRect()->y == 0){
            row = 0;
        }


        //cout << sprite->getSpriteRect()->h << endl;
        //cout << yCoordinate << endl;
        //cout << row - (27-level->getLevelMatrix().size()) << " " << column+pomeraj << endl;

        int plocica = 0;
        int i = 0;
        int brojTrava = 0;
        if(row > 27-level->getLevelMatrix().size()-1){
            //cout << column << endl;
            //cout << row << endl;
            row = row - (27-level->getLevelMatrix().size());
            /*cout << "usao" << endl;
            cout << row << endl;
            cout << "izasao" << endl;*/
            i = row;
            bool broji = false;

            for(i; i < level->getLevelMatrix().size(); i++){

                //cout << level->getLevelMatrix()[i][column+pomeraj] << endl;
                if(broji){
                    if(level->getLevelMatrix()[i][column+pomeraj] != "0"){
                        //cout << level->getLevelMatrix()[i][column] << endl;
                        plocica++;
                        if(level->getLevelMatrix()[i][column+pomeraj] == "t"){
                            brojTrava++;
                        }
                    }
                }

                if(i < level->getLevelMatrix().size()-1){
                    if(level->getLevelMatrix()[i+1][column+pomeraj] == "t" ||
                       level->getLevelMatrix()[i+1][column+pomeraj] == "t7" ||
                       level->getLevelMatrix()[i+1][column+pomeraj] == "t9" ||
                       level->getLevelMatrix()[i+1][column+pomeraj] == "t478" ||
                       level->getLevelMatrix()[i+1][column+pomeraj] == "t896"){
                        broji = true;
                        //cout << "trava" << endl;
                    }
                }

            }

        } else {
            for(i; i < level->getLevelMatrix().size(); i++){
                if(level->getLevelMatrix()[i][column+pomeraj] != "0"){
                    //cout << level->getLevelMatrix()[i][column+pomeraj] << endl;
                    plocica++;
                }
            }
        }
        //cout << brojTrava << endl;
        if(brojTrava == 2){
            if(level->getLevelMatrix()[row][column+pomeraj+1] == "t478" ||
               level->getLevelMatrix()[row][column+pomeraj+1] == "t4"){
                //cout << "zabrana" << endl;
                sprite->move(-1, 0);
            }
        }



        if(yCoordinate < 432-plocica*16){
            sprite->getSpriteRect()->y += 2;
            sprite->setOnTheGround(false);
        }

        if(!sprite->getOnTheGround()){
            if (yCoordinate == 432-plocica*16) {
                sprite->setOnTheGround(true);
            }
        }

        // OGRANICENJE SA LEVE STRANE
        if(xCoordinate < sprite->getSpriteRect()->w/2){
            //cout << "predji na levu stranu" << endl;
            if(prozor > 1){
                pomeraj -= 80;
                prozor -= 1;
                sprite->getSpriteRect()->x = 1280 - sprite->getSpriteRect()->w;
                background->move(1280, 0);
                level->move(1280, 0);
            } else {
                sprite->getSpriteRect()->x = 0;
            }
        // OGRANICENJE SA DESNE STRANE
        } else if(xCoordinate > 1280-sprite->getSpriteRect()->w/2){
            sprite->getSpriteRect()->x = 1280 - sprite->getSpriteRect()->w;
            //cout << "predji na desnu stranu" << endl;
            if(prozor < 4){
                pomeraj += 80;
                prozor += 1;
                sprite->getSpriteRect()->x = 0;
                background->move(-1280, 0);
                level->move(-1280, 0);
            }
        }

    };
};

#endif // SPRITE_H_INCLUDED
