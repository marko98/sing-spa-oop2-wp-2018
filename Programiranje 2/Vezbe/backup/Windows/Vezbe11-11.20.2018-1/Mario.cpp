#include "Mario.h"

const int walkingAnimationFrames = 5;
SDL_Rect spriteMarioClips[2*walkingAnimationFrames];

Mario::Mario(){
    path = "resources/sheets/marioWalking.png";
}

string Mario::getPath(){
    return path;
}

void Mario::loadSpriteWalking(){
    for(int i = 0; i < 5; i++){
        spriteMarioClips[i].x = (5 + i)*40;
        spriteMarioClips[i].y = 1;
        spriteMarioClips[i].w = 18;
        spriteMarioClips[i].h = 29;
    }
    for(int i = 0; i < 5; i++){
        spriteMarioClips[5+i].x = i*40;
        spriteMarioClips[5+i].y = 1;
        spriteMarioClips[5+i].w = 18;
        spriteMarioClips[5+i].h = 29;
    }
}

SDL_Rect * Mario::getSpriteClip(int iClip){
 return &spriteMarioClips[iClip];
}

int Mario::getWalkingAnimationFrames(){
    return walkingAnimationFrames;
}
