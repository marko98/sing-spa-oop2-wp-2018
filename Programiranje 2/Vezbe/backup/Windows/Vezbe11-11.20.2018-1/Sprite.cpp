#include "Sprite.h"

Sprite::Sprite(){
    spriteTexture = nullptr;
    spriteHeight = 0;
    spriteWidth = 0;
}

Sprite::~Sprite(){
    free();
}

void Sprite::free(){
    if(spriteTexture != nullptr){
        SDL_DestroyTexture(spriteTexture);
        spriteTexture = nullptr;
        spriteHeight = 0;
        spriteWidth = 0;
    }
}

bool Sprite::loadFromFile(string path, SDL_Renderer *renderer){
    free();
    SDL_Texture *texture = nullptr;
    SDL_Surface *surface = IMG_Load(path.c_str());

    if(surface == NULL){
        printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
    } else {
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0xFF, 0xFF ));

        texture = SDL_CreateTextureFromSurface(renderer, surface);
        if(texture == nullptr){
            printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        } else {
            spriteWidth = surface->w;
            spriteHeight = surface->h;
        }
        SDL_FreeSurface(surface);
    }

    spriteTexture = texture;
    return spriteTexture != nullptr;
}

void Sprite::render(Coordinate coordinate, SDL_Renderer *renderer, SDL_Rect *clip){
    SDL_Rect renderQuad;
    renderQuad.x = coordinate.getCoordinateX();
    renderQuad.y = coordinate.getCoordinateY();
    renderQuad.h = spriteHeight;
    renderQuad.w = spriteWidth;

    if(clip != nullptr){
        renderQuad.h = 2*clip->h;
        renderQuad.w = 2*clip->w;
    };

    SDL_RenderCopy(renderer, spriteTexture, clip, &renderQuad);
}


