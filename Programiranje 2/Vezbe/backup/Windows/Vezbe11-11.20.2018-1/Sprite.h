#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"
#include "Vector.h"

using namespace std;

class Sprite{
private:
    SDL_Texture *spriteTexture;
    int spriteHeight, spriteWidth;
public:
    Sprite();
    ~Sprite();
    void free();

    bool loadFromFile(string path, SDL_Renderer *renderer);
    virtual void loadSpriteWalking() = 0;
    void render(Coordinate coordinate, SDL_Renderer *renderer, SDL_Rect *clip = nullptr);

    virtual SDL_Rect *getSpriteClip(int iClip) = 0;
    virtual int getWalkingAnimationFrames() = 0;
    virtual string getPath() = 0;
};

#endif // SPRITE_H_INCLUDED
