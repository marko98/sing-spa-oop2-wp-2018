#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Sprite.h"
#include "Coordinate.h"
#include "Vector.h"
#include "Engine.h"

using namespace std;

int main(int argc, char ** argv)
{
    /*Coordinate k1(10, 20);
    Vector v1(5, 5);
    Coordinate k2 = k1.addVector(v1);
    cout << k1.getCoordinateX() << k1.getCoordinateY() << endl;
    cout << k2.getCoordinateX() << k2.getCoordinateY() << endl;*/

    /*bool quit = false;
    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow("Mario", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 640, 0);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    Sprite *mario = new Sprite;
    mario->loadFromFile("resources/sheets/marioWalking.png", renderer);
    mario->loadSpriteWalking();

    int dx = 1;
    int frame = 0;
    int x = 0;

    while(!quit){
    SDL_Delay(10);
    SDL_PollEvent(&event);
    switch (event.type){
        case SDL_QUIT:
            quit = true;
            break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_LEFT:
                        dx = -1;
                        break;
                    case SDLK_RIGHT:
                        dx = 1;
                        break;
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    default:
                        break;
                };
                break;
    }
    // move objects
    x = x + dx;
    if(x < 0){
        dx = 1;
    }

    if(x > 640-18){
        dx = -1;
    }

    SDL_SetRenderDrawColor(renderer, 230, 230, 230, 255);
    SDL_RenderClear(renderer);


    int iClip = frame / 8;
    if(dx < 0)
        iClip = 5 + frame / 8;

    SDL_Rect *clip = mario->getSpriteClip(iClip);
    mario->render(Coordinate (x, 170), renderer, clip);

    frame++;
    if(frame / 8 >= mario->getWalkingAnimationFrames()){
        frame = 0;
    }

    SDL_RenderPresent(renderer);
    };

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    renderer = nullptr;
    window = nullptr;
    SDL_Quit();
    IMG_Quit();
    TTF_Quit();*/
    Engine *pEngine = new Engine("Mario");
    pEngine->init();

    pEngine->run();
    delete pEngine;

    return 0;
}
