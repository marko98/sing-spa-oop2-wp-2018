#include "Engine.h"

Engine::Engine(){
    eTitle = "";
}

Engine::Engine(const string &gameTitle): eTitle(gameTitle){

}

Engine::~Engine(){
    free();
}

void Engine::init(){
    SDL_Init(SDL_INIT_VIDEO);
    eWindow = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 640, 0);
    eRenderer = SDL_CreateRenderer(eWindow, -1, 0);
    TTF_Init();
    eFont = TTF_OpenFont("Super-Mario-World.ttf", 20);
}

void Engine::free(){
    SDL_DestroyRenderer(eRenderer);
    SDL_DestroyWindow(eWindow);
    eWindow = nullptr;
    eRenderer = nullptr;
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void Engine::drawText(string text, Coordinate coordinate, TTF_Font* eFont, SDL_Renderer* eRenderer){
    SDL_Color white = {255, 255, 255};

    stringstream ss;
    ss << text ;
    SDL_Surface *surface = nullptr;
    SDL_Texture *texture = nullptr;

    surface = TTF_RenderText_Solid(eFont, ss.str().c_str(), white);
    texture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_Rect message;
    message.x = coordinate.getCoordinateX();
    message.y = coordinate.getCoordinateY();
    message.h = surface->h;
    message.w = surface->w;

    SDL_RenderCopy(eRenderer, texture, NULL, &message);
}

bool Engine::run(){
    bool quit = false;
    SDL_Event event;

    Sprite *mario = new Sprite;
    mario->loadFromFile("resources/sheets/marioWalking.png", eRenderer);
    mario->loadSpriteWalking();

    int dx = 1;
    int frame = 0;
    int x = 0;

    while(!quit){
    SDL_Delay(10);
    SDL_PollEvent(&event);
    switch (event.type){
        case SDL_QUIT:
            quit = true;
            break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_LEFT:
                        dx = -1;
                        break;
                    case SDLK_RIGHT:
                        dx = 1;
                        break;
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    default:
                        break;
                };
                break;
    }
    // move objects
    x = x + dx;
    if(x < 0){
        dx = 1;
    }

    if(x > 640-2*18){
        dx = -1;
    }

    SDL_SetRenderDrawColor(eRenderer, 230, 230, 230, 255);
    SDL_RenderClear(eRenderer);


    int iClip = frame / 8;
    if(dx < 0)
        iClip = 5 + frame / 8;

    SDL_Rect *clip = mario->getSpriteClip(iClip);
    mario->render(Coordinate (x, 170), eRenderer, clip);

    frame++;
    if(frame / 8 >= mario->getWalkingAnimationFrames()){
        frame = 0;
    }

    SDL_RenderPresent(eRenderer);
    };
}
