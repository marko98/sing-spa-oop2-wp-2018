#include "Mario.h"

const int walkingAnimationFramesMario = 5;
SDL_Rect spriteMarioClips[2*walkingAnimationFramesMario];

Mario::Mario(): Sprite(){
    path = "resources/sheets/mario.png";
    jump = 17;
}

Mario::Mario(Coordinate position, Vector velocity): Sprite(position, velocity){
    path = "resources/sheets/mario.png";
    jump = 17;
}

string Mario::getPath(){
    return path;
}

void Mario::loadSpriteWalking(){
    for(int i = 0; i < 5; i++){
        spriteMarioClips[i].x = 7 + (5 + i)*40;
        spriteMarioClips[i].y = 156;
        spriteMarioClips[i].w = 18;
        spriteMarioClips[i].h = 29;
    }
    for(int i = 0; i < 5; i++){
        spriteMarioClips[5+i].x = 7 + i*40;
        spriteMarioClips[5+i].y = 156;
        spriteMarioClips[5+i].w = 18;
        spriteMarioClips[5+i].h = 29;
    }
}

SDL_Rect * Mario::getSpriteClip(int iClip){
 return &spriteMarioClips[iClip];
}

int Mario::getWalkingAnimationFrames(){
    return walkingAnimationFramesMario;
}

void Mario::update(Background *background){
    // update position ----------------------
    position.addVector(velocity);

    // gravity ----------------------------
    if(position.getCoordinateY()+height < background->getStartCoordinate().getCoordinateY()){
        position.addVector(background->getGravity());

        if(velocity.getVectorY() > 0){

            int i;

            if(frameJump < 2){
                i = 2;
            } else if (frameJump < 3){
                i = 6;
            } else if (frameJump < 6){
                i = 12;
            }

            velocity-i;
            frameJump++;
        } else if (velocity.getVectorY() == 0 && frameJump != 0){
            frameJump = 0;
        }
    }
}

void Mario::handleEvent(SDL_Event *pEvent){

    SDL_Event event = *pEvent;
    switch (event.type){
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym){
                case SDLK_LEFT:
                    velocity = Vector(-1, velocity.getVectorY());
                    break;
                case SDLK_RIGHT:
                    velocity = Vector(1, velocity.getVectorY());
                    break;
                case SDLK_SPACE:
                    //cout << jump << endl;
                    velocity = Vector(velocity.getVectorX(), 2*jump);
                    //cout << velocity.getVectorX() << velocity.getVectorY() << endl;
                    break;
                default:
                    break;
            };
            break;
        default:
            break;
    }
}
