#ifndef DRAWABLE_H_INCLUDED
#define DRAWABLE_H_INCLUDED

#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <fstream>
#include <vector>

using namespace std;

class Drawable{
private:

public:
    virtual void draw(SDL_Renderer *renderer) = 0;
};

#endif // DRAWABLE_H_INCLUDED
