#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

using namespace std;

class Vector{
private:
    int vectorX, vectorY;
public:
    Vector();
    Vector(int vectorX, int vectorY);
    void operator+(Vector v);
    void operator-(int downFor);

    int getVectorX();
    void setVectorX(int x);

    int getVectorY();
    void setVectorY(int y);
};

#endif // VECTOR_H_INCLUDED
