#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

#include "Shape.h"
#include "Tacka.h"
#include "Line.h"
#include "Circle.h"
#include "Reket.h"
#include "Score.h"

using namespace std;

int main(int argc, char ** argv)
{
    bool quit = false;
    SDL_Event event;

    vector<Shape *> lista;
    lista.push_back(new Line(Tacka(20, 200), Tacka(620, 200)));
    lista.push_back(new Circle(Tacka(320, 240), 30));
    lista.push_back(new Line(Tacka(20, 280), Tacka(620, 280)));

    Reket *reket = new Reket(Line(Tacka(20, 230), Tacka(60, 230)));


    // init SDL ======================================

    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow("Programiranje 2 Singidunum",
                                            SDL_WINDOWPOS_CENTERED,
                                            SDL_WINDOWPOS_CENTERED,
                                            640, 480, 0);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    int ret = TTF_Init();
    TTF_Font *font = TTF_OpenFont("lazy.ttf", 24);

    // ================================================

    int dx = 1;
    int dy = 0;
    int nastaviUPravcu = 0;

    int score = 0;
    int brojPokusaja = 0;

    // main loop ======================================

    while(!quit){
        SDL_Delay(2);
        SDL_PollEvent(&event);
        // event handling ------------------

        switch(event.type){
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_LEFT:
                        dx = -1;
                        break;
                    case SDLK_RIGHT:
                        dx = 1;
                        break;
                    case SDLK_UP:
                        dy = -1;
                        break;
                    case SDLK_DOWN:
                        dy = 1;
                        break;
                    case SDLK_r:
                        brojPokusaja = 0;
                        score = 0;
                        break;
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    case SDLK_SPACE:
                        if (dx != 0){
                            nastaviUPravcu = dx;
                            dx = 0;
                            brojPokusaja ++;
                            if(reket->getGornjiDeoReketa().getPocetna().getX() > 290 &&  reket->getGornjiDeoReketa().getKrajnja().getX() < 350){
                                score ++;
                            }
                        } else {
                            dx = nastaviUPravcu;
                        }
                        break;
                    default:
                        break;
                }
                break;
        }
        reket->move(dx, dy);
        if(reket->getGornjiDeoReketa().getPocetna().getX() < 0){
            dx = 1;
        }

        if(reket->getGornjiDeoReketa().getPocetna().getY() < 200){
            dy = 1;
        }

        if(reket->getGornjiDeoReketa().getPocetna().getY() > 260){
            dy = -1;
        }

        if(reket->getGornjiDeoReketa().getKrajnja().getX() > 640){
            dx = -1;
        }

        // clear window --------------------
        SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
        SDL_RenderClear(renderer);

        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
        for(int i = 0; i < lista.size(); i++){
            lista[i]->draw(renderer);
        }

        SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
        reket->draw(renderer);

        Score().drawScore(score, brojPokusaja, font, renderer);
        // drawScore(score, total, font, renderer);

        // render window -------------------
        SDL_RenderPresent(renderer);
    }

    // cleanup SDL ======================================

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();

    return 0;
}
