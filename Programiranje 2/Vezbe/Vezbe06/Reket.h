#ifndef REKET_H_INCLUDED
#define REKET_H_INCLUDED

#include <SDL2/SDL.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

#include "Line.h"

using namespace std;

class Reket: public Line{
private:
    Line gornjiDeoReketa;
public:
    Reket(Line gornjiDeoReketa): Line(), gornjiDeoReketa(gornjiDeoReketa){};

    virtual void draw(SDL_Renderer *renderer){
        SDL_RenderDrawLine(renderer, gornjiDeoReketa.getPocetna().getX(), gornjiDeoReketa.getPocetna().getY(), gornjiDeoReketa.getKrajnja().getX(), gornjiDeoReketa.getKrajnja().getY());
        SDL_RenderDrawLine(renderer, gornjiDeoReketa.getPocetna().getX(), gornjiDeoReketa.getPocetna().getY()+20, gornjiDeoReketa.getKrajnja().getX(), gornjiDeoReketa.getKrajnja().getY()+20);
    };

    virtual void move(int dX, int dY){

        gornjiDeoReketa.move(dX, dY);
    };

    Line getGornjiDeoReketa(){ return gornjiDeoReketa; };

};

#endif // REKET_H_INCLUDED
