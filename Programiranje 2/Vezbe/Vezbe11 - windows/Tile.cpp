#include "Tile.h"

Tile::Tile(Coordinate coordinate, int w, int h){
    srcTileRect = new SDL_Rect;
    srcTileRect->x = coordinate.getCoordinateX();
    srcTileRect->y = coordinate.getCoordinateY();
    srcTileRect->w = w;
    srcTileRect->h = h;
}

Tile::Tile(istream &inputStream){
    srcTileRect = new SDL_Rect;
    inputStream >> srcTileRect->x >> srcTileRect->y >> srcTileRect->w >> srcTileRect->h;
}

SDL_Rect * Tile::getSrcTileRect(){
    return srcTileRect;
}

int Tile::getSrcTileRectHeight(){
    return srcTileRect->h;
}

int Tile::getSrcTileRectWidth(){
    return srcTileRect->w;
}
