#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Sprite.h"
#include "Coordinate.h"
#include "Vector.h"
#include "Engine.h"

using namespace std;

int main(int argc, char ** argv)
{
    Engine *pEngine = new Engine("Mario");
    pEngine->init();

    pEngine->run();
    delete pEngine;

    return 0;
}
