#ifndef MARIO_H_INCLUDED
#define MARIO_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Sprite.h"
#include "Background.h"
#include "Coordinate.h"
#include "Vector.h"

using namespace std;

class Mario: public Sprite{
private:
    string path;
public:
    Mario();
    Mario(Coordinate position, Vector velocity);
    string getPath();

    virtual void loadSpriteWalking();
    virtual SDL_Rect *getSpriteClip(int iClip);
    virtual int getWalkingAnimationFrames();
    virtual void update(Background *background);
    virtual void handleEvent(SDL_Event *pEvent);
};

#endif // MARIO_H_INCLUDED
