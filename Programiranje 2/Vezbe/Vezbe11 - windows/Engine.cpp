#include "Engine.h"

Engine::Engine(){
    eTitle = "";
}

Engine::Engine(const string &gameTitle): eTitle(gameTitle){

}

Engine::~Engine(){
    free();
}

void Engine::init(){
    SDL_Init(SDL_INIT_VIDEO);
    // eWindow = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 432, SDL_WINDOW_RESIZABLE);
    eWindow = SDL_CreateWindow(eTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 432, 0);
    eRenderer = SDL_CreateRenderer(eWindow, -1, SDL_RENDERER_ACCELERATED);
    TTF_Init();
    eFont = TTF_OpenFont("Super-Mario-World.ttf", 20);
}

void Engine::free(){
    SDL_DestroyRenderer(eRenderer);
    SDL_DestroyWindow(eWindow);
    eWindow = nullptr;
    eRenderer = nullptr;
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void Engine::drawText(string text, Coordinate coordinate, TTF_Font* eFont, SDL_Renderer* eRenderer){
    SDL_Color white = {255, 255, 255};

    stringstream ss;
    ss << text ;
    SDL_Surface *surface = nullptr;
    SDL_Texture *texture = nullptr;

    surface = TTF_RenderText_Solid(eFont, ss.str().c_str(), white);
    texture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_Rect message;
    message.x = coordinate.getCoordinateX();
    message.y = coordinate.getCoordinateY();
    message.h = surface->h;
    message.w = surface->w;

    SDL_RenderCopy(eRenderer, texture, NULL, &message);
}

bool Engine::run(){
    bool quit = false;
    SDL_Event event;

    Sprite *mario = new Mario(Coordinate (20, 300-29), Vector (1, 0));
    mario->loadFromFile(mario->getPath(), eRenderer);
    mario->loadSpriteWalking();

    Background *pBackground = new Background();

    // SLIKA LEVELA 0 ----------------------
    ifstream tilesetImageFileLevel0("resources/tilesets/tilesets_level0_image.txt");
    TileSet *tSIFL0 = new TileSet(tilesetImageFileLevel0, eRenderer);

    ifstream level0ImageFile("resources/levels/level0_image.txt");
    Level *l0IF = new Level(level0ImageFile, tSIFL0);

    drawables.push_back(l0IF);
    // --------------------------------

    /*ifstream tilesetFileLevel0("resources/tilesets/tilesets_level0.txt");
    TileSet *tSFL0 = new TileSet(tilesetFileLevel0, eRenderer);

    ifstream level0File("resources/levels/level0_tiles.txt");
    Level *l0F = new Level(level0File, tSFL0);

    drawables.push_back(l0F);*/

    int frame = 0;
    int startTime, endTime;
    int maxDelay = 16;

    //drawables[0]->setPos(50, 50);

    while(!quit){
        startTime = SDL_GetTicks();
        SDL_PollEvent(&event);

        switch (event.type){
            case SDL_QUIT:
                quit = true;
                break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){
                        case SDLK_ESCAPE:
                            quit = true;
                            break;
                        default:
                            break;
                    };
                    break;
        }
        mario->handleEvent(&event);
        mario->update(pBackground);

        SDL_SetRenderDrawColor(eRenderer, 230, 230, 230, 255);
        SDL_RenderClear(eRenderer);

        for(size_t i = 0; i < drawables.size(); i++){
            drawables[i]->draw(eRenderer);
            //drawables[i]->move(-1, 0);
        }

        SDL_SetRenderDrawColor(eRenderer, 0, 200, 0, 255);
        pBackground->render(eRenderer);

        int iClip = frame / 8;

        mario->render(mario->getPosition(), eRenderer, mario->getSpriteClip(iClip));

        frame++;
        if(frame / 8 >= mario->getWalkingAnimationFrames()){
            frame = 0;
        }

        SDL_RenderPresent(eRenderer);
        endTime = SDL_GetTicks();
        if(endTime - startTime < maxDelay){
            SDL_Delay(maxDelay - (endTime - startTime));
        }
    }
}
