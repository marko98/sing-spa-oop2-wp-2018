#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <sstream>
#include <math.h>
#include <vector>

#include "Igrac.h"

using namespace std;

SDL_Window *window;
SDL_Renderer *renderer;
TTF_Font *font;

Igrac gSpriteSheetTexture;
const int WALKING_ANIMATION_FRAMES = 8;
SDL_Rect gSpriteClips[ 2*WALKING_ANIMATION_FRAMES ]; // lista likova

void close();
void init();
void loadMedia();

int main(int argc, char ** argv)
{

    //cout << 7 / 8 << endl;
    bool quit = false;
    SDL_Event event;

    // init SDL ========================
    init();
    loadMedia();

    int dx = 1;
    int frame = 0;
    int x = 0;

    // main loop ================================

    while(!quit){
        SDL_Delay(10);
        SDL_PollEvent(&event);
        // event handling ------------------
        switch(event.type){
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_LEFT:
                        dx = -1;
                        break;
                    case SDLK_RIGHT:
                        dx = 1;
                        break;
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    default:
                        break;
                };
            break;
        };
        // move objects
        x = x + dx;
        if(x < 0){
            dx = 1;
        }

        if(x > 640-108){
            dx = -1;
        }

        // clear window --------------------
        SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
        SDL_RenderClear(renderer);

        int iClip = frame / 8;
        if(dx < 0)
            iClip = 8 + frame / 8;
        SDL_Rect *currentClip = &gSpriteClips[iClip];
        gSpriteSheetTexture.render(x, 170, currentClip);

        frame++;
        if(frame / 8 >= WALKING_ANIMATION_FRAMES){
            frame = 0;
        }


        // render window -------------------
        SDL_RenderPresent(renderer);
    }

    // cleanup SDL ==============================
    close();

    return 0;
}

void loadMedia(){
    gSpriteSheetTexture.loadFromFile("boy.png");
    for(int i = 0; i < 8; i++){
        gSpriteClips[i].x = i*108;
        gSpriteClips[i].y = 0;
        gSpriteClips[i].w = 108;
        gSpriteClips[i].h = 140;
    }
    for(int i = 0; i < 8; i++){
        gSpriteClips[8+i].x = i*108;
        gSpriteClips[8+i].y = 140;
        gSpriteClips[8+i].w = 108;
        gSpriteClips[8+i].h = 140;
    }
};

void init(){
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("Programiranje 2",
                                            SDL_WINDOWPOS_CENTERED,
                                            SDL_WINDOWPOS_CENTERED,
                                            640, 480, 0);
    renderer = SDL_CreateRenderer(window, -1, 0);

    int ret = TTF_Init();
    font = TTF_OpenFont("lazy.ttf", 24);
};

void close(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = nullptr;
    renderer = nullptr;

    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
};
