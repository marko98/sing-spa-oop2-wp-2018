#include "Igrac.h"

extern SDL_Renderer *renderer;

bool Igrac::loadFromFile(string path){
    free();
    SDL_Texture *novaTeksturaIgraca = nullptr;
    SDL_Surface *ucitanaPovrsina = IMG_Load(path.c_str());

    if(ucitanaPovrsina == NULL){
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	} else {
		SDL_SetColorKey(ucitanaPovrsina, SDL_TRUE, SDL_MapRGB(ucitanaPovrsina->format, 0, 0xFF, 0xFF ));

		novaTeksturaIgraca = SDL_CreateTextureFromSurface(renderer, ucitanaPovrsina);
        if(novaTeksturaIgraca == nullptr){
            printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        } else {
            igracDuzina = ucitanaPovrsina->w;
            igracVisina = ucitanaPovrsina->h;
        }
        SDL_FreeSurface(ucitanaPovrsina);
    }

    igracTekstura = novaTeksturaIgraca;
    return igracTekstura != nullptr;

}

void Igrac::free(){
    if (igracTekstura != nullptr){
        SDL_DestroyTexture(igracTekstura);
        igracTekstura = nullptr;
        igracDuzina = 0;
        igracVisina = 0;
    }
}

void Igrac::render(int x, int y, SDL_Rect *clip){
    SDL_Rect renderQuad;
    renderQuad.x = x;
    renderQuad.y = y;
    renderQuad.h = igracVisina;
    renderQuad.w = igracDuzina;

    if(clip != nullptr){
        renderQuad.h = clip->h;
        renderQuad.w = clip->w;
    }

    SDL_RenderCopy(renderer, igracTekstura, clip, &renderQuad);
}
