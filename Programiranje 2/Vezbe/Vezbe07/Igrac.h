#ifndef IGRAC_H_INCLUDED
#define IGRAC_H_INCLUDED

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <sstream>
#include <math.h>
#include <vector>

using namespace std;

class Igrac{
private:
    SDL_Texture *igracTekstura;
    int igracDuzina, igracVisina;
public:
    Igrac(){
        this->igracTekstura = nullptr;
        this->igracDuzina = 0;
        this->igracVisina = 0;
    };
    ~Igrac(){
        free();
    };

    bool loadFromFile(string path);
    void free();
    void render(int x, int y, SDL_Rect *clip = nullptr);
};

#endif // IGRAC_H_INCLUDED
