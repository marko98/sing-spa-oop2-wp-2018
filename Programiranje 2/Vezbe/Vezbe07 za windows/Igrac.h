#ifndef IGRAC_H_INCLUDED
#define IGRAC_H_INCLUDED

#include <iostream>
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <sstream>
#include <math.h>
#include <vector>

using namespace std;

class Igrac{
private:
    SDL_Texture *igracTekstura;
    int igracDuzina, igracVisina;
    int igracTrenutniClipDuzina, igracTrenutniClipVisina;
public:
    Igrac(){
        this->igracTekstura = nullptr;
        this->igracDuzina = 0;
        this->igracVisina = 0;
    };
    ~Igrac(){
        free();
    };

    bool loadFromFile(string path);
    void free();
    void render(int x, int y, SDL_Rect *clip = nullptr);

    int getIgracDuzina(){ return igracDuzina; };
    int getIgracVisina(){ return igracVisina; };

    int getIgracTrenutniClipDuzina(){ return igracTrenutniClipDuzina; };
    int getIgracTrenutniClipVisina(){ return igracTrenutniClipVisina; };
};

#endif // IGRAC_H_INCLUDED
