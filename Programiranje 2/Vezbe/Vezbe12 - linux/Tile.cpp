#include "Tile.h"

Tile::Tile(Coordinate coordinate, int h, int w){
    srcTileRect->x = coordinate.getCoordinateX();
    srcTileRect->y = coordinate.getCoordinateY();
    srcTileRect->h = h;
    srcTileRect->w = w;
}

Tile::Tile(istream &inputStream){
    inputStream >> srcTileRect->x >> srcTileRect->y >> srcTileRect->w >> srcTileRect->h;
}

SDL_Rect * Tile::getSrcTileRect(){
    return srcTileRect;
}

int Tile::getSrcTileRectHeight(){
    return srcTileRect->h;
}

int Tile::getSrcTileRectWidth(){
    return srcTileRect->w;
}
