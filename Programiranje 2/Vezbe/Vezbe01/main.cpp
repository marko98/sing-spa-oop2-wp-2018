#include <iostream>

using namespace std;

int main()
{
    int *pbroj = nullptr;

    int broj1 = 5;
    pbroj = &broj1;
    *pbroj += 10;
    int broj2 = 52;
    //cout << broj1 << endl;
    pbroj = &broj2;
    *pbroj = 12;
    //cout << broj2 << endl;
    (*pbroj)++;
    //cout << broj2 << endl;

    float fniz[4] = {0.5, 1.5, 3.4, 8.9};
    float *pfniz = nullptr;
    pfniz = fniz;
    //cout << *(pfniz + 1) << endl;

    char sniz[] = "oVO JE tEsT";
    char *psniz = nullptr;
    psniz = sniz;
    // psniz[i] ili *(p + i)
    while(*psniz){
        if(islower(*psniz)){
            *psniz = toupper(*psniz);
        } else if(isupper(*psniz)){
            *psniz = tolower(*psniz);
        }

        psniz++;
    }
    cout << sniz << endl;

    pfniz = fniz;
    for(int i = 0; i < 4; i++){
        cout << pfniz[i] << endl;
    }

    cout << "-------" << endl;
    cout << *pfniz << endl;


    return 0;
}
