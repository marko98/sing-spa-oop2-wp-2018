#include "Pravougaonik.h"

Pravougaonik::Pravougaonik(double stranicaA, double stranicaB): stranicaA(stranicaA), stranicaB(stranicaB){}

double Pravougaonik::povrsina(){
    return stranicaA*stranicaB;
}

double Pravougaonik::obim(){
    return 2*stranicaA+2*stranicaB;
}

void Pravougaonik::iscrtavanje(){
    cout << "Pravougaonik stranice a: " << stranicaA << ", b: " << stranicaB << endl;
}

double Pravougaonik::getStranicaA(){
    return stranicaA;
}

void Pravougaonik::setStranicaA(double stranicaA){
    if(stranicaA < 0){
        stranicaA = 0;
    }
    this->stranicaA = stranicaA;
}

double Pravougaonik::getStranicaB(){
    return stranicaB;
}

void Pravougaonik::setStranicaB(double stranicaB){
    if(stranicaB < 0){
        stranicaB = 0;
    }
    this->stranicaB = stranicaB;
}

