#ifndef KVADRAT_H_INCLUDED
#define KVADRAT_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Oblik.h"

using namespace std;

class Kvadrat: public Oblik{
private:
    double stranica;
public:
    Kvadrat(double stranica);
    virtual double povrsina();
    virtual double obim();
    virtual void iscrtavanje();

    double getStranica();
    void setStranica(double stranica);
};

#endif // KVADRAT_H_INCLUDED
