#include "Kvadrat.h"

Kvadrat::Kvadrat(double stranica): stranica(stranica){}

double Kvadrat::povrsina(){
    return stranica*stranica;
}

double Kvadrat::obim(){
    return 4*stranica;
}

void Kvadrat::iscrtavanje(){
    cout << "Kvadrat stranice: " << stranica << endl;
}

double Kvadrat::getStranica(){
    return stranica;
}

void Kvadrat::setStranica(double stranica){
    if(stranica < 0){
        stranica = 0;
    }
    this->stranica = stranica;
}
