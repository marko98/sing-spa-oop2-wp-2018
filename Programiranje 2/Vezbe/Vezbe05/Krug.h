#ifndef KRUG_H_INCLUDED
#define KRUG_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include "cmath"

#include "Oblik.h"

using namespace std;

class Krug: public Oblik{
private:
    double poluprecnik;
public:
    Krug(double poluprecnik);
    virtual double povrsina();
    virtual double obim();
    virtual void iscrtavanje();

    double getPoluprecnik();
    void setPoluprecnik(double poluprecnik);
};

#endif // KRUG_H_INCLUDED
