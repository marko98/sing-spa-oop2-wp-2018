#include "Krug.h"

Krug::Krug(double poluprecnik): poluprecnik(poluprecnik){}

double Krug::povrsina(){
    return poluprecnik*poluprecnik*(atan(1)*4);
}

double Krug::obim(){
    return 2*poluprecnik*(atan(1)*4);
}

void Krug::iscrtavanje(){
    cout << "Krug poluprecnika: " << poluprecnik << endl;
}

double Krug::getPoluprecnik(){
    return poluprecnik;
}

void Krug::setPoluprecnik(double poluprecnik){
    if(poluprecnik < 0){
        poluprecnik = 0;
    }
    this->poluprecnik = poluprecnik;
}
