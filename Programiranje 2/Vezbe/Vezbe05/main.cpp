#include <iostream>
#include <string>
#include <vector>

#include "Oblik.h"
#include "Krug.h"
#include "Pravougaonik.h"
#include "Kvadrat.h"
#include "Recept.h"
#include "Sastojak.h"
#include "SastojakIGramaza.h"

using namespace std;

int main()
{
    Oblik *poblik = new Krug(5);
    Krug *pkrug = new Krug(8);
    Pravougaonik *ppravougaonik = new Pravougaonik(5, 8);
    Kvadrat *pkvadrat = new Kvadrat(55);

    poblik->iscrtavanje();
    pkrug->getPoluprecnik();

    ppravougaonik->iscrtavanje();

    pkvadrat->iscrtavanje();

    cout << "-----------------------" << endl;

    Sastojak sastojak1 ("jaja", 20, 50);
    Sastojak *psastojak = &sastojak1;
    Recept recept1 ("Obrok1");

    SastojakIGramaza *psastojakIGramaza = new SastojakIGramaza(psastojak, 200);

    recept1.dodavanjeSastojka(psastojakIGramaza);
    recept1.uklanjanjeSastojka(psastojakIGramaza);
    recept1.predstaviSe();



    return 0;
}
