#ifndef SASTOJAKIGRAMAZA_H_INCLUDED
#define SASTOJAKIGRAMAZA_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include "Sastojak.h"

using namespace std;

class SastojakIGramaza: public Sastojak{
private:
    Sastojak *sastojak;
    double gramaza;
public:
    SastojakIGramaza(Sastojak *sastojak, double gramaza);
    double getGramaza();
    void setGramaza(double gramaza);
    Sastojak* getSastojak();
};

#endif // SASTOJAKIGRAMAZA_H_INCLUDED
