#ifndef SASTOJAK_H_INCLUDED
#define SASTOJAK_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Sastojak{
private:
    string naziv;
    double energetskaVrednost, cena;
public:
    Sastojak(){};
    Sastojak(string naziv, double energetskaVrednost, double cena): naziv(naziv), energetskaVrednost(energetskaVrednost), cena(cena){};

    string getNaziv() { return naziv; };
    double getEnergetskaVrednost(){ return energetskaVrednost; };
    double getCena(){ return cena; };
};

#endif // SASTOJAK_H_INCLUDED
