#ifndef PRAVOUGAONIK_H_INCLUDED
#define PRAVOUGAONIK_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Oblik.h"

using namespace std;

class Pravougaonik: public Oblik{
private:
    double stranicaA, stranicaB;
public:
    Pravougaonik(double stranicaA, double stranicaB);
    virtual double povrsina();
    virtual double obim();
    virtual void iscrtavanje();

    double getStranicaA();
    void setStranicaA(double stranicaA);
    double getStranicaB();
    void setStranicaB(double stranicaB);
};

#endif // PRAVOUGAONIK_H_INCLUDED
