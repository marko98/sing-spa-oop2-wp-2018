#include "SastojakIGramaza.h"

SastojakIGramaza::SastojakIGramaza(Sastojak *sastojak, double gramaza): sastojak(sastojak), gramaza(gramaza){}

double SastojakIGramaza::getGramaza(){
    return gramaza;
}

void SastojakIGramaza::setGramaza(double gramaza){
    if(gramaza < 0){
        gramaza = 0;
    }
    this->gramaza = gramaza;
}

Sastojak* SastojakIGramaza::getSastojak(){
    return sastojak;
}
