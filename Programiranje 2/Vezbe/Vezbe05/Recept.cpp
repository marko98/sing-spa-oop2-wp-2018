#include "Recept.h"

Recept::Recept(string naziv): naziv(naziv){}

void Recept::uklanjanjeSastojka(SastojakIGramaza *sastojakIGramaza){
    for(unsigned int i = 0; i < sastojci.size() ; i++){
        if(sastojci[i]->getSastojak() == sastojakIGramaza->getSastojak()){
            sastojci.erase(sastojci.begin()+i);
            cout << "Izbrisan sastojak." << endl;
        }
    }

}

void Recept::dodavanjeSastojka(SastojakIGramaza *sastojakIGramaza){
    sastojci.push_back(sastojakIGramaza);
    cout << "Dodat sastojak." << endl;
}


double Recept::energetskaVrednostJela(){
    double energetskaVrednostJela;
    for(unsigned int i = 0; i < sastojci.size() ; i++){
        energetskaVrednostJela = energetskaVrednostJela + sastojci[i]->getSastojak()->getEnergetskaVrednost() * sastojci[i]->getGramaza()/100;
    }
    return energetskaVrednostJela;
}

double Recept::cenaJela(){
    double cenaJela;
    for(unsigned int i = 0; i < sastojci.size(); i++){
        cenaJela = cenaJela + sastojci[i]->getSastojak()->getCena() * sastojci[i]->getGramaza()/100;
    }
    return cenaJela;
}

void Recept::predstaviSe(){
    if(sastojci.size() != 0){
        for(unsigned int i = 0; i < sastojci.size(); i++){
            cout << "Naziv sastojka: " << sastojci[i]->getSastojak()->getNaziv() << endl;
        }
    } else {
        cout << "lista sastojaka je prazna" << endl;
    }

}
