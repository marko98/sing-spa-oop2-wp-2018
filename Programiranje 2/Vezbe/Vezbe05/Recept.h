#ifndef RECEPT_H_INCLUDED
#define RECEPT_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include "SastojakIGramaza.h"

using namespace std;

class Recept{
private:
    string naziv;
    vector <SastojakIGramaza*> sastojci;
public:
    Recept(string naziv);
    void uklanjanjeSastojka(SastojakIGramaza *sastojakIGramaza);
    void dodavanjeSastojka(SastojakIGramaza *sastojakIGramaza);
    double energetskaVrednostJela();
    double cenaJela();
    void predstaviSe();
};

#endif // RECEPT_H_INCLUDED
