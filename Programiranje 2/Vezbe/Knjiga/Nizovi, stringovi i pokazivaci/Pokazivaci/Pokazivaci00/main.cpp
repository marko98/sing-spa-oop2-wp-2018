#include <iostream>

using namespace std;

int main()
{
    cout << "Nizovi, stringovi i pokazivaci" << endl;
    cout << "Pokazivaci - strana 76\n" << endl;
    int slucaj;

    // tip *promenljiva
    // int *pbroj = nullptr; je isto sto i int *pbroj = 0;
    int *pbroj = nullptr;

    cout << "Unesite slucaj koji zelite da vidite: " << endl;
    cin >> slucaj;

    switch(slucaj){
        case 1: {
            cout << "------------------------" << endl;

            if(!pbroj){
                cout << "pbroj ne pokazuje ni na sta" << endl;
            }

            int broj = 99;
            int vrednost;
            pbroj = &broj;
            vrednost = *pbroj;

            cout << vrednost << endl;

            cout << "------------------------" << endl;
            break;
        }
        case 2: {
            cout << "------------------------" << endl;
            cout << "1. Dodeljivanje ili menjanje vrednosti promenljivoj indirektno preko pokazivaca." << endl;

            int broj1 = 55, broj2 = 99;
            pbroj = &broj1;
            (*pbroj) += 11;
            cout << broj1 << endl;
            pbroj = &broj2;
            broj1 = (*pbroj) * 10;
            cout << broj1 << endl;
            *pbroj = 100;

            cout << "------------------------" << endl;
            break;
        }
        case 3: {
            cout << "------------------------" << endl;

            /*int *p;
            double f;
            p = &f;*/
            cout << "nastace greska - dva pokazivaca moraju da pokazuju na isti tip da bi se dodeljivali jedan drugome" << endl;


            cout << "------------------------" << endl;
            break;
        }
        case 4: {
            cout << "------------------------" << endl;
            cout << "2. Dodeljivanje ili menjanje vrednosti promenljivoj indirektno preko pokazivaca." << endl;

            int broj = 10;
            pbroj = &broj;
            cout << "Vrednost promenljive broj: " << broj << endl;
            (*pbroj)++; // broj se inkrementira preko pokazivaca
            cout << "(*pbroj)++; // broj se inkrementira preko pokazivaca" << endl;
            cout << "Vrednost promenljive broj: " << broj << endl;

            cout << "------------------------" << endl;
            break;
        }
        case 5: {
            cout << "------------------------" << endl;
            cout << "Pokazivacka aritmrtika\nSapokazivacima se koriste samo 4 aritmeticka operatora: ++,--,+,-\n" << endl;
            int niz[5] = {0, 1, 2, 3, 4};
            pbroj = niz; // pokazivac na prvi element
            cout << "Adresa 0 clana niza u radnoj memoriji: " << pbroj << ", vrednost: " << *pbroj << endl;
            pbroj++;
            cout << "Adresa 1 clana niza u radnoj memoriji: " << pbroj << ", vrednost: " << *pbroj << endl;
            pbroj++;
            cout << "Adresa 2 clana niza u radnoj memoriji: " << pbroj << ", vrednost: " << *pbroj << endl;
            pbroj++;
            cout << "Adresa 3 clana niza u radnoj memoriji: " << pbroj << ", vrednost: " << *pbroj << endl;
            pbroj++;
            cout << "Adresa 4 clana niza u radnoj memoriji: " << pbroj << ", vrednost: " << *pbroj << endl;
            pbroj = pbroj - 4;

            cout << "\n ILI \n" << endl;
            cout << "Adresa 0 clana niza u radnoj memoriji: " << pbroj << ", vrednost: " << *pbroj << endl;
            cout << "Adresa 1 clana niza u radnoj memoriji: " << pbroj + 1 << ", vrednost: " << *(pbroj + 1) << endl;
            cout << "Adresa 2 clana niza u radnoj memoriji: " << pbroj + 2 << ", vrednost: " << *(pbroj + 2) << endl;
            cout << "Adresa 3 clana niza u radnoj memoriji: " << pbroj + 3 << ", vrednost: " << *(pbroj + 3) << endl;
            cout << "Adresa 4 clana niza u radnoj memoriji: " << pbroj + 4 << ", vrednost: " << *(pbroj + 4) << endl;

            cout << "------------------------" << endl;
            break;
        }
        case 6: {
            cout << "------------------------" << endl;
            cout << "Tehnika indeksiranja niza" << endl;

            char str[] = "Ovo je Test";
            char *slovo = nullptr;
            slovo = str; // isto sto i slovo = str[0]
            // slovo[i] je isto sto i *(slovo + i)
            int duzina_niza_karaktera;
            for(int i = 0; str[i]; i++){
                if(isupper(slovo[i])){
                    slovo[i] = tolower(slovo[i]);
                } else if(islower(slovo[i])){
                    slovo[i] = toupper(slovo[i]);
                }
                duzina_niza_karaktera = i;
            }
            // duzina_niza_karaktera se odnosi na posednji element u nizu, posto u for petlji na kraju imamo i++ nakon izlaska iz petlje
            // pod uslovom da je i definisan ranije pokazivace na posle poslednje elementa u nizu tj. na kraj stringa a to je " " tj. 0(false)

            // isprobati prethodno komentarisano -> duzina_niza_karaktera = duzina_niza_karaktera + 1
            if(slovo[duzina_niza_karaktera] == 0){
                cout << "Na kraju niza nalazi se 0. Ona se tumaci kao false!" << endl;
            }
            cout << "\nslovo[duzina_niza_karaktera] ima vrednost (" << slovo[duzina_niza_karaktera] << "), ta u zagradi. Inace slovo[duzina_niza_karaktera] ukazuje na kraj stringa tj. 0 \n" << endl;
            cout << str << endl;

            cout << "------------------------" << endl;
            break;
        }
        case 7: {
            cout << "------------------------" << endl;
            cout << "Isti program kao prethodni primer, samo napisan pomocu pokazivaca.\n" << endl;

            char str[] = "Ovo je Test";
            char *slovo = nullptr;
            slovo = str;

            // slovo[i] je isto sto i *(slovo + i)
            while(*slovo){
                if(islower(*slovo)){
                    *slovo = toupper(*slovo);
                } else if(isupper(*slovo)){
                    *slovo = tolower(*slovo);
                }

                slovo++;
            }
            cout << str << endl;
            cout << "Znakom 0 se zavrsava niz str." << endl;

            cout << "------------------------" << endl;
            break;
        }
        case 8: {
            cout << "------------------------" << endl;
            cout << "Dinamicka alokacija memorije" << endl;
            cout << "Dinamicko alociranje memorije zahteva se operatorom new" << endl;

            cout << "-------" << endl;
            int *p1 = new int;
            cout << p1 << endl;
            delete p1;
            cout << p1 << endl;
            p1 = new int;
            cout << p1 << endl;
            cout << "-------\n" << endl;

            cout << "-------" << endl;
            int *p2 = new int[100];
            // isto sto i if(p == NULL)
            if(!p2){
                cout << "greska u alociranju memorije" << endl;
            }

            for(int i = 0; i < 100; i++){
                cout << p2[i] << endl;
            }
            delete [] p2;
            cout << "-------" << endl;


            int broj_slucajeva;
            cout << "Unesite broj slucajeva: " << endl;
            cin >> broj_slucajeva;
            p2 = new int[broj_slucajeva];
            cout << p2 << endl;
            cout << "-------" << endl;
            // isto sto i if(p == NULL)
            if(!p2){
                cout << "greska u alociranju memorije" << endl;
            }

            for(int i = 0; i < broj_slucajeva; i++){
                cout << &p2[i] << endl;
            }
            delete [] p2;
            cout << "-------" << endl;
            cout << p2 << endl;

            cout << "------------------------" << endl;
            break;
        }
        default: {
            cout << "DEFAULT - BREAK" << endl;
            break;
        }
    }

    return 0;
}
