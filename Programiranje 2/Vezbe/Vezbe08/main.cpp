#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <map>

#include "Sprite.h"
#include "Pozadina.h"
#include "Game.h"

using namespace std;

SDL_Window *window = nullptr;
SDL_Renderer *renderer = nullptr;

void close();
void init();

int main(int argc, char ** argv)
{
    bool quit = false;
    SDL_Event event;
    // SDL init
    init();

    // main loop
    while(!quit){
    SDL_Delay(10);
    SDL_PollEvent(&event);
    // event handling
    switch(event.type){
        case SDL_QUIT:
            quit = true;
            break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym){
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                default:
                    break;
            };
            break;
    };

    // clear window
    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
    SDL_RenderClear(renderer);







    // render window
    SDL_RenderPresent(renderer);
    };

    // cleanup SDL
    close();
    return 0;
}

void close(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = nullptr;
    renderer = nullptr;

    IMG_Quit();
    SDL_Quit();
}

void init(){
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("Programiranje 2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, 0);
    renderer = SDL_CreateRenderer(window, -1, 0);
}
