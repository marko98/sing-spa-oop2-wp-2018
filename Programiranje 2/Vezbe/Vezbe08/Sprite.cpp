#include "Sprite.h"


void Sprite::render(SDL_Renderer *renderer){
    SDL_Rect renderQuad = {spriteX, spriteY, 2*spriteDuzina, 2*spriteVisina};

    SDL_Rect clip = {sx, sy, spriteDuzina, spriteVisina};

    SDL_RenderCopy(renderer, spriteTekstura, &clip, &renderQuad);
}

void Sprite::tick(int time){
    spriteX = spriteX + dx;
    spriteY = spriteY + dy;
}

void Sprite::left(){
    dx = -1;
    dy = 0;
    sx = boxes[3].x;
}

void Sprite::right(){
    dx = 1;
    dy = 0;
    sx = boxes[0].x;
}

void Sprite::up(){
    dx = 0;
    dy = -1;
    sx = boxes[4].x;
}

void Sprite::down(){
    dx = 0;
    dy = 1;
    sx = boxes[6].x;
}
