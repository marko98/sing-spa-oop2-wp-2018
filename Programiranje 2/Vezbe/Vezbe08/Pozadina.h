#ifndef POZADINA_H_INCLUDED
#define POZADINA_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <map>

#include "Sprite.h"

using namespace std;

class Pozadina: public Sprite{
private:

public:
    Pozadina(){
        spriteX = 640/2-165;
        spriteY = 480/2-214;
        sx = 201;
        sy = 2;
        spriteDuzina = 165;
        spriteVisina = 214;
    };

    void render(SDL_Renderer *renderer);
};

#endif // POZADINA_H_INCLUDED
