#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <map>

#include "Pozadina.h"
#include "Sprite.h"

using namespace std;

class Game{
private:
    Pozadina pozadina;
    Sprite duhA, duhB;
    SDL_Texture *mTexture;
    int mWidth, mHeight;
public:
    Game();
    ~Game(){ free(); };

    void free();
    bool loadFromFile(string putanja, SDL_Renderer* renderer);
    void eventHandling(SDL_Event event);
    void tick(int);
    void render(SDL_Renderer* renderer);
    map<int, SpriteEventHandler*> event_listeners;
};

#endif // GAME_H_INCLUDED
