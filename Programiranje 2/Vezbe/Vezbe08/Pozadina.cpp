#include "Pozadina.h"


void Pozadina::render(SDL_Renderer *renderer){
    SDL_Rect renderQuad = {spriteX, spriteY, 2*spriteDuzina, 2*spriteVisina};

    SDL_Rect clip = {sx, sy, spriteDuzina, spriteVisina};

    SDL_RenderCopy(renderer, spriteTekstura, &clip, &renderQuad);
}
