#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <map>

using namespace std;

class Sprite{
protected:
    SDL_Texture *spriteTekstura;
    int spriteX, spriteY, spriteVisina, spriteDuzina;
    int dx, dy;
    int sx, sy;
    SDL_Rect boxes[8];

public:
    Sprite();
    ~Sprite();

    virtual void render(SDL_Renderer *renderer);

    virtual void tick(int time);
    virtual void left();
    virtual void right();
    virtual void up();
    virtual void down();
};

/* Definicija novog tipa:
      pokazivac na funkciju iz klase Sprite
*/

typedef void (Sprite::*SpriteFunction)();

class SpriteEventHandler{
private:
    int code;
    Sprite *objekat;
    SpriteFunction function;
public:
    SpriteEventHandler(){};
    SpriteEventHandler(int code, Sprite *objekat, SpriteFunction function): code(code), objekat(objekat), function(function){};

    void execute(){
        (objekat->*function)();
    };
};

#endif // SPRITE_H_INCLUDED
