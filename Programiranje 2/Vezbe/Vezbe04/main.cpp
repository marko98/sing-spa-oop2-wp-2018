#include <iostream>
#include <string>
#include <vector>
#include "item.h"
#include "Inventory.h"

using namespace std;

int main()
{
    Inventory *inv = new Inventory();
    //cout << inv->getItem(0, 0) << endl;

    Item *pitem = new Item("Prvi item");

    inv->addItem(pitem, 0, 0);
    pitem = new Item("Drugi item");

    cout << inv->getItem(0, 0)->getName() << endl;

    //inv->remove(0, 0);

    inv->addItem(pitem, 1, 0);

    cout << inv->getItem(1, 0)->getName() << endl;
    //cout << inv->getItem(4, 0) << endl;
    cout << "Popunjenost: " << inv->getSize() << endl;

    delete inv;
    //inv->remove(0, 0);
    //cout << inv->getItem(0, 0) << endl;

    return 0;
}
