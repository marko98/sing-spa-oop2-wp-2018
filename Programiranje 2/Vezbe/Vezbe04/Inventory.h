#ifndef INVENTORY_H_INCLUDED
#define INVENTORY_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "item.h"

class Inventory{
private:
    Item ***items;
    int rows, columns, size;
public:
    Inventory(int rows = 3, int columns = 3): rows(rows), columns(columns){
        size = 0;
        items = new Item **[rows];
        for(int i = 0; i < rows; i++){
            items[i] = new Item *[columns];
            for(int j = 0; j < columns; j++){
                items[i][j] = nullptr;
            }
        }
    }
    ~Inventory(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                delete items[i][j];
            }
            delete items[i];
        }
        delete [] items;
    }
    Item * getItem(int indexRows, int indexColumns){
        if(indexRows < 0 || indexColumns < 0 || indexRows >= rows || indexColumns >= columns){
            cout << "Trazeni Item ne postoji." << endl;
            return nullptr;
        }
        return items[indexRows][indexColumns];
    }
    bool addItem(Item *item, int indexRows, int indexColumns){
        if(indexRows < 0 || indexColumns < 0 || indexRows >= rows || indexColumns >= columns || items[indexRows][indexColumns] != nullptr){
            cout << "Item nije dodat." << endl;
            return false;
        }
        items[indexRows][indexColumns] = item;
        size++;
        cout << "Item je dodat." << endl;
        return true;
    }
    bool remove(int indexRows, int indexColumns){
        if(indexRows < 0 || indexColumns < 0 || indexRows >= rows || indexColumns >= columns){
            return false;
        }
        delete items[indexRows][indexColumns];
        items[indexRows][indexColumns] = nullptr;
        size--;
        return true;
    }

    int getRows(){
        return rows;
    }
    int getColumns(){
        return columns;
    }
    int getSize(){
        return size;
    }

};

#endif // INVENTORY_H_INCLUDED
