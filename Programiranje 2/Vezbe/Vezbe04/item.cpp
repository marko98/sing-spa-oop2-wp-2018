#include "item.h"

void Item::use() {
    cout << "Item " << name << " has no effect!" << endl;
}
