#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED

#include <iostream>

using namespace std;

class Creature;

class Item {
    protected:
        string name;
    public:
        Item(string name) : name(name){};
        void use();
        string getName(){
            return name;
        }
};

#endif // ITEM_H_INCLUDED
