#include <iostream>
#include "item.h"
#include "Inventory.h"

using namespace std;

int main()
{
    Inventory *inv = new Inventory();

    Item *pitem = new Item("Prvi Item");
    inv->addItem(pitem);

    pitem = new Item("Drugi Item");
    inv->addItem(pitem);

    pitem = new Item("Treci Item");
    inv->addItem(pitem);

    inv->introduce();

    inv->remove(1);

    pitem = inv->getItem(1);
    cout << "\n" << pitem->getName() << endl;

    delete inv;
    //inv->introduce();

    return 0;
}
