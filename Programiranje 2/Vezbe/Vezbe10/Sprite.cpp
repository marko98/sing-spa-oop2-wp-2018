#include "Sprite.h"

Sprite::Sprite(){
    spriteTexture = nullptr;
    spriteVisina = 0;
    spriteDuzina = 0;
}

Sprite::~Sprite(){
    free();
}

void Sprite::free(){
    if(spriteTexture != nullptr){
        SDL_DestroyTexture(spriteTexture);
        spriteTexture = nullptr;
        spriteVisina = 0;
        spriteDuzina = 0;
    }
}

extern Engine *engine;

bool Sprite::loadFromFile(string path){
    free();
    SDL_Texture *texture = nullptr;
    SDL_Surface *surface = nullptr;

    surface = IMG_Load(path.c_str());
    if(surface == nullptr){
        printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
    } else {
        texture = SDL_CreateTextureFromSurface(engine->getRenderer() , surface);
        if(texture == nullptr){
            printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        } else {
            spriteVisina = surface->h;
            spriteDuzina = surface->w;
        }
        SDL_FreeSurface(surface);
    }
    spriteTexture = texture;
    SDL_DestroyTexture(texture);
    texture = nullptr;

    return spriteTexture != nullptr;
}

/*void Sprite::render(Coordinate coordinate, SDL_Rect *clip){
    SDL_Rect renderQuad;
    renderQuad.x = coordinate.getCoordinateX();
    renderQuad.y = coordinate.getCoordinateY();
    renderQuad.h = spriteVisina;
    renderQuad.w = spriteDuzina;

    if(clip != nullptr){
        renderQuad.h = clip->h;
        renderQuad.w = clip->w;
    }
    SDL_RenderCopy(engine->getRenderer(), spriteTexture, clip, &renderQuad);
}*/
