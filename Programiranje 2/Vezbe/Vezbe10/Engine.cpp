#include "Engine.h"

Engine::Engine(){
    title = "";
    window = nullptr;
    gRenderer = nullptr;
    font = nullptr;
}

Engine::Engine(const string &gameTitle): title(gameTitle){
    window = nullptr;
    gRenderer = nullptr;
    font = nullptr;
}

Engine::~Engine(){
    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(window);
    gRenderer = nullptr;
    window = nullptr;

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void Engine::init(){
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 640, 0);
    gRenderer = SDL_CreateRenderer(window, -1, 0);

    //int ret = TTF_Init();
    TTF_Init();
    font = TTF_OpenFont("Super-Mario-World.ttf", 20);
}

void Engine::drawText(string text, Coordinate coordinates, TTF_Font *font, SDL_Renderer *renderer){
    SDL_Color white = {255, 255, 255};

    stringstream ss;
    ss << text ;
    SDL_Surface *surface = nullptr;
    SDL_Texture *texture = nullptr;

    surface = TTF_RenderText_Solid(font, ss.str().c_str(), white);
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_Rect message;
    message.x = coordinates.getCoordinateX();
    message.y = coordinates.getCoordinateY();
    message.h = surface->h;
    message.w = surface->w;

    SDL_RenderCopy(renderer, texture, NULL, &message);
}

#include "Sprite.h"
#include "Mario.h"

void Engine::render(Coordinate coordinate, SDL_Rect *clip, Sprite* sprite, SDL_Renderer *renderer){
    SDL_Rect renderQuad;
    renderQuad.x = coordinate.getCoordinateX();
    renderQuad.y = coordinate.getCoordinateY();
    renderQuad.h = sprite->getSpriteVisina();
    renderQuad.w = sprite->getSpriteDuzina();

    if(clip != nullptr){
        renderQuad.h = clip->h;
        renderQuad.w = clip->w;
    }

    sprite->setIgracTrenutniClipDuzina(renderQuad.w);
    sprite->setIgracTrenutniClipVisina(renderQuad.h);

    cout << "x: " << renderQuad.x << ", y: " << renderQuad.y << ", h: " << renderQuad.h << ", w: " << renderQuad.w << " tekstura" << sprite->getSpriteTexture() << endl;
    cout << sprite->getIgracTrenutniClipDuzina() << sprite->getIgracTrenutniClipVisina() << endl;
    SDL_RenderCopy(renderer, sprite->getSpriteTexture(), clip, &renderQuad);
}

bool Engine::run(Sprite *mario){
    bool quit = false;
    SDL_Event event;

    Sprite *dada = new Mario;

    int dx = 1;
    int frame = 0;
    int x = 0;

    while(!quit){
        SDL_Delay(10);
        SDL_PollEvent(&event);

        //event handling
        switch(event.type){
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    default:
                        break;
                }
                break;
        };
        // move objects
        x = x + dx;
        if(x < 0){
            dx = 1;
        }

        if(x > 640-18){
            dx = -1;
        }
        SDL_SetRenderDrawColor(gRenderer, 200, 200, 200, 255);
        SDL_RenderClear(gRenderer);


        SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);
        drawText("SUPER MARIO WORLD", Coordinate(), font, gRenderer);

        int iClip = frame / 8;
        if(dx < 0)
            iClip = 5 + frame / 8;
        Coordinate kordinata(100, 250);

        render(kordinata, dada->getSpriteWalkingClip(0), dada, gRenderer);

        frame++;
        if(frame / 8 >= dada->getWalkingAnimationFrames()){
            frame = 0;
        };

        //render window
        SDL_RenderPresent(gRenderer);
    }

    return true;
}
