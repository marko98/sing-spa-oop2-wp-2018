#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Engine.h"
#include "Sprite.h"
#include "Mario.h"

using namespace std;

Engine *engine = new Engine("Super Mario World");

int main(int argc, char ** argv)
{
    engine->init();

    Sprite *mario = new Mario;

    engine->run(mario);
    delete engine;

    /*Engine *engine = new Engine("Super Mario World");
    engine->init();
    engine->run();
    delete engine;*/

    return 0;
}
