#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Engine.h"
#include "Coordinate.h"

using namespace std;

class Sprite: public Engine{
private:
    SDL_Texture *spriteTexture;
    int spriteVisina, spriteDuzina;
    int igracTrenutniClipDuzina, igracTrenutniClipVisina;
public:
    Sprite();
    ~Sprite();


    int getIgracTrenutniClipDuzina(){ return igracTrenutniClipDuzina; };
    void setIgracTrenutniClipDuzina(int duzina){ igracTrenutniClipDuzina = duzina; };
    int getIgracTrenutniClipVisina(){ return igracTrenutniClipVisina; };
    void setIgracTrenutniClipVisina(int visina){ igracTrenutniClipVisina = visina; };

    int getSpriteVisina(){ return spriteVisina; };
    int getSpriteDuzina(){ return spriteDuzina; };
    SDL_Texture * getSpriteTexture(){ return spriteTexture; };

    bool loadFromFile(string path);
    void free();
    //void render(Coordinate coordinate, SDL_Rect *clip = nullptr);

    virtual void loadSpriteWalking(){};
    virtual SDL_Rect * getSpriteWalkingClip(int iClip){};
    virtual int getWalkingAnimationFrames(){};
};

#endif // SPRITE_H_INCLUDED
