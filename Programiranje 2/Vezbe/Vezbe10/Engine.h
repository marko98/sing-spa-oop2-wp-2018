#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <string>

#include "Coordinate.h"

class Sprite;

using namespace std;

class Engine{
private:
    string title;
    SDL_Window *window;
    TTF_Font *font;
    SDL_Renderer *gRenderer;
public:
    Engine();
    Engine(const string &gameTitle);
    ~Engine();

    void init();
    //void loadMedia();
    void drawText(string text, Coordinate coordinates, TTF_Font *font, SDL_Renderer *gRenderer);
    void render(Coordinate coordinate, SDL_Rect *clip, Sprite* sprite, SDL_Renderer *gRenderer);

    bool run(Sprite *mario);
    SDL_Renderer * getRenderer(){ return gRenderer; };
};

#endif // ENGINE_H_INCLUDED
