#include "Mario.h"

Mario::Mario(){
    this->loadFromFile("marioWalking.png");
    loadSpriteWalking();
}

const int WALKING_ANIMATION_FRAMES = 5;
SDL_Rect spriteMarioClips[ 2*WALKING_ANIMATION_FRAMES ];

void Mario::loadSpriteWalking(){
    for(int i = 0; i < 5; i++){
        spriteMarioClips[i].x = (5 + i)*40;
        spriteMarioClips[i].y = 1;
        spriteMarioClips[i].w = 18;
        spriteMarioClips[i].h = 29;
    }
    for(int i = 0; i < 5; i++){
        spriteMarioClips[5+i].x = i*40;
        spriteMarioClips[5+i].y = 1;
        spriteMarioClips[5+i].w = 18;
        spriteMarioClips[5+i].h = 29;
    }
}

SDL_Rect * Mario::getSpriteWalkingClip(int iClip){
    SDL_Rect *clip = &spriteMarioClips[iClip];
    return clip;
}

int Mario::getWalkingAnimationFrames(){
    return WALKING_ANIMATION_FRAMES;
}
