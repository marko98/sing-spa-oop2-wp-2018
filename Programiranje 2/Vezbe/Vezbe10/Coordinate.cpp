#include "Coordinate.h"

Coordinate::Coordinate(){
    coordinateX = 0;
    coordinateY = 0;
}

Coordinate::Coordinate(int x, int y): coordinateX(x), coordinateY(y){

}

int Coordinate::getCoordinateX(){
    return coordinateX;
}

void Coordinate::setCoordinateX(int x){
    if(x < 0){
        x = 0;
    }
    coordinateX = x;
}

int Coordinate::getCoordinateY(){
    return coordinateY;
}

void Coordinate::setCoordinateY(int y){
    if(y < 0){
        y = 0;
    }
    coordinateY = y;
}
