#include <iostream>

using namespace std;

#define NEWLINE '\n'

// DEKLARACIJA f-je
double povrsina(double a, double b);

class Pravougaonik{
private:
    float visina;
    float sirina;
public:
    Pravougaonik(float v, float s){
        this->visina = v;
        this->sirina = s;
    }

    void setVisina(float v){
        this->visina = v;
    }
    void setSirina(float s){
        this->sirina = s;
    }

    float getVisina();
    float getSirina();
    double povrsina();
};

float Pravougaonik::getVisina(){
    return this->visina;
}

float Pravougaonik::getSirina(){
    return this->sirina;
}

double Pravougaonik::povrsina(){
    return ::povrsina(this->sirina, this->visina);
}

int main()
{
    double x = 5;
    cout << x << NEWLINE;
    cout << "Hello world!" << endl;

    int suma = 0;
    int niz[10] = {1,2,3,4,5,6,7,8,9,10};

    for(unsigned int i = 0; i < 10; i++){
        suma = suma + niz[i];
    }
    cout << suma << NEWLINE;

    cout << povrsina(5, 6) << NEWLINE;


    Pravougaonik p1 = Pravougaonik(5, 9);
    cout << p1.getVisina() << NEWLINE;

    cout << p1.povrsina() << NEWLINE;

    return 0;
}

// definicija
double povrsina(double a, double b){
    return a*b;
}
