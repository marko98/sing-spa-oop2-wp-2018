#ifndef SCORE_H_INCLUDED
#define SCORE_H_INCLUDED

#include <SDL.h>
#include <SDL_ttf.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

using namespace std;

class Score{
public:
    static void drawScore(int score, int brojPokusaja, TTF_Font *font, SDL_Renderer *renderer){
        SDL_Color white = {255, 255, 255};
        stringstream ss;
        ss << "[" << score << "/" << brojPokusaja << "]";
        SDL_Surface *sm = TTF_RenderText_Solid(font, ss.str().c_str(), white);
        SDL_Texture *poruka = SDL_CreateTextureFromSurface(renderer, sm);
        SDL_Rect porukaBox;
        porukaBox.x = 320-sm->w/2;
        porukaBox.y = 195-sm->h;
        porukaBox.h = sm->h;
        porukaBox.w = sm->w;
        SDL_RenderCopy(renderer, poruka, NULL, &porukaBox);
    };
};

#endif // SCORE_H_INCLUDED
