#ifndef SHAPE_H_INCLUDED
#define SHAPE_H_INCLUDED

#include <SDL.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

using namespace std;

class Shape{
private:

public:
    Shape(){};
    virtual void draw(SDL_Renderer *renderer) = 0;
    virtual void move(int dX, int dY) = 0;
};

#endif // SHAPE_H_INCLUDED
