#ifndef CIRCLE_H_INCLUDED
#define CIRCLE_H_INCLUDED

#include <SDL.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>
#include <cmath>

#include "Shape.h"
#include "Tacka.h"

using namespace std;

class Circle: public Shape{
private:
    Tacka centar;
    int poluprecnik;
public:
    Circle(Tacka centar, int poluprecnik): Shape(), centar(centar), poluprecnik(poluprecnik){};

    virtual void draw(SDL_Renderer *renderer){
        int s = 30;
        float d_a = 2*M_PI/s;
        float angle = d_a;

        float x0, y0;
        float x1, y1;
        x1 = centar.getX()+poluprecnik;
        y1 = centar.getY();
        for (int i=0; i<s; i++){
            x0 = x1;
            y0 = y1;
            x1 = centar.getX() + cos(angle) * poluprecnik;
            y1 = centar.getY() + sin(angle) * poluprecnik;
            angle += d_a;
            SDL_RenderDrawLine(renderer, x0, y0, x1, y1);
        }
    };

    virtual void move(int dX, int dY){
        centar.setX(centar.getX() + dX);
        centar.setY(centar.getY() + dY);
    };

};

#endif // CIRCLE_H_INCLUDED
