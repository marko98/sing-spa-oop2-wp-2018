#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <istream>

#include "Coordinate.h"

using namespace std;

class Tile{
private:
    SDL_Rect *tileRect;
public:
    Tile(Coordinate coordinate, int h, int w);
    Tile(istream &inputStream);

    SDL_Rect *getTileRect();
    int getTileRectWidth();
    int getTileRectHeight();
};

#endif // TILE_H_INCLUDED
