#include "Tile.h"

Tile::Tile(Coordinate coordinate, int h, int w){
    tileRect->x = coordinate.getCoordinateX();
    tileRect->y = coordinate.getCoordinateY();
    tileRect->h = h;
    tileRect->w = w;
}

Tile::Tile(istream &inputStream){
    inputStream >> tileRect->x >> tileRect->y >> tileRect->w >> tileRect->h;
}

SDL_Rect * Tile::getTileRect(){
    return tileRect;
}

int Tile::getTileRectHeight(){
    return tileRect->h;
}

int Tile::getTileRectWidth(){
    return tileRect->w;
}
