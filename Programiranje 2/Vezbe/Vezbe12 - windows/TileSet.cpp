#include "TileSet.h"

TileSet::TileSet(istream &inputStream, SDL_Renderer *eRenderer){
    string path;
    inputStream >> path;
    SDL_Surface *surface = IMG_Load(path.c_str());
    texture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_FreeSurface(surface);
    surface = nullptr;

    char code;
    while(!inputStream.eof()){
        inputStream >> code;
        tiles[code] = new Tile(inputStream);
    }
}

void TileSet::draw(char code, Coordinate coordinate, SDL_Renderer *eRenderer){
    destRect->x = coordinate.getCoordinateX();
    destRect->y = coordinate.getCoordinateY();
    destRect->w = tiles[code]->getTileRectWidth();
    destRect->h = tiles[code]->getTileRectHeight();
    SDL_RenderCopy(eRenderer, texture, tiles[code]->getTileRect(), destRect);
}
