#ifndef CREATURE_H_INCLUDED
#define CREATURE_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Item.h"

using namespace std;

class Creature{
protected:
    int hp, baseDamage;
    string name;
    vector<Item*> inventory;

public:
    Creature(int, int, string);
    bool isAlive();
    virtual void attack(Creature &) = 0;
    void introduce();

    int getHp();
    void setHp(int);

    int getbaseDamage();
    void setBaseDamage(int);

    string getName();
    void setName(string);

    vector<Item*> getInventory(){ return inventory; };
    void setInventory(vector<Item*> inventory){ this->inventory = inventory; };
    void addInInventory(Item *item){ inventory.push_back(item); };
};

#endif // CREATURE_H_INCLUDED
