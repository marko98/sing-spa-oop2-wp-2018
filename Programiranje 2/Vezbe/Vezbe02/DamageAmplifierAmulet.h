#ifndef DAMAGEAMPLIFIERAMULET_H_INCLUDED
#define DAMAGEAMPLIFIERAMULET_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "PassiveItem.h"

using namespace std;

class DamageAmplifierAmulet: public PassiveItem{
private:

public:
    DamageAmplifierAmulet(): PassiveItem(){ this->name = "Damage Amplifier Amulet"; };
    virtual void use();
};

#endif // DAMAGEAMPLIFIERAMULET_H_INCLUDED
