#ifndef MAGICCREATURE_H_INCLUDED
#define MAGICCREATURE_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Creature.h"

using namespace std;

class MagicCreature: public Creature{
private:
    int mp;

public:
    MagicCreature(int hp, int baseDamage, string name, int mp):Creature(hp, baseDamage, name), mp(mp){};
    virtual void attack(Creature &);
    void magicAttack(MagicCreature &);

    int getMp();
    void setMp(int);
    void introduce();
};

#endif // MAGICCREATURE_H_INCLUDED
