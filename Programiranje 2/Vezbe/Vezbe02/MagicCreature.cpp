#include "MagicCreature.h"

int MagicCreature::getMp(){
    return mp;
}

void MagicCreature::setMp(int mp){
    if(mp < 0){
        mp = 0;
    }
    this->mp = mp;
}

void MagicCreature::attack(Creature &enemy){
    for(unsigned int i = 0; i < inventory.size() ; i++){
        //(*inventory[i]).use();
        inventory[i]->use();
    }
    enemy.setHp(enemy.getHp() - this->getbaseDamage());
}

void MagicCreature::magicAttack(MagicCreature &enemy){
    for(unsigned int i = 0; i < inventory.size() ; i++){
        //(*inventory[i]).use();
        inventory[i]->use();
    }
    enemy.setMp(enemy.getMp() - this->getbaseDamage());
}

void MagicCreature::introduce(){
    string alive = "false";

    if(this->isAlive()){
        alive = "true";
    }

    cout << "Name: " << name << ", hp: " << hp << ", base damage: " << baseDamage << ", alive: " << alive << ", mp: " << mp << endl;
}
