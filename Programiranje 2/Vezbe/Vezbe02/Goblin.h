#ifndef GOBLIN_H_INCLUDED
#define GOBLIN_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Creature.h"

using namespace std;

class Goblin: public Creature{
private:
    int powerAttack;

public:
    Goblin(int hp, int baseDamage, string name, int powerAttack): Creature(hp, baseDamage, name), powerAttack(powerAttack){};
    virtual void attack(Creature &);

    int getPowerAttack();
    void setPowerAttack(int);
};

#endif // GOBLIN_H_INCLUDED
