#include "Goblin.h"

void Goblin::attack(Creature &enemy){
    for(unsigned int i = 0; i < inventory.size() ; i++){
        //(*inventory[i]).use();
        inventory[i]->use();
    }
    enemy.setHp(enemy.getHp() - this->getbaseDamage());
}

int Goblin::getPowerAttack(){
    return powerAttack;
}
void Goblin::setPowerAttack(int powerAttack){
    if(powerAttack < 0){
        powerAttack = 0;
    }
    this->powerAttack = powerAttack;
}
