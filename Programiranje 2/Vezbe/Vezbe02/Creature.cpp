#include "Creature.h"

Creature::Creature(int hp, int baseDamage, string name){
    this->hp = hp;
    this->baseDamage = baseDamage;
    this->name = name;
}

bool Creature::isAlive(){
    if(hp > 0){
        return true;
    } else {
        return false;
    }
}

void Creature::introduce(){
    string alive = "false";

    if(this->isAlive()){
        alive = "true";
    }

    cout << "Name: " << name << ", hp: " << hp << ", base damage: " << baseDamage << ", alive: " << alive << endl;
}

int Creature::getHp(){
    return hp;
}
void Creature::setHp(int hp){
    if(hp < 0){
        this->hp = 0;
    } else {
        this->hp = hp;
    }
}

int Creature::getbaseDamage(){
    return baseDamage;
}
void Creature::setBaseDamage(int baseDamage){
    this->baseDamage = baseDamage;
}

string Creature::getName(){
    return name;
}
void Creature::setName(string name){
    this->name = name;
}
