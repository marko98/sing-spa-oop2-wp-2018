#include <iostream>
#include <string>
#include <vector>

#include "Creature.h"
#include "MagicCreature.h"
#include "Item.h"
#include "PassiveItem.h"
#include "DamageAmplifierAmulet.h"
#include "Goblin.h"

using namespace std;

int main()
{
    //Creature marko(10, 4, "Marko");
    //Creature david(8, 3, "David");
    MagicCreature maja(10, 4, "Maja", 5);
    MagicCreature stefan(8, 2, "Stefan", 4);

    /*david.introduce();
    marko.attack(david);
    david.introduce();*/

    Creature *pcreature = new Goblin(10, 4, "Marko", 4);
    Goblin david (8, 3, "David", 5);
    Creature &pdavid = david;
    //PassiveItem &pitem = da;

    Item *pitem = new DamageAmplifierAmulet();

    maja.addInInventory(pitem);
    pcreature->addInInventory(pitem);
    pdavid.addInInventory(pitem);

    pitem = new PassiveItem();

    maja.addInInventory(pitem);
    pcreature->addInInventory(pitem);
    pdavid.addInInventory(pitem);

    cout << maja.getInventory().size() << endl;

    /*for(unsigned int i = 0; i < maja.getInventory().size() ; i++){
        //(*inventory[i]).use();
        maja.getInventory()[i]->use();
    }*/

    david.introduce();
    pcreature->attack(david);
    pcreature->introduce();
    pdavid.introduce();
    for(unsigned int i = 0; i < pdavid.getInventory().size() ; i++){
        //(*inventory[i]).use();
        pdavid.getInventory()[i]->use();
    }
    cout << "------------" << endl;
    stefan.introduce();
    maja.magicAttack(stefan);
    stefan.introduce();

    return 0;
}
