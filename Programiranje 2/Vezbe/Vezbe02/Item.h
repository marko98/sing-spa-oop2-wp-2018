#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Item{
protected:
    string name;

public:
    Item(){};
    virtual void use() = 0;
};

#endif // ITEM_H_INCLUDED
