#ifndef PASSIVEITEM_H_INCLUDED
#define PASSIVEITEM_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

#include "Item.h"

using namespace std;

class PassiveItem: public Item{
private:

public:
    PassiveItem(): Item(){ this->name = "Passive Item"; };
    virtual void use();
};

#endif // PASSIVEITEM_H_INCLUDED
