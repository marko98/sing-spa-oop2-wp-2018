#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <iostream>
#include <sstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <vector>
#include <string>

using namespace std;

class Sprite{
private:
    SDL_Texture *spriteTexture;
    int spriteVisina, spriteDuzina;
public:
    Sprite();
    ~Sprite();

    void free();
    bool ucitajTeksturu(string putanja);
    void render(int x, int y, SDL_Rect *clip = nullptr);
};

#endif // SPRITE_H_INCLUDED
