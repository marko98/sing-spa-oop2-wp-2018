#include "Sprite.h"

extern SDL_Renderer *renderer;

Sprite::Sprite(){
    this->spriteTexture = nullptr;
    this->spriteDuzina = 0;
    this->spriteVisina = 0;
}

Sprite::~Sprite(){
    free();
}

bool Sprite::ucitajTeksturu(string putanja){
    SDL_Texture *trenutnaTekstura = nullptr;
    SDL_Surface *povrsina = nullptr;
    povrsina = IMG_Load(putanja.c_str());

    if(povrsina == nullptr){
        printf("Unable to load image %s! SDL_image Error: %s\n", putanja.c_str(), IMG_GetError());
    } else {
        // SDL_SetColorKey(ucitanaPovrsina, SDL_TRUE, SDL_MapRGB(ucitanaPovrsina->format, 0, 0xFF, 0xFF ));
        trenutnaTekstura = SDL_CreateTextureFromSurface(renderer, povrsina);
        if(trenutnaTekstura == nullptr){
            printf("Unable to create texture from %s! SDL Error: %s\n", putanja.c_str(), SDL_GetError());
        } else {
            spriteTexture = trenutnaTekstura;

            spriteDuzina = povrsina->w;
            spriteVisina = povrsina->h;
        }
    }
    SDL_FreeSurface(povrsina);

    return spriteTexture != nullptr;
}

void Sprite::free(){
    if(spriteTexture != nullptr){
        SDL_DestroyTexture(spriteTexture);
        spriteTexture = nullptr;
        spriteDuzina = 0;
        spriteVisina = 0;
    };
}

void Sprite::render(int x, int y, SDL_Rect *clip){
    SDL_Rect prikazClipa;
    prikazClipa.x = x;
    prikazClipa.y = y;
    prikazClipa.h = spriteVisina;
    prikazClipa.w = spriteDuzina;

    if(clip != nullptr){
        prikazClipa.h = clip->h;
        prikazClipa.w = clip->w;
    };
    SDL_RenderCopy(renderer, spriteTexture, clip, &prikazClipa);
}
