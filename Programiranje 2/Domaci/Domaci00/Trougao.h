#ifndef TROUGAO_H_INCLUDED
#define TROUGAO_H_INCLUDED

#include <SDL.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

#include "Line.h"
#include "Tacka.h"

using namespace std;

class Trougao: public Line{
private:
    Line osnovnaStranica, krakA, krakB;
    int visina;

public:
    Trougao(Line osnovnaStranica, int visina): Line(), osnovnaStranica(osnovnaStranica), visina(visina){
        this->krakA = Line (Tacka(osnovnaStranica.getPocetna().getX(), osnovnaStranica.getPocetna().getY()), Tacka(osnovnaStranica.getKrajnja().getX() - osnovnaStranica.getPocetna().getX(), osnovnaStranica.getKrajnja().getY() - visina));
        this->krakB = Line (Tacka(osnovnaStranica.getKrajnja().getX() - osnovnaStranica.getPocetna().getX(), osnovnaStranica.getPocetna().getY() - visina), Tacka(osnovnaStranica.getKrajnja().getX(), osnovnaStranica.getKrajnja().getY()));
    };

    virtual void draw(SDL_Renderer *renderer){
        krakA.draw(renderer);
        krakB.draw(renderer);
        osnovnaStranica.draw(renderer);
    };

    virtual void move(int dX, int dY){
        krakA.move(dX, dY);
        krakB.move(dX, dY);
        osnovnaStranica.move(dX, dY);
    };

    Line getOsnovnaStranica(){ return osnovnaStranica; };
};

#endif // TROUGAO_H_INCLUDED
