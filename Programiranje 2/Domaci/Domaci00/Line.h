#ifndef LINE_H_INCLUDED
#define LINE_H_INCLUDED

#include <SDL.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

#include "Shape.h"
#include "Tacka.h"

using namespace std;

class Line: public Shape{
private:
    Tacka pocetna, krajnja;

public:
    Line(){};
    Line(Tacka pocetna, Tacka krajnja): Shape(), pocetna(pocetna), krajnja(krajnja){};

    virtual void draw(SDL_Renderer *renderer){
        SDL_RenderDrawLine(renderer, pocetna.getX(), pocetna.getY(), krajnja.getX(), krajnja.getY());
    };

    virtual void move(int dX, int dY){
        pocetna.setX(pocetna.getX() + dX);
        pocetna.setY(pocetna.getY() + dY);

        krajnja.setX(krajnja.getX() + dX);
        krajnja.setY(krajnja.getY() + dY);
    };

    Tacka getPocetna(){ return pocetna; };
    Tacka getKrajnja(){ return krajnja; };

};

#endif // LINE_H_INCLUDED
