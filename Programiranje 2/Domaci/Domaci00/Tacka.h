#ifndef KORDINATA_H_INCLUDED
#define KORDINATA_H_INCLUDED

#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

using namespace std;

class Tacka{
private:
    int x, y;
public:
    Tacka(){};
    Tacka(int x, int y): x(x), y(y){};

    int getX(){ return x; };
    void setX(int x) {
        this->x = x;
    };

    int getY(){ return y; };
    void setY(int y) {
        this->y = y;
    };

};

#endif // KORDINATA_H_INCLUDED
