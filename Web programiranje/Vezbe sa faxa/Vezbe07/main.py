import flask
import datetime
from flask import Flask
from flask import request

app = Flask(__name__, static_url_path="")

stavke = [{"naziv": "Stavka 1", "kolicina": 100},
          {"naziv": "Stavka 2", "kolicina": 25},
          {"naziv": "Stavka 3", "kolicina": 33},
          {"naziv": "Stavka 4", "kolicina": 16},
          {"naziv": "Stavka 5", "kolicina": 91}] # Globalna promenljiva koja predstavlja kolekciju stavki u magacinu. !Veoma losa praksa. Upotrebljivo samo kao primer!

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html") # Dostavlja datoteku iz static direktorijuma.

@app.route("/stavke", methods=["GET"])
def dobavljanje_stavki():
    # Podaci o stavkama se vracaju u JSON reprezentaciji.
    # Funkcija jsonify predstavlja omotac oko funkcije dumps
    # iz modula json koji je standardno dostupan uz Python,
    # jsonify umesto stringa vraca Response objekat koji
    # je konstruisan tako da su mu postavljene ispravne
    # vrednosti zaglavlja. Na primer vrednost Contet-Type
    # zaglavlje se automatski postavlja na application/json.
    # Funkcija jsonify kao i funkcija json.dumps podrazumevano
    # radi samo sa primitivnim tipovima, listama i recnicima.
    # Implementacijom JSONEncoder-a moguce je dodati podrsku
    # za izvedene tipove.
    return flask.json.jsonify(stavke)

@app.route("/stavke", methods=["POST"])
def dodavanje_stavke():
    # Objekat request u sebi sadrzi izvedeni atribut data
    # koji predstavlja sadrzaj tela zahteva upucenog web
    # aplikaciji. Pored toga request objekat sadrzi i
    # izvedeni atribut json cija se vrednost dobija na
    # osnovu atributa data. Prvo se proverava da li je
    # u zaglavlju zahteva specificirano da se u telu
    # dostavlja json, ukoliko jeste vrsi se parsiranje json
    # stringa iz atributa data u Python objekte. Podrazumevano
    # je podrzana samo podrska za pretvaranje json-a u
    # primitivne tipove, liste i recnike. Implementacijom
    # JSONDecoder-a moguce je dodati podrsku za izvedene tipove.
    stavke.append(request.json)
    return "", 201

@app.route("/stavke/<int:indeks>", methods=["DELETE"])
def uklanjanje_stavke(indeks):
    del stavke[indeks]
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.