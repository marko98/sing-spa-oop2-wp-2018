import flask
from flask import Flask
import datetime

app = Flask (__name__, static_url_path="") #naziv kalse prosledjen

korisnici = [{"ime": "Korisnik1", "prezime": "prezime1", "lozinka": "lozinka","email": "asd@sad" , "datum_rodjenja": "1945-05-12", "aktivan": True}]

# praviti filtere, ima ih i ugradjenih
@app.template_filter("date")
def dateFilter(date_string):
    date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    return date.strftime("%d.%m.%Y.")
    
@app.route("/korisnici")
def lista_korisnika():
    return flask.render_template("korisnici.tpl.html", korisnici = korisnici)

@app.route("/") #NIJE ANOTACIJA!!!!!
def index_page ( ) :
    return "Uspesno obradjen zahtev!"

@app.route("/kontaktForma")
@app.route("/kontaktForma.html")
def home ( ) :
    return flask.send_file("kontaktForma.html")

@app.route("/registracija", methods=["POST", "GET"])
def registracija():
    #return str(flask.request.args)
    korisnik = {
        "ime": flask.request.form["ime"],
        "prezime": flask.request.form["prezime"],
        "email": flask.request.form["email"],
        "datum_rodjenja": flask.request.form["datumRodjenja"],
        "aktivan": True
    }
    
    korisnici.append(korisnik)
    # print(flask.request.form)
    print(korisnici)
    return flask.render_template("test.tpl.html", naslov="Prikaz Korisnika") #flask.render_template_string("<li>{{request.form['ime']}}{{x+100}}</li>", x=y) #str(flask.request.form)

@app.route("/ukloniKorisnika/<int:korisnik_index>")
def ukloni_korisnika(korisnik_index):
    korisnik_index -= 1
    korisnici[korisnik_index]["aktivan"] = False
    return flask.redirect("/korisnici")

@app.route("/izmenaKorisnika/<int:korisnik_index>", methods=["POST"])
def izmena(korisnik_index):
    #return str(flask.request.args)
    korisnici[korisnik_index]["ime"] = flask.request.form["ime"]
    korisnici[korisnik_index]["prezime"] = flask.request.form["prezime"]
    korisnici[korisnik_index]["email"] = flask.request.form["email"]
    korisnici[korisnik_index]["datumRodjenja"] = flask.request.form["datumRodjenja"]
    korisnici[korisnik_index]["aktivan"] = True

    print(korisnici)
    return flask.redirect("/korisnici")

@app.route("/izmeniKorisnika/<int:korisnik_index>", methods=["GET"])
def izmeni_korisnika(korisnik_index):
    print(korisnici[korisnik_index-1])
    korisnik_index -= 1
    return flask.render_template("kontaktForma.tpl.html", korisnik = korisnici[korisnik_index], korisnik_index = korisnik_index)

app.run(host="0.0.0.0" , port=5000, threaded=True)

