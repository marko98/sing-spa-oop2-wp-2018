// Izolujemo scope
(function (d){
    var datum = d.getElementById("datumRodjenja");
    var datumValidacija = function(e){
        var datumRodjenja = new Date(e.target.value);
        var trenutno = new Date();

        var razlika = new Date(trenutno - datumRodjenja);
        razlika = razlika.getFullYear() - 1970;
        if(razlika < 18){
            e.target.setCustomValidity("Nemate dovoljno godina");
        } else {
            e.target.setCustomValidity("");
        }
        console.log(datumRodjenja);
        console.log(trenutno);
        console.log(razlika);
    }
    datum.onchange = datumValidacija;
    

})(document);