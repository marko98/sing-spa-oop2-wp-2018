(function(angular){
    //Kreiranje novog AngularJS modula pod nazivom app.
    //Modul nema zavisnosti.
    var app = angular.module("app", ["ui.router"]);

    app.config(function($stateProvider, $urlRouterProvider){
        $stateProvider.state("home", {
            url: "/",
            templateUrl: "app/magacin/magacin.tpl.html",
            controller: "MagacinCtrl",
            controllerAs: "mc"
        });
        $stateProvider.state("proizvodi", {
            url: "/proizvodi",
            templateUrl: "app/magacin/magacin.tpl.html",
            controller: "MagacinCtrl",
            controllerAs: "mc"
        }).state("proizvod", {
            url: "/proizvodi/:id",
            templateUrl: "app/proizvod/proizvod.tpl.html",
            controller: "ProizvodCtrl",
            controllerAs: "pr"
        });

        $urlRouterProvider.otherwise("/");
    })
})(angular);