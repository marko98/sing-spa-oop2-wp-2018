(function(angular){
    angular.module("myApp").controller("MyCtrl", function(){
        var mc = this;
        mc.novaStavka = {"naziv": "", "kolicina": 0};
        mc.stavke = [
            {"naziv": "stavka1",
            "kolicina": 10},
            {"naziv": "stavka2",
            "kolicina": 20}
        ]

        mc.ukloniStavku = function(index){
            mc.stavke.splice(index, 1);
        };

        mc.dodajStavku = function(){
            mc.stavke.push(angular.copy(mc.novaStavka));
        };
    });
})(angular);