import flask
from flask import Flask
import datetime

app = Flask (__name__, static_url_path="") #naziv kalse prosledjen

# praviti filtere, ima ih i ugradjenih
@app.template_filter("date")
def dateFilter(date_string):
    date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    return date.strftime("%d.%m.%Y.")
    

@app.route("/") #NIJE ANOTACIJA!!!!!
def index_page ( ) :
    return "Uspesno obradjen zahtev!"

@app.route("/kontaktForma")
@app.route("/kontaktForma.html")
def home ( ) :
    return flask.send_file("kontaktForma.html")

@app.route("/registracija", methods=["POST", "GET"])
def registracija():
    #return str(flask.request.args)
    y = 10
    return flask.render_template("test.tpl.html", naslov="Prikaz Korisnika") #flask.render_template_string("<li>{{request.form['ime']}}{{x+100}}</li>", x=y) #str(flask.request.form)



app.run(host="0.0.0.0" , port=5000, threaded=True)

