(function(angular) {
    //Injektuje se $stateParams kako bi se moglo pristupiti parametrima prosledjenim u URL-u stanju.
    angular.module("app").controller("KorisnikCtrl", ["$stateParams", "$http", function($stateParams, $http) {
        var that = this;
        this.korisnik = {};
        this.korisnici = [];

        this.noviKorisnik = {
            "korisnicko_ime": "",
            "ime": "",
            "prezime": "",
            "email": ""
        }

        this.dobaviKorisnike = function(){
            $http.get("/korisnici").then(function(response){
                that.korisnici = response.data;
            }, function(response){
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.code);
            })
        }

        this.dobaviKorisnika = function(korisnickoIme) {
            $http.get("/korisnici/"+korisnickoIme).then(function(response) {
                that.korisnik = response.data;
            }, function(response) {
                console.log(response.status);
            });
        }

        this.obrisiKorisnika = function(korisnickoIme) {
            $http.delete("/korisnici/"+korisnickoIme).then(function(response) {
                that.dobaviKorisnike();
                // that.korisnik = response.data;
            }, function(response) {
                console.log(response.status);
            });
        }

        //Parametrima navedenim u URL-u stanja pristupa se preko atributa objekta $stateParams.
        //Imena atributa jednaka su nazivima parametara stanja. Vrednost atributa kojem se pristupa
        //predstavlja vrednost odabranog parametra stanja.

        
        this.dobaviKorisnika($stateParams["korisnicko_ime"]);
        this.dobaviKorisnike();
    }]);
})(angular);