(function(angular) {
    //Injektuje se $stateParams kako bi se moglo pristupiti parametrima prosledjenim u URL-u stanju.
    angular.module("app").controller("StavkaCtrl", ["$stateParams", "$http", function($stateParams, $http) {
        var that = this;
        this.stavka = {};

        this.dobaviStavku = function(id) {
            $http.get("/stavke/"+id).then(function(response) {
                that.stavka = response.data;
            }, function(response) {
                console.log(response.status);
            });
        }

        //Parametrima navedenim u URL-u stanja pristupa se preko atributa objekta $stateParams.
        //Imena atributa jednaka su nazivima parametara stanja. Vrednost atributa kojem se pristupa
        //predstavlja vrednost odabranog parametra stanja.
        this.dobaviStavku($stateParams["id"]);
    }]);
})(angular);