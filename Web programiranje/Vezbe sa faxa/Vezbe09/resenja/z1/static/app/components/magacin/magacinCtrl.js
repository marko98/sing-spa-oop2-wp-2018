(function (angular) {
    //Dobavljanje postojeceg modula pod nazivom app.
    var app = angular.module("app");

    //Registrovanje kontrolera pod nazivom MagacinCtrl
    //U kontroler se injektuje $http servis koji sluzi
    //za slanje asinhronih zahteva ka serveru. 
    app.controller("MagacinCtrl", ["$http", function ($http) {
        /*
         * Neophodno kako bi referenca na funkciju kontrolera bila
         * dostupna unutar funkcija definisanih u samom kontroleru.
         */
        var that = this;
        //Definisanje niza stavki koje ce biti prikazane.
        //Svaka stavka predstavljena je JavaScript objektom.
        this.roba = [];
        

        //Objekat kojim je predstavljena roba koju treba dodati.
        this.novaRoba = {
            "naziv": "",
            "kolicina": 0
        }


        this.dobaviRobu = function() {
            // Upucuje get zahtev na zadati URL, vraca promise objekat.
            $http.get("/stavke").then(function(response){
                //Ukoliko se promise uspresno razresi, pozvace
                //se ova callback funkcija.
                //Sav sadrzaj vracen sa servera smesta se u response
                //objekat u atribut data. Sadrzaj se automatski iz
                //JSON-a transformise u JavaScript objekte.
                that.roba = response.data;
            }, function(response) {
                //U slucaju neuspesnog razresavanja promise objekta
                //bice izvrsena ova callback funkcija.
                console.log("Greska pri dobavljanju robe! Kod: " + response.code);
            })
        }

        //Funckija, metoda, kontrolera koja vrsi uklanjanje robe.
        this.ukloniRobu = function(indeks) {
            //Salje delete zahtev na URL /stavke/INDEKS_STAVKE
            //pri cemu je INDEKS_STAVKE redni broj stavke koju
            //treba ukloniti.
            $http.delete("/stavke/"+indeks).then(function(response){
                that.dobaviRobu(); //Nakon uklanjanja se ponovo dobavlja citava
                                   //kolekcija robe sa servera. Moguce je i samo
                                   //na klijentu ukloniti stavku koja je uspesno
                                   //uklonjena.
            }, function(response){
                console.log("Greska pri uklanjanju robe! Kod: " + response.code);
            });
        }

        //Funkcija, metoda, kontrolera koja vrsi dodavanje nove robe
        this.dodajRobu = function() {
            //Salje POST zahtev na URL /stavke u cilju dodavanja nove stavke.
            //Drugi argument predstavlja objekat koji treba poslati.
            //Objekat se automatski pretvara u JSON i njegova JSON
            //reprezentacija se salje serveru.
            $http.post("/stavke", that.novaRoba).then(function(response){
                that.dobaviRobu(); //Nakon uspesnog dodavanja dobavljaju se sve stavke sa servera.
                                   //Moguce je bilo i samo na klijentu dodati uspesno dodatu stavku.
            }, function(){
                console.log("Greska pri dodavanju robe! Kod: " + response.code);
            });
        }

        //Prilikom pristupa kontroleru vrsi se inicijalno dobavljanje stavki sa servera.
        this.dobaviRobu();
    }]);
})(angular);