import flask
import datetime
from flask import Flask
from flask import request

from flaskext.mysql import MySQL

from pymysql.cursors import DictCursor

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")

app.config['MYSQL_DATABASE_USER'] = 'student'
app.config['MYSQL_DATABASE_PASSWORD'] = 'student'
app.config['MYSQL_DATABASE_DB'] = 'magacin'

mysql_db.init_app(app)

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html") # Dostavlja datoteku iz static direktorijuma.

@app.route("/stavke", methods=["GET"])
def dobavljanje_stavki():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM stavke")
    stavke = cr.fetchall()
    print(stavke)
    return flask.json.jsonify(stavke)

@app.route("/stavke", methods=["POST"])
def dodavanje_stavke():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO stavke (naziv, kolicina) VALUES(%(naziv)s, %(kolicina)s)", request.json)
    db.commit()
    return "", 201

@app.route("/stavke/<int:indeks>", methods=["GET"])
def dobavljanje_stavke(indeks):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM stavke WHERE id=%s", (indeks, ))
    stavka = cr.fetchone()
    return flask.jsonify(stavka)

@app.route("/stavke/<int:indeks>", methods=["DELETE"])
def uklanjanje_stavke(indeks):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM stavke WHERE id=%s", (indeks,))
    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

# @app.route("/stavke/<naziv>", methods=["GET"])
# def pretraga_stavke_po_nazivu(naziv):
#     cr = mysql_db.get_db().cursor()
#     cr.execute("SELECT * FROM korisnici WHERE naziv LIKE '%naziv%'")


@app.route("/korisnici", methods=["GET"])
def dobavljanje_svih_korisnika():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM korisnici")
    korisnici = cr.fetchall()
    print(korisnici)
    return flask.json.jsonify(korisnici)

@app.route("/korisnici/<korisnicko_ime>", methods=["GET"])
def dobavljanje_jednog_korisnika(korisnicko_ime):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM korisnici WHERE korisnicko_ime=%s", (korisnicko_ime, ))
    korisnik = cr.fetchone()
    return flask.jsonify(korisnik)

@app.route("/korisnici", methods=["POST"])
def dodavanje_korisnika():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO korisnici (korisnicko_ime, ime, prezime, email) VALUES(%(korisnicko_ime)s, %(ime)s, %(prezime)s, %(email)s)", request.json)
    db.commit()
    return "", 201

@app.route("/korisnici/<korisnicko_ime>", methods=["DELETE"])
def uklanjanje_korisnika(korisnicko_ime):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM korisnici WHERE korisnicko_ime=%s", (korisnicko_ime,))
    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.