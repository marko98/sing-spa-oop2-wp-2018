import flask
from flask import Flask

app = Flask ("__main__", static_url_path="") #naziv kalse prosledjen

@app.route("/") #NIJE ANOTACIJA!!!!!
def index_page ( ) :
    return "Uspesno obradjen zahtev!"


@app.route("/index")
@app.route("/index.html")
def home ( ) :
    return flask.send_file("kontaktForma.html")

@app.route("/registracija", methods=["POST", "GET"])
def registracija():
    #return str(flask.request.args)
    return str(flask.request.form)


app.run(host="0.0.0.0" , port=5000, threaded=True)

