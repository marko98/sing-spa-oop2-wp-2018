(function(angular){
    //Kreiranje novog AngularJS modula pod nazivom app.
    //Ovaj modul zavisi od ui.router modula.
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "home", //Naziv stanja.
            url: "/", //URL koji se mapira na zadato stanje. Ovaj URL ce zapravo biti vidljiv kao identifikator fragmenta.
            templateUrl: "/app/components/magacin/magacin.tpl.html", //URL do sablona za generisanje prikaza.
            controller: "MagacinCtrl", //Kontroler koji je potrebno injektovati u prikaz.
            controllerAs: "mc" //Posto ce biti upotrebljen imenovani kontroler, navodi se i ime kontrolera.
        }).state("stavka", { //Naziv je izostavljen iz objekta stanja, ali je naveden kao prvi argument funkcije state.
            url: "/stavka/{id: int}", //Parametrizovana ruta, kljuc predstavlja naziv parametra a vrednost tip parametra.
            templateUrl: "/app/components/stavka/stavka.tpl.html",
            controller: "StavkaCtrl",
            controllerAs: "sc"
        }).state("korisnici", { //Naziv je izostavljen iz objekta stanja, ali je naveden kao prvi argument funkcije state.
            url: "/korisnici", //Parametrizovana ruta, kljuc predstavlja naziv parametra a vrednost tip parametra.
            templateUrl: "/app/components/korisnici/korisnici.tpl.html",
            controller: "KorisnikCtrl",
            controllerAs: "kc"
        }).state("korisnik", { //Naziv je izostavljen iz objekta stanja, ali je naveden kao prvi argument funkcije state.
            url: "/korisnik/{korisnicko_ime}", //Parametrizovana ruta, kljuc predstavlja naziv parametra a vrednost tip parametra.
            templateUrl: "/app/components/korisnik/korisnik.tpl.html",
            controller: "KorisnikCtrl",
            controllerAs: "kc"
        });

        //Ukoliko zadata ruta ne odgovara ni jednoj od ruta stanja vrace se na link
        //za home stanje.
        $urlRouterProvider.otherwise("/");
    }]);
})(angular);