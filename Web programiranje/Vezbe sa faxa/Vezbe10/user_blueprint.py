import flask
from flask import Blueprint
from functools import wraps

user_blueprint = Blueprint("user_blueprint", __name__)

def secured(roles=[]):
    def secured_with_roles(f):
        # lista argumenata foo(1, 2, 3)
        # kwargs foo(x=5, y=3)
        @wraps(f)
        def login_provera(*args, **kwargs):
            # print(flask.session.get("user"))
            if flask.session.get("user") is not None and flask.session.get("user")["pravo"] in roles:
                return f()
            return "greska pri proveri", 401
        return login_provera
    return secured_with_roles


@user_blueprint.route("/", methods=["GET"])
@secured(roles=["admin", "user"])
def dobavi_korisnike():
    # flask.current_app
    return flask.json.jsonify(["korisnik1", "korisnik2"])
