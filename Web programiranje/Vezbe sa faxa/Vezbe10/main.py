import flask
import datetime
from flask import Flask
from flask import request
from user_blueprint import user_blueprint
from functools import wraps

from flaskext.mysql import MySQL

from pymysql.cursors import DictCursor

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")
app.register_blueprint(user_blueprint, url_prefix="/korisnici")

app.secret_key = "tralabu"

app.config['MYSQL_DATABASE_USER'] = 'marko' # Korisnicko ime korisnika baze podataka.
app.config['MYSQL_DATABASE_PASSWORD'] = 'rumenka2012' # Lozinka izabranog korisnika. 
app.config['MYSQL_DATABASE_DB'] = 'magacin' # Ime seme baze podataka koja se koristi.

mysql_db.init_app(app)

# ----------------------------------------------------
@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html")

@app.route("/login", methods=["POST"])
def login():
    # print("usao")
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("SELECT * FROM korisnici WHERE korisnicko_ime=%(username)s AND lozinka=%(password)s", request.json)
    user = cr.fetchone()
    if user is not None:
        flask.session["user"] = request.json
        return "", 200
    return ""

@app.route("/logout", methods=["GET"])
def logout():
    flask.session.pop("user", None)
    return "", 200

def secured(roles=[]):
    def secured_with_roles(f):
        # lista argumenata foo(1, 2, 3)
        # kwargs foo(x=5, y=3)
        @wraps(f)
        def login_provera(*args, **kwargs):
            # print(flask.session.get("user"))
            if flask.session.get("user") is not None and flask.session.get("user")["pravo"] in roles:
                return f()
            return "greska pri proveri", 401
        return login_provera
    return secured_with_roles
    

@app.route("/stavke", methods=["GET"])
@secured(roles=["admin"])
def dobavljanje_stavki():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM stavke")
    stavke = cr.fetchall()
    print(stavke)
    return flask.json.jsonify(stavke)

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)