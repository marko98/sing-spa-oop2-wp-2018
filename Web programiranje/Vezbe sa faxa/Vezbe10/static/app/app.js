(function(angular){
    var app = angular.module("MyApp", ["ui.router", "ui.bootstrap", "ngFileUpload"]);

    app.config(["$stateProvider", "$urlRouterProvider", "$transitionsProvider", function($stateProvider, $urlRouterProvider, $transitionsProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "home",
            url: "/",
            templateUrl: "app/components/user/user.tpl.html",
            controller: "userCtrl",
            controllerAs: "uc"
        });

        $urlRouterProvider.otherwise("/");
    }]);
})(angular)