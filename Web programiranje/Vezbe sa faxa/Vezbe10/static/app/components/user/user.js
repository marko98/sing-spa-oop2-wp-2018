(function(angular) {
    
    angular.module("MyApp").controller("userCtrl", ["$stateParams", "$http", function($stateParams, $http) {
        var vm = this;
        vm.user = {
            username: "",
            password: ""
        };

        vm.login = function(){
            $http.post("/login", vm.user).then(function(response){
                
            }, function(response){
                console.log("Greska pri logovanja! Kod: " + response.code);
            });
        };

    }]);
})(angular);