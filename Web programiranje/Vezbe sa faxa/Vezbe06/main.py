import flask
from flask import Flask

app = Flask (__name__, static_url_path="") #naziv kalse prosledjen

magacin = [{"naziv": "stavka1",
            "kolicina": 10,
            "aktivan": True},
            {"naziv": "stavka2",
            "kolicina": 20,
            "aktivan": True}]

# praviti filtere, ima ih i ugradjenih

@app.route("/") #NIJE ANOTACIJA!!!!!
@app.route("/primer1")
@app.route("/primer1.html")
def index_page ():
    return flask.send_file("primer1.html")

@app.route("/stavke", methods=["GET", "POST"])
def stavke():
    if flask.request.method == "GET":
        return flask.json.jsonify(magacin)
    else:
        magacin.append(flask.request.json)
        return flask.json.jsonify(magacin[-1])

@app.route("/stavke/<int:index>", methods=["DELETE"])
def ukloni_stavku(index):
    magacin[index]["aktivan"] = False
    return ""

app.run(host="0.0.0.0" , port=5000, threaded=True)
