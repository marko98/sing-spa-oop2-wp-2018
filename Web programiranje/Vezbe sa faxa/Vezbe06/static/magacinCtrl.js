(function(angular){
    angular.module("myApp").controller("MyCtrl", ["$http", function($http){
        var mc = this;
        mc.novaStavka = {"naziv": "", "kolicina": 0, "aktivan": true};
        mc.stavke = [];

        mc.getAllStavke = function(){
            // ponekad treba poslediti i celi url
            $http.get("/stavke").then(
            function(response){
                mc.stavke = response.data;
                console.log(response);

            }, function(response){
                console.log("neuspesno");

            });
        }

        mc.ukloniStavku = function(index){
            $http.delete("/stavke/" + index).then(
            function(response){
                mc.getAllStavke();
            }, function(response){

            });
            //mc.stavke.splice(index, 1);
        };

        mc.dodajStavku = function(){
            $http.post("/stavke", mc.novaStavka).then(
            function(response){
                // sve stavke ce se azurirati
                // mc.getAllStavke();

                // sve stavke se nece azurirati
                mc.stavke.push(angular.copy(mc.novaStavka));
            }, function(response){
                console.log("neuspesno");

            });
        };
        mc.getAllStavke();
    }]);
})(angular);