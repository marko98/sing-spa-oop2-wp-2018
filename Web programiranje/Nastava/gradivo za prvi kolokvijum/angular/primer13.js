var app = angular.module('myApp', []);
app.controller('MyCtrl', function($scope, $window) {
    var vm = this;
    vm.email= "nesto@proba.net";
    vm.password= "1234";
    vm.message = "Trenutno niste prijavljeni na sistem!";
    vm.autorizovan = false;
    vm.search = "";

    vm.login = function(){
      if(vm.password == '1234'){
        vm.message = "Prijevljen je korisnik: "+vm.email;
        vm.autorizovan = true;
        $window.localStorage.setItem('user', vm.email);
      }else{
        vm.message = "Pogresna lozinka";
      }
    }

    vm.logout = function(){
      vm.password = "";
      vm.autorizovan = false;
      vm.message = "Hvala sto ste koristili aplikaciju.";
      $window.localStorage.removeItem('user')
    }

    vm.studenti = []

    vm.init = function(){
      var data = ['1 2016270320 Јово Мирковић 1',
                  '2 2016270752 Стефан Ивановић 1',
                  '3 2016270205 Душан Медић 1',
                  '4 2016270377 Лука Зец 1',
                  '5 2016270527 Стефан Комлушан 1',
                  '6 2016270214 Дино Суботички 1',
                  '7 2016270602 Ана Печенковић 1',
                  '8 2016270192 Давид Маљик 1',
                  '9 2016270251 Алекса Ћирић 1',
                  '10 2016270710 Кристина Никић 1',
                  '11 2016270866 Никола Наумовски 1',
                  '12 2016271004 Дејан Марковић 1',
                  '13 2016270185 Радован Радовановић 1',
                  '14 2016270897 Стеван Селих 1',
                  '15 2016270159 Denis Povolny 1',
                  '16 2016270995 Александар Будинчевић 1',
                  '17 2016270100 Милан Бачкоња 1',
                  '18 2016270694 Станко Гаврић 1',
                  '19 2016270832 Алекса Вујић 1',
                  '1 2016270365 Вујош Петровић 2',
                  '2 2016270061 Стефан Бига 2',
                  '3 2016270515 Марко Живанов 2',
                  '4 2016270171 Алексеj Ковачевић 2',
                  '5 2016270267 Давид Чампар 2',
                  '6 2016270383 Владимир Свирачевић 2',
                  '7 2016270116 Немања Мудрински 2',
                  '8 2016270821 Игор Бечелић 2',
                  '9 2016270493 Филип Ћурчић 2',
                  '10 2016270799 Вања Николић 2',
                  '11 2016270233 Кристијан Кончар 2',
                  '12 2016271015 Ружица Билановић 2',
                  '13 2016270429 Бојан Вујановић 2',
                  '14 2016270396 Немања Влаисављевић 2',
                  '15 2016270656 Марко Рајић 2',
                  '16 2016270280 Александар Живковић 2',
                  '17 2016270486 Јаков Митровић 2',
                  '18 2016270472 Бранислав Вучковић 2',
                  '19 2016270969 Милош Гојковић 2'];
        for(var i in data){
          var linija = data[i];
          var parts = linija.split(' ');
          var student = {
            rb: parts[0],
            indeks: parts[1],
            ime: parts[2],
            prezime: parts[3],
            grupa: "Г"+parts[4]
          }
          vm.studenti.push(student);
        }
        vm.email = $window.localStorage.getItem('user');
        if(vm.email)
          vm.autorizovan = true;
    }

    vm.call = function(el){
      el.f();
    }

    vm.student = null;

    vm.selectStudenta = function(el){
      vm.student = el;
      vm.breadcrumb.push({f:null, label: el.ime+' '+el.prezime});
    };

    vm.create = false;

    vm.noviStudent = function(){
      vm.create = true;
      vm.student = {};
      vm.breadcrumb.push({f:null, label: 'Novi student'});
    }

    vm.save = function(){
      if(vm.student != null){
        if(vm.create){
          vm.studenti.push(vm.student);
        }
        vm.student = null;
        vm.breadcrumb.pop();
      }
      vm.create = false;
    }

    vm.cancel = function(){    /*    NOVO   */
      if(vm.student != null){
        vm.student = null;
        vm.breadcrumb.pop();
      }
      vm.create = false;
    }

    vm.breadcrumb = [
        {f: vm.cancel, label:'Studenti'}
    ];

    vm.init();
});
