var app = angular.module('myApp', []);
app.controller('MyCtrl', function($scope, $window, $timeout) {
    var vm = this;
    vm.email= "nesto@proba.net";
    vm.password= "1234";
    vm.message = "Trenutno niste prijavljeni na sistem!";
    vm.autorizovan = false;
    vm.search = "";
    vm.menuVisible = true;

    vm.hideMenu = function(){
      $timeout(function(){vm.menuVisible = false;}, 3000);
    }

    vm.login = function(){
      if(vm.password == '1234'){
        vm.message = "Prijevljen je korisnik: "+vm.email;
        vm.autorizovan = true;
        $window.localStorage.setItem('user', vm.email);
        vm.hideMenu();
      }else{
        vm.message = "Pogresna lozinka";
      }
    }

    vm.logout = function(){
      vm.password = "";
      vm.autorizovan = false;
      vm.message = "Hvala sto ste koristili aplikaciju.";
      $window.localStorage.removeItem('user')
    }

    vm.init = function(){
        vm.menuVisible = true;
        vm.email = $window.localStorage.getItem('user');
        if(vm.email){
          vm.autorizovan = true;
          vm.hideMenu();
        }
    }

    vm.breadcrumb = [];

    vm.init();
});
