import flask
from flask import Flask

app = Flask(__name__, static_url_path="")

#http://localhost:5000/
@app.route("/")
def main():
    return "Uspesno"

@app.route("/forma")
@app.route("/forma.html")
def home ( ) :
    return flask.send_file("forma.html")

@app.route("/regstracija", methods=["POST", "GET"])
def registracija():
    return flask.render_template("registracija.html")

app.run(host="0.0.0.0", port=5000, debug=True, threaded=True)