(function(angular){
    var app = angular.module("MyApp", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "home", //Naziv stanja.
            url: "/", //URL koji se mapira na zadato stanje. Ovaj URL ce zapravo biti vidljiv kao identifikator fragmenta.
            templateUrl: "app/components/objekti/objekti.tpl.html", //URL do sablona za generisanje prikaza.
            controller: "ObjektiCtrl", //Kontroler koji je potrebno injektovati u prikaz.
            controllerAs: "oc" //Posto ce biti upotrebljen imenovani kontroler, navodi se i ime kontrolera.
        }).state({
            name: "objekat", //Naziv stanja.
            url: "/objekat/{id:int}", // PRVO NAZIV, PA TIP VREDNOSTI!!!!!
            templateUrl: "app/components/objekat/objekat.tpl.html", //URL do sablona za generisanje prikaza.
            controller: "ObjekatCtrl", //Kontroler koji je potrebno injektovati u prikaz.
            controllerAs: "oc" //Posto ce biti upotrebljen imenovani kontroler, navodi se i ime kontrolera.
        }); 

        $urlRouterProvider.otherwise("/");
    }]);
})(angular)