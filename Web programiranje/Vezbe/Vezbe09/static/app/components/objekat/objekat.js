(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("ObjekatCtrl", ["$stateParams", "$http", function ($stateParams, $http) {
        var vm = this;
        vm.trazeniObjekat = {};
        vm.izmenjeniObjekat = {
            "kolicina": 0,
            "opis": "Nema opisa"
        };

        vm.dobaviObjekat = function(id){
            $http.get("/objekti/" + id).then(function(response){
                vm.trazeniObjekat = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju objekta! Kod: " + response.status);
            });
        };

        vm.izmeniObjekat = function(){
            $http.put("/objekti/" + vm.trazeniObjekat.id, vm.izmenjeniObjekat).then(function(response){
                vm.dobaviObjekat(vm.trazeniObjekat.id);
            }, function(response) {
                console.log("Greska pri izmeni objekta! Kod: " + response.status);
            });
        };

        vm.dobaviObjekat($stateParams["id"]);

    }]);
})(angular);