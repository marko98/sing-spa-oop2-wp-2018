(function(angular){
    var app = angular.module("MyApp");

    app.controller("ObjektiCtrl", ["$http", function ($http) {
        var vm = this;
        vm.objekti = [];
        vm.noviObjekat = {
            "naziv": "",
            "kolicina": 0,
            "opis": "Nema opisa"
        };
        vm.pretraga = {
            "naziv": "",
            "opis": "",
            "kolicinaOd": undefined,
            "kolicinaDo": undefined
        };


        vm.dobaviObjekte = function(){
            $http.get("/objekti", {params: vm.pretraga}).then(function(response){
                vm.objekti = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju objekata! Kod: " + response.status);
            });
        };

        vm.dodajObjekat = function(){
            $http.post("/objekti", vm.noviObjekat).then(function(response){
                vm.dobaviObjekte();
            }, function(response) {
                console.log("Greska pri dobavljanju objekata! Kod: " + response.status);
            });
        };

        vm.ukloniObjekte = function(id){
            $http.delete("/objekti/" + id).then(function(response){
                vm.dobaviObjekte();
            }, function(response) {
                console.log("Greska pri brisanju objekta! Kod: " + response.status);
            });
        }

        vm.dobaviObjekte();

    }]);

})(angular);