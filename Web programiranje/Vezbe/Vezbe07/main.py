import flask
import datetime
from flask import Flask
from flask import request
from werkzeug.utils import secure_filename
from functools import wraps
import os
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

from blueprints.users_blueprint import users_blueprint
from blueprints.followings_blueprint import followings_blueprint
from blueprints.followers_blueprint import followers_blueprint
from blueprints.messages_blueprint import messages_blueprint
from blueprints.likes_blueprint import likes_blueprint
from blueprints.icons_blueprint import icons_blueprint
from blueprints.comments_blueprint import comments_blueprint
from blueprints.posts_blueprint import posts_blueprint
from blueprints.uploads_blueprint import uploads_blueprint
from blueprints.genders_blueprint import genders_blueprint
from blueprints.likes_followings_comments_blueprint import likes_followings_comments_blueprint
from blueprints.countries_states_cities_blueprint import countries_states_cities_blueprint

from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")
app.register_blueprint(users_blueprint, url_prefix="/users")
app.register_blueprint(followings_blueprint, url_prefix="/followings")
app.register_blueprint(followers_blueprint, url_prefix="/followers")
app.register_blueprint(messages_blueprint, url_prefix="/messages")
app.register_blueprint(likes_blueprint, url_prefix="/likes")
app.register_blueprint(icons_blueprint, url_prefix="/icons")
app.register_blueprint(comments_blueprint, url_prefix="/comments")
app.register_blueprint(posts_blueprint, url_prefix="/posts")
app.register_blueprint(uploads_blueprint, url_prefix="/upload")
app.register_blueprint(genders_blueprint, url_prefix="/genders")
app.register_blueprint(likes_followings_comments_blueprint, url_prefix="/lfc")
app.register_blueprint(countries_states_cities_blueprint)
app.secret_key = "tralabu"

app.config['MYSQL_DATABASE_USER'] = 'marko' # Korisnicko ime korisnika baze podataka.
app.config['MYSQL_DATABASE_PASSWORD'] = 'rumenka2012' # Lozinka izabranog korisnika. 
app.config['MYSQL_DATABASE_DB'] = 'instagram' # Ime seme baze podataka koja se koristi.

mysql_db.init_app(app)

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)