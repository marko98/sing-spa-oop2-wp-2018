(function(angular){
    var app = angular.module("MyApp");

    app.controller("UploadCtrl", ["$rootScope", "$interval", "Upload", "$state", "$http", function ($rootScope, $interval, Upload, $state, $http) {
        var vm = this;

        vm.pictures = [];
        vm.user = {
            "activation_key": null,
            "active": 0,
            "birth_date": null,
            "city_id": null,
            "date_created": null,
            "date_updated": null,
            "email": "",
            "gender_id": 0,
            "password": "",
            "profile_picture_url": null,
            "real_name": "",
            "user_id": 0,
            "username": ""
        };
        vm.firstPictureName = "";
        vm.caption = null;
        vm.oneStepToPost = true;

        vm.fillData = function(params){
            vm.user = params.user;
            if(params.upload.length > 1){
                vm.oneStepToPost = false;
            } else {
                vm.firstPictureName = params.upload[0].name;
            };
            for(let i = 0; i < params.upload.length; i++){
                vm.pictures.push(params.upload[i]);
            };
        };

        vm.goExplore = function(){
            $state.go('explore', {username: vm.user.username, userId: vm.user.user_id});
        };
        
        vm.goSearch = function(){
            $state.go('search', {username: vm.user.username, userId: vm.user.user_id});
        };

        vm.goEvents = function(){
            $state.go('events', {username: vm.user.username, userId: vm.user.user_id});
        };
        
        vm.goUser = function(){
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.post = function(){
            var files = $state.params.upload;
            var post = {
                "userId": vm.user.user_id,
                "caption": vm.caption,
                "comment": null,
                "latitude": null,
                "longitude": null,
                "type": "image",
                "postUrl": "users/" + vm.user.username + "/posts/;" + vm.firstPictureName
            };

            for (var i = 0; i < files.length; i++) {
                if(files[i].name != vm.firstPictureName){
                    post.postUrl = post.postUrl + ";" + files[i].name;
                };
            };

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                console.log("Geolocation is not supported by this browser.");
            };

            function showPosition(position) {
                post.latitude = position.coords.latitude;
                post.longitude = position.coords.longitude;
                while (post.latitude == null || post.longitude == null){
                };
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (!file.$error) {
                        Upload.upload({
                            url: '/upload',
                            data: {
                                file: file,
                                files: files
                            },
                            method: "POST",
                            params: {username: vm.user.username, folder: "posts", post: post}
                        }).then(function (response) {
                            if(response.status == 201){
                                $state.go("user", {username: $state.params.username, userId: $state.params.userId}, {reload: "user"});
                            };
                        }, function (response) {
                            $state.go('sign_up');
                        });
                    };
                };
                $http.post("/upload", post).then(function(response){
                }, function(response) {
                    console.log("Greska pri uploadovanju! Kod: " + response.status);
                });
            };
        };

        vm.selectFirstImage = function(firstImageName){
            vm.oneStepToPost = true;
            vm.firstPictureName = firstImageName;
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(params){
            vm.cancelIntervals();
            if(params.user == null || params.upload == null){
                $state.go("user", {username: $state.params.username, userId: $state.params.userId}, {reload: "user"});
            } else {
                vm.fillData(params);
            };
        };

        vm.onLoad($state.params);       

    }]);

})(angular);