(function(angular){
    var app = angular.module("MyApp");

    app.directive('ngRightClick', function($parse) {
        return function(scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function(event) {
                scope.$apply(function() {
                    event.preventDefault();
                    fn(scope, {$event:event});
                });
            });
        };
    });

    app.controller("UserCtrl", ["$rootScope", "$interval", "$state", "$http", "Upload", "$timeout", "$scope", function ($rootScope, $interval, $state, $http, Upload, $timeout, $scope) {
        var vm = this;

        vm.city = null;
        vm.currentUpload = null;
        vm.showUploads = true;
        vm.showEmail = false;
        vm.uploads = [];
        vm.followings = [];
        vm.followers = [];
        vm.iconPath = "images/user.png";
        vm.user = {
            "birth_date": null,
            "city_id": null,
            "date_created": null,
            "date_updated": null,
            "email": "",
            "gender_id": 0,
            "password": "",
            "real_name": "",
            "username": ""
        };
        vm.params = {
            "email": "",
            "username": $state.params.username,
            "password": "",
            "userId": $state.params.userId
        };

        $scope.decrement = function(show, upload) {
            if(show == "true"){
                vm.showUploads = false;
                var params = {
                    "postId": upload.data.post_id
                };

                // COMMENTS
                $http.get("/comments", {params: params}).then(function(response){
                    if(response.data.length > 0){
                        upload["numberOfComments"] = response.data.length;
                    } else {
                        upload["numberOfComments"] = 0;
                    };
                }, function(response) {
                    console.log("Error! Code: " + response.status);
                });

                // LIKES
                params["action"] = "allLikes";
                $http.get("/likes", {params: params}).then(function(response){
                    if(response.data.length > 0){
                        upload["numberOfLikes"] = response.data.length;
                    } else {
                        upload["numberOfLikes"] = 0;
                    };
                }, function(response) {
                    console.log("Error! Code: " + response.status);
                });

                vm.currentUpload = upload;
            } else {
                vm.showUploads = true;
                vm.currentUpload = null;
            };
        };

        vm.startInterval = function(){
            $rootScope.intervalUser = $interval(function(){
                // console.log("radi user");
                vm.getUploads();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalUser);
        };

        vm.showPosts = function(){
            vm.showUploads = true;
            vm.currentUpload = null;
        };

        $scope.log = '';
        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });
        $scope.$watch('file', function () {
            if ($scope.file != null) {
                $scope.files = [$scope.file]; 
            }
        });

        vm.deletePost = function(postId){
            $http.delete("/posts/"+postId).then(function(response){
                vm.getUploads();
                vm.showPosts();
            }, function(response){
                console.log("Error! Code: " + response.status);
            });
        };

        vm.deleteUser = function(){
            $http.delete("/users/"+$state.params.userId).then(function(response){
                vm.logout();
            }, function(response){
                console.log("Error! Code: " + response.status);
            });
        };

        vm.editProfile = function(){
            vm.stop();
            $state.go('editProfile', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.action = "uploadPost";
        vm.chooseAction = function(action){
            vm.action = action;
        };
    
        $scope.upload = function (files) {
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (!file.$error) {
                        var params;
                        if(vm.action == "uploadPost"){
                            params = {username: vm.user.username, folder: "temp"};
                        } else {
                            params = {username: vm.user.username, folder: "icon"};
                        };
                        Upload.upload({
                            url: '/upload',
                            data: {
                                file: file,
                                files: files
                            },
                            params: params
                        }).then(function (response) {
                            if(response.status == 201 && vm.action != "uploadIcon"){
                                vm.stop();
                                $state.go('upload', {username: vm.user.username, userId: vm.user.user_id, user: vm.user, upload: files});
                            } else if(response.status == 201 && vm.action == "uploadIcon"){
                                vm.getIcon();
                            };
                        }, function (response) {

                        });
                    }
                }
            }
        };

        vm.changePic = function(index, direction){
            if(index == null){
                var upload = vm.currentUpload;
            } else {
                var upload = vm.uploads[index];
            };
            if(direction == 'toLeft'){
                if(upload.show - 1 < 0){
                    upload.show = upload.urls.length - 1;
                } else {
                    upload.show = upload.show - 1;
                };
            } else {
                if(upload.show + 1 == upload.urls.length){
                    upload.show = 0;
                } else {
                    upload.show = upload.show + 1;
                };
            }; 
        };

        vm.logout = function(){
            $http.get("/users/logout/"+$state.params.username).then(function(response){
                vm.cancelIntervals();
                vm.stop();
                $state.go('login');
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.getFollowings = function(){
            $http.get("/followings", {params: vm.params}).then(function(response){
                vm.followings = response.data;
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getFollowers = function(){
            $http.get("/followers", {params: vm.params}).then(function(response){
                vm.followers = response.data;
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.seePost = function(postId, userId){
            vm.stop();
            $state.go('post', {username: $state.params.username, userId: $state.params.userId, otherUserId: userId, postId: postId});
        };

        vm.goFollowers = function(){
            vm.stop();
            $state.go('followers', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goFollowings = function(){
            vm.stop();
            $state.go('followings', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.getUploads = function(){
            $http.get("/posts", {params: vm.params}).then(function(response){
                vm.uploads = [];
                if(response.data.length > 0){
                    for(let i = response.data.length-1; i >= 0; i--){
                        var paths = response.data[i].post_url.split(";");
                        paths.shift();
                        var post = {
                            "onePage": true,
                            "show": 0,
                            "urls": paths,
                            "data": response.data[i]
                        };
                        if(paths.length > 1){
                            post.onePage = false;
                        };
                        vm.uploads.push(post);
                    };
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getUser = function(params){
            $http.get("/users", {params: vm.params}).then(function(response){
                if(response.data.length > 0){
                    vm.user = response.data[0];
                    vm.getCity(response.data[0].city_id);
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.showEmailAddress = function(){
            if(vm.showEmail == true){
                vm.showEmail = false;
            } else {
                vm.showEmail = true;
            };
        };

        vm.getIcon = function(){
            $http.get("/icons", {params: vm.params}).then(function(response){
                if(response.data != null){
                    vm.iconPath = response.data["icon_url"];
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.getCity = function(cityId){
            $http.get("/city/"+cityId).then(function(response){
                if(response.data.length > 0){
                    vm.city = response.data[0].name;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getUser();
            vm.getIcon();
            vm.getUploads();
            vm.getFollowings();
            vm.getFollowers();
        };

        vm.goExplore = function(){
            vm.stop();
            $state.go('explore', {username: vm.user.username, userId: vm.user.user_id});
        };
        
        vm.goSearch = function(){
            vm.stop();
            $state.go('search', {username: vm.user.username, userId: vm.user.user_id});
        };

        vm.goEvents = function(){
            vm.stop();
            $state.go('events', {username: vm.user.username, userId: vm.user.user_id});
        };
        
        vm.onLoad();

    }]);

})(angular);