(function(angular){
    var app = angular.module("MyApp");

    app.controller("EditProfileCtrl", ["$state", "$http", "$interval", "$rootScope", function ($state, $http, $interval, $rootScope) {
        var vm = this;
        
        vm.oldPassword = "";
        vm.username = $state.params.username;
        vm.userId = $state.params.userId;
        vm.user = {
            "birthDate": null,
            "cityId": null,
            "email": "",
            "genderId": null,
            "password": "",
            "realName": ""
        };
        vm.birthDate = null;

        vm.submit = function(){
            $http.get("/users/"+$state.params.username+"/"+$state.params.userId+"/"+vm.oldPassword).then(function(response){
                if(response.data == "True"){
                    if(vm.birthDate != null){
                        var date = vm.birthDate.toISOString().slice(0, 19).split("T");
                        date = date[0];
                        vm.user.birthDate = date;
                    };            
                    vm.user["userId"] = $state.params.userId;
                    $http.put("/users", vm.user).then(function(response) {
                        vm.oldPassword = "";
                        vm.goUser();             
                    }, function(response) {
                        console.log("Error! Code: " + response.status);
                    });
                } else {
                    $http.get("/users/logout/"+$state.params.username).then(function(response){
                        vm.cancelIntervals();
                        $state.go('login');
                    }, function(response) {
                        console.log("Error! Code: " + response.status);
                        $state.go('sign_up');
                    });
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                $state.go('sign_up');
            });
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.getCountries();
        };

        vm.goExplore = function(){
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };
        
        vm.goSearch = function(){
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goEvents = function(){
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.countries = [];
        vm.country = null;
        vm.isCountrySelected = function(){
            if(vm.country != null){
                return true;
            };
            return false;
        };
        vm.states = [];
        vm.state = null;
        vm.isStateSelected = function(){
            if(vm.state != null){
                return true;
            };
            return false;
        };
        vm.cities = [];
        vm.isCitySelected = function(){
            if(vm.user.cityId != null){
                return true;
            };
            return false;
        };  
        vm.genders = [];
        vm.isGenderSelected = function(){
            if(vm.user.genderId != null){
                return true;
            };
            return false;
        };
        vm.birthDate = null;

        vm.getCountries = function(){
            $http.get("/countries").then(function(response){
                if(response.data.length > 0){
                    vm.countries = response.data;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getStates = function(){
            $http.get("/countries/"+vm.country).then(function(response){
                if(response.data.length > 0){
                    vm.states = response.data;
                };
            }, function(response) {
                console.log("Select country! Error! Code: " + response.status);
            });
        };

        vm.getCities = function(){
            $http.get("/cities/"+vm.state).then(function(response){
                if(response.data.length > 0){
                    vm.cities = response.data;
                };
            }, function(response) {
                console.log("Select state! Error! Code: " + response.status);
            });
        };

        vm.getGenders = function(){
            $http.get("/genders").then(function(response){
                if(response.data.length > 0){
                    vm.genders = response.data;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };
        
        vm.onLoad();

    }]);

})(angular);