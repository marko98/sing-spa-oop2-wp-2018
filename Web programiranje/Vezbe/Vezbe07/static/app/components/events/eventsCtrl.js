(function(angular){
    var app = angular.module("MyApp");

    app.controller("EventsCtrl", ["$http", "$state", "$interval", "$rootScope", function ($http, $state, $interval, $rootScope) {
        var vm = this;
        
        vm.currentUserName = $state.params.username;
        vm.data = [];
        vm.params = {
            "userId": $state.params.userId
        };

        vm.startInterval = function(){
            $rootScope.intervalEvents = $interval(function(){
                // console.log("radi events");
                vm.getData();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalEvents);
        };

        vm.goExplore = function(){
            vm.stop();
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goSearch = function(){
            vm.stop();
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            vm.stop();
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.check = function(text){
            if(text == "started following you."){
                return false;
            };
            return true;
        };

        vm.seeAccount = function(otherUserId){
            vm.stop();
            $state.go('foundPerson', {username: $state.params.username, userId: $state.params.userId, foundPersonId: otherUserId});
        };

        vm.seePost = function(postId, otherUserId){
            vm.stop();
            $state.go('post', {username: $state.params.username, userId: $state.params.userId, otherUserId: otherUserId, postId: postId});
        };

        vm.getData = function(){
            $http.get("/lfc", {params: vm.params}).then(function(response){
                if(response.data.length > 0){
                    vm.data = [];
                    for(let i = 0; i < response.data.length; i++){
                        if(response.data[i]["table_name"] == "like"){
                            response.data[i]["text"] = "liked your post.";
                        } else if (response.data[i]["table_name"] == "comment"){
                            response.data[i]["text"] = "commented: " + response.data[i]["comment"];
                        } else {
                            response.data[i]["text"] = "started following you.";
                        };

                        var stringDate = "";
                        var difference = new Date(new Date() - new Date(response.data[i]["created"]));
                        var day = new Date(response.data[i]["created"]).getDay();
                        if(new Date().getDay() == new Date(response.data[i]["created"]).getDay()){
                            if(difference.getHours()-1 != 0){
                                stringDate += difference.getHours()-1 + "h ";
                            };
                            if(difference.getMinutes() != 0){
                                stringDate += difference.getMinutes()-1 + "min ";
                            };
                            if(difference.getSeconds() != 0){
                                stringDate += difference.getSeconds()-1 + "s ";
                            };
                            if(stringDate != ""){
                                stringDate += "ago";
                            };
                             
                        } else if(new Date() - new Date(response.data[i]["created"]) >= 604800000){
                            stringDate += "more than a week ago";
                        } else{
                            if(day == 1){
                                stringDate = "monday";
                            } else if(day == 2){
                                stringDate = "tuesday";
                            } else if(day == 3){
                                stringDate = "wednesday";
                            } else if(day == 4){
                                stringDate = "thursday";
                            } else if(day == 5){
                                stringDate = "friday";
                            } else if(day == 6){
                                stringDate = "saturday";
                            } else if(day == 0){
                                stringDate = "sunday";
                            };
                        };
                        
                        response.data[i]["ago"] = stringDate;

                        vm.data.push(response.data[i]);
                    };
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getData();
        };

        vm.onLoad();

    }]);

})(angular);