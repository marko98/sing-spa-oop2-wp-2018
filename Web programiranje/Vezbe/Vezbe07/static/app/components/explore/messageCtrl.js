(function(angular){
    var app = angular.module("MyApp");

    app.controller("MessageCtrl", ["$http", "$state", "$interval", "$rootScope", function ($http, $state, $interval, $rootScope) {
        var vm = this;
        
        vm.currentUserName = $state.params.username;
        vm.foundPersonName;
        vm.newMessage = {
            "userId": $state.params.userId,
            "foundPersonId": $state.params.friendId,
            "content": ""
        };
        vm.params = {
            "userId": $state.params.userId,
            "friendId": $state.params.friendId
        };
        vm.messages = [];

        vm.startInterval = function(){
            $rootScope.intervalMessage = $interval(function(){
                // console.log("radi message");
                vm.getMessages();
            }, 5000);
        };   

        vm.stop = function(){
            $interval.cancel($rootScope.intervalMessage);
        };

        vm.goExplore = function(){
            vm.stop();
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goSearch = function(){
            vm.stop();
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goEvents = function(){
            vm.stop();
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            vm.stop();
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.isMine = function(id){
            if(id == $state.params.userId){
                return true;
            } else {
                return false;
            }
        };

        vm.showDate = function(id){
            for(let i = 0; i < vm.messages.length; i++){
                if(vm.messages[i].message_id == id){
                    if(vm.messages[i].showDate == false){
                        vm.messages[i].showDate = true;
                    } else {
                        vm.messages[i].showDate = false;
                    };
                };
            };
        };

        vm.getMessages = function(){
            $http.get("/messages", {params: vm.params}).then(function(response){
                if(response.data.length > 0){
                    vm.messages = [];
                    for(let i = response.data.length-1; i >= 0; i--){
                        vm.messages.push(response.data[i]);
                    };
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };
        
        vm.sendNewMessage = function(){
            if(vm.newMessage.content == ""){
                return;
            };
            $http.post("/messages", vm.newMessage).then(function(response) {
                vm.newMessage.content = "";
                vm.messages = [];
                vm.getMessages();
            }, function() {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getFriend = function(){
            params = {
                "foundPersonId": $state.params.friendId
            };
            $http.get("/users", {params: params}).then(function(response){
                vm.foundPersonName = response.data.username;
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getMessages();
            vm.getFriend();
        };

        vm.onLoad();

    }]);

})(angular);