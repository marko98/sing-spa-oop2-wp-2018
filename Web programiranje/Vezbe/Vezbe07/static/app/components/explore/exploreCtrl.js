(function(angular){
    var app = angular.module("MyApp");

    app.controller("ExploreCtrl", ["$http", "$state", "$interval", "$rootScope", function ($http, $state, $interval, $rootScope) {
        var vm = this;
        
        vm.currentUserName = $state.params.username;
        vm.followings = [];
        vm.uploads = [];
        vm.params = {
            "email": "",
            "username": "",
            "password": "",
            "followingUsers": []
        };

        vm.startInterval = function(){
            $rootScope.intervalExplore = $interval(function(){
                // console.log("radi explore");
                vm.getFollowings();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalExplore);
        };

        vm.openMessages = function(){
            vm.stop();
            $state.go('messages', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goSearch = function(){
            vm.stop();
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goEvents = function(){
            vm.stop();
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            vm.stop();
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.changePic = function(index, direction){
            if(direction == 'toLeft'){
                if(vm.uploads[index].show - 1 < 0){
                    vm.uploads[index].show = vm.uploads[index].urls.length - 1;
                } else {
                    vm.uploads[index].show = vm.uploads[index].show - 1;
                };
            } else {
                if(vm.uploads[index].show + 1 == vm.uploads[index].urls.length){
                    vm.uploads[index].show = 0;
                } else {
                    vm.uploads[index].show = vm.uploads[index].show + 1;
                };
            }; 
        };

        vm.getFollowings = function(){
            params = vm.params;
            params["userId"] = $state.params.userId;
            $http.get("/followings", {params: params}).then(function(response){
                vm.followings = response.data;
                vm.params.followingUsers = response.data;
                vm.getUploads();
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.getUploads = function(){
            data = {
                "action": "explore",
                "userId": $state.params.userId,
                "followingUsersId": ""
            }
            for(let i = 0; i < vm.followings.length; i++){
                data.followingUsersId = data.followingUsersId + ";" + vm.followings[i].following_id;
            };
            $http.get("/posts", {params: data}).then(function(response){
                if(response.data.length > 0){
                    vm.uploads = [];
                    for(let i = 0; i < response.data.length; i++){
                        var paths = response.data[i].post_url.split(";");
                        var path = paths.shift();
                        var username = path.split("/");
                        username = username[1];
                        var post = {
                            "path": path,
                            "onePage": true,
                            "show": 0,
                            "username": username,
                            "urls": paths,
                            "data": response.data[i]
                        };
                        if(paths.length > 1){
                            post.onePage = false;
                        };
                        vm.uploads.push(post);
                    };
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.goToProfile = function(otherUserId){
            vm.stop();
            $state.go('foundPerson', {username: $state.params.username, userId: $state.params.userId, foundPersonId: otherUserId});
        };

        vm.seePost = function(postId, otherUserId){
            vm.stop();
            $state.go('post', {username: $state.params.username, userId: $state.params.userId, otherUserId: otherUserId, postId: postId});
        };
        
        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getFollowings();
        };

        vm.onLoad();

    }]);

})(angular);