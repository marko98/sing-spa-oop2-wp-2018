(function(angular){
    var app = angular.module("MyApp");

    app.controller("MessagesCtrl", ["$http", "$state", "$interval", "$rootScope", function ($http, $state, $interval, $rootScope) {
        var vm = this;
        
        vm.currentUserName = $state.params.username;
        vm.params = {
            "usernameSearch": "",
            "userId": $state.params.userId
        };
        vm.found = [];
        vm.newestMessages = [];
        vm.userIdFrom;

        vm.startInterval = function(){
            $rootScope.intervalMessages = $interval(function(){
                // console.log("radi messages");
                vm.getMessages();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalMessages);
        };

        vm.goExplore = function(){
            vm.stop();
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goSearch = function(){
            vm.stop();
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goEvents = function(){
            vm.stop();
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            vm.stop();
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.search = function(){
            $http.get("/users", {params: vm.params}).then(function(response){
                vm.found = response.data;
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getMessages = function(){
            $http.get("/messages", {params: vm.params}).then(function(response){
                vm.newestMessages = [];
                if(response.data.length > 0){
                    for(let i = response.data.length-1; i >= 0; i--){
                        var add = true;
                        for(let j = 0; j < vm.newestMessages.length; j++){
                            if(vm.newestMessages[j].from.user_id == response.data[i].from.user_id){
                                add = false;
                            };
                        };
                        if(add){
                            vm.newestMessages.push(response.data[i]);
                        };                        
                    };
                };
                if(vm.newestMessages.length > 0){
                    vm.userIdFrom = vm.newestMessages[0].from.user_id;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.showMessages = function(friendId){
            vm.stop();
            $state.go('message', {username: $state.params.username, userId: $state.params.userId, friendId: friendId});
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getMessages();
        };

        vm.onLoad();

    }]);

})(angular);