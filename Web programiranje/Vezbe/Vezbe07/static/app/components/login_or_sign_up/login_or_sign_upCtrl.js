(function(angular){
    var app = angular.module("MyApp");

    app.controller("LoginOrSignUpCtrl", ["$rootScope", "$interval", "$http", "$state", function ($rootScope, $interval, $http, $state) {
        var vm = this;
        
        vm.userSignIn = {
            "email": "",
            "realName": "",
            "password": "",
            "username": "",
            "city_id": null,
            "gender": null,
            "birth_date": null
        };
        vm.params = {
            "email": "",
            "username": "",
            "password": ""
        };
        vm.duplicateEmail;
        vm.duplicateUsername;
        vm.loginUsername;
        vm.password;
        vm.loginMessage;

        vm.login = function(){
            if(vm.loginUsername.includes("@")){
                vm.params.email = vm.loginUsername;
                vm.params.username = "";
            } else {
                vm.params.username = vm.loginUsername;
                vm.params.email = "";
            };
            vm.params.password = vm.password;
            vm.params.action = "login",
            $http.get("/users/login/"+vm.loginUsername, {params: vm.params}).then(function(response){
                if(response.data.length != 0){
                    vm.loginMessage = "";          
                    $state.go('user', {username: response.data[0].username, userId: response.data[0].user_id});
                } else {
                    vm.loginMessage = "User doesn't exist";
                };
            }, function(response) {
                vm.loginMessage = "User doesn't exist";
            });

        };

        vm.signUp = function(){
            if(vm.birthDate == null){
                return;
            };

            var date = vm.birthDate.toISOString().slice(0, 19).split("T");
            date = date[0];

            vm.userSignIn["city_id"] = vm.city;
            vm.userSignIn["birth_date"] = date;
            vm.userSignIn["gender"] = vm.gender;
            $http.post("/users", vm.userSignIn).then(function(response){
                vm.duplicateUsername = "";
                vm.duplicateEmail = "";
                vm.country = null;
                vm.state = null;
                vm.city = null;
                vm.birthDate = null;
                vm.gender = null;
                $state.go('login');
            }, function(response) {
                var su = this
                this.duplicateEntry = response.data.split("&quot;");
                duplicateEntry = duplicateEntry[1].split(" for key ");

                this.duplicateValue = duplicateEntry[0].split("'");
                duplicateValue = duplicateValue[1];
                
                this.mySqlMessage = duplicateEntry[1].split("'");
                mySqlMessage = mySqlMessage[1];
                
                if(mySqlMessage == "email_UNIQUE"){
                    vm.duplicateEmail = "Email address is taken."
                    vm.duplicateUsername = "";
                } else {
                    vm.duplicateUsername = "Username is taken."
                    vm.duplicateEmail = "";
                };
            });
        };

        vm.countries = [];
        vm.country = null;
        vm.isCountrySelected = function(){
            if(vm.country != null){
                return true;
            };
            return false;
        };
        vm.states = [];
        vm.state = null;
        vm.isStateSelected = function(){
            if(vm.state != null){
                return true;
            };
            return false;
        };
        vm.cities = [];
        vm.city = null;
        vm.isCitySelected = function(){
            if(vm.city != null){
                return true;
            };
            return false;
        };  
        vm.genders = [];
        vm.gender = null;
        vm.isGenderSelected = function(){
            if(vm.gender != null){
                return true;
            };
            return false;
        };
        vm.birthDate = null;

        vm.getCountries = function(){
            $http.get("/countries").then(function(response){
                if(response.data.length > 0){
                    vm.countries = response.data;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getStates = function(){
            $http.get("/countries/"+vm.country).then(function(response){
                if(response.data.length > 0){
                    vm.states = response.data;
                };
            }, function(response) {
                console.log("Select country! Error! Code: " + response.status);
            });
        };

        vm.getCities = function(){
            $http.get("/cities/"+vm.state).then(function(response){
                if(response.data.length > 0){
                    vm.cities = response.data;
                };
            }, function(response) {
                console.log("Select state! Error! Code: " + response.status);
            });
        };

        vm.getGenders = function(){
            $http.get("/genders").then(function(response){
                if(response.data.length > 0){
                    vm.genders = response.data;
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.getCountries();
        };

        vm.onLoad();

    }]);

})(angular);