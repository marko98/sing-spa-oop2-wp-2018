(function(angular){
    var app = angular.module("MyApp");

    app.controller("FoundPersonFollowersCtrl", ["$state", "$http", "Upload", "$interval", "$rootScope", function ($state, $http, Upload, $interval, $rootScope) {
        var vm = this;
        
        vm.username = $state.params.foundPersonUsername;
        vm.followers = [];

        vm.startInterval = function(){
            $rootScope.intervalFPFollowers = $interval(function(){
                // console.log("radi found person followers");
                vm.getFollowers();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalFPFollowers);
        };

        vm.seeAccount = function(otherUserId){
            vm.stop();
            $state.go('foundPerson', {username: $state.params.username, userId: $state.params.userId, foundPersonId: otherUserId});
        };

        vm.getFollowers = function(){
            var params = {
                "userId": $state.params.foundPersonId,
                "action": "getAllFollowers"
            };
            $http.get("/followers", {params: params}).then(function(response){
                if(response.data.length > 0){
                    vm.followers = [];
                    for(let i = 0; i < response.data.length; i++){
                        vm.followers.push(response.data[i]);
                    };
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                vm.stop();
                $state.go('sign_up');
            });
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            vm.cancelIntervals();
            vm.startInterval();
            vm.getFollowers();
        };

        vm.goExplore = function(){
            vm.stop();
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };
        
        vm.goSearch = function(){
            vm.stop();
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goEvents = function(){
            vm.stop();
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            vm.stop();
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };
        
        vm.onLoad();

    }]);

})(angular);