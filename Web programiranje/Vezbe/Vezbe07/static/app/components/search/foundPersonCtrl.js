(function(angular){
    var app = angular.module("MyApp");

    app.directive('ngRightClick', function($parse) {
        return function(scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function(event) {
                scope.$apply(function() {
                    event.preventDefault();
                    fn(scope, {$event:event});
                });
            });
        };
    });

    app.controller("FoundPersonCtrl", ["$state", "$http", "$scope", "$interval", "$rootScope", function ($state, $http, $scope, $interval, $rootScope) {
        var vm = this;
        
        vm.iconPath = "images/user.png";
        vm.foundPerson;
        vm.follow = false;
        vm.uploads = [];
        vm.followings = [];
        vm.followers = [];
        vm.params = {
            "email": "",
            "username": "",
            "password": "",
            "foundPersonId": $state.params.foundPersonId
        };
        vm.showEmail = false;
        vm.showUploads = true;
        vm.currentUpload = null;

        $scope.decrement = function(show, upload) {
            if(show == "true"){
                vm.showUploads = false;
                var params = {
                    "postId": upload.data.post_id
                };

                // COMMENTS
                $http.get("/comments", {params: params}).then(function(response){
                    if(response.data.length > 0){
                        upload["numberOfComments"] = response.data.length;
                    } else {
                        upload["numberOfComments"] = 0;
                    };
                }, function(response) {
                    console.log("Error! Code: " + response.status);
                });

                // LIKES
                params["action"] = "allLikes";
                $http.get("/likes", {params: params}).then(function(response){
                    if(response.data.length > 0){
                        upload["numberOfLikes"] = response.data.length;
                    } else {
                        upload["numberOfLikes"] = 0;
                    };
                }, function(response) {
                    console.log("Error! Code: " + response.status);
                });

                vm.currentUpload = upload;
            } else {
                vm.showUploads = true;
                vm.currentUpload = null;
            };
        };

        vm.startInterval = function(){
            $rootScope.intervalFPUser = $interval(function(){
                // console.log("radi found person user");
                vm.getUploads();
            }, 5000);
        };

        vm.stop = function(){
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.showPosts = function(){
            vm.showUploads = true;
            vm.currentUpload = null;
        };

        vm.goFollowers = function(){
            $state.go('foundPersonFollowers', {username: $state.params.username, userId: $state.params.userId, foundPersonUsername: vm.params.username, foundPersonId: $state.params.foundPersonId});
        };

        vm.goFollowings = function(){
            $state.go('foundPersonFollowings', {username: $state.params.username, userId: $state.params.userId, foundPersonUsername: vm.params.username, foundPersonId: $state.params.foundPersonId});
        };

        vm.wantToSendMessage = function(){
            $state.go('message', {username: $state.params.username, userId: $state.params.userId, friendId: $state.params.foundPersonId});
        };

        vm.changePic = function(index, direction){
            if(direction == 'toLeft'){
                if(vm.uploads[index].show - 1 < 0){
                    vm.uploads[index].show = vm.uploads[index].urls.length - 1;
                } else {
                    vm.uploads[index].show = vm.uploads[index].show - 1;
                };
            } else {
                if(vm.uploads[index].show + 1 == vm.uploads[index].urls.length){
                    vm.uploads[index].show = 0;
                } else {
                    vm.uploads[index].show = vm.uploads[index].show + 1;
                };
            }; 
        };

        vm.followPerson = function(){
            params = vm.params;
            params["userId"] = $state.params.userId;
            $http.post("/followings", params).then(function(response){
                vm.getFollowings();
                vm.getFollowers();
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.unfollowPerson = function(){
            params = vm.params;
            params["userId"] = $state.params.userId;
            $http.delete("/followings", {params: params}).then(function(response){
                vm.follow = false;
                vm.getFollowings();
                vm.getFollowers();
            }, function(response){
                console.log("Error! Code: " + response.status);
            });
        };

        vm.isFollowing = function(){
            $http.get("/followings/"+$state.params.userId+"/"+$state.params.foundPersonId).then(function(response) {
                vm.follow = response.data;
            }, function(response) {
                console.log(response.status);
            });
        };

        vm.getFollowings = function(){
            params = vm.params;
            params["userId"] = $state.params.userId;
            $http.get("/followings", {params: params}).then(function(response){
                vm.followings = response.data;
                vm.isFollowing();
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getFollowers = function(){
            $http.get("/followers", {params: vm.params}).then(function(response){
                vm.followers = response.data;
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getUploads = function(){
            $http.get("/posts", {params: vm.params}).then(function(response){
                if(response.data.length > 0){
                    vm.uploads = [];
                    for(let i = 0; i < response.data.length; i++){
                        var paths = response.data[i].post_url.split(";");
                        paths.shift();
                        var post = {
                            "onePage": true,
                            "show": 0,
                            "urls": paths,
                            "data": response.data[i]
                        };
                        if(paths.length > 1){
                            post.onePage = false;
                        };
                        vm.uploads.push(post);
                    };
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
            });
        };

        vm.getFoundUser = function(){
            $http.get("/users", {params: vm.params}).then(function(response){
                vm.foundPerson = response.data;
                vm.params.username = response.data.username;
            }, function(response) {
                console.log("Error! Code: " + response.status);
                $state.go('sign_up');
            });
        };

        vm.getIcon = function(){
            $http.get("/icons", {params: vm.params}).then(function(response){
                if(response.data != null){
                    vm.iconPath = response.data["icon_url"];
                };
            }, function(response) {
                console.log("Error! Code: " + response.status);
                $state.go('sign_up');
            });
        };

        vm.showEmailAddress = function(){
            if(vm.showEmail == true){
                vm.showEmail = false;
            } else {
                vm.showEmail = true;
            };
        };

        vm.seePost = function(postId, userId){
            $state.go('post', {username: $state.params.username, userId: $state.params.userId, otherUserId: userId, postId: postId});
        };

        vm.cancelIntervals = function(){
            $interval.cancel($rootScope.intervalEvents);
            $interval.cancel($rootScope.intervalExplore);
            $interval.cancel($rootScope.intervalMessage);
            $interval.cancel($rootScope.intervalMessages);
            $interval.cancel($rootScope.intervalPost);
            $interval.cancel($rootScope.intervalFPFollowers);
            $interval.cancel($rootScope.intervalFPFollowings);
            $interval.cancel($rootScope.intervalSearch);
            $interval.cancel($rootScope.intervalFollowers);
            $interval.cancel($rootScope.intervalFollowings);
            $interval.cancel($rootScope.intervalUser);
            $interval.cancel($rootScope.intervalFPUser);
        };

        vm.onLoad = function(){
            if($state.params.userId == $state.params.foundPersonId){
                $state.go('user', {username: $state.params.username, userId: $state.params.userId});
                return;
            };
            vm.cancelIntervals();
            vm.startInterval();
            vm.getFoundUser();
            vm.getUploads();
            vm.getIcon();
            vm.getFollowings();
            vm.getFollowers();
        };

        vm.goExplore = function(){
            $state.go('explore', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goSearch = function(){
            $state.go('search', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goEvents = function(){
            $state.go('events', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.goUser = function(){
            $state.go('user', {username: $state.params.username, userId: $state.params.userId});
        };

        vm.onLoad();
    }]);

})(angular);