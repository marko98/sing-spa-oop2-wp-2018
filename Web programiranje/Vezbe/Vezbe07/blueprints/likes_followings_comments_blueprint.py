import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

likes_followings_comments_blueprint = Blueprint("likes_followings_comments_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

@likes_followings_comments_blueprint.route("", methods=["GET"])
@secured()
def get_lfc():
    # print(request.args)
    data = []
    cr = main.mysql_db.get_db().cursor()
    
    if request.args.get("userId") is not None and request.args.get("userId") is not "":
        # print("you")
        query = "SELECT * FROM posts WHERE user_id LIKE %s "
        cr.execute(query, (request.args.get("userId"),))
        posts = cr.fetchall()
        # print(posts)
        if len(posts) > 0:
            # print("trazi sve")
            query = ""
            queryComments = "SELECT user_id as id, date_created as created, post_id as post_id, 'comment' as table_name FROM comments"
            queryLikes = "SELECT user_id as id, date_created as created, post_id as post_id, 'like' as table_name FROM likes"
            queryFollowings = "SELECT user_id as id, date_created as created, NULL as post_id, 'follow' as table_name FROM followings"
            selectionComments = " WHERE ("
            selectionLikes = " WHERE ("
            selectionFollowings = " WHERE "
            selection = "UNION ALL "
            for i in range(0, len(posts)):
                if i > 0:
                    selectionComments += "OR "
                    selectionLikes += "OR "
                selectionComments += "post_id LIKE " + str(posts[i]["post_id"]) + " "
                selectionLikes += "post_id LIKE " + str(posts[i]["post_id"]) + " "

            queryComments += selectionComments + ") " + "AND user_id != " + str(request.args.get("userId")) + " "
            queryLikes += selectionLikes + ") " + "AND user_id != " + str(request.args.get("userId")) + " "

            selectionFollowings += "following_id LIKE " + request.args.get("userId") + " "
            queryFollowings += selectionFollowings
            
            query = queryComments + selection + queryLikes + selection + queryFollowings + "ORDER BY created DESC"
            
            cr.execute(query,)
            lfc = cr.fetchall()
            # print(query)
            # print(lfc)
            for one in lfc:
                if one != None:
                    # print(one)

                    query = "SELECT * FROM icons WHERE user_id LIKE %s "
                    cr.execute(query, (one["id"],))
                    icon = cr.fetchone()
                    if icon == None:
                        one["icon_url"] = 'images/user.png'
                    else:
                        one["icon_url"] = icon["icon_url"]


                    query = "SELECT * FROM users WHERE user_id LIKE %s "
                    cr.execute(query, (one["id"],))
                    user = cr.fetchone()
                    if user is not None:
                        one["username"] = user["username"]


                    if one["post_id"] is not None:
                        query = "SELECT * FROM posts WHERE post_id LIKE %s "
                        cr.execute(query, (one["post_id"],))
                        post = cr.fetchone()
                        post = post["post_url"]
                        post = post.split(";")
                        post = post[0] + post[1]
                        one["post_url"] = post

                        # print(one)

                        query = "SELECT * FROM comments WHERE user_id LIKE %s AND post_id LIKE %s AND date_created LIKE %s"
                        cr.execute(query, (one["id"], one["post_id"], one["created"],))
                        comment = cr.fetchone()
                        # print(comment)
                        if comment != None:
                            one["comment"] = comment["content"]
                    else:
                        one["post_url"] = None
                    one["created"] = one["created"].isoformat()

                    data.append(one)
            # print(data) 
        else:
            # print("trazi following")
            query = "SELECT user_id as id, date_created as created, NULL as post_id, 'follow' as table_name FROM followings WHERE following_id LIKE %s ORDER BY created DESC"
            cr.execute(query, (request.args.get("userId"),))
            lfc = cr.fetchall()
            # print(lfc)
            for one in lfc:
                if one != None:
                    # print(one)
                    one["created"] = one["created"].isoformat()

                    query = "SELECT * FROM icons WHERE user_id LIKE %s "
                    cr.execute(query, (one["id"],))
                    icon = cr.fetchone()                    
                    if icon == None:
                        one["icon_url"] = 'images/user.png'
                    else:
                        one["icon_url"] = icon["icon_url"]


                    query = "SELECT * FROM users WHERE user_id LIKE %s "
                    cr.execute(query, (one["id"],))
                    user = cr.fetchone()
                    if user is not None:
                        one["username"] = user["username"]

                data.append(one)
            # print(data) 
        return flask.json.jsonify(data)
    return "", 401