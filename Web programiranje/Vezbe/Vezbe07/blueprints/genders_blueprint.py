import flask
import datetime
import main

from flask import Blueprint
from flask import request
from functools import wraps

genders_blueprint = Blueprint("genders_blueprint", __name__)

# GET ALL GENDERS
@genders_blueprint.route("", methods=["GET"])
def get_all_genders():
    query = "SELECT * FROM genders"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query,)
    genders = cr.fetchall()
    return flask.json.jsonify(genders)