import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

followings_blueprint = Blueprint("followings_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

# ------------ FOLLOWINGS ---------------------------------
# IS FOLLOWING?
@followings_blueprint.route("/<int:userId>/<int:foundPersonId>", methods=["GET"])
def is_following(userId, foundPersonId):
    follow = False
    # print(userId, foundPersonId)
    cr = main.mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM followings WHERE user_id LIKE %s AND following_id LIKE %s", (userId, foundPersonId, ))
    followings = cr.fetchall()
    if len(followings) > 0:
        follow = True
    return flask.json.jsonify(follow)

# GET ALL FOLLOWINGS
@followings_blueprint.route("", methods=["GET"])
@secured()
def get_all_followings():
    query = "SELECT * FROM followings"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()
    # print(request.args)

    # FOUND PERSON ------------------------------------------------------------------------------------
    if request.args.get("foundPersonId") is not None and request.args.get("foundPersonId") is not "":
        params.append(int(request.args.get("foundPersonId")))
        selection += "user_id LIKE %s "

        if len(params) > 0:
            query += selection

        # print(query)
        # print(params)
        cr.execute(query, params)
        followings = cr.fetchall()
        for following in followings:
            if following != None:
                following["date_created"] = following["date_created"].isoformat()
                following["date_created"] = following["date_created"].split("T")
                following["date_created"] = following["date_created"][0]
        return flask.json.jsonify(followings)
    # ------------------------------------------------------------------------------------------------

    # USER PAGE ILI FOLLOWINGS PAGE ------------------------------------------------------------------------------------
    elif request.args.get("userId") is not None and request.args.get("userId") is not "":
        params.append(int(request.args.get("userId")))
        selection += "user_id LIKE %s "

        if request.args.get("action") == "getAllFollowings":
            data = []

            selection += "ORDER BY date_created DESC"
            if len(params) > 0:
                query += selection

            cr.execute(query, params)
            followers = cr.fetchall()
            # print(query)
            for follower in followers:
                if follower != None:

                    query = "SELECT * FROM icons WHERE user_id LIKE %s "
                    cr.execute(query, (follower["following_id"],))
                    icon = cr.fetchone()              
                    if icon == None:
                        follower["icon_url"] = 'images/user.png'
                    else:
                        follower["icon_url"] = icon["icon_url"]

                    query = "SELECT * FROM users WHERE user_id LIKE %s "
                    cr.execute(query, (follower["following_id"],))
                    user = cr.fetchone()
                    if user != None:
                        follower["username"] = user["username"]

                    follower["date_created"] = follower["date_created"].isoformat()
                    follower["date_created"] = follower["date_created"].split("T")
                    follower["date_created"] = follower["date_created"][0]
                data.append(follower)

            return flask.json.jsonify(data)
    # ------------------------------------------------------------------------------------------------

    if len(params) > 0:
        query += selection

    # print(query)
    # print(params)

    cr.execute(query, params)
    followings = cr.fetchall()
    for following in followings:
        if following != None:
            following["date_created"] = following["date_created"].isoformat()
            following["date_created"] = following["date_created"].split("T")
            following["date_created"] = following["date_created"][0]
    return flask.json.jsonify(followings)

# ADD FOLLOWING
@followings_blueprint.route("", methods=["POST"])
@secured()
def add_following():
    data = dict(request.json)
    # print(data)
    db = main.mysql_db.get_db()
    cr = db.cursor()
    data["dateCreated"] = datetime.datetime.now()
    cr.execute("INSERT INTO followings (user_id, following_id, date_created) VALUES(%(userId)s, %(foundPersonId)s, %(dateCreated)s)", data)
    db.commit()
    return "", 201

# DELETE FOLLOWING
@followings_blueprint.route("", methods=["DELETE"])
@secured()
def delete_following():
    # print(request.args)
    db = main.mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM followings WHERE user_id=%s AND following_id=%s", (int(request.args.get("userId")), int(request.args.get("foundPersonId")), ))
    db.commit()
    return "", 204