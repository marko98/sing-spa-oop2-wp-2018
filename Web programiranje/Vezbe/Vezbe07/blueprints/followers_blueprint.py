import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

followers_blueprint = Blueprint("followers_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

# GET ALL FOLLOWERS
@followers_blueprint.route("", methods=["GET"])
@secured()
def get_all_followers():
    query = "SELECT * FROM followings"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()
    # print(request.args)

    # FOUND PERSON ------------------------------------------------------------------------------------
    if request.args.get("foundPersonId") is not None and request.args.get("foundPersonId") is not "":
            params.append(request.args.get("foundPersonId"))
            selection += "following_id LIKE %s "
    # ------------------------------------------------------------------------------------------------

    # USER PAGE ILI FOLLOWERS PAGE ------------------------------------------------------------------------------------
    elif request.args.get("userId") is not None and request.args.get("userId") is not "":
        params.append(request.args.get("userId"))
        selection += "following_id LIKE %s "

        if request.args.get("action") == "getAllFollowers":
            data = []

            selection += "ORDER BY date_created DESC"
            if len(params) > 0:
                query += selection

            cr.execute(query, params)
            followers = cr.fetchall()
            for follower in followers:
                if follower != None:

                    query = "SELECT * FROM icons WHERE user_id LIKE %s "
                    cr.execute(query, (follower["user_id"],))
                    icon = cr.fetchone()              
                    if icon == None:
                        follower["icon_url"] = 'images/user.png'
                    else:
                        follower["icon_url"] = icon["icon_url"]

                    query = "SELECT * FROM users WHERE user_id LIKE %s "
                    cr.execute(query, (follower["user_id"],))
                    user = cr.fetchone()
                    if user != None:
                        follower["username"] = user["username"]

                    follower["date_created"] = follower["date_created"].isoformat()
                    follower["date_created"] = follower["date_created"].split("T")
                    follower["date_created"] = follower["date_created"][0]
                data.append(follower)

            return flask.json.jsonify(data)
    # ------------------------------------------------------------------------------------------------
    
    if len(params) > 0:
        query += selection

    cr.execute(query, params)
    followers = cr.fetchall()
    for follower in followers:
        if follower != None:
            follower["date_created"] = follower["date_created"].isoformat()
            follower["date_created"] = follower["date_created"].split("T")
            follower["date_created"] = follower["date_created"][0]
    return flask.json.jsonify(followers)
