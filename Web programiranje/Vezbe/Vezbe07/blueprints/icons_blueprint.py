import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

icons_blueprint = Blueprint("icons_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

@icons_blueprint.route("", methods=["GET"])
@secured()
def get_icon():
    query = "SELECT * FROM icons"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()
    # print(request.args)

    if request.args.get("foundPersonId") is not None and request.args.get("foundPersonId") is not "":
        params.append(request.args.get("foundPersonId"))
        selection += "user_id LIKE %s "

    elif request.args.get("userId") is not None and request.args.get("userId") is not "":
        params.append(request.args.get("userId"))
        selection += "user_id LIKE %s "

    if len(params) > 0:
        query += selection

    cr.execute(query, params)
    icon = cr.fetchone()
    # if icon != None:
    #     # icon["date_created"] = icon["date_created"].isoformat()

    return flask.json.jsonify(icon)