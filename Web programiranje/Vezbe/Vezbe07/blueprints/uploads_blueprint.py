import flask
import datetime
import main
import os
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

uploads_blueprint = Blueprint("uploads_blueprint", __name__)

# ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'zip', 'xml', 'rar', 'shp'])
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@uploads_blueprint.route("", methods=["POST"])
@secured()
def upload():
    username = str(request.args.get("username"))
    folder = str(request.args.get("folder"))
    # print(request.args)
    # print(username, folder)
    # print(len(request.files))
    # print(request.json)

    if folder == "icon" and len(request.files) == 2 and request.json is None:
        # print("icon")
        target = os.path.join(main.APP_ROOT, "static", "users", username)
        # print(target)
        if not os.path.isdir(target):
            os.mkdir(target)

        target = os.path.join(main.APP_ROOT, "static", "users", username, folder)
        if not os.path.isdir(target):
            os.mkdir(target)

        for file in request.files.getlist("file"):
            filename = ""
            if allowed_file(file.filename):
                filename = file.filename
                destination = "/".join([target, filename])
                file.save(destination)

        if request.args.get("username") is not None and request.args.get("username") is not "":
            query = "SELECT * FROM users"
            selection = " WHERE "
            params = []
            cr = main.mysql_db.get_db().cursor()

            params.append(request.args.get("username"))
            if len(params) > 1:
                selection += "AND "
            selection += "username LIKE %s "

            if len(params) > 0:
                query += selection

            cr.execute(query, params)
            user = cr.fetchone()


            db = main.mysql_db.get_db()
            cr.execute("DELETE FROM icons WHERE user_id=%s", (user["user_id"], ))
            db.commit()

            data = {
                "user_id": user["user_id"],
                "icon_url": "users/" + user["username"] + "/icon/" + filename,
                "date_created": datetime.datetime.now()
            }
            cr.execute("INSERT INTO icons (user_id, icon_url, date_created) VALUES(%(user_id)s, %(icon_url)s, %(date_created)s)", data)
            db.commit()

            return "", 201

        return "", 401

    if folder == "temp" and len(request.files) < 12 and request.json is None or folder == "posts" and len(request.files) < 12 and request.json is None:
        # print("upload")
        target = os.path.join(main.APP_ROOT, "static", "users", username)
        if not os.path.isdir(target):
            os.mkdir(target)
        target = os.path.join(main.APP_ROOT, "static", "users", username, folder)
        if not os.path.isdir(target):
            os.mkdir(target)

        for file in request.files.getlist("file"):
            if allowed_file(file.filename):
                filename = file.filename
                destination = "/".join([target, filename])
                file.save(destination)

        return "", 201
    if request.json is not None:
        data = dict(request.json)
        db = main.mysql_db.get_db()
        cr = db.cursor()
        data["dateCreated"] = datetime.datetime.now()
        data["dateUpdated"] = datetime.datetime.now()
        # print(data)
        cr.execute("INSERT INTO posts (user_id, caption, latitude, longitude, type, post_url, date_created, date_updated) VALUES(%(userId)s, %(caption)s, %(latitude)s, %(longitude)s, %(type)s, %(postUrl)s, %(dateCreated)s, %(dateUpdated)s)", data)
        db.commit()
        return "", 201
    return "", 401