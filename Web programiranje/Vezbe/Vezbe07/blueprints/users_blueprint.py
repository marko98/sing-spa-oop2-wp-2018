import flask
import datetime
import main

from flask import Blueprint
from flask import request
from functools import wraps

users_blueprint = Blueprint("users_blueprint", __name__)

usernames = []

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

def get_usernames():
    query = "SELECT username FROM users"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query,)
    users = cr.fetchall()
    if len(users) > 0:
        for user in users:
            if user != None:
                usernames.append(user["username"])
    # print(usernames)

# CHECK PASSWORD
@users_blueprint.route("/<username>/<int:user_id>/<password>", methods=["GET"])
def check_password(username, user_id, password):
    query = "SELECT * FROM users WHERE username LIKE %s AND user_id LIKE %s AND password LIKE %s"
    db = main.mysql_db.get_db()
    cr = db.cursor()
    cr.execute(query, (username, user_id, password,))
    user = cr.fetchone()
    # print(user)
    if user is not None:
        return "True"
    return "False"

# ------------ USERS ---------------------------------
# LOGIN
@users_blueprint.route("/login/<username>", methods=["GET"])
def login(username):
    get_usernames()
    # print(request.args)
    query = "SELECT * FROM users"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()

    flask.session[username] = username
    # print(flask.session)

    # LOGIN --------------------------------------------------------------------------------------------------
    # if request.args.get("email") is not None and request.args.get("email") is not "":
    #     params.append(request.args.get("email"))
    #     selection += "email LIKE %s "

    if request.args.get("username") is not None and request.args.get("username") is not "":
        params.append(request.args.get("username"))
        if len(params) > 1:
            selection += "AND "
        selection += "username LIKE %s "

    if request.args.get("password") is not None and request.args.get("password") is not "":
        params.append(request.args.get("password"))
        if len(params) > 1:
            selection += "AND "
        selection += "password LIKE %s "
    # ------------------------------------------------------------------------------------------------------------ 
    
    selection += "AND active LIKE 1 "

    if len(params) > 0:
        query += selection

    # print(query)
    # print(params)
    cr.execute(query, params)
    users = cr.fetchall()
    return flask.json.jsonify(users)

@users_blueprint.route("/logout/<username>", methods=["GET"])
def logout(username):
    flask.session.pop(username, None)
    # print(flask.session)
    return ""

# GET ALL USERS
@users_blueprint.route("", methods=["GET"])
@secured()
def get_all_users():
    # print(request.args)
    query = "SELECT * FROM users"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()

    # flask.session.clear()

    # if 'email|username&password' in flask.session:
    #     print(flask.session["email|username&password"])
    # else:
    #     print("prazno")

    # FOUND PERSON --------------------------------------------------------------------------------------
    if request.args.get("foundPersonId") is not None and request.args.get("foundPersonId") is not "":
        params.append(int(request.args.get("foundPersonId")))
        if len(params) > 1:
            selection += "AND "
        selection += "user_id LIKE %s "

        if len(params) > 0:
            query += selection

        # print("FOUND PERSON", query)
        # print(params)
        cr.execute(query, params)
        user = cr.fetchone()
        return flask.json.jsonify(user)
    # ---------------------------------------------------------------------------------------------------

    # SEARCH --------------------------------------------------------------------------------------------
    if request.args.get("userId") is not None and request.args.get("userId") is not "" and request.args.get("username") is "":
        params.append(int(request.args.get("userId")))
        if len(params) > 1:
            selection += "AND "
        selection += "user_id LIKE %s "

    if request.args.get("usernameSearch") is not None and request.args.get("usernameSearch") is "":
        return "", 201
    elif request.args.get("usernameSearch") is not None and request.args.get("usernameSearch") is not "":
        params.append("%" + request.args.get("usernameSearch") + "%")
        if len(params) > 1:
            selection += "AND "
        selection += "username LIKE %s"
        params.append(int(request.args.get("userId")))
        if len(params) > 1:
            selection += "AND "
        selection += "user_id != %s"
    # -------------------------------------------------------------------------------------------------------

    # USER PAGE --------------------------------------------------------------------------------------------------
    if request.args.get("email") is not None and request.args.get("email") is not "":
        params.append(request.args.get("email"))
        selection += "email LIKE %s "

    if request.args.get("username") is not None and request.args.get("username") is not "":
        params.append(request.args.get("username"))
        if len(params) > 1:
            selection += "AND "
        selection += "username LIKE %s "

    if request.args.get("password") is not None and request.args.get("password") is not "":
        params.append(request.args.get("password"))
        if len(params) > 1:
            selection += "AND "
        selection += "password LIKE %s "
    # ------------------------------------------------------------------------------------------------------------ 
    
    selection += " AND active LIKE 1 "

    if len(params) > 0:
        query += selection

    # print("USER PAGE, SEARCH", query)
    # print(params)
    cr.execute(query, params)
    users = cr.fetchall()
    for user in users:
        if user != None:
            user["birth_date"] = user["birth_date"].isoformat()
            user["birth_date"] = user["birth_date"].split("T")
            user["birth_date"] = user["birth_date"][0]
    return flask.json.jsonify(users)

# ADD USER
@users_blueprint.route("", methods=["POST"])
def add_user():
    data = dict(request.json)
    # print(data)
    db = main.mysql_db.get_db()
    cr = db.cursor()
    data["dateCreated"] = datetime.datetime.now()
    cr.execute("INSERT INTO users (email, username, password, real_name, date_created, city_id, birth_date, gender_id) VALUES(%(email)s, %(username)s, %(password)s, %(realName)s, %(dateCreated)s, %(city_id)s, %(birth_date)s, %(gender)s)", data)
    db.commit()
    return "", 201

# CHANGE USER
@users_blueprint.route("", methods=["PUT"])
@secured()
def change_user():
    db = main.mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    query = "UPDATE users SET "
    if data["birthDate"] != None:
        query += "birth_date=%(birthDate)s, "
    if data["cityId"] != None:
        query += "city_id=%(cityId)s, "
    if data["genderId"] != None:
        query += "gender_id=%(genderId)s, "
    if data["email"] != "":
        query += "email=%(email)s, "
    if data["password"] != "":
        query += "password=%(password)s, "
    if data["realName"] != "":
        query += "real_name=%(realName)s, "
    query += "date_updated=%(date_updated)s "
    data["date_updated"] = datetime.datetime.now()

    query += "WHERE user_id=%(userId)s"

    cr.execute(query, data)
    db.commit()
    return "", 200

# DELETE USER
@users_blueprint.route("/<int:user_id>", methods=["DELETE"])
def delete_user(user_id):
    db = main.mysql_db.get_db()
    cr = db.cursor()
    cr.execute("UPDATE users SET active = 0 WHERE user_id = %s", (user_id,))
    db.commit()
    # ili 200 jer je to kao post
    return "", 204
# ----------------------------------------------------