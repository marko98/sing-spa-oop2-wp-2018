import flask
import datetime
import main
import blueprints.users_blueprint

usernames = blueprints.users_blueprint.usernames

from flask import Blueprint
from flask import request
from functools import wraps

messages_blueprint = Blueprint("messages_blueprint", __name__)

def secured():
    def secured_with_roles(f):
        @wraps(f)
        def check(*args, **kwargs):
            allow = False
            if len(usernames) > 0:
                for username in usernames:
                    if flask.session.get(username) is not None:
                        allow = True
            if allow:
                return f()
            else:
                return "", 401
        return check
    return secured_with_roles

@messages_blueprint.route("", methods=["GET"])
@secured()
def get_messages():
    query = "SELECT * FROM messages"
    selection = " WHERE "
    params = []
    cr = main.mysql_db.get_db().cursor()
    data = []
    # print(request.args)

    if request.args.get("friendId") is not None and request.args.get("friendId") is not "" and request.args.get("userId") is not None and request.args.get("userId") is not "":
        query = query + " WHERE " + "user_id_from LIKE " + request.args.get("userId") + " AND " + "user_id_to LIKE " + request.args.get("friendId") + " OR " + "user_id_from LIKE " + request.args.get("friendId") + " AND " + "user_id_to LIKE " + request.args.get("userId") + " ORDER BY date_created DESC;"
        cr.execute(query,)
        messages = cr.fetchall()
        for message in messages:
            if message != None:
                message["showDate"] = False
                # message["date_created"] = message["date_created"].isoformat()
        return flask.json.jsonify(messages)

        # if len(params) > 0:
        #     query += selection

    elif request.args.get("userId") is not None and request.args.get("userId") is not "":
        params.append(int(request.args.get("userId")))
        selection += "user_id_to LIKE %s "

    if len(params) > 0:
        query += selection

    cr.execute(query, params)
    messages = cr.fetchall()
    for message in messages:
        if message != None:
            query2 = "SELECT * FROM users WHERE user_id LIKE %s "
            cr.execute(query2, (message["user_id_from"], ))
            user = cr.fetchone()
            message["date_created"] = message["date_created"].isoformat()
            object_data = {
                "from": user,
                "message": message
            }
            data.append(object_data)

    return flask.json.jsonify(data)
    
@messages_blueprint.route("", methods=["POST"])
@secured()
def add_message():
    data = dict(request.json)
    # print(data)
    db = main.mysql_db.get_db()
    cr = db.cursor()
    data["dateCreated"] = datetime.datetime.now()
    cr.execute("INSERT INTO messages (content, user_id_from, user_id_to, date_created) VALUES(%(content)s, %(userId)s, %(foundPersonId)s, %(dateCreated)s)", data)
    db.commit()
    return "", 201