import flask
import datetime
import main

from flask import Blueprint
from flask import request
from functools import wraps

countries_states_cities_blueprint = Blueprint("countries_states_cities_blueprint", __name__)

# GET ALL COUNTRIES
@countries_states_cities_blueprint.route("/countries", methods=["GET"])
def get_all_countries():
    query = "SELECT * FROM countries"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query,)
    countries = cr.fetchall()
    return flask.json.jsonify(countries)

# GET ALL STATES
@countries_states_cities_blueprint.route("/countries/<int:country_id>", methods=["GET"])
def get_all_states(country_id):
    query = "SELECT * FROM states WHERE country_id LIKE %s"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query, (country_id,))
    states = cr.fetchall()
    return flask.json.jsonify(states)

# GET ALL CITIES
@countries_states_cities_blueprint.route("/cities/<int:state_id>", methods=["GET"])
def get_all_cities(state_id):
    query = "SELECT * FROM cities WHERE state_id LIKE %s"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query, (state_id,))
    cities = cr.fetchall()
    return flask.json.jsonify(cities)

# GET CITY
@countries_states_cities_blueprint.route("/city/<int:city_id>", methods=["GET"])
def get_city(city_id):
    query = "SELECT name FROM cities WHERE city_id LIKE %s"
    cr = main.mysql_db.get_db().cursor()
    cr.execute(query, (city_id,))
    cities = cr.fetchall()
    return flask.json.jsonify(cities)