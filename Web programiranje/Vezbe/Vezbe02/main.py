import flask
from flask import Flask
from datetime import datetime

app = Flask(__name__, static_url_path = "")

korisnici = [{"ime": "david", "prezime": "zahorodni", "lozinka": "david", "datum_rodjenja": "1995-05-18", "email": "david@jej.com", "aktivan": True}]

# @app.route("/")
# def main_index():
#     return "Uspesno obradjen zahtev!"

@app.template_filter("date")
def date_filter(datum):
    datum = datetime.strptime(datum, "%Y-%m-%d")
    return datum.strftime("%d. %m. %Y")

@app.route("/")
@app.route("/formaKorisnika")
def forma_korisnika():
    return flask.send_file("formaKorisnika.html")

@app.route("/korisnici")
def lista_korisnika():
    return flask.render_template("korisnici.tpl.html", korisnici = korisnici)

@app.route("/registracija", methods=["POST"])
def registracija_korisnika():
    dozvola = True
    for korisnik in korisnici:
        if(korisnik["ime"] == flask.request.form["ime"]):
            dozvola = False
    
    if(dozvola):
        korisnici.append({
            "ime": flask.request.form["ime"],
            "prezime": flask.request.form["prezime"],
            "lozinka": flask.request.form["lozinka"],
            "datum_rodjenja": flask.request.form["datumRodjenja"],
            "email": flask.request.form["email"],
            "aktivan": True
        })
    return flask.redirect("/korisnici")

@app.route("/ukloniKorisnika/<int:index>")
def ukloni_korisnika(index):
    korisnici[index]["aktivan"] = False
    return flask.render_template("korisnici.tpl.html", korisnici = korisnici)

@app.route("/izmenaKorisnika/<int:index>")
def izmena_korisnika(index):
    return flask.render_template("izmenaKorisnika.html", korisnik = korisnici[index], index = index)

@app.route("/izmeni/<int:korisnik_index>", methods=["POST"])
def izmena(korisnik_index):
    korisnici[korisnik_index]["ime"] = flask.request.form["ime"]
    korisnici[korisnik_index]["prezime"] = flask.request.form["prezime"]
    korisnici[korisnik_index]["email"] = flask.request.form["email"]
    korisnici[korisnik_index]["datum_rodjenja"] = flask.request.form["datumRodjenja"]
    korisnici[korisnik_index]["aktivan"] = True
        
    return flask.redirect("/korisnici")

if __name__ == "__main__":
    app.run(host = "0.0.0.0", port = 5000, threaded = True)



#---------------------------------------------------------------
# SABLON
# import flask
# from flask import Flask

# app = Flask(__name__, static_url_path = "")

# @app.route("/")
# def main_index():
#     return "Uspesno obradjen zahtev!"

# @app.route("/formaKorisnika")
# def forma_korisnika():
#     return flask.send_file("formaKorisnika.html")

# app.run(host = "0.0.0.0", port = 5000, threaded = True)