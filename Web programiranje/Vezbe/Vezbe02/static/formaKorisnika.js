(function (d){
    (d.getElementById("datumRodjenja")).onchange = function validacijaDatuma(event){
        var datum = new Date(event.target.value);

        var starost = new Date() - datum;
        starost = new Date(starost).getFullYear() - 1970;
        if(starost >= 18){
            console.log(event.target.setCustomValidity(""));
        } else {
            console.log(event.target.setCustomValidity("Nemate dovoljno godina!"));
        }       
    }
})(document)