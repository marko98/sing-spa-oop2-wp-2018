(function(angular){
    // dobavljamo napravljeni angular modul pod nazivom app
    var app = angular.module("app");
    
    app.controller("MyCtrl", function(){
        var mc = this;
        mc.stavke = [{"naziv": "Stavka 1", "kolicina": 60},
                    {"naziv": "Stavka 2", "kolicina": 20},
                    {"naziv": "Stavka 3", "kolicina": 50}];
        mc.novaStavka = {"naziv": "", "kolicina": 0};

        mc.ukloniStavku = function(index){
            mc.stavke.splice(index, 1);
        };

        mc.dodajStavku = function(){
            // angular.copy(mc.novaStavka) !!!
            mc.stavke.push(angular.copy(mc.novaStavka));
        };

    });

})(angular);