import flask
from flask import Flask, flash

app = Flask(__name__, static_url_path="")
app.secret_key = "dont tell anyone"

@app.route("/")
def main_page():
    return flask.send_file("pocetna.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, threaded=True)    