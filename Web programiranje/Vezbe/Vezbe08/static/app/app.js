(function(angular){
    var app = angular.module("MyApp", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "home", //Naziv stanja.
            url: "/", //URL koji se mapira na zadato stanje. Ovaj URL ce zapravo biti vidljiv kao identifikator fragmenta.
            templateUrl: "app/components/magacin/magacin.tpl.html", //URL do sablona za generisanje prikaza.
            controller: "MagacinCtrl", //Kontroler koji je potrebno injektovati u prikaz.
            controllerAs: "mc" //Posto ce biti upotrebljen imenovani kontroler, navodi se i ime kontrolera.
        }).state({
            name: "stavka",
            url: "/stavka/{id:int}",
            templateUrl: "app/components/stavka/stavka.tpl.html",
            controller: "StavkaCtrl",
            controllerAs: "sc"
        }).state({
            name: "stavke",
            url: "/stavke",
            templateUrl: "app/components/stavke/stavke.tpl.html",
            controller: "StavkeCtrl",
            controllerAs: "sc"      
        }).state({
            name: "korisnici",
            url: "/korisnici",
            templateUrl: "app/components/korisnici/korisnici.tpl.html",
            controller: "KorisniciCtrl",
            controllerAs: "kc"      
        }).state({
            name: "korisnik",
            url: "/korisnik/{korisnicko_ime}",
            templateUrl: "app/components/korisnik/korisnik.tpl.html",
            controller: "KorisnikCtrl",
            controllerAs: "kc"
        }).state({
            name: "trebovanja",
            url: "/trebovanja",
            templateUrl: "app/components/trebovanja/trebovanja.tpl.html",
            controller: "TrebovanjaCtrl",
            controllerAs: "tc"      
        }).state({
            name: "trebovanje",
            url: "/trebovanje/{id:int}",
            templateUrl: "app/components/trebovanje/trebovanje.tpl.html",
            controller: "TrebovanjeCtrl",
            controllerAs: "tc"      
        }); 

        $urlRouterProvider.otherwise("/");
    }]);
})(angular)