(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("StavkaCtrl", ["$stateParams", "$http", function ($stateParams, $http) {
        var vm = this;
        vm.trazenaStavka = {};
        vm.izmenjenaStavka = {
            "kolicina": 0,
            "opis": ""
        };

        vm.dobaviStavku = function(id){
            $http.get("/stavke/" + id).then(function(response){
                vm.trazenaStavka = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju stavke! Kod: " + response.status);
            });
        };

        vm.izmeniStavku = function(){
            $http.put("/stavke/" + vm.trazenaStavka.id, vm.izmenjenaStavka).then(function(response){
                vm.dobaviStavku(vm.trazenaStavka.id);
            }, function(response) {
                console.log("Greska pri izmeni stavke! Kod: " + response.status);
            });
        };

        vm.dobaviStavku($stateParams["id"]);

    }]);
})(angular);