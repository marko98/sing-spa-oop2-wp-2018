(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("TrebovanjaCtrl", ["$http", function ($http) {
        var vm = this;
        vm.trebovanja = [];
        vm.novoTrebovanje = {
            "kolicina": 0,
            "korisnici_username": null,
            "stavke_id": null,
            "datum": null
        };
        vm.stavke = [];
        vm.korisnici = [];
        vm.pretraga = {
            "username": "",
            "stavka_id": undefined,
            "kolicinaDo": undefined,
            "kolicinaOd": undefined,
            "datumOd": undefined,
            "datumDo": undefined
        };

        vm.dobaviTrebovanja = function(){
            $http.get("/trebovanja", {params: vm.pretraga}).then(function(response){
                vm.trebovanja = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju trebovanja! Kod: " + response.status);
            })
        };

        vm.ukloniTrebovanje = function(id_trebovanja){
            $http.delete("/trebovanja/" + id_trebovanja).then(function(response){
                vm.dobaviTrebovanja();
            }, function(response) {
                console.log("Greska pri brisanju trebovanja! Kod: " + response.status);
            });
        };

        vm.dodajTrebovanje = function(){
            $http.post("/trebovanja", vm.novoTrebovanje).then(function(response){
                vm.dobaviTrebovanja();
            }, function(response) {
                console.log("Greska pri dodavanju trebovanja(id je primarni kljuc)! Kod: " + response.status);
            })
        };

        vm.dobaviStavke = function(){
            $http.get("/stavke").then(function(response){
                vm.stavke = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju stavke! Kod: " + response.status);
            })
        };

        vm.dobaviKorisnike = function(){
            $http.get("/korisnici").then(function(response){
                vm.korisnici = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.status);
            });
        };

        vm.dobaviTrebovanja();
        vm.dobaviStavke();
        vm.dobaviKorisnike();

    }]);
})(angular);