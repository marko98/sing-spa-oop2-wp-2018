(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("KorisnikCtrl", ["$stateParams", "$http", function ($stateParams, $http) {
        var vm = this;
        vm.trazeniKorisnik = {};
        vm.noviKorisnik = {
            "ime": "",
            "prezime": ""
        };

        vm.dobaviKorisnika = function(korisnicko_ime){
            $http.get("/korisnici/" + korisnicko_ime).then(function(response){
                vm.trazeniKorisnik = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.status);
            });
        };

        vm.izmeniKorisnika = function(korisnicko_ime){
            $http.put("/korisnici/" + vm.trazeniKorisnik.username, vm.noviKorisnik).then(function(response){
                vm.dobaviKorisnika(vm.trazeniKorisnik.username);
            }, function(response) {
                console.log("Greska pri izmeni korisnika! Kod: " + response.status);
            });
        };

        vm.dobaviKorisnika($stateParams["korisnicko_ime"]);

    }]);
})(angular);