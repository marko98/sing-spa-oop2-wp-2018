(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("KorisniciCtrl", ["$http", function ($http) {
        var vm = this;
        vm.korisnici = [];
        vm.noviKorisnik = {
            "username": "",
            "ime": "",
            "prezime": ""
        };
        vm.pretraga = {
            "username": "",
            "ime": "",
            "prezime": ""
        };


        vm.dobaviKorisnike = function(){
            $http.get("/korisnici", {params: vm.pretraga}).then(function(response){
                vm.korisnici = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.status);
            });
        };

        vm.ukloniKorisnika = function(username){
            $http.delete("/korisnici/" + username).then(function(response){
                vm.dobaviKorisnike();
            }, function(response) {
                console.log("Greska pri brisanju korisnika! Kod: " + response.status);
            });
        };

        vm.dodajKorisnika = function(){
            $http.post("/korisnici", vm.noviKorisnik).then(function(response){
                vm.dobaviKorisnike();
            }, function(response) {
                console.log("Greska pri dodavanju korisnika(username je primarni kljuc)! Kod: " + response.status);
            });
        };

        vm.dobaviKorisnike();

    }]);
})(angular);