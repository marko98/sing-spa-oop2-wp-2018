(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("MagacinCtrl", ["$http", function ($http) {
        var vm = this;
        vm.roba = [];
        vm.novaRoba = {
            "naziv": "",
            "kolicina": 0,
            "opis": ""
        };

        vm.dobaviRobu = function(){
            $http.get("/stavke").then(function(response){
                vm.roba = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju robe! Kod: " + response.status);
            })
        };

        vm.dodajRobu = function(){
            $http.post("/stavke", vm.novaRoba).then(function(response){
                vm.dobaviRobu();
            }, function(response) {
                console.log("Greska pri dodavanju robe! Kod: " + response.status);
            })
        };

        vm.ukloniRobu = function(id){
            $http.delete("/stavke/" + id).then(function(response){
                vm.dobaviRobu();
            }, function(response) {
                console.log("Greska pri brisanju robe! Kod: " + response.status);
            })
        }

        vm.izmeniRobu = function(){

        };

        vm.dobaviRobu();

    }]);
})(angular);