(function(angular){
    var app = angular.module("MyApp");

    app.controller("StavkeCtrl", ["$http", function ($http) {
        var vm = this;
        vm.stavke = [];
        vm.novaStavka = {
            "naziv": "",
            "kolicina": 0,
            "opis": ""
        };
        vm.pretraga = {
            "naziv": "",
            "kolicinaOd": undefined,
            "kolicinaDo": undefined
        };


        vm.dobaviStavke = function(){
            $http.get("/stavke", {params: vm.pretraga}).then(function(response){
                vm.stavke = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju stavke! Kod: " + response.status);
            })
        };

        vm.ukloniStavku = function(id){
            $http.delete("/stavke/" + id).then(function(response){
                vm.dobaviStavke();
            }, function(response) {
                console.log("Greska pri brisanju stavke! Kod: " + response.status);
            })
        }

        vm.dobaviStavke();

    }]);

})(angular);