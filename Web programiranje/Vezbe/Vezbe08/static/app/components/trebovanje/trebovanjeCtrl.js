(function(angular){
    var app = angular.module("MyApp");
    
    app.controller("TrebovanjeCtrl", ["$stateParams", "$http", function ($stateParams, $http) {
        var vm = this;
        vm.trazenoTrebovanje = {};
        vm.izmenjenoTrebovanje = {
            "stavka_id": "",
            "korisnicko_ime": "",
            "kolicina": 0,
            "datum": null
        };
        vm.stavke = [];
        vm.korisnici = [];

        vm.dobaviTrebovanje = function(id){
            $http.get("/trebovanja/" + id).then(function(response){
                vm.trazenoTrebovanje = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju trebovanja! Kod: " + response.status);
            });
        };

        vm.izmeniTrebovanje = function(){
            $http.put("/trebovanja/" + vm.trazenoTrebovanje.id, vm.izmenjenoTrebovanje).then(function(response){
                vm.dobaviTrebovanje(vm.trazenoTrebovanje.id);
            }, function(response) {
                console.log("Greska pri izmeni trebovanja! Kod: " + response.status);
            });
        };

        vm.dobaviStavke = function(){
            $http.get("/stavke").then(function(response){
                vm.stavke = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju stavke! Kod: " + response.status);
            })
        };

        vm.dobaviKorisnike = function(){
            $http.get("/korisnici").then(function(response){
                vm.korisnici = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.status);
            });
        };

        vm.dobaviStavke();
        vm.dobaviKorisnike();
        vm.dobaviTrebovanje($stateParams["id"]);

    }]);
})(angular);