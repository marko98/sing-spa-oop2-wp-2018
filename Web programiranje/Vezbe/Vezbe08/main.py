import flask
import datetime
from flask import Flask
from flask import request

from flaskext.mysql import MySQL

from pymysql.cursors import DictCursor

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")

app.config['MYSQL_DATABASE_USER'] = 'marko' # Korisnicko ime korisnika baze podataka.
app.config['MYSQL_DATABASE_PASSWORD'] = 'rumenka2012' # Lozinka izabranog korisnika. 
app.config['MYSQL_DATABASE_DB'] = 'magacin' # Ime seme baze podataka koja se koristi.

mysql_db.init_app(app)

# ---------------- FILTERI -----------------------
@app.template_filter("str")
def string(object):
    return str(object)

# <td>{{korisnik["datum_rodjenja"]|date}}</td>
@app.template_filter("date")
def dateFilter(date_string):
    date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    return date.strftime("%d.%m.%Y.")

@app.template_filter("datumIsoformat")
def datum_u_isoformat(datum):
    datum = datum.isoformat()
    datum = datum.split("T")
    datum = datum[0]
    return datum
# ------------------------------------------------

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html")

# ------ POCETAK RADA SA STAVKAMA ------------------- 
# DOBAVI SVE STAVKE
@app.route("/stavke", methods=["GET"])
def dobavljanje_stavki():
    print(request.args)
    upit = "SELECT * FROM stavke"
    selekcija = " WHERE "
    parametri_pretrage = []
    cr = mysql_db.get_db().cursor()
    
    if request.args.get("naziv") != "" and request.args.get("naziv") != None:
        # print("naziv postoji")
        parametri_pretrage.append("%" + request.args.get("naziv") + "%")
        selekcija += "naziv LIKE %s "

    if request.args.get("kolicinaDo") != None:
        # print("kolicina do postoji")
        parametri_pretrage.append(request.args.get("kolicinaDo"))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "kolicina <= %s "
        
    if request.args.get("kolicinaOd") != None:
        # print("kolicina od postoji")
        parametri_pretrage.append(request.args.get("kolicinaOd"))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "kolicina >= %s "

    if len(parametri_pretrage) > 0:
        upit += selekcija

    # print(upit)
    # print(parametri_pretrage)
    cr.execute(upit, parametri_pretrage)
    stavke = cr.fetchall()
    return flask.json.jsonify(stavke)

# SELECT * FROM stavke WHERE naziv LIKE 'mis';
# SELECT * FROM stavke WHERE kolicina <= 50 ;
# SELECT * FROM stavke WHERE kolicina <= 50 AND kolicina>= 0 AND naziv LIKE 'mis';

# DOBAVI STAVKU PO ID-JU
@app.route("/stavke/<int:id_stavke>", methods=["GET"])
def dobavljanje_stavke_po_idu(id_stavke):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM stavke WHERE id=%s", (id_stavke, ))
    stavka = cr.fetchone()
    return flask.jsonify(stavka)

# DOBAVI STAVKU PO NAZIVU
@app.route("/stavke/<naziv_stavke>", methods=["GET"])
def dobavljanje_stavke_po_nazivu(naziv_stavke):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM stavke WHERE naziv=%s", (naziv_stavke, ))
    stavka = cr.fetchone()
    return flask.jsonify(stavka)

# DODAJ STAVKU
# https://dev.mysql.com/doc/refman/8.0/en/insert.html
@app.route("/stavke", methods=["POST"])
def dodavanje_stavke():
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    # if 'opis' not in data:
    #     data["opis"] = "Nema opisa"
    if data["opis"] == "":
        data["opis"] = "Nema opisa"
    cr.execute("INSERT INTO stavke (naziv, kolicina, opis) VALUES(%(naziv)s, %(kolicina)s, %(opis)s)", data)
    db.commit()
    return "", 201

# @app.route("/stavke", methods=["POST"])
# def dodavanje_stavke():
#     db = mysql_db.get_db()
#     cr = db.cursor()
#     cr.execute("INSERT INTO stavke (naziv, kolicina) VALUES(%(naziv)s, %(kolicina)s)", request.json)
#     db.commit()
#     return "", 201

# IZMENI STAVKU PO ID-JU
@app.route("/stavke/<int:id_stavke>", methods=["PUT"])
def izmeni_stavku_po_idu(id_stavke):
    print(request.json)
    data = dict(request.json)
    print(data)
    # id = request.json["id"]
    # naziv = request.json["naziv"]
    # kolicina = request.json["kolicina"]
    # opis = request.json["opis"]
    if data["opis"] == "":
        data["opis"] = "Nema opisa"
    data["id"] = id_stavke
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("UPDATE stavke SET kolicina=%(kolicina)s, opis=%(opis)s WHERE id=%(id)s", data)
    # cr.execute(UPDATE stavke SET naziv='Jej', kolicina=0, opis='jejje'  WHERE id=6, (params))
    db.commit()
    return "", 201

# OBRISI STAVKU PO ID-JU
@app.route("/stavke/<int:id_stavke>", methods=["DELETE"])
def uklanjanje_stavke_po_idu(id_stavke):
    print(id_stavke)
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM stavke WHERE id=%s", (id_stavke,))
    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

# OBRISI STAVKU PO NAZIVU
@app.route("/stavke/<naziv_stavke>", methods=["DELETE"])
def uklanjanje_stavke_po_nazivu(naziv_stavke):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM stavke WHERE naziv=%s", (naziv_stavke,))
    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

# ------ KRAJ RADA SA STAVKAMA ------------------- 

# ------ POCETAK RADA SA KORISNICIMA ------------------- 
# DOBAVI SVE KORISNIKE
@app.route("/korisnici", methods=["GET"])
def dobavljanje_svih_korisnika():
    print(request.args)
    upit = "SELECT * FROM korisnici"
    selekcija = " WHERE "
    parametri_pretrage = []
    cr = mysql_db.get_db().cursor()
    
    if request.args.get("username") != "" and request.args.get("username") != None:
        parametri_pretrage.append("%" + request.args.get("username") + "%")
        selekcija += "username LIKE %s "

    if request.args.get("ime") != "" and request.args.get("ime") != None:
        parametri_pretrage.append("%" + request.args.get("ime") + "%")
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "ime LIKE %s "

    if request.args.get("prezime") != "" and request.args.get("prezime") != None:
        parametri_pretrage.append("%" + request.args.get("prezime") + "%")
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "prezime LIKE %s "

    if len(parametri_pretrage) > 0:
        upit += selekcija

    # print(upit)
    # print(parametri_pretrage)

    cr.execute(upit, parametri_pretrage)
    korisnici = cr.fetchall()
    return flask.json.jsonify(korisnici)

# DOBAVI KORISNIKA PO KORISNICKOM IMENU
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! cr.FETCHONE() !!!!!!!! --------------
@app.route("/korisnici/<korisnicko_ime>", methods=["GET"])
def dobavljanje_korisnika_po_korisnickom_imenu(korisnicko_ime):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM korisnici WHERE username=%s", (korisnicko_ime, ))
    korisnik = cr.fetchone()
    return flask.json.jsonify(korisnik)

# DODAJ KORISNIKA
@app.route("/korisnici", methods=["POST"])
def dodavanje_korisnika():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO korisnici (username, ime, prezime) VALUES(%(username)s, %(ime)s, %(prezime)s)", request.json)
    db.commit()
    return "", 201

# IZMENI KORISNIKA
@app.route("/korisnici/<korisnicko_ime>", methods=["PUT"])
def izmeni_korisnika_po_korisnickom_imenu(korisnicko_ime):
    data = dict(request.json)
    data["username"] = korisnicko_ime
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("UPDATE korisnici SET ime=%(ime)s, prezime=%(prezime)s WHERE username=%(username)s", data)
    db.commit()
    return "", 201

# OBRISI KORISNIKA PO KORISNICKOM IMENU
@app.route("/korisnici/<korisnicko_ime>", methods=["DELETE"])
def uklanjanje_korisnika_po_korisnickom_imenu(korisnicko_ime):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM korisnici WHERE username=%s", (korisnicko_ime,))
    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

# ------ KRAJ RADA SA KORISNICIMA ------------------- 

# ------ POCETAK RADA SA TREBOVANJIMA ------------------- 
# DOBAVI SVA TREBOVANJA
@app.route("/trebovanja", methods=["GET"])
def dobavljanje_svih_trebovanja():
    print(request.args)
    upit = "SELECT * FROM trebovanja"
    selekcija = " WHERE "
    parametri_pretrage = []
    print(request.args.get("username"))

    if request.args.get("username") != "" and request.args.get("username") != None:
        parametri_pretrage.append("%" + request.args.get("username") + "%")
        selekcija += "korisnici_username LIKE %s "

    if request.args.get("stavka_id") != None:
        parametri_pretrage.append(request.args.get("stavka_id"))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "stavke_id = %s "

    if request.args.get("kolicinaDo") != None:
        parametri_pretrage.append(request.args.get("kolicinaDo"))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "kolicina <= %s "
        
    if request.args.get("kolicinaOd") != None:
        parametri_pretrage.append(request.args.get("kolicinaOd"))
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "kolicina >= %s "

    if request.args.get("datumOd") != None:
        datum = request.args.get("datumOd")
        datum = datum.split("T")
        datum = datum[0]
        date_time_str = datum  
        date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d')
        datum = date_time_obj

        parametri_pretrage.append(datum)
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "datum >= %s "
    
    if request.args.get("datumDo") != None:
        datum = request.args.get("datumDo")
        datum = datum.split("T")
        datum = datum[0]
        date_time_str = datum  
        date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d')
        datum = date_time_obj

        parametri_pretrage.append(datum)
        if len(parametri_pretrage) > 1:
            selekcija += "AND "
        selekcija += "datum <= %s "


    if len(parametri_pretrage) > 0:
        upit += selekcija

    cr = mysql_db.get_db().cursor()
    print(upit)
    print(parametri_pretrage)
    cr.execute(upit, parametri_pretrage)
    trebovanja = cr.fetchall()
    # Angular ume da deserijalizuje datum u ISO formatu,
    # podrazumevani JSON encoder ne serijalizuje datum
    # u ISO formatu pa je neophodno ili napraviti drugu
    # implementaciju JSON encoder-a ili eksplicitno
    # konvertovati datum u ISO format.

    # print(trebovanja[0])
    # print(type(trebovanja[0]["datum"]))
    for trebovanje in trebovanja:
        trebovanje["datum"] = trebovanje["datum"].isoformat()
        trebovanje["datum"] = trebovanje["datum"].split("T")
        trebovanje["datum"] = trebovanje["datum"][0]
    return flask.json.jsonify(trebovanja)

# DOBAVI TREBOVANJE PO DATUMU
@app.route("/trebovanja/<datum_trebovanja>", methods=["GET"])
def dobavi_trebovanje_po_datumu(datum_trebovanja):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM trebovanja WHERE datum LIKE %s", (datum_trebovanja + "%", ))
    trebovanje = cr.fetchone()
    if trebovanje != None:
        trebovanje["datum"] = trebovanje["datum"].isoformat()
        trebovanje["datum"] = trebovanje["datum"].split("T")
        trebovanje["datum"] = trebovanje["datum"][0]
    return flask.json.jsonify(trebovanje)

# SELECT * FROM trebovanja WHERE datum LIKE '2018-10-05%';

# DOBAVI TREBOVANJE PO ID-JU
@app.route("/trebovanja/<int:id_trebovanja>", methods=["GET"])
def dobavi_trebovanje_po_idu(id_trebovanja):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM trebovanja WHERE id=%s", (id_trebovanja, ))
    trebovanje = cr.fetchone()
    if trebovanje != None:
        trebovanje["datum"] = trebovanje["datum"].isoformat()
        trebovanje["datum"] = trebovanje["datum"].split("T")
        trebovanje["datum"] = trebovanje["datum"][0]
    return flask.json.jsonify(trebovanje)

# DODAJ TREBOVANJE
@app.route("/trebovanja", methods=["POST"])
def dodavanje_trebovanja():
    trebovanje = dict(request.json)
    trebovanje["datum"] = trebovanje["datum"].split("T")
    trebovanje["datum"] = trebovanje["datum"][0]
    date_time_str = trebovanje["datum"]  
    date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d')
    trebovanje["datum"] = date_time_obj

    # ASISTENTOVO ----------------------------------
    # # Za datum trebovanja se uzima trenutni datum.
    # trebovanje["datum"] = datetime.datetime.now()
    # ----------------------------------------------

    if trebovanje["korisnici_username"] == None or trebovanje["stavke_id"] == None:
        return ""

    # print(trebovanje)
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO trebovanja (datum, kolicina, korisnici_username, stavke_id) VALUES(%(datum)s, %(kolicina)s, %(korisnici_username)s, %(stavke_id)s)", trebovanje)
    db.commit()
    return "", 201

# IZMENI TREBOVANJE
@app.route("/trebovanja/<int:id_trebovanja>", methods=["PUT"])
def izmeni_trebovanje_po_idu(id_trebovanja):
    print(request.json)
    db = mysql_db.get_db()
    cr = db.cursor()
    trebovanje = dict(request.json)
    trebovanje["id"] = id_trebovanja

    trebovanje["datum"] = trebovanje["datum"].split("T")
    trebovanje["datum"] = trebovanje["datum"][0]
    date_time_str = trebovanje["datum"]  
    date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d')
    trebovanje["datum"] = date_time_obj

    if trebovanje["stavka_id"] == "" or trebovanje["stavka_id"] == "korisnicko_ime":
        return ""
    # print(trebovanje)
    cr.execute("UPDATE trebovanja SET datum=%(datum)s, kolicina=%(kolicina)s, stavke_id=%(stavka_id)s, korisnici_username=%(korisnicko_ime)s WHERE id=%(id)s", trebovanje)
    db.commit()
    return "", 201

# OBRISI TREBOVANJE PO ID-JU
@app.route("/trebovanja/<int:id_trebovanja>", methods=["DELETE"])
def ukloni_trebovanje_po_idu(id_trebovanja):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM trebovanja WHERE id=%s", (id_trebovanja, ))
    db.commit()
    return "", 204 # https://www.ietf.org/rfc/rfc2616.txt

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)