from flask import Flask, flash
import flask

app = Flask(__name__, static_url_path="")
app.secret_key = "don't tell anyone"

korisnici = []
recepti = []
sastojci = ["jagode", "jabuke", "kruske", "kajsije", "mleko", "krompir"]

@app.route("/")
def index():
    return flask.render_template("index.tpl.html")

@app.route("/formaPodaci", methods=["POST"])
def forma():
    if flask.request.form["button"] == "registration":
        if provera_korisnika(flask.request.form["username"]):
            korisnik = {
                "username": flask.request.form["username"],
                "password": flask.request.form["password"],
                "email": flask.request.form["user's email"]
            }
            korisnici.append(korisnik)
            flash("Registracija je uspesna, ulogujte se")
            return flask.render_template("index.tpl.html")
        else:
            flash("Zauzeto je korisnicko ime: ")
            return flask.render_template("index.tpl.html", username = flask.request.form["username"])
    elif flask.request.form["button"] == "login":
        dozvola = False
        for korisnik in korisnici:
            if korisnik["username"] == flask.request.form["username"] and korisnik["password"] == flask.request.form["password"]:
                dozvola = True
                break
        if dozvola:
            return flask.render_template("users.tpl.html", users = korisnici, sastojci = sastojci)
        else:
            flash('Pogresno korisnicko ime ili lozinka.')
            return flask.render_template("index.tpl.html")

def provera_korisnika(username):
    dozvola = True
    for korisnik in korisnici:
        if korisnik["username"] == username:
            dozvola = False
    if dozvola:
        return True
    else:
        return False

@app.route("/receptPodaci", methods=["POST"])
def recept_podaci():
    if flask.request.form["button"] == "recept":
        dozvola = True
        for recept in recepti:
            if flask.request.form["nazivRecepta"] == recept["naziv"]:
                dozvola = False
 
        if dozvola:
            recepti.append({
                "naziv": flask.request.form["nazivRecepta"],
                "opis": flask.request.form["kratakOpis"],
                "lista_sastojaka": [],
                "aktivan": True
            })

        return flask.render_template("users.tpl.html", users = korisnici, sastojci = sastojci, recepti = recepti)
    elif flask.request.form["button"] == "sastojak":
        for recept in recepti:
            if flask.request.form["nazivRecepta"] == recept["naziv"]:
                dozvola = True
                for sastojak in recept["lista_sastojaka"]:
                    if flask.request.form["sastojak"] == sastojak["naziv"]:
                        dozvola = False
                
                if dozvola:                    
                    recept["lista_sastojaka"].append({
                        "naziv": flask.request.form["sastojak"],
                        "kolicina": flask.request.form["mernaJedinica"],
                        "aktivan": True
                    })
        return flask.render_template("tabelaRecepata.tpl.html", recepti = recepti)

@app.route("/izmeniSastojake/<int:index>")
def izmeni_sastojke(index):
    return flask.render_template("izmenaRecepta.tpl.html", recept = recepti[index])

@app.route("/ukloniSastojak/<int:index>/<recept_naziv>")
def ukloni_sastojak(index, recept_naziv):
    for recept in recepti:
        if recept_naziv == recept["naziv"]:
            for i in range(len(recept["lista_sastojaka"])):
                if i == index:
                    recept["lista_sastojaka"][i]["aktivan"] = False
    return flask.render_template("tabelaRecepata.tpl.html", recepti = recepti)

@app.route("/izmeniSastojak/<recept>")
def izmeni_sastojak(recept):
    recept = eval(recept)
    print(recept)
    return str(recept)

@app.template_filter("str")
def string(object):
    return str(object)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True, threaded=True)