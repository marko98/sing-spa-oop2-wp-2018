(function(d){
    var select = d.getElementById("sastojak");
    var mernaJedinica;
    
    var dobaviSastojak = function(e){
        if(e.target.value == "krompir"){
            mernaJedinica = "kg";
        } else if(e.target.value == "mleko"){
            mernaJedinica = "l";
        } else {
            mernaJedinica = "g";
        }

        //----------------------------------------------
        var div = d.getElementById("mernaJedinicaDiv");
        while(div.firstChild) {
            div.removeChild(div.firstChild);
        }

        var label = d.createElement("label");
        label.setAttribute("for", "mernaJedinica");
        var input = d.createElement("input");
        input.setAttribute("type", "text");
        input.setAttribute("id", "mernaJedinica");
        input.setAttribute("name", "mernaJedinica");
        input.setAttribute("class", "form-control");
        input.setAttribute("placeholder", "Unesite");
        input.setAttribute("aria-describedby", "basic-addon3");
        input.setAttribute("autocomplete", "off");
        input.setAttribute("required", "required");
        div.appendChild(label);
        div.appendChild(input);
        var divSpan = d.createElement("div");
        divSpan.setAttribute("class", "input-group-prepend");
        div.appendChild(divSpan);
        var span = d.createElement("span");
        span.setAttribute("class", "input-group-text");
        span.innerHTML = mernaJedinica;
        divSpan.appendChild(span);
        //----------------------------------------------

        var pokusaj = function(e){
            input.value = input.value + " " + mernaJedinica;
        }
        input.onblur = pokusaj;
    }
    select.onblur = dobaviSastojak;

})(document)